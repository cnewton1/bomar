<?php
// NC1
include ('dblib.inc');
include ('lib.inc');
include ('makeReglib.inc');
// NC2

#########################################################
#                                                       #
#  Program         : IPN Development Handler            #
#  Author          : Marcus Cicero                      #
#  File            : notify.php                         #
#  Function        : Skeleton IPN Handler               #
#  Version         : 1.1                                #
#  Last Modified   : 08/22/2002                         #
#  Copyright       : EliteWeaver UK                     #
#                                                       #
#########################################################
#    THIS SCRIPT IS FREEWARE AND IS NOT FOR RE-SALE     #
#########################################################

// IPN Posting Modes, Choose: 1 or 2
$postmode = "2";

           //* 1 = Live Via PayPal Network
           //* 2 = Test Via EliteWeaver UK


// Read the Posted IPN and Add "cmd" for Post back Validation
/*
            $postvars = array();
            while (list ($key, $value) = each ($_POST))
            {
                   $postvars[] = $key;
                                  }
                   $req = 'cmd=_notify-validate';
            for ($var = 0; $var < count ($postvars); $var++)
            {
                   $postkey = $postvars[$var];
                   $postvalue = $$postvars[$var];
                   $req .= "&" . $postkey . "=" . urlencode ($postvalue);
             }

extract($_POST);
*/

// Else, if globals is "off" and you are Security Conscious then you
// will have to assign all posted variables to local variables

// Note: If globals is "on" or you extracted then you don't need to localize


// Standard - Instant Payment Notifiction Variables (Localization)
/*
                   $receiver_email = $_POST['receiver_email'];
                   $item_name = $_POST['item_name'];
                   $item_number = $_POST['item_number'];
                   $quantity = $_POST['quantity'];
                   $invoice = $_POST['invoice'];
                   $custom = $_POST['custom'];
                   $option_name1 = $_POST['option_name1'];
                   $option_selection1 = $_POST['option_selection1'];
                   $option_name2 = $_POST['option_name2'];
                   $option_selection2 = $_POST['option_selection2'];

                   $num_cart_items = $_POST['num_cart_items'];
                   $payment_status = $_POST['payment_status'];
                   $pending_reason = $_POST['pending_reason'];
                   $payment_date = $_POST['payment_date'];
                   $payment_gross = $_POST['payment_gross'];
                   $payment_fee = $_POST['payment_fee'];
                   $mc_gross = $_POST['mc_gross']; // Live: 07/09/2002
                   $mc_fee = $_POST['mc_fee']; // Live: 07/09/2002
                   $mc_currency = $_POST['mc_currency']; // Live: 07/09/2002
                   $txn_id = $_POST['txn_id'];

                   $txn_type = $_POST['txn_type'];
                   $first_name = $_POST['first_name'];
                   $last_name = $_POST['last_name'];
                   $address_street = $_POST['address_street'];
                   $address_city = $_POST['address_city'];
                   $address_state = $_POST['address_state'];
                   $address_zip = $_POST['address_zip'];
                   $address_country = $_POST['address_country'];
                   $address_status = $_POST['address_status'];
                   $payer_email = $_POST['payer_email'];
                   $payer_id = $_POST['payer_id']; // Live: 09/09/2002
                   $payer_status = $_POST['payer_status'];
                   $payment_type = $_POST['payment_type'];
                   $notify_version = $_POST['notify_version'];
                   $verify_sign = $_POST['verify_sign'];
*/
//--------------------------------------
// NC1
// Add new entries to the database
//------------------------------------------
// Fudge the data, NOT TO BE COPIED.
//------------------------------------------
$option_name1="PRODUCT KEY";
$option_selection1="AMP95X21YJ1234567890A";
$payer_email="cnewton1@optonline.net";

//---------------------------------------
// END OF FUDGE
//---------------------------------------

//-----------------------------------------
// First Create the Invoice Number
// Use to generate a new invoice number
// pass in genInvoice
//-----------------------------------------
$InvoiceRec=array();
$InvoiceRec[item_number]=$item_number;
$InvoiceRec[username]=$payer_email; // we user the email for uniquiness
$genInvoice=lib_insert("cinvoicegen",$InvoiceRec);


//------------------------------------
// Log transaction
//------------------------------------
       $newEntry=array();

                 $newEntry[receiver_email] =  $receiver_email;
                 $newEntry[item_name]= $item_name;
                 $newEntry[item_number]= $item_number;
                 $newEntry[quantity] = $quantity;
                 //----------------------------------
                 // Use the GenInvioce Instead
                 //----------------------------------
                 $newEntry[invoice] = $genInvoice;
                 //----------------------------------
                 $newEntry[custom] = $custom;
                 //---------------------------------------
                 // Now get the product key to generate
                 // the registration key.
                 // Use the option_name1 field to 
                 // pass the value of the product key
                 // if it is valid, fill in option_name2
                 // and selection
                 //---------------------------------------
                 $newEntry[option_name1] = $option_name1;
                 $newEntry[option_selection1] = $option_selection1;
                 $qual=strcmp($option_name1,"PRODUCT KEY");
                 if ($qual == 0) {
                   $newRegKey=makeRegister($option_selection1);
                   echo "YOUR NEW REG KEY IS:" . $newRegKey . "\n";
                   if ($newRegKey != "") {
                      $option_name2="REGISTATION KEY";
                      $option_selection2 = $newRegKey;
                   }
                 }

                 $newEntry[option_name2] = $option_name2;
                 $newEntry[option_selection2] = $option_selection2;
                 //-----------------------------------------------

                 $newEntry[num_cart_items] = $num_cart_items;
                 $newEntry[payment_status] = $payment_status;
                 $newEntry[pending_reason] = $pending_reason;
                 $newEntry[payment_date] = $payment_date;
                 $newEntry[payment_gross] = $payment_gross;
                 $newEntry[payment_fee] = $payment_fee;
                 $newEntry[mc_gross] = $mc_gross;
                 $newEntry[mc_fee] = $mc_fee;
                 $newEntry[mc_currency] = $mc_currency;
                 $newEntry[txn_id] = $txn_id;

                 $newEntry[txn_type] = $txn_type;
                 $newEntry[first_name] = $first_name;
                 $newEntry[last_name] = $last_name;
                 $newEntry[address_street] = $address_street;
                 $newEntry[address_city] = $address_city;
                 $newEntry[address_state] = $address_state;
                 $newEntry[address_zip] = $address_zip;
                 $newEntry[address_country] = $address_country;
                 $newEntry[address_status] = $address_status;
                 $newEntry[payer_email] = $payer_email;
                 $newEntry[payer_id] = $payer_id;
                 $newEntry[payer_status] = $payer_status;
                 $newEntry[payment_type] = $payment_type;
                 $newEntry[notify_version] = $notify_version;
                 $newEntry[verify_sign] = $verify_sign;

$x = lib_insert("std_notify",$newEntry);
print "YOUR TRANSACTION IS PROCESS:" . $x;
//--------------------------------------------------------------------
// Now send the buy an email
//--------------------------------------------------------------------
$thankyou="Thank you for your purchase order.\n" . "Your product registration number is: " . $newRegKey . "\nPlease enter this number in the help menu registration key option" .  "\n\nPlease Note: This registration number is the matching key for product key: " . $option_selection1;

$phrase = "Your product registration key"; // This is the subject line.

mail ($payer_email,$phrase,$thankyou,"From: sales@chronsystems.com\nReply-To: support@chronsystems.com");

//NC2

// fputs ($fp, $header . $req);

//    while (!feof($fp))
//    {
//        $res = fgets ($fp, 1024);
//        $res = trim ($res); // Required on some Environments

//---------------------------------------------------  
// IPN was Confirmed as both Genuine and VERIFIED
//---------------------------------------------------  

if (strcmp ($res, "VERIFIED") == 0)
{
// Check that the "payment_status" variable is: Completed
// If it is Pending you may Want to Inform your Customer?
// Check your DB to Ensure this "txn_id" is Not a Duplicate
// You may want to Check the "payment_gross" matches listed Prices?
// You definately want to Check the "receiver_email" is yours
// Update your DB and Process this Payment accordingly
echo "\nResult: $res\n"; // Remove: # for Testing
}

//---------------------------------------------------  
// IPN was Not Validated as Genuine and is INVALID
//---------------------------------------------------  
elseif (strcmp ($res, "INVALID") == 0)
{
// Check your code for any Post back Validation problems
// Investigate the Fact that this Could be a spoofed IPN
// If updating your DB, Ensure this "txn_id" is Not a Duplicate
echo "Result: $res"; // Remove: # for Testing
}


// Terminate the Socket connection and Exit


#########################################################
#    THIS SCRIPT IS FREEWARE AND IS NOT FOR RE-SALE     #
#########################################################

?>
