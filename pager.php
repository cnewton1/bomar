<?php

if (!defined("Pager")) {
    define("Pager", "1");

    class Pager {

        function getPagerData($numHits, $limit, $page) {
            $numHits = (int) $numHits;
            $limit = max((int) $limit, 1);
            $page = (int) $page;
            $numPages = ceil($numHits / $limit);

            $page = max($page, 1);
            $page = min($page, $numPages);

            $offset = ($page - 1) * $limit;

            $ret = new stdClass;

            $ret->offset = $offset;
            $ret->limit = $limit;
            $ret->numPages = $numPages;
            $ret->page = $page;

            return $ret;
        }

    }

}

function remove_querystring_var($url, $key) {
//	$url = preg_replace('/(.*)(?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&'); 
    $url = preg_replace('/(.*)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
    $url = substr($url, 0, -1);
    return $url;
}

function getPaginationString($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=") {
    /* Setup page vars for display. */
    if ($page == 0)
        $page = 1;     //if no page var is given, default to 1.
    $prev = $page - 1;       //previous page is page - 1
    $next = $page + 1;       //next page is page + 1
    $lastpage = ceil($totalitems / $limit);  //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;      //last page minus 1

    /*
      Now we apply our rules and draw the pagination object.
      We're actually saving the code to a variable in case we want to draw it more than once.
     */
    $pagination = "";
    if ($lastpage > 1) {
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1)
            $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$prev\">&lt;&lt; previous</a>";
        else
            $pagination .= "<span class=\"disabled\">&lt;&lt; previous</span>";

        //pages	
        if ($lastpage < 7 + ($adjacents * 2)) { //not enough pages to bother breaking it up
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination .= "<span class=\"current\">$counter</span>";
                else
                    $pagination .= "<a href=\"$targetpage" . $pagestring . "$counter\">$counter</a>";
            }
        }
        elseif ($lastpage > 5 + ($adjacents * 2)) { //enough pages to hide some
            //close to beginning; only hide later pages
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination .= "<span class=\"current\">$counter</span>";
                    else
                        $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$counter\">$counter</a>";
                }
                $pagination .= "...";
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$lpm1\">$lpm1</a>";
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$lastpage\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "1\">1</a>";
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "2\">2</a>";
                $pagination .= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<span class=\"current\">$counter</span>";
                    else
                        $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$counter\">$counter</a>";
                }
                $pagination .= "...";
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$lpm1\">$lpm1</a>";
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$lastpage\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else {
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "1\">1</a>";
                $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "2\">2</a>";
                $pagination .= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<span class=\"current\">$counter</span>";
                    else
                        $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$counter\">$counter</a>";
                }
            }
        }

        //next button
        if ($page < $counter - 1)
            $pagination .= "<a target=\"_self\" href=\"$targetpage" . $pagestring . "$next\">next &gt;&gt;</a>";
        else
            $pagination .= "<span class=\"disabled\">next &gt;&gt;</span>";
        $pagination .= "</div>";
    }

    return $pagination;
}

function PageNavigation($pages, $pager, $page_name, $req_parameters) {
    $page = $pages;
    echo '<div class="sortpage">
		
		<!-- PAGING -->
		

			<div class="paging">
				<div class="arrows">';


    // output paging system (could also do it before we output the page content) 
    if ($page == 1) { // this is the first page - there is no previous page 
        // echo "&nbsp; ";
    } // FIRST PAGE NO PREV
    else {            // not the first page, link to the previous page 
        echo "<a class=\"arrow first\"  href=\"$page_name?$req_parameters&page=" . 1 . "\" target=\"_self\"></a>";
        echo "<a class=\"arrow prev\" href=\"$page_name?$req_parameters&page=" . ($page - 1) . "\" target=\"_self\"></a>";
    }


    for ($i = 1; $i <= $pager->numPages; $i++) {
        echo '
		<span class="nos">';

        if (($i > ($pager->page + 5)) or ( $i < ($pager->page - 5))) {
            $nothing = 0; // do nothing
        } else {
            // echo " | "; 
            if ($i == $pager->page)
                echo "<span class=\"sel\">$i</span>";
            else
                echo "<a href=\"$page_name?$req_parameters&page=$i\" target=\"_self\">$i</a>";
        }

        echo '</span>';
    }

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;";
    else
    if ($SearchCategory != "") {
        echo "<a class=\"arrow next\" href=\"$page_name?$req_parameters&page=" . ($page + 1) . "\" target=\"_self\"></a>";
    } else           // not the last page, link to the next page 
        echo "<a class=\"arrow next\" href=\"$page_name?$req_parameters&page=" . ($page + 1) . "\" target=\"_self\"></a>";

    echo "<a class=\"arrow last\" href=\"$page_name?$req_parameters&page=" . $pager->numPages . "\" target=\"_self\"> </a>";

    echo '</div></div></div>';
}
?>

