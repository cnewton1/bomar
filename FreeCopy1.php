<?php
session_start();
// Include MySQL class
// Copyright (c) 2002-2007 Chronicles Systems, Inc.
include "users.php";
include "dateDropFunction.php";

include "./rightglobals.php"; 
require_once('mysql.class.php');
if(isset($_SESSION)) {
        $menu=$_SESSION['selmenu']; 
  }
?>
<html>
<head><meta http-equiv='page-enter' content='blendtrans(duration=0.1)'>
<script language='javascript' src='datam.js'></script>
<link href='datamcss.css' type='text/css' rel='stylesheet'></head>
<body onload="SwitchMenu1('<?php echo $menu;?>')"> 

        <table  border="0" align="center" width="950px">
        
                <TR>
        
                        <TD><?php include "mainheader.php"; ?></TD>
        
                </TR>
        </table>
        
        <table border="0" width="950px" align="center">
        
                <TR>
        
                        <TD valign="top" width="100px"><?php include "LeftNavBar.php"; ?></TD>
        
                        <TD valign="top" width="850px" >

<?php
// Process actions
$cart = $_SESSION['cart'];
$action = $_GET['action'];

$_SESSION['BusinessName']=$COMPANYNAME;

if(isset($_SESSION))
 {
        
        $company_name=$_SESSION['FCompanyF'];
        $first_name=$_SESSION['FFNameF'];
        $last_name=$_SESSION['FLastNameF'];
        $account=$_SESSION['FAccountF'];
        $email=$_SESSION['FEmailF'];
        $phone=$_SESSION['FPhoneF'];
        $day=$_SESSION['Dday'];
        $month=$_SESSION['Dmonth'];
        $year=$_SESSION['Dyear'];
        $address1=$_SESSION['FSAddress'];
        $address2=$_SESSION['FSAddress2'];
        $city=$_SESSION['FSCity'];
        $state=$_SESSION['FSState'];
        $zip=$_SESSION['FSZip'];
 }

else
 {
        $company_name="";
        $first_name="";
        $last_name="";
        $account="";
        $email="";
        $phone="";
        $address1="";
        $address2="";
        $city="";
        $state="";
        $zip="";
 }

print "<p><h3>  Request a Free Copy of " . $COMPANYNAME . "<h3></p>";
print "<form method=\"POST\" action=\"FreeCopy2.php\">";
print "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber2\" height=\"399\">";
print "<tr>";
print "<td width=\"159%\" height=\"19\" align=\"left\" colspan=\"3\" bgcolor=\"#CCCCFF\">";
print "<font face=\"Arial\" size=\"2\">Enter your Address Below</font></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<b><font size=\"2\" face=\"Arial\">Personal Info</font></b></td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">First Name </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FFName\" value=\"$first_name\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Last Name </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FLastName\" value=\"$last_name\" size=\"20\"></td>";
print "</tr>";


print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Contact Phone</font><font color=\"#FF0000\"></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FPhone\" value=\"$phone\" size=\"20\"></td>";
print "</tr>";


print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Email </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FEmail\" value=\"$email\" size=\"20\"></td>";
print "</tr>";

// PROMDATE
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Your Wedding Date<font color=\"#FF0000\">*  </font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
ShowFromDate(4,"Future");
print "<input type=\"hidden\" name=\"PROMDATE\" size=\"20\"></td>";
print "</tr>";


print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "</tr>";
print "<tr>";

print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<b><font size=\"2\" face=\"Arial\">Shipping Address</font></b></td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Address  </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSAddress\" value=\"$address1\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Address 2  </font><font color=\"#FF0000\">&nbsp;</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSAddress2\" value=\"$address2\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">City  </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSCity\" value=\"$city\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">State  </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSState\" value=\"$state\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Zip  </font><font color=\"#FF0000\">*</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSZip\" value=\"$zip\" size=\"20\"></td>";
print "</tr>";

print "</p>";
print "</table>";
print "<p><input type=\"Submit\" name=\"B1\" value=\"Send Request\">";
print "</form>";
?>
  </TD> 
        
                </TR>
        </table>

        <table border="0" align="center" width="950px">

                <TR>

                        <TD><?php include "gfooter.php"; ?></TD></TR>

        </table>
</body>
</html>
