<?php

//Configuration


class UPS_Rates {

    var $access = "DD533B6DD77F3D75";
    var $userid = "Westcarb";
    var $passwd = "MOVel*153";
    var $wsdl = "RateWS.wsdl";
    var $operation = "ProcessRate";
    var $endpointurl = '';
    var $outputFileName = "XOLTResult.xml";
    var $from_address = array();
    var $to_address = array();
    var $parcel = array();

    function __construct() {
        $this->wsdl = dirname(__FILE__) . '/RateWS.wsdl';
        $this->outputFileName = dirname(__FILE__) . '/XOLTResult.wsdl';
    }

    function xml2array($contents, $get_attributes = 1, $priority = 'tag') {
        if (!$contents)
            return array();
        if (!function_exists('xml_parser_create')) {
            return array();
        }
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if (!$xml_values)
            return;

        //Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();

        $current = &$xml_array; //Refference
        //Go through the tags.
        $repeated_tag_index = array();
        foreach ($xml_values as $data) {
            unset($attributes, $value);
            extract($data);
            $result = array();
            $attributes_data = array();
            if (isset($value)) {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value;
            }
            if (isset($attributes) and $get_attributes) {
                foreach ($attributes as $attr => $val) {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val;
                }
            }
            if ($type == "open") {
                $parent[$level - 1] = &$current;
                if (!is_array($current) or ( !in_array($tag, array_keys($current)))) {
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;

                    $current = &$current[$tag];
                } else {
                    if (isset($current[$tag][0])) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level] ++;
                    } else {
                        $current[$tag] = array($current[$tag], $result);
                        $repeated_tag_index[$tag . '_' . $level] = 2;

                        if (isset($current[$tag . '_attr'])) {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = &$current[$tag][$last_item_index];
                }
            } elseif ($type == "complete") {

                if (!isset($current[$tag])) {
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                } else {
                    if (isset($current[$tag][0]) and is_array($current[$tag])) {

                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;

                        if ($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level] ++;
                    } else {
                        $current[$tag] = array($current[$tag], $result);
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes) {
                            if (isset($current[$tag . '_attr'])) {

                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }
                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level] ++;
                    }
                }
            } elseif ($type == 'close') {
                $current = &$parent[$level - 1];
            }
        }
        return($xml_array);
    }

    function processRate() {
        //create soap request
        $option['RequestOption'] = 'Shop';
        $request['Request'] = $option;

        $pickuptype['Code'] = '01';
        $pickuptype['Description'] = 'Daily Pickup';
        $request['PickupType'] = $pickuptype;

        $customerclassification['Code'] = '01';
        $customerclassification['Description'] = 'Classfication';
        $request['CustomerClassification'] = $customerclassification;

        $shipper['Name'] = 'Westcarb Enterprises Inc';
        $shipper['ShipperNumber'] = '222006';
        $address['AddressLine'] = '122 Florida Street';
        $address['City'] = 'Springfield';
        $address['StateProvinceCode'] = 'MA';
        $address['PostalCode'] = '01109';
        $address['CountryCode'] = 'US';
        $shipper['Address'] = $address;
        $shipment['Shipper'] = $shipper;

        $shipto['Name'] = $this->to_address['name'];
        $addressTo['AddressLine'] = $this->to_address['street1'];
        $addressTo['City'] = $this->to_address['city'];
        $addressTo['StateProvinceCode'] = $this->to_address['state'];
        $addressTo['PostalCode'] = $this->to_address['zip'];
        $addressTo['CountryCode'] = $this->to_address['country'];
        $addressTo['ResidentialAddressIndicator'] = '';
        $shipto['Address'] = $addressTo;
        $shipment['ShipTo'] = $shipto;

        $shipfrom['Name'] = $this->from_address['name'];
        $addressFrom['AddressLine'] = $this->from_address['street1'];
        $addressFrom['City'] = $this->from_address['city'];
        $addressFrom['StateProvinceCode'] = $this->from_address['state'];
        $addressFrom['PostalCode'] = $this->from_address['zip'];
        $addressFrom['CountryCode'] = $this->from_address['country'];
        $shipfrom['Address'] = $addressFrom;
        $shipment['ShipFrom'] = $shipfrom;

        $service['Code'] = '03';
        $service['Description'] = 'Service Code';
        $shipment['Service'] = $service;

        $Package = array();
        foreach ($this->parcel as $key => $parcel_info) {
            $package1 = array();
            $packaging1['Code'] = '02';
            $packaging1['Description'] = 'Rate';
            $package1['PackagingType'] = $packaging1;
            $dunit1['Code'] = 'IN';
            $dunit1['Description'] = 'inches';
            $dimensions1['Length'] = ($parcel_info['length'] + 0);
            $dimensions1['Width'] = ($parcel_info['width'] + 0);
            $dimensions1['Height'] = ($parcel_info['height'] + 0);
            $dimensions1['UnitOfMeasurement'] = $dunit1;
            $package1['Dimensions'] = $dimensions1;
            $punit1['Code'] = 'LBS';
            $punit1['Description'] = 'Pounds';
            $packageweight1['Weight'] = ($parcel_info['weight'] + 0);
            $packageweight1['UnitOfMeasurement'] = $punit1;
            $package1['PackageWeight'] = $packageweight1;
            $Package[] = $package1;
        }
        $shipment['Package'] = $Package;
        $shipment['ShipmentServiceOptions'] = '';
        $shipment['LargePackageIndicator'] = '';
        $request['Shipment'] = $shipment;
        return $request;
    }

    public function getRates($from_address, $to_address, $parcel) {
        $this->from_address = $from_address;
        $this->to_address = $to_address;
        $this->parcel = $parcel;

        try {

            $mode = array
                (
                'soap_version' => 'SOAP_1_1', // use soap 1.1 client
                'trace' => 1
            );

            // initialize soap client
            $client = new SoapClient($this->wsdl, $mode);

            //set endpoint url
            $client->__setLocation($this->endpointurl);


            //create soap header
            $usernameToken['Username'] = $this->userid;
            $usernameToken['Password'] = $this->passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $this->access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
            $client->__setSoapHeaders($header);


            //get response
            $resp = $client->__soapCall($this->operation, array($this->processRate()));

            //get status
            //  echo "Response Status: " . $resp->Response->ResponseStatus->Description . "\n";
            //echo $client->__getLastResponse();

            $array = $this->xml2array($client->__getLastResponse());
            $rates = $array['soapenv:Envelope']['soapenv:Body']['rate:RateResponse']['rate:RatedShipment'];

            $final_rate = array();
            foreach ($rates as $rate) {
                $final_rate[] = array(
                    'amount' => $rate['rate:TotalCharges']['rate:MonetaryValue'],
                    'currency' => $rate['rate:TotalCharges']['rate:CurrencyCode'],
                    'days' => isset($rate['rate:GuaranteedDelivery']['rate:BusinessDaysInTransit']) ? $rate['rate:GuaranteedDelivery']['rate:BusinessDaysInTransit'] : ''
                );
            }
            $return['status'] = true;
            $return['data'] = $final_rate;
            return $return;
        } catch (Exception $ex) {
            // echo $ex->getMessage();
            $return['status'] = false;
            $return['data'] = $ex->detail->Errors->ErrorDetail->PrimaryErrorCode->Description;
            return $return;
        }
    }

}
