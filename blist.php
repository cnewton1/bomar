<?php
include ('dblib.inc');
include ('lib.inc');
include ('makeReglib.inc');
include ('chron1.inc');

//------------------------------------
// listall.php
//------------------------------------
       $accountType="cart";
//--------------------------------------------------
// Get the amount of values in the DB/table
//--------------------------------------------------
$counter=lib_countrecords("txn_type","std_notify","txn_type",$accountType);

//--------------------------------------------------
// OK, Now get all of the account type
//--------------------------------------------------
$record=Array();
$record=getAllRecords("std_notify","txn_type",$accountType);

startHTML();

Print "<H1> BenA Boutiques, Limited Online Ordering Systems List </H1>";
Print "Number of Orders: " . $counter;
print "<br>";
print "<br>";
print "<TABLE BORDER CELLSPACING=1 CELLPADDING=2 WIDTH=590>";
print "<TR><TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Buyer Email</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">First</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Last</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Invoice Num</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Payment Date</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Item Name</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Gross Price</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Payment Fee</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Product Key</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">User Key Sent</FONT></TD>";
// Address Info
print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Street Address</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">City</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">State</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Zip</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=2 COLOR=\"#ffffff\">Country</FONT></TD>";

print "</TR>";

for ($i=0;$i<$counter;$i++) {
  $recordLine=$record[$i];
//  print "<p>" . $recordLine[PASSWORD] . "<p>";
//  print $recordLine[username] . "<p><br>";

if ($col==1) {
   $col=0;
   $lineColor="BGCOLOR=\"#008080\"";
} else {
   $lineColor="BGCOLOR=\"#808000\"";
   $col=1;
}
   

 print "<TR><TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[payer_email] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[first_name] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[last_name] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[invoice] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[payment_date] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[item_name] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[payment_gross] . "</FONT></TD>";
 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[payment_fee] . "</FONT></TD>";


 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[option_selection1] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[option_selection2] . "</FONT></TD>";

// Address Info
 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[address_street] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[address_city] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[address_state] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[address_zip] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#ffffff\">" . $recordLine[address_country] . "</FONT></TD>";

 print "</TR>";

}

endHTML();

exit;

?>

<html>

