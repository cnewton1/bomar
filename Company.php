<?php
include "users.php";
include "connect.php";
include "./rightglobals.php"; 
include "MyImageFunction.php";
include "getCartFunction.php";
include "ShopCartTempFunctions.php";
include "pager.php";
include "mainheader.php";

print " <table width=\"950px\" align=\"left\" border=\"0\">";
print "<TR><TD valign=\"top\" width=\"100px\">";
include "LeftNavBar.php"; 
print "</TD><TD valign=\"top\" width=\"850px\" style='padding-left:40px;'>";
print "<table cellspacing=\"0\" cellpadding=\"0\" hspace=\"0\" vspace=\"0\" width=\"100%\" height=\"48\" border=\"0\">";
print "<tr><td valign=\"top\" align=\"left\" height=\"48\" style=\"padding: 0in\" width=\"852\">";


$FromID=$HTTP_GET_VARS['ID'];
$ListName = $HTTP_GET_VARS['listName'];
$PrevPage=$HTTP_GET_VARS['PrevPAGE'];
$CateName=$HTTP_GET_VARS['CateName'];   
$page = $_GET['page']; 

if ($page == NULL)
{
 $page=1;   
 $PrevPage=1;
}
$limit=30;
$count_sql = "select count(*) from product";
if($ListName != "")
$count_sql.=" where subcategory LIKE '$ListName%'";

$count_sql.=" group by subcategory";

$result = mysql_query($count_sql);     

$total = mysql_num_rows($result);

if ($total==0)
{
  print "<br><br><font face=\"Arial\" size=\"2\"><b>No images were found in this category</b>";
  print "<br><font face=\"Arial\" size=\"2\"><b>Please search again for another images</b>";
  exit;
}
 
$pager  = Pager::getPagerData($total, $limit, $page); 
$offset = $pager->offset; 
$limit  = $pager->limit; 
$page   = $pager->page;  

//--------------------------------------------------
// General list of a table in this cast the category
//--------------------------------------------------
 
 
  
$sql="Select distinct manufacturername from product";
if($ListName != "")
$sql.=" where manufacturername LIKE '$ListName%'";
$sql.=" order by manufacturername limit $offset, $limit";
$resultID=mysql_query($sql);  
$inc=0;
?>
<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-width: 0" bordercolor="#111111" width="100%" id="AutoNumber1">
<tr> 
<td width="100%" style="border-style: none; border-width: medium">
<p class="MsoNormal" style="margin-right:.75pt"><b>
<span style="font-size:10.0pt;line-height:115%;font-family:Arial;
color:windowtext">Westcarb Enterprises, Inc.,</span></b><font face="Arial"><span style="font-size:
10.0pt;line-height:115%;color:windowtext"> was founded in 2002 by Mrs. Morrell 
Thomas</span><span style="font-size:10.0pt;line-height:115%"> and is 
headquartered in Boston, MA with offices in Tampa, Fl.&nbsp; Westcarb</span><span style="font-size:10.0pt;line-height:115%;color:windowtext"> 
is a Minority women owned business and, under Federal Guidelines, is considered 
a Small Disadvantaged Business (SDB) Enterprise.&nbsp;The Company has been certified 
as a participant in the U.S. Small Business Administration's (SBA) 8(A) Program, 
has a U.S. Small Business Association (SBA) Hubzone Certification, WOB/SDB 
Certification &amp; Florida DOT DBE Certification.&nbsp;</span></font></p>
<p class="MsoNormal"><font face="Arial">
<span style="font-size:10.0pt;line-height:115%;color:windowtext">The company has 
a Construction &amp; Services division and a Products and Supplies Distribution 
Division.</span></font></p>
<p class="MsoNormal"><b>
<span style="font-size:10.0pt;line-height:115%;font-family:Arial;color:windowtext">
Construction &amp; Services Division</span></b></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:.75pt;margin-bottom:0in;
margin-left:0in;margin-bottom:.0001pt;line-height:normal;text-autospace:none">
<font face="Arial"><span style="font-size:10.0pt;color:windowtext">As part of 
its construction services division, Westcarb Enterprises, Inc., operates as a
</span><span style="font-size:10.0pt">General Contractor specializing in 
renovation projects for Hospitals, Clean Rooms, Laboratories, and Administrative 
Areas with a strong emphasis in Design Build services.&nbsp; Please visit </span>
</font>
<a href="http://www.wetelcom.com/" style="font-family: Arial; color: blue; text-decoration: underline; text-underline: single">
<span style="font-size:10.0pt">www.wetelcom.com</span></a><font face="Arial"><span style="font-size:10.0pt"> 
to learn more.</span></font></p>
<p class="MsoNormal" style="margin-top:0in;margin-right:.75pt;margin-bottom:0in;
margin-left:0in;margin-bottom:.0001pt"><font face="Arial">
<span style="font-size:10.0pt;
line-height:115%;color:windowtext">&nbsp;</span></font></p>
<p class="MsoNormal"><b>
<span style="font-size:10.0pt;line-height:115%;font-family:Arial;color:windowtext">
Products and Supplies Division</span></b></p>
<p class="MsoNormal" style="margin-right:.75pt;text-align:justify">
<font face="Arial">
<span style="font-size:10.0pt;line-height:115%;color:windowtext">Westcarb is 
well on its way to its goal of becoming a Hardware Superstore and your �One 
Stop� source for industrial and commercial products and supplies.&nbsp; Since its 
inception in 2002, Westcarb has grown as a business-to-business distributor of 
products and supplies, steadily increasing its categories, manufacturers and 
product offerings.&nbsp; Westcarb services defense contractors, large commercial 
corporations, DoD</span><span style="font-size:10.0pt;line-height:
115%"> and Federal Civilian Government agencies as well as State and Local 
government agencies.</span></font></p>
<p class="MsoNormal" style="margin-right:.75pt;text-align:justify">
<font face="Arial">
<span style="font-size:10.0pt;line-height:115%;color:windowtext">Westcarb and 
its supplier partners recognize the importance of supplier diversity to the 
Federal Government and to the contractors that service the Federal Government.&nbsp; 
The Westcarb supplier partnerships allow Westcarb to reduce COGS and increase 
supply chain procurement process efficiency, driving savings and reducing costs. 
Westcarb is also able to leverage its supplier Distribution Centers throughout 
the US to maximize product fulfillment and timely delivery of our products.&nbsp; We 
ensure, �On Time Delivery � Quality Products � Fair and Reasonable Pricing�.</span></font></p>
<p class="MsoNormal">
<a href="http://www.wetelcom.com/" style="font-family: Arial; color: blue; text-decoration: underline; text-underline: single">
<font size="2">Westcarb Enterprises Construction &amp; Services Division</font></a></p>
<p class="MsoNormal">
<a href="https://www.bpn.gov/CCRSearch/detail.aspx?VAR_DUN1=107199999&VAR_DUN2=" style="font-family: Arial; color: blue; text-decoration: underline; text-underline: single">
<font size="2">Westcarb Enterprises CCR Profile</font></a></p>
<p class="MsoNormal">
<a href="http://dsbs.sba.gov/dsbs/search/dsp_profile.cfm?user_id=P0258586" style="font-family: Arial; color: blue; text-decoration: underline; text-underline: single">
<font size="2">Westcarb Enterprises Inc. SBA Profile</font></a></p>
<p class="MsoNormal">
<a href="https://evolution.mantech-wva.com/cchsWelcomePage/wp.aspx?contract=OPENMKT0890" style="font-family: Arial; color: blue; text-decoration: underline; text-underline: single">
<font size="2">Westcarb Enterprises Inc. DOD EMall Profile</font></a></p>
<p>&nbsp;</td>
</tr>
</table>

   
   
   
<?php   

//***************************************
// FUNCTION SECTION
//***************************************
   
  include "gfooter.php"; 




