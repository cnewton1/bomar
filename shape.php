<?php

$red = $_GET['r'];
$green = $_GET['g'];
$blue = $_GET['b'];

function RandomShape() {
	global $red, $green, $blue;
	$image = imagecreate(1, 1) ;
	
	// background color
	$bg = imagecolorallocate($image, $red, $green, $blue) ;
	
	// line color
	$color = imagecolorallocate($image, $red, $green, $blue) ;
	
	// draw the shape
	imagepolygon($image, $color);
	
	// output the picture
	header("Content-type: image/png") ;
	imagepng($image) ;
}

RandomShape();



?>