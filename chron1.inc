<?php
/*--------------------------------------------------------------------------------------
Copyright Chronicles Systems, Inc.  
--------------------------------------------------------------------------------------*/
//--------------------------------------------------------------
// HTML HELPER FUNCTIONS
//--------------------------------------------------------------
function makeHTMLTitle($titleName = "") {
          print "<HTML><T1>" . $titleName . "</T1><p>";
          print "<body>";
}
		
function startHTML(){
  makeHTMLTitle();
}


function endHTML() {
          print "</body>";
          print "</HTML>";
}


//------------------------------------------------------------
// General Systems Messages
//------------------------------------------------------------
function sysMesg($text="") {
  makeHTMLTitle(" ");

  print "<B><FONT SIZE=3><P>System Message. Please Read:</P>";
  print "</B></FONT>";
  print "<TABLE BORDER CELLSPACING=1 CELLPADDING=1 WIDTH=590>";
  print "<TR><TD VALIGN=\"TOP\" BGCOLOR=\"#000080\">";
  print "<B><FONT SIZE=3 COLOR=\"#00ff00\">" . $text;
  print "</B></FONT></TD>";
  print "</TR>";
  print "<TR><TD VALIGN=\"TOP\" BGCOLOR=\"#000080\">&nbsp;</TD>";
  print "</TR>";
  print "</TABLE>";

  endHTML();
}


//--------------------------------------------------------------
// DB FUNCTIONS
//--------------------------------------------------------------

//--------------------------------------------------
// Return the first records of the table
// that matches the search criteron
//--------------------------------------------------
function getFirstRecord($table="",$field="",$value="") {
 $recordArray = Array();
 $recordArray=lib_getrecords($table,$field,$value);
 $record=$recordArray[0]; // Only get the first record returned
 return $record;
}


//--------------------------------------------------
// Return all the records that match returns array.
//--------------------------------------------------
function getAllRecords($table="",$field="",$value="") {
 $recordArray = Array();
 $recordArray=lib_getrecords($table,$field,$value);
 return $recordArray; // Only get the first record returned
}

?>
