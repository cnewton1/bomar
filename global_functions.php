<?php

define("API_KEY", "shippo_test_d6cab8013736a66391d68b51eb218c3239dff5d0");
require_once(dirname(__FILE__) . '/PageAccesslog.php');

function GenKey($length = 7)
{
    $password = "";
    $possible = "0123456789abcdefghijkmnopqrstuvwxyz";
    $i = 0;
    while ($i < $length) {
        $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
        if (!strstr($password, $char)) {
            $password .= $char;
            $i++;
        }
    }
    return $password;
}

function getCurrentuserid()
{
    if ($_SESSION['USERID'] == '') {
        if ($_COOKIE['newuser'] != '') {
            $userKey = $_COOKIE['newuser'];
            setcookie("newuser", $userKey, time() + 24 * 60 * 60, "/");
        } else {
            $userKey = md5(GenKey() . time() . $_SERVER['HTTP_USER_AGENT']);
            setcookie("newuser", $userKey, time() + 24 * 60 * 60, "/");
            //$addtocartlink = 'AccountLogin1.php';
        }
        return $userKey;
    } else {
        return $_SESSION['USERID'];
    }
}

function count_cart_items($userid)
{
    $count_cart_items = mysql_fetch_array(mysql_query("SELECT sum(`qty`) AS `total_qty` FROM `cart` WHERE `userid`= '$userid'"));
    $totalItems = ($count_cart_items['total_qty'] != '') ? $count_cart_items['total_qty'] : '0';
    return $totalItems;
}

function get_state_code($state_name)
{
    $result = mysql_fetch_array(mysql_query("SELECT `state_code` FROM `state` WHERE `state_name` = '$state_name' LIMIT 0,1"));
    return $result['state_code'];
}


function page_protect()
{
    session_start();
    if (isset($_SESSION['HTTP_USER_AGENT'])) {
        if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'])) {
            logout();
            exit;
        }
    }
    if (!isset($_SESSION['USERID']) && !isset($_SESSION['USER_EMAIL'])) {
        if (isset($_COOKIE['USERID'])) {
            $cookie_user_id  = filter($_COOKIE['USERID']);
            $user_result = mysql_fetch_array(mysql_query("SELECT * FROM `member` WHERE `id` = '$cookie_user_id'"));

            if (is_numeric($_COOKIE['USERID']) && ($_COOKIE['USER_NAME'] != '') && ($_COOKIE['USERID'] == $user_result['id'])) {
                session_regenerate_id(); //against session fixation attacks.
                $_SESSION['USERID'] = $_COOKIE['USERID'];
                $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
            } else {
                logout();
            }
        } else {
            $url = ($_SERVER['HTTP_REFERER'] != '') ? 'AccountLogin1.php?url=' . urlencode($_SERVER['HTTP_REFERER']) : 'AccountLogin1.php';
            header("Location: $url");
            exit();
        }
    }
}
