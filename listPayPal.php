<?php
include ('dblib.inc');
include ('lib.inc');
include ('makeReglib.inc');
include ('chron1.inc');

include "globals.php"; 
include "apanelNav.php";

adminTitle("PAYPAL SERVICE FEES DETAIL");

//------------------------------------
// listall.php
//------------------------------------
       $accountType="web_accept";
       $totalfees=0;
       $grandTotal=0;
//--------------------------------------------------
// Get the amount of values in the DB/table
//--------------------------------------------------
$counter=lib_countrecords("txn_type","std_notify","txn_type",$accountType);

//--------------------------------------------------
// OK, Now get all of the account type
//--------------------------------------------------
$record=Array();
$record=getAllRecords("std_notify","txn_type",$accountType);

startHTML();


print "<TABLE BORDER CELLSPACING=1 CELLPADDING=2 WIDTH=590>";
print "<TR><TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=1 COLOR=\"#ffffff\">TRANS ID</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=1 COLOR=\"#ffffff\">DATE</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=1 COLOR=\"#ffffff\">ORD TYPE</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=1 COLOR=\"#ffffff\">GROSS</FONT></TD>";

print "<TD WIDTH=\"20%\" VALIGN=\"TOP\" BGCOLOR=\"#0000ff\">";
print "<FONT SIZE=1 COLOR=\"#ffffff\">FEE</FONT></TD>";

print "</TR>";

$oldMonth="";

for ($i=0;$i<$counter;$i++) {
  $recordLine=$record[$i];
//  print "<p>" . $recordLine[PASSWORD] . "<p>";
//  print $recordLine[username] . "<p><br>";

  if ($col==1) {
   $col=0;
   $lineColor="BGCOLOR=\"#FBE6C6\"";
   } else {
   $lineColor="BGCOLOR=\"#FEF2DF\"";
   $col=1;
   }
 
 
 $v=$recordLine[payment_date];
 $TMonth=substr($v,9,3);
  if (($TMonth != $oldMonth) && ($oldMonth !=""))
  {
    $oldMonth = $TMonth;
    $saveLineColor = $lineColor;
    $lineColor="BGCOLOR=\"#FFFFFF\"";
    print "<TR><TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
    print "<FONT SIZE=2 COLOR=\"#ff0000\">Month Total <B>$" . $totalfees . "</b></FONT></TD></TR>";
    $grandTotal=$grandTotal+$totalfees;
    $totalfees=0;
    $lineColor = $saveLineColor;
  }
 $oldMonth=$TMonth;
  
  $TYear=substr($v,16,5);

 print "<TR><TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#000000\">" . $recordLine[invoice] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 
 
 print "<FONT SIZE=2 COLOR=\"#000000\">" . $TMonth . ", " . $TYear . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#000000\">" . $recordLine[item_name] . "</FONT></TD>";

 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#000000\">" . $recordLine[payment_gross] . "</FONT></TD>";
 print "<TD WIDTH=\"20%\" VALIGN=\"TOP\"" . $lineColor . ">";
 print "<FONT SIZE=2 COLOR=\"#000000\">" . $recordLine[payment_fee] . "</FONT></TD>";
 
 $totalfees = $totalfees + $recordLine[payment_fee];
 print "</TR>";
 
}

Print "ACTIVE: " . $counter;
print "<br>";
print "<p></h2> GRAND TOTAL FEES: $" . $grandTotal . "</h2>";

endHTML();

include "gfooter.php";

exit;

?>

<html>

