<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'pager.php';

include 'main_page_functions.php';

function get_youtube_videoid($url) {
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
        $id = $match[1];
    }
    return $id;
}
?>

<?php ?>



<div id="home_body">


    <div id="midsec">


        <div style="max-width: 720px; margin: 0 auto" >
            <?php
            $page = (isset($_GET['page'])) ? $_GET['page'] : 1;
            $result = mysql_fetch_array(mysql_query("SELECT COUNT(`id`) AS `num` FROM `videos`"));
            $total = $result['num'];
            $limit = 1;
            $pager = Pager::getPagerData($total, $limit, $page);
            $offset = $pager->offset;
            $limit = $pager->limit;
            $page = $pager->page;


            PageNavigation($page, $pager, $page_name, $req_parameters);

            $resultID = mysql_query("SELECT * FROM `videos` ORDER BY `id` DESC LIMIT $offset, $limit");

            while ($row = mysql_fetch_array($resultID)) {
                $vid_id = get_youtube_videoid($row['video_url']);
                ?>
                <iframe width="720" height="500" src="https://www.youtube.com/embed/<?php echo $vid_id; ?>" frameborder="0" allowfullscreen></iframe>

                <?php
            }
            PageNavigation($page, $pager, $page_name, $req_parameters);
            ?>
        </div>


    </div>
</div>






<?php include 'footer.php'; ?>



</body>
</html>