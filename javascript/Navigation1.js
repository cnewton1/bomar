var nav_Navigation1 = new Object();

nav_Navigation1.mouseoverBold="false";
nav_Navigation1.selectedBgcolor="";
nav_Navigation1.importedImageMouseOver="images\/4000_navoff.png";
nav_Navigation1.numLinks="5";
nav_Navigation1.textColor="#FFFFFF";
nav_Navigation1.mouseoverBgcolor="";
nav_Navigation1.tabCategory="basic";
nav_Navigation1.border="";
nav_Navigation1.selectedItalic="false";
nav_Navigation1.graphicMouseover="true";
nav_Navigation1.type="Navigation";
nav_Navigation1.basicTab="White";
nav_Navigation1.horizontalSpacing="40";
nav_Navigation1.horizontalWrap="5";
nav_Navigation1.shinyButton="Shiny_Aqua";
nav_Navigation1.mouseoverEffect="true";
nav_Navigation1.modernButton="Basic_Black";
nav_Navigation1.orientation="horizontal";
nav_Navigation1.funButton="Arts_and_Crafts";
nav_Navigation1.darkButton="Basic_Black";
nav_Navigation1.selectedTextcolor="#55A14D";
nav_Navigation1.lineWidth="2";
nav_Navigation1.mouseoverTextcolor="#999999";
nav_Navigation1.bold="false";
nav_Navigation1.texturedButton="Brick";
nav_Navigation1.accentStyle="Arrow";
nav_Navigation1.style="imported";
nav_Navigation1.holidayButton="Christmas_Ornaments";
nav_Navigation1.textSize="12";
nav_Navigation1.lineColor="#000000";
nav_Navigation1.brightButton="Chicky";
nav_Navigation1.mouseoverUnderline="false";
nav_Navigation1.accentColor="Black";
nav_Navigation1.imageHeight="48";
nav_Navigation1.background="";
nav_Navigation1.textFont="Helvetica";
nav_Navigation1.hasLinks="true";
nav_Navigation1.sophisticatedButton="Antique";
nav_Navigation1.underline="false";
nav_Navigation1.simpleButton="Autumn_Leaves";
nav_Navigation1.italic="false";
nav_Navigation1.importedImageSelected="images\/4000_navoff.png";
nav_Navigation1.basicButton="Gray";
nav_Navigation1.navID="nav_Navigation1";
nav_Navigation1.buttonCategory="basic";
nav_Navigation1.dirty="false";
nav_Navigation1.selectedBold="false";
nav_Navigation1.selectedEffect="true";
nav_Navigation1.graphicSelected="true";
nav_Navigation1.version="5";
nav_Navigation1.verticalSpacing="10";
nav_Navigation1.squareTab="Camel";
nav_Navigation1.mouseoverItalic="false";
nav_Navigation1.justification="center";
nav_Navigation1.imageWidth="56";
nav_Navigation1.selectedUnderline="false";
nav_Navigation1.accentType="";
nav_Navigation1.importedImage="images\/4000_navoff.png";
nav_Navigation1.width="440";
nav_Navigation1.height="48";

nav_Navigation1.navName = "Navigation1";
nav_Navigation1.imagePath = strRelativePathToRoot + "images/4000_navoff.png";
nav_Navigation1.selectedImagePath = strRelativePathToRoot + "images/4000_navoff.png";
nav_Navigation1.mouseOverImagePath = strRelativePathToRoot + "images/4000_navoff.png";
nav_Navigation1.imageWidth = "56";
nav_Navigation1.imageHeight = "48";
nav_Navigation1.fontClass = "size12 Helvetica12";
nav_Navigation1.fontFace = "Helvetica, Arial, sans-serif";


var baseHref = '';
// this will only work if getElementsByTagName works
if (document.getElementsByTagName)
{
    // this will only work if we can find a base tag
    var base = document.getElementsByTagName('base');
    // Verify that the base object exists
    if (base && base.length > 0)
    {
        // if you don't specify a base href, href comes back as undefined
        if (base[0].href != undefined)
        {
            // get the base href
            baseHref = base[0].href;
            // add a trailing slash if base href doesn't already have one
            if (baseHref != '' && baseHref.charAt(baseHref.length - 1) != '/')
            {
                baseHref += '/';
            }
        }
    }
}


nav_Navigation1.links=new Array(5);
var nav_Navigation1_Link1 = new Object();
nav_Navigation1_Link1.type = "existing";
nav_Navigation1_Link1.displayName = "Home";
nav_Navigation1_Link1.linkWindow = "_self";
nav_Navigation1_Link1.linkValue = "index.php";
nav_Navigation1_Link1.linkIndex = "1";
nav_Navigation1.links[0] = nav_Navigation1_Link1;
var nav_Navigation1_Link2 = new Object();
nav_Navigation1_Link2.type = "existing";
nav_Navigation1_Link2.displayName = "About Us";
nav_Navigation1_Link2.linkWindow = "_self";
nav_Navigation1_Link2.linkValue = "AboutUs.php";
nav_Navigation1_Link2.linkIndex = "2";
nav_Navigation1.links[1] = nav_Navigation1_Link2;
var nav_Navigation1_Link3 = new Object();
nav_Navigation1_Link3.type = "existing";
nav_Navigation1_Link3.displayName = "Services";
nav_Navigation1_Link3.linkWindow = "_self";
nav_Navigation1_Link3.linkValue = "Services.php";
nav_Navigation1_Link3.linkIndex = "3";
nav_Navigation1.links[2] = nav_Navigation1_Link3;
var nav_Navigation1_Link4 = new Object();
nav_Navigation1_Link4.type = "existing";
nav_Navigation1_Link4.displayName = "Products";
nav_Navigation1_Link4.linkWindow = "_self";
nav_Navigation1_Link4.linkValue = "Products.php";
nav_Navigation1_Link4.linkIndex = "4";
nav_Navigation1.links[3] = nav_Navigation1_Link4;
var nav_Navigation1_Link5 = new Object();
nav_Navigation1_Link5.type = "existing";
nav_Navigation1_Link5.displayName = "IT Solutions";
nav_Navigation1_Link5.linkWindow = "_self";
nav_Navigation1_Link5.linkValue = "ITSolutions.php";
nav_Navigation1_Link5.linkIndex = "5";
nav_Navigation1.links[4] = nav_Navigation1_Link5;
function mouseOn(tdElement, newBackgroundImage)
{
	if(tdElement != null) {
		tdElement.style.backgroundImage = 'url(' + escapeScript(newBackgroundImage) + ')';
	}
}

function mouseOff(tdElement, newBackgroundImage)
{
	if(tdElement != null) {
		tdElement.style.backgroundImage = 'url(' + escapeScript(newBackgroundImage) + ')';
	}
}

function doMouseChange(Navigation,tdElement,linkIndex,bisMouseOver) {
	if (Navigation.mouseoverEffect != 'true') {
		return;
	}
	var link = Navigation.links[linkIndex-1];
	var bIsCurrentPage = isCurrentPage(link);
	if ((Navigation.graphicSelected == 'true' || Navigation.selectedTextcolor)
			&& bIsCurrentPage && 'true' == Navigation.selectedEffect) {
		return;
	}
	
	var fontElement = getLinkFontElement(tdElement);
	
	if(fontElement != null) {
		doFontChange(Navigation,fontElement,bIsCurrentPage,bisMouseOver);
	}
	
	if (Navigation.graphicMouseover == 'true') {
		if (bisMouseOver) {
			mouseOn(tdElement,escapeHtmlInlineScript(Navigation.mouseOverImagePath));
		} else {
			mouseOff(tdElement, escapeHtmlInlineScript(Navigation.imagePath));
		}
	}
}
function addStyle(Navigation, Link,tdElement) {
	if(tdElement == null) {
		return;
	}
	var strImg = Navigation.imagePath;
	var strFontColor = Navigation.textColor;
	if ('true' == Navigation.selectedEffect) {
		if (Navigation.graphicSelected == 'true') {
			strImg = Navigation.selectedImagePath;
		}
		if (Navigation.selectedTextcolor) {
			strFontColor = Navigation.selectedTextcolor;
		}
	}
	var strImgUrl = "url('"+escapeHtmlInlineScript(strImg)+"')";
	tdElement.style.backgroundImage=strImgUrl;
	var fontElement = getLinkFontElement(tdElement);
	if (fontElement != null) {
		fontElement.style.color = strFontColor;
		if ('true' == Navigation.selectedEffect) {
			if ('true' == Navigation.selectedBold) {
				fontElement.style.fontWeight = "bold";
			}
			if ('true' == Navigation.selectedItalic) {
				fontElement.style.fontStyle = "italic";
			}
			if ('true' == Navigation.selectedUnderline) {
				fontElement.style.textDecoration = "underline";
			}
		}
	}
}
// Combined escape html and javascript
function escapeHtmlInlineScript(s, escapeSingleQuotes, escapeDoubleQuotes){
	return htmlEncode(escapeScript(s, escapeSingleQuotes, escapeDoubleQuotes));
}

function htmlEncode(s){
	if (typeof(s) != "string") return "";
	
	var result = "";
	for (var i = 0; i < s.length; i++) {
		var ch = s.charAt(i);
		switch (ch) {
		case '<':
			result += "&lt;";
			break;
		case '>':
			result += "&gt;";
			break;
		case '&':
			result += "&amp;";
			break;
		case '"':
			result += "&quot;";
			break;
		case "'":
			result += "&#39;";
			break;
		default:
			result += ch;
		}
	}
	return result;
}

/* escapes slashes and quotes. the default is to escape quotes,
 * but this can be turned off.
 * this function is used for javascript and also for escaping urls
 * within background-image css.	 
 */
function escapeScript(s, escapeSingleQuotes, escapeDoubleQuotes){
	if (typeof(s) != "string") return "";
	
	var result = "";
	for (var i = 0; i < s.length; i++) {
		var ch = s.charAt(i);
		switch (ch) {
		case '\'':
			if (escapeSingleQuotes == null || escapeSingleQuotes)
				result += "\\\'";
			break;
		case '\"':
			if (escapeDoubleQuotes == null || escapeDoubleQuotes)
				result += "\\\"";
			break;
		case '\\':
			result += "\\\\";
			break;
		default:
			result += ch;
		}
	}
	return result;
}

//
// This .js file includes utility functions used by both graphical and text navs
// in their rendering.  User pages including a nav element will import this file, along
// with TextNavigation.js and GraphicNavigation.js.  The functions within will
// be called by the [navname].js file generated at publish time.

function fixLinkValue(Link)
{
	if(Link.type!='existing')
	{
		return Link.linkValue;
	}
	else
	{
		return baseHref + strRelativePathToRoot + Link.linkValue;
	}
}

function isCurrentPage(Link)
{
	if(Link.type!='existing')
	{
		return false;
	}
	var strLinkValue = Link.linkValue.toLowerCase();
	return (strRelativePagePath == strLinkValue);
}

function toggleOnMouseChange(fontElement,newColor, bold, underline, italic)
{
	if(fontElement == null) {
		return;
	}
	if(newColor)
	{
		fontElement.style.color=newColor;
	}
	fontElement.style.fontWeight = (bold=='true' ? 'bold' : 'normal');
	fontElement.style.textDecoration = (underline=='true' ? 'underline' : 'none');
	fontElement.style.fontStyle = (italic=='true' ? 'italic' : 'normal');

}

function doFontChange(Navigation,fontElement,bIsCurrentPage,bisMouseOver) {
	if(fontElement == null) {
		return;
	}
	var textColor;
	var baseTextColor = Navigation.textColor;
	var bold;
	var baseBold = Navigation.bold;
	var underline;
	var baseUnderline = Navigation.underline;
	var italic;
	var baseItalic = Navigation.italic;
	if (bIsCurrentPage && 'true' == Navigation.selectedEffect) {
		textColor = Navigation.selectedTextcolor ? Navigation.selectedTextcolor
				: (Navigation.mouseoverTextColor ? Navigation.mouseoverTextcolor
						: Navigation.textColor);
		baseTextColor = Navigation.selectedTextcolor ? Navigation.selectedTextcolor
				: Navigation.textColor;
		baseBold = bold = Navigation.selectedBold;
		baseUnderline = underline = Navigation.selectedUnderline;
		baseItalic = italic = Navigation.selectedItalic;
	} else {
		textColor = Navigation.mouseoverTextcolor ? Navigation.mouseoverTextcolor
				: Navigation.textColor;
		bold = Navigation.mouseoverBold;
		underline = Navigation.mouseoverUnderline;
		italic = Navigation.mouseoverItalic;
	}
	
	if(bisMouseOver) {
		toggleOnMouseChange(fontElement,textColor,bold,underline,italic);
	} else {
		toggleOnMouseChange(fontElement,baseTextColor,baseBold,baseUnderline,baseItalic);
	}
	

}

function addMouseAndStyleSupportNavigation1(Navigation,navTbId) {
	var startNode;

	if(typeof(nav_element_id) != 'undefined' && document.getElementById(nav_element_id) != null) {
		startNode = document.getElementById(nav_element_id);
			
	} else if(navTbId != null) {
		startNode = document.getElementById(navTbId);
			
	}
	
	if(startNode != null) {
	  searchForCurrentPageTd(Navigation,startNode);
	}
	

}

function searchForCurrentPageTd(Navigation,startNode) {
	
	if(startNode.childNodes != null) {
		for(var i=0;i<startNode.childNodes.length;i++){
			if(addStyleForCurrentPageTd(Navigation,startNode.childNodes[i])){
			   return;	
			} else {
			   searchForCurrentPageTd(Navigation,startNode.childNodes[i]);
			}
		}
	}

}

function addStyleForCurrentPageTd(Navigation,currentNode) {
	if(Navigation.orientation == 'horizontal') {
		if(currentNode.tagName == 'TD' && currentNode.id != '' && currentNode.id.indexOf(Navigation.navName+navTDLinkPart) != -1){
			var currentTDIdPrefix = Navigation.navName+navTDLinkPart;
			var linkId = currentNode.id.substring(currentTDIdPrefix.length,currentNode.id.length);
			if(isCurrentPage(Navigation.links[linkId-1]) == true) {
				addStyle(Navigation, Navigation.links[linkId-1],currentNode);
				return true;
			}
		}
	} else {
		if(currentNode.tagName == 'TR' && currentNode.id != '' && currentNode.id.indexOf(navTRLinkPrefix) != -1){	
			var currentTRIdPrefix = navTRLinkPrefix+Navigation.navName;
			var linkId = currentNode.id.substring(currentTRIdPrefix.length,currentNode.id.length);
			if(isCurrentPage(Navigation.links[linkId-1]) == true && currentNode.childNodes != null) {
				var currentPageTd;
				for(var i=0;currentNode.childNodes.length;i++) {
					if(typeof(currentNode.childNodes[i].tagName) != 'undefined' && currentNode.childNodes[i].tagName == 'TD' && currentNode.childNodes[i].id.indexOf(Navigation.navName+navTDLinkPart) != -1) {
						currentPageTd = currentNode.childNodes[i];
						addStyle(Navigation, Navigation.links[linkId - 1],currentPageTd,currentNode);
						return true;
					}
				}
			}
		}
	}
	return false;
}

function getChildElementFromTree(startNode,nodesToTraverse) {
	var currentChildNode = startNode;
	
	for(var n= 0;n<nodesToTraverse.length;n++) {
		currentChildNode = getMatchingChildByTag(currentChildNode.childNodes,nodesToTraverse[n]);
	}
	
	return currentChildNode;
}


function getMatchingChildByTag(childNodes,tagName) {
	var child;
	for(var i=0;childNodes.length;i++) {
		if(childNodes[i].tagName == tagName) {
			child = childNodes[i];
			break;
		}
	}
	return child;
}
function getLinkFontElement(tdElement){
	var fontElement;
	var aElement = getChildElementFromTree(tdElement,['A']);
	for(var i=0;i < aElement.childNodes.length;i++) {
		if(aElement.childNodes[i].tagName == 'DIV') {
		 	fontElement = getChildElementFromTree(aElement.childNodes[i],['FONT']);
		 	break;
		} else if(aElement.childNodes[i].tagName == 'FONT'){
		 	fontElement = 	aElement.childNodes[i];
		 	break;
		}
	
	}
	return fontElement;
}



	if(typeof(navTRLinkPrefix) == 'undefined') {
		navTRLinkPrefix = 'vNavTR_Link_';
	}
	if(typeof(navTDLinkPart) == 'undefined') {
		navTDLinkPart = '_Link';
	}
	if(document.getElementById('nav_version') == null) {
	if (typeof(navTBSuffix) == 'undefined') {
	navTBSuffix = 0;
	} else {navTBSuffix++;}
		document.write('<TABLE ID="ntb'+navTBSuffix+'"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><TR ALIGN=\"CENTER\" VALIGN=\"MIDDLE\"><TD><TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"0\"><TR><TD NOWRAP HEIGHT=\"48\" ALIGN=\"center\" VALIGN=\"MIDDLE\" id=\"Navigation1_Link1\" style=\"cursor: pointer;cursor: hand;color:#FFFFFF;background-image:url(images\/4000_navoff.png);background-repeat:no-repeat;background-position:center;\"  onmouseover=\"doMouseChange(nav_Navigation1,this,\'1\',true);\" onmouseout=\"doMouseChange(nav_Navigation1,this,\'1\',false);\"><A HREF=\"\/index.php\" TARGET=\"_self\" STYLE=\"text-decoration:none;vertical-align:middle;\" NAME=\"Home\"><DIV  STYLE=\"width:56px;height:48px;cursor: pointer;cursor: hand;\"><FONT ID=\"Navigation1_f1\" FACE=\"Helvetica, Arial, sans-serif\" CLASS=\"size12 Helvetica12\" STYLE=\"vertical-align:middle;color:#FFFFFF;line-height:48px;\">Home<\/FONT><\/DIV><\/A><\/TD><TD WIDTH=\"40\"><IMG style=\"display: block;\" SRC=\"\/tp.gif\" WIDTH=\"40\" HEIGHT=\"1\" BORDER=\"0\" ALT=\"\"><\/TD><TD NOWRAP HEIGHT=\"48\" ALIGN=\"center\" VALIGN=\"MIDDLE\" id=\"Navigation1_Link2\" style=\"cursor: pointer;cursor: hand;color:#FFFFFF;background-image:url(\/files\/MasterImages\/4000_navoff.png);background-repeat:no-repeat;background-position:center;\"  onmouseover=\"doMouseChange(nav_Navigation1,this,\'2\',true);\" onmouseout=\"doMouseChange(nav_Navigation1,this,\'2\',false);\"><A HREF=\"\/About-Us.html\" TARGET=\"_self\" STYLE=\"text-decoration:none;vertical-align:middle;\" NAME=\"About Us\"><DIV  STYLE=\"width:56px;height:48px;cursor: pointer;cursor: hand;\"><FONT ID=\"Navigation1_f2\" FACE=\"Helvetica, Arial, sans-serif\" CLASS=\"size12 Helvetica12\" STYLE=\"vertical-align:middle;color:#FFFFFF;line-height:48px;\">About&nbsp;Us<\/FONT><\/DIV><\/A><\/TD><TD WIDTH=\"40\"><IMG style=\"display: block;\" SRC=\"\/tp.gif\" WIDTH=\"40\" HEIGHT=\"1\" BORDER=\"0\" ALT=\"\"><\/TD><TD NOWRAP HEIGHT=\"48\" ALIGN=\"center\" VALIGN=\"MIDDLE\" id=\"Navigation1_Link3\" style=\"cursor: pointer;cursor: hand;color:#FFFFFF;background-image:url(\/files\/MasterImages\/4000_navoff.png);background-repeat:no-repeat;background-position:center;\"  onmouseover=\"doMouseChange(nav_Navigation1,this,\'3\',true);\" onmouseout=\"doMouseChange(nav_Navigation1,this,\'3\',false);\"><A HREF=\"\/Services.html\" TARGET=\"_self\" STYLE=\"text-decoration:none;vertical-align:middle;\" NAME=\"Services\"><DIV  STYLE=\"width:56px;height:48px;cursor: pointer;cursor: hand;\"><FONT ID=\"Navigation1_f3\" FACE=\"Helvetica, Arial, sans-serif\" CLASS=\"size12 Helvetica12\" STYLE=\"vertical-align:middle;color:#FFFFFF;line-height:48px;\">Services<\/FONT><\/DIV><\/A><\/TD><TD WIDTH=\"40\"><IMG style=\"display: block;\" SRC=\"\/tp.gif\" WIDTH=\"40\" HEIGHT=\"1\" BORDER=\"0\" ALT=\"\"><\/TD><TD NOWRAP HEIGHT=\"48\" ALIGN=\"center\" VALIGN=\"MIDDLE\" id=\"Navigation1_Link4\" style=\"cursor: pointer;cursor: hand;color:#FFFFFF;background-image:url(\/files\/MasterImages\/4000_navoff.png);background-repeat:no-repeat;background-position:center;\"  onmouseover=\"doMouseChange(nav_Navigation1,this,\'4\',true);\" onmouseout=\"doMouseChange(nav_Navigation1,this,\'4\',false);\"><A HREF=\"\/Products.html\" TARGET=\"_self\" STYLE=\"text-decoration:none;vertical-align:middle;\" NAME=\"Products\"><DIV  STYLE=\"width:56px;height:48px;cursor: pointer;cursor: hand;\"><FONT ID=\"Navigation1_f4\" FACE=\"Helvetica, Arial, sans-serif\" CLASS=\"size12 Helvetica12\" STYLE=\"vertical-align:middle;color:#FFFFFF;line-height:48px;\">Products<\/FONT><\/DIV><\/A><\/TD><TD WIDTH=\"40\"><IMG style=\"display: block;\" SRC=\"\/tp.gif\" WIDTH=\"40\" HEIGHT=\"1\" BORDER=\"0\" ALT=\"\"><\/TD><TD NOWRAP HEIGHT=\"48\" ALIGN=\"center\" VALIGN=\"MIDDLE\" id=\"Navigation1_Link5\" style=\"cursor: pointer;cursor: hand;color:#FFFFFF;background-image:url(\/files\/MasterImages\/4000_navoff.png);background-repeat:no-repeat;background-position:center;\"  onmouseover=\"doMouseChange(nav_Navigation1,this,\'5\',true);\" onmouseout=\"doMouseChange(nav_Navigation1,this,\'5\',false);\"><A HREF=\"\/IT-Solutions.html\" TARGET=\"_self\" STYLE=\"text-decoration:none;vertical-align:middle;\" NAME=\"IT Solutions\"><DIV  STYLE=\"width:56px;height:48px;cursor: pointer;cursor: hand;\"><FONT ID=\"Navigation1_f5\" FACE=\"Helvetica, Arial, sans-serif\" CLASS=\"size12 Helvetica12\" STYLE=\"vertical-align:middle;color:#FFFFFF;line-height:48px;\">IT&nbsp;Solutions<\/FONT><\/DIV><\/A><\/TD><\/TR><\/TABLE><\/TD><\/TR><\/TABLE><script type="text/javascript">addMouseAndStyleSupportNavigation1(nav_Navigation1,"ntb'+navTBSuffix+'");'+'</scri'+'pt>');
	}

