<?php
//-----------------------------------
// UNIX BASED UPLOAD SCRIPT
// THIS SHOULD BE THE STANDARD
//-----------------------------------
include "globals.php"; 
include "CategoryBoxFunction.php";
include "SubCategoryBoxFunction.php";
include "makeIconFunction.php";
include "SizeCheckBoxMOD.php";


if ($_FILES['FUpload']['size'] <= 0)
{
 print "Image file not uploaded please retry";
 include "gfooter.php";
 exit;
}

$ImageName = $_POST['FUpload'];
$ParentCategory = $_POST['CatBox'];

$ImageRealName=$_FILES['FUpload']['name']; // get the real name   
$tempName=$_FILES['FUpload']['tmp_name']; // get the temp name

//--------------------------------
// STANDARD UPLOAD FILE BLOCK
//--------------------------------
$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']); 
$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . 'ProductImage/';
$target_path= $_SERVER['DOCUMENT_ROOT'] . '/ProductImage/' . basename ($ImageRealName); // calc the target location
$ImageThumbName= $_SERVER['DOCUMENT_ROOT'] . '/ProductImage/thumb_' . basename ($ImageRealName); 


// REMOVING SPACES FROM IMAGE NAME
//$target_path = ereg_replace(" ","_",$target_path);
//$ImageThumbName = ereg_replace(" ","_",$ImageThumbName);

// UPLOAD AND COPY FILE
@move_uploaded_file($tempName, $target_path);
copy($target_path,$ImageThumbName);    // Copy the image to the server
//--------------------------------
// END OF STANDARD BLOCK
//--------------------------------                 
                 
                 
print "<body>";
print "<font face=\"Arial\" size=\"3\"><b>Add this product into the catalog</font></b>";
print "<form method=\"GET\" action=\"DBProduct.php\">";

print "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\"";
print "bordercolor=\"#111111\" width=\"40%\" id=\"AutoNumber1\" height=\"155\">";
print "<tr>";

print "<td width=\"10%\" height=\"150\" style=\"\border-style: none; border-width: medium\">";

print "<img border=\"0\" src=\"/ProductImage/" . $ImageRealName . "\" width=\"200\" height=\"200\"></p>";

print "</td>";

print "<td width=\"10%\" height=\"150\" style=\"border-style: none; border-width: medium\">";

print "<font face=\"Arial\" align=\"left\" size=\"1\"><b>Select Subcategory:<br></font>";
SubCategoryByCat($linkID,$ParentCategory);

print "</td>";

print "</tr></table>";


// Display Form
print "<div style=\"background-color: #E1F0FF\">";

print "&nbsp;<p><font face=\"Arial\"><font size=\"2\"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
print "Header: </b></font><b><input type=\"text\" name=\"FHeader\" size=\"54\"></b></font></p>";
print "<p><font face=\"Arial\"><b><font size=\"2\">Sub Header: </font>";
print "<input type=\"text\" name=\"FSubheader\" size=\"54\"></b></font></p>";
print "<p><font face=\"Arial\"><b><font size=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
print "Item #:&nbsp; </font><input type=\"text\" name=\"FItemNum\" size=\"23\"><font size=\"2\">&nbsp; ";
print "Price:&nbsp; </font><input type=\"text\" name=\"FPrice\" size=\"15\"><font size=\"2\">";
print "</font></b></font><font size=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
print "Position:&nbsp; </font><input type=\"text\" name=\"FPosition\" size=\"15\">";
print "</b></font></p>";
print "&nbsp;&nbsp;&nbsp;<font face=\"Arial\"><font size=\"2\"><b>&nbsp;&nbsp;keyword: </b></font><b><input type=\"text\" name=\"Fkeyword\" size=\"23\"></b></font></p>";

// Size Input
print "<p><font face=\"Arial\"><b><font size=\"2\">Sizes Text:&nbsp; </font>";
print "<input type=\"text\" name=\"FComment\" size=\"54\" value=\"$PComment\" ></b></font>";


//=======================================
// SHOULD BE IN TABLE LEFT
// Display Size Check Box [OPTION]
// SizeCheckBoxMod();

SizeCheckBoxModCHECK($PSizeBoxCheck);

//$PSizeBoxCheck
//=======================================

// Color Text Input
print "<p><font face=\"Arial\"><b><font size=\"2\">Available Colors:&nbsp; </font>";
print "<input type=\"text\" name=\"FColorsText\" size=\"48\" value=\"$PColorsText\" ></b></font></p>";

print "<div style=\"background-color: #E1F0FF\">";
print "<font face=\"Arial\"><b><font size=\"2\">Description:&nbsp;<br></font>";
print "<textarea rows=\"9\" name=\"FDescription\" cols=\"46\"></textarea></b></font></div>";
//========================

// Key Features
print "<br><font face=\"Arial\"><b><font size=\"3\">Key Features:</font>";
//B1
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp;&nbsp;Text 1:&nbsp; </font>";
print "<input type=\"text\" name=\"B1\" size=\"54\" value=\"$B1\" ></b></font>";

//B2
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp; Text 2:&nbsp; </font>";
print "<input type=\"text\" name=\"B2\" size=\"54\" value=\"$B2\" ></b></font>";

//B3
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp; Text 3:&nbsp; </font>";
print "<input type=\"text\" name=\"B3\" size=\"54\" value=\"$B3\" ></b></font>";

//B4
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp; Text 4:&nbsp; </font>";
print "<input type=\"text\" name=\"B4\" size=\"54\" value=\"$B4\" ></b></font>";

//B5
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp; Text 5:&nbsp; </font>";
print "<input type=\"text\" name=\"B5\" size=\"54\" value=\"$B5\" ></b></font>";


//B6
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp; Text 6:&nbsp; </font>";
print "<input type=\"text\" name=\"B6\" size=\"54\" value=\"$B1\" ></b></font>";

//B7
print "<br><font face=\"Arial\"><b><font size=\"2\">&nbsp; Text 7:&nbsp; </font>";
print "<input type=\"text\" name=\"B7\" size=\"54\" value=\"$B6\" ></b></font>";

//========================
print "<p><font face=\"Arial\"><b><font size=\"2\">Case Pack: </font>";
print "<input type=\"text\" name=\"FCase\" size=\"12\"><font size=\"2\">&nbsp;&nbsp; Order ";
print "Minimum:&nbsp; </font><input type=\"text\" name=\"FMin\" size=\"12\"></b></font></p>";
print "<p><font face=\"Arial\"><b><font size=\"2\">Discount:&nbsp;&nbsp; </font>";
print "<input type=\"text\" name=\"FDiscount\" size=\"8\"><font ";
print " size=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shipping:&nbsp; </font><input type=\"text\" name=\"FShipping\"";
print "size=\"12\"></b></font></p><p>&nbsp;";
print "<font face=\"Arial\" color=\"#FF0000\" size=\"2\"><b>Upload Image File:</font>&nbsp;<input type=\"file\" name=\"FUpload\" size=\"40\"></div>";

print "<div style=\"background-color:#D9FFDC\">";
print "<input name=\"DisplayImage\" size=\"100\" value=\"$target_path\" type=\"hidden\">";
print "<input name=\"Catname\" size=\"100\" value=\"$ParentCategory\" type=\"hidden\">";


print "<font face=\"Arial\"><b><input type=\"submit\" value=\"Submit\" name=\"SUBMIT\">";
print "<input type=\"reset\" value=\"Reset\" ";
print "name=\"B2\"></b></font></div>";

print "</form></body>";

nextProduct();


include "gfooter.php";


//===============================================================
function nextProduct()
{
 print "</font>";
 print "<div style=\"background-color: #DDFFEE\">";
 
 print "<p><a href=\"addTable.php\">";

 print "<img border=\"0\" src=\"../images/but_mnewtable.gif\"></a></font></b>";

 print "<a href=\"mlistTables.php\">";

 print "<img border=\"0\" src=\"../images/but_backcategorylist.gif\"></a></font></b>";
 
 print "</div>";
}   
   
?>

