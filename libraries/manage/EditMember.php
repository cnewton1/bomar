<?php
// Include MySQL class
// Copyright (c) 2002-2008 Chronicles Systems, Inc.
include "../globals.php"; 
include "../users.php";
include "../rightglobals.php"; 
include "../html_mime_mail_2.5/htmlMimeMail.php";
include "../ordernumber.php";
include "../orderhistory.php";
include "adminheader.php";

adminTitle("Edit Member Profile");

// Process actions

$action = $_GET['action'];
global $COMPANYNAME;

$id=$_GET['MemberNumber'];
$sql="SELECT id,company,name,last,email,phone,saddress1,saddress2,scity,sstate,szip,status from member where id=$id";
$resultID=mysql_query($sql,$linkID);

if($_GET['error_message'])
  {
      print "<font color=\"red\">";
      print $_GET['error_message'];
      print "</font>";
  }

if(isset($_GET['f']))
 {
        $userid=$_SESSION['id'];
        $Bussiness_name=$_SESSION['BusinessName'];
        $first_name=$_SESSION['FFName'];
        $last_name=$_SESSION['FLastName'];
        $email=$_SESSION['FEmail'];
        $password1=$_SESSION['FPassword1'];
        $password2=$_SESSION['FPassword2'];
        $phone=$_SESSION['FPhone'];
        $address1=$_SESSION['FSAddress'];
        $address2=$_SESSION['FSAddress2'];
        $city=$_SESSION['FSCity'];
        $state=$_SESSION['FSState'];
        $zip=$_SESSION['FSZip'];
 }

else
  {
        while($row=mysql_fetch_row($resultID))
        {
        $fieldcount=1;
        foreach($row as $field)
        {
                if($fieldcount==1)
                {
                $userid=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==2)
                {
                $Bussiness_name=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==3)
                {
                $first_name=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==4)
                {
                $last_name=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==5)
                {
                $email=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==6)
                {
                $phone=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==7)
                {
                $address1=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==8)
                {
                $address2=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==9)
                {
                $city=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==10)
                {
                $state=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==11)
                {
                $zip=$field;
                $fieldcount++;
                continue;
                }
                if($fieldcount==12)
                {
                $status=$field;
                $fieldcount++;
                continue;
                }
        }
        }
}
print "<body>";
print "<p><h3> Register for " . $COMPANYNAME . " Membership Profile <h3></p>";
print "<form method=\"POST\" action=\"UpdateMember.php\">";
print "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"60%\" id=\"AutoNumber2\" height=\"399\">";
print "<tr>";
print "<td width=\"159%\" height=\"19\" align=\"left\" colspan=\"3\" bgcolor=\"#CCCCFF\">";
print "<font face=\"Arial\" size=\"2\"><br><b>&nbsp;&nbsp;Enter your new membership information below</b><br><br></font></td>";
print "</tr>";

print "<input type=\"hidden\" name=\"id\" value=\"$userid\" ";

print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<b><font size=\"2\" face=\"Arial\"></font></b></td>";

// Business Name
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Business Name <font color=\"#FF0000\"> </font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"BusinessName\" value=\"$Bussiness_name\" size=\"20\"></td>";
print "</tr>";

// Personal Info First Name
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<b><font size=\"2\" face=\"Arial\">Personal Info</font></b></td>";

print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">First Name <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FFName\" value=\"$first_name\" size=\"20\"></td>";
print "</tr>";

// Last Name
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Last Name <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FLastName\" value=\"$last_name\" size=\"20\"></td>";
print "</tr>";
print "<tr>";


// Email
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Email/User ID <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FEmail\" size=\"20\" value=\"$email\" ></td>";
print "</tr>";

// Password
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Password</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"password\" name=\"FPassword1\" size=\"20\"></td>";
print "</tr>";

// Password  Check
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Verify Password</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"password\" name=\"FPassword2\" size=\"20\"></td>";
print "</tr>";

// Contact Phone
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Contact Phone</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FPhone\" value=\"$phone\" size=\"20\"></td>";
print "</tr>";

// print "<tr>";
// print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
// print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
// print "<font face=\"Arial\" size=\"2\">Status</font></td>";
// print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
// print "<select name=\"change_status\"><option value=\"active\" >active</option><option value=\"not active\" >not active</option></select></td>";
// print "</tr>";


print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Status</font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
?>
<select name=change_status><option value="active" <?php if($status=="active") { ?> selected="selected" <?php } ?> >active</option><option value="not active" <?php if($status=="not active") { ?> selected="selected" <?php } ?> >not active</option></select>
<?php
print "</td></tr>";


print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "</tr>";
print "<tr>";

print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<b><font size=\"2\" face=\"Arial\">Shipping Address</font></b></td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Address <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSAddress\" value=\"$address1\"size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Address 2 <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSAddress2\" value=\"$address2\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">City <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSCity\" value=\"$city\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">State <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSState\" value=\"$state\" size=\"20\"></td>";
print "</tr>";
print "<tr>";
print "<td width=\"27%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">&nbsp;</td>";
print "<td width=\"21%\" height=\"19\" align=\"right\" style=\"border-style: none; border-width: medium\">";
print "<font face=\"Arial\" size=\"2\">Zip <font color=\"#FF0000\">*</font></font></td>";
print "<td width=\"111%\" height=\"19\" style=\"border-style: none; border-width: medium\">&nbsp;";
print "<input type=\"text\" name=\"FSZip\" value=\"$zip\" size=\"20\"></td>";
print "</tr>";
print "</table>";

print "<button type=\"Submit\" name=\"B1\">Apply Change ";
print "</button>";

print "</form>";
print "</body>";
print "</html>";
include "adminfooter.php";  
?>