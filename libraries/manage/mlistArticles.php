<?php
include "../globals.php"; 
include "adminheader.php";
include "mlistArticlesFunction.php";
include "../pager.php";

//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------

adminTitle("Manage Articles");

if(isset($_GET['page']))
 {
 $page = $_GET['page']; 
 }
else
 {
 $page=1;
 }

listArticlesFunction($linkID,$page);

?>

