<?php
session_start();
include "../globals.php"; 
include "../users.php";
include "../rightglobals.php"; 
include "../html_mime_mail_2.5/htmlMimeMail.php";
include "../ordernumber.php";
include "../orderhistory.php";
include "adminheader.php";

adminTitle("Update Member Profile");

global $linkID;
   $id=$_POST['id'];
   $FCompany=$_POST['BusinessName'];   
   $FFName=$_POST['FFName'];
   $FLastName=$_POST['FLastName'];
   $FEmail=$_POST['FEmail'];
   $FSAddress=$_POST['FSAddress'];
   $FSAddress2=$_POST['FSAddress2'];
   $FSCity=$_POST['FSCity'];   
   $FSState=$_POST['FSState'];
   $FSZip=$_POST['FSZip'];
   $FPhone=$_POST['FPhone'];
   $FPassword1=$_POST['FPassword1'];
   $FPassword2=$_POST['FPassword2'];
   $status=$_POST['change_status'];
   $error=0;
   $error_message="";
   if(empty($FFName) || empty($FLastName) || empty($FEmail) || empty($FSAddress) || empty($FSAddress2) || empty($FSCity) || empty($FSState) || empty($FSZip))
     {
       $error_message="<ul><li>All Fields are mandatory.</li>";
       $sess_exist=1;
       $error=1;
     }
   if($FPassword1 != $FPassword2)
     {
       $error_message.="<li>Passwords do not match.</li>";
       $sess_exist=1;
       $error=1;
     }
   if($FEmail!="" && !eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $FEmail)) {
       $error_message.="<li>Invalid email address.</li>";
       $sess_exist=1;
       $error=1;
    }
   $error_message.="</ul>";

   
if($sess_exist==1)
  {
        $_SESSION['id']=$id;
        $_SESSION['BusinessName']=$FCompany;
        $_SESSION['FFName']=$FFName;
        $_SESSION['FLastName']=$FLastName;
        $_SESSION['FEmail']=$FEmail;
        $_SESSION['FPhone']=$FPhone;
        $_SESSION['FSAddress']=$FSAddress;
        $_SESSION['FSAddress2']=$FSAddress2;
        $_SESSION['FSCity']=$FSCity;
        $_SESSION['FSState']=$FSState;
        $_SESSION['FSZip']=$FSZip;
        header("location: EditMember.php?f=1&error_message=$error_message");
  }
if($error==0)
  {
        session_destroy();
        update($linkID,$FFName,$FLastName,$FCompany,$FAccount,$FPhone,$FEmail,$FAddress,$FAddress2,
        $FCity,$FState,$FSAddress,$FSAddress2,$FSCity, $FSState, $FSZip,$FPassword1,$FPassword2,$id,$status);
        include "adminfooter.php";  
  }
function update($linkID,$FFName,$FLastName,$FCompany,$FAccount,$FPhone,$FEmail,$FAddress,$FAddress2,
     $FCity,$FState,$FSAddress,$FSAddress2,$FSCity, $FSState, $FSZip, $FPassword1,$FPassword2,$id,$status)
{
// Get the order number
$OrderNumber=GetNextOrderNumber();
//=====================================================================


//------------------------------------------------------

if(empty($FPassword1) && empty($FPassword2))
  {
  $sql="update member SET name='".$FFName."',last='".$FLastName."',company='".$FCompany."',saddress1='".$FSAddress."',saddress2='".$FSAddress2."',scity='".$FSCity."',sstate='".$FSState."',szip='".$FSZip."',email='".$FEmail."',phone='".$FPhone."',status='".$status."' WHERE id=$id";
  $resultID=mysql_query($sql,$linkID);
  }
else
  {
  $sql="update member SET name='".$FFName."',last='".$FLastName."',company='".$FCompany."',saddress1='".$FSAddress."',saddress2='".$FSAddress2."',scity='".$FSCity."',sstate='".$FSState."',szip='".$FSZip."',email='".$FEmail."',phone='".$FPhone."',password='".$FPassword1."',status='".$status."' WHERE id=$id";
  $resultID=mysql_query($sql,$linkID);
  }
      
mysql_close($linkID);

if ($resultID == TRUE) 
{

print "<h2>The Account has been updated </h2><p>";

  $cart = $_SESSION['cart'];
//======================================================
   addOrderHistroy($linkID,$OrderNumber,$FFName,$FLastName,$cart,$FEmail,$FPhone);
//=====================================================

// Instantiate a new HTML Mime Mail object
    $mail = new htmlMimeMail();
    
// FORMAT THE HTML HEADER
   $headers = "From: $FEmail $\r\n";
//   $contents=  BuilddisplayCustomerInfo();   

// Instantiate a new HTML Mime Mail object
    $mail = new htmlMimeMail();
// Set the sender address
    $mail->setFrom($FEmail);
    
// Set the reply-to address
    $mail->setReturnPath($FEmail);

// Set the mail subject
    $SUBJECT = "UPDATED ACCOUNT INFORMATION";
    $mail->setSubject($SUBJECT);

// Set the mail body text
//  $mail->setText($contents);
  
    $contents = "<html><body>" . BuilddisplayCustomerInfo() . $contents . "</body></html>";
     
    $mail->setHTML($contents, "http://"  . $MAINURL . "/images/");
    global $COMPEMAIL;
    $everyone = $COMPEMAIL . "," . $FEmail;
    // Send the email!
    if (!$mail->send(array($everyone)))
    {
       print "<H2> Error in sending updated account information to your email address, please try again <h2><p>";
    }
    
   // END OF FORMATING
    print "<font color=\"#FF0000\">";
    print "<p></font>";
    print "<h4><p>";
/*    print "<p> Check your email in a few minutes for the updated information<p><h4>"; */   

//     $to="ncharles@chronsystems.com";
//     $subject="SHIRTMAKERS MEMEBER SHIP REQUEST WAS PROCESSED" . $COMPANYNAME;
    
//     displayCustomerInfo();
  
}
else
{
        print $FFName. "<P>";
        print $FLastName. "<P>";
        print $FCompany . "<P>"; 
        print $FAccount . "<P>"; 
        print $FPhone . "<P>"; 
        print $FEmail . "<P>"; 
        print $FAddress . "<P>"; 
        print $FAddress2 . "<P>"; 
        print $FCity . "<P>"; 
        print $FState . "<P>"; 
        print $FZip . "<P>"; 
        print $FSAddress . "<P>"; 
        print $FSAddress2 . "<P>"; 
        print $FSCity . "<P>"; 
        print $FSState . "<P>";
        print $FSZip . "<P>";
        print " YOUR ACCOUNT HAS NOT BEEN UPDATED, PLEASE CHECK YOUR ENTRIES AND AGAIN. <p>"; 
}
}


function displayCustomerInfo()
{

global $FFName;
global $FLastName;
global $FCompany ; 
global $FAccount ; 
global $FPhone ; 
global $FEmail ; 
global $FAddress ; 
global $FAddress2 ; 
global $FCity ; 
global $FState ; 
global $FZip ; 
global $FSAddress ; 
global $FSAddress2 ; 
global $FSCity ; 
global $FSState ;
global $FSZip ;

print "<body>";
//TEST
print "<br>";
print "<font face=\"Arial\"><b>Email:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$FEmail";
print "<br>";
print "<b>Phone:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>$FPhone <br>";
print "<b>Account:</b>&nbsp; $FAccount </font></p>";
// GOOD
print "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\"";

print "bordercolor=\"#111111\" width=\"53%\" id=\"AutoNumber1\">";
print "<tr>";
print "<td width=\"46%\" bgcolor=\"#3399FF\"><b><font face=\"Arial\" color=\"#FFFFFF\">&nbsp;Billing</font></b></td>";
print "<td width=\"6%\" bgcolor=\"#3399FF\">&nbsp;</td>";
print "<td width=\"48%\" bgcolor=\"#3399FF\"><b><font face=\"Arial\" color=\"#FFFFFF\">&nbsp;Shipping</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width: none\"><b>";
print "<font face=\"Arial\" size=\"2\">$FFName  $FLastName</font></b></td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: none\">&nbsp;</td>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FFName  $FLastName</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FCompany</font></b></td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FCompany</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FAddress</font></b></td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FSAddress</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FAddress2</font></b></td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FSAddress2</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FCity</font></b></td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FSCity</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FState&nbsp; $FZip</font></b></td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>";
print "<font face=\"Arial\" size=\"2\">$FSState&nbsp; $FSZip</font></b></td>";
print "</tr>";
print "<tr>";
print "<td width=\"46%\" style=\"border-style: none; border-width:none \">&nbsp;</td>";
print "<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "<td width=\"48%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>";
print "</tr>";
print "</table>";

print "</body>";

}



function BuilddisplayCustomerInfo()
{

global $FFName;
global $FLastName;
global $FCompany ; 
global $FAccount ; 
global $FPhone ; 
global $FEmail ; 
global $FAddress ; 
global $FAddress2 ; 
global $FCity ; 
global $FState ; 
global $FZip ; 
global $FSAddress ; 
global $FSAddress2 ; 
global $FSCity ; 
global $FSState ;
global $FSZip ;

$vout[]="<body>";
$vout[] = "<img src=http://shirtmakersusa.com/images/mainheader_blue.jpg>";
$vout[]= '<br>';
$vout[]= '<font face=\"Arial\" size=\"3\">' . $COMPANYNAME;
$vout[]= '<br>';
$vout[]= '<font face=\"Arial\"><b>Email:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $FEmail;
$vout[]= '<br>';
$vout[]= '<b>Phone:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>' . $FPhone . '<br>';
$vout[]= '<b>Account:</b>&nbsp;' . $FAccount . '</font></p>';


$vout[]= '<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\"';
$vout[]= 'bordercolor=\"#111111\" width=\"53%\" id=\"AutoNumber1\">';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" bgcolor=\"#3399FF\"><b><font face=\"Arial\" color=\"#FFFFFF\">&nbsp;Billing</font></b></td>';
$vout[]= '<td width=\"6%\" bgcolor=\"#3399FF\">&nbsp;</td>';
$vout[]= '<td width=\"48%\" bgcolor=\"#3399FF\"><b><font face=\"Arial\" color=\"#FFFFFF\">&nbsp;Shipping</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: none\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FFName . '&nbsp;' . $FLastName . '</font></b></td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: none\">&nbsp;</td>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FFName . '&nbsp;' . $FLastName . '</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FCompany . '</font></b></td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">'. $FCompany . '</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FAddress . '</font></b></td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FSAddress . '</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FAddress2 . '</font></b></td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FSAddress2 . '</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FCity . '</font></b></td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FSCity . '</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' . $FState . '&nbsp;'. $FZip.'</font></b></td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width: 0\"><b>';
$vout[]= '<font face=\"Arial\" size=\"2\">' .  $FSState . '&nbsp;' . $FSZip. '</font></b></td>';
$vout[]= '</tr>';
$vout[]= '<tr>';
$vout[]= '<td width=\"46%\" style=\"border-style: none; border-width:none \">&nbsp;</td>';
$vout[]= '<td width=\"6%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '<td width=\"48%\" style=\"border-style: none; border-width: 0\">&nbsp;</td>';
$vout[]= '</tr>';
$vout[]= '</table>';
$vout[]= '</body>';

 return join('',$vout);
}


   
?>