<footer>

<!-- Only IE 10 and later understand "placeholder" attribute -->
<!--[if lt IE 10]>
<script>
jQuery(document).ready(function($) {
	$("footer #quick_order input").css("color", "#bbb");
	$("footer #quick_order input").each(function(index) {
		if (!($(this).hasClass("btn"))) {
			var placeHolder = $(this).attr("placeholder");
			$(this).val(placeHolder);
		};
	}); 
	
	$("footer #quick_order input").click(function() {
		if (!($(this).hasClass("btn"))) {
			if ($(this).val() == "Qty." || $(this).val() == "Item #") {
				$(this).val("");
				$(this).css("color", "#333");
			}
			$(this).blur(function() {
				if ($(this).val() == "") {
					if ($(this).hasClass("qty")) {
						$(this).val("Qty.");
						$(this).css("color", "#bbb");
					}
					
					else {
						$(this).val("Item #");
						$(this).css("color", "#bbb");
					}
				}
				
				else {
						if ($(this).val() == "Qty." || $(this).val() == "Item #") {
							$(this).css("color", "#bbb");
						}
						else { 
							$(this).css("color", "#333")
						}
				}
			});	
		}		
	});
});
</script>
<![endif]-->
<!-- end div top-->

<div id="center">
<div class="vlbdr"></div>

	<div class="contact">   
        <div class="call">
           <div class="hdr">Call Us</div>
           <div class="no" id="salsePhoneNumFt">1-727-474-1327</div>
      </div>
        <div class="email">
           <div class="hdr">Email Us</div>
           <p><a href="/contactUs.php">Click to send us an email</a></p>
      </div>
    </div>

<div class="vrbdr"></div>
    
    <nav>
       <ul>
         <li>Customer Service</li>
         <li><a href="help.php">Help</a></li>
         <li><a href="contactUs.php">Contact Us</a></li>
         <li><a href="about_us.php">About Us</a></li>
         <li><a href="terms_condition.php">Terms &amp; Conditions</a></li>
         <li><a href="shipping_returns.php">Shipping &amp; Returns</a></li>
       </ul>
       <ul>
         <li>Account Information</li>
         <li><a href="order_status.php">Order Status</a></li>
         <li><a href="shopping_cart.php">Shopping Cart</a></li>
       </ul> 
       <ul>
          <li>Site Tools</li>
          <li><a href="site_map.php">Site Map</a></li>
          <li><a href="rabate_center.php">Rebate Center</a></li>
          <li><a href="limited_warranty_info.php">Limited Warranty Information</a></li>
       </ul>   
    </nav>

    
</div><!-- end div center -->
<div class="hbdr"></div>

<ul class="sponsors">
    <li><a href="//www.systemax.com/" target="_blank"><img src="//ak-i21.geccdn.net/site/img/sp_syx.gif" alt="Systemax"></a></li>
    <li><a href="//www.bbb.org/new-york-city/business-reviews/industrial-equipment-and-supplies/global-industrial-in-port-washington-ny-143415/#bbbonlineclick" target="_blank"><img src="//ak-i21.geccdn.net/site/img/sp_bbb.gif" alt="Accredited Business"></a></li>
    <li><a href="//www.trustwave.com" target="_blank"><img src="//ak-i21.geccdn.net/site/img/sp_twave.gif" alt="TrustWave"></a></li>
    <li><a href="//www.mastercard.com" target="_blank"><img src="//ak-i21.geccdn.net/site/img/sp_mcard.gif" alt="MasterCard SecureCode"></a></li>
    <li><a href="/mdm" target="_blank"><img src="images/MDM_badge.gif" alt="MDM Market Leaders Web Badge"></a></li>
    <li><a href="//usa.visa.com" target="_blank"><img src="//ak-i21.geccdn.net/site/img/sp_visa.gif" alt="Verified by Visa"></a></li>
    <li><a class="paypal" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=600, height=400');">
     <img src="//ak-i21.geccdn.net/site/img/btn_paypal_solutions.gif" alt="PayPal Payment Solutions">
    </a></li>
</ul>


<p id="copyright">Copyright &#169; 2015 Westcarb Enterprises Company. All Rights Reserved. <br />
  <strong>Shop With Confidence - 30 Day Satisfaction Guarantee</strong><br />
</p>

<div id="node">60</div>



</footer>





    </div>

   