<?
#----------------------------------#
# Contains a library of functions  #
# to manage Virtualinsults         #
#----------------------------------#
#- Manage some other includes to benefit other programs

#-------------------------------#
# Shows a template page all     #
# filled in from vars instead   #
# of templates.			#
#-------------------------------#
function lib_showpagevars($template="", $body="",$title="")
  {
    global $TEMPLATE, $db, $user, $mycookie;
    
    	#retrieving the datas of the user through the cookie
	if (($mycookie != "nocookie") && ($mycookie))	
	{
		$userdatas = Array();
		$userdatas = lib_getrecords("users","id", $mycookie);
		$userdata = $userdatas[0];
		$user[name] = $userdata[name];
		$user[useremail]  = $userdata[email];
		$user[nivel] = $userdata[nivel];
	
		$welcome = "Welcome $user[name]";
		$cookiename = "<a href=index.php?command=deletecookie class=menubar> I am not $user[name] </a> | ";
		$cookiename .= "<a href=index.php?command=viewbio&form[username]=" .$user[username] . " class=menubar> View your bio page </a> |";
		$cookiename .= "<a href=index.php?command=updatebio class=menubar> Update your bio page </a>";
		}
	
	else
	
	{
		$mycookie = "nocookie";
		
		$user[name] = "";
		$user[useremail] = "";
		$user[nivel] = "u";
		$user[login] = 0;

		$welcome = "Welcome to Virtualinsults.com";
		$cookiename= "<a href=index.php?command=login class=menuitem> LOGIN </a>";
	}
	
	
	switch ($user[nivel])
		{
	 
			case "u":
			$nivel = "you are a user";
			break;
		
			case "m":
			$nivel = "you are a member";
			break;
		
			default:
			$nivel = "you are a gold member";
		}  	  
		
		
	if(!$title) $title = "Virtualinsults.com";
	
	if ((!$user[login]) OR ($user[login] == 0))
	$loginfo = "NOT LOGGED IN";
	else
	$loginfo = " LOGGED IN";
	if ($loginfo=="NOT LOGGED IN")
		$footer="<tr><td align=\"right\"><span><a href=\"index.php\" class=\"bottombar\">Home</a></span> |<span><a href=\"index.php?command=login\" class=\"bottombar\"> Login</a></span> | <span><a href=\"index.php?command=signup\"  class=\"bottombar\">Join us</a></span> | <span><A HREF=\"/index.php?command=viewbio\" class=\"bottombar\">View user bio</a></span></td></tr>";
	else	$footer="<tr><td align=\"right\"><span><a href=\"index.php\" class=\"bottombar\">Home</a></span> |<span><a href=\"index.php?command=login\" class=\"bottombar\"> Login</a></span> | <span><a href=\"index.php?command=signup\"  class=\"bottombar\">Join us</a></span> | <span><A HREF=\"{UPLOADLINK}\" class=\"bottombar\">Upload a Card</a></span> | <span><A HREF=\"/index.php?command=browse&mode=user\" class=\"bottombar\">My Cards</a></span> | <span><A HREF=\"/index.php?command=viewbio\" class=\"bottombar\">View user bio</a></span> | <span><A HREF=\"/index.php?command=browse&mode=viewFavorites\" class=\"bottombar\">Favorites </a></span></td></tr>";
	
        #- briefme window
        if ($user[briefme]== 0)
        {
        $popup = "onLoad=PopUp();";
        $user[briefme]= 1;
        }
        else
        $popup = "";
          
        #-more functions
        if ($user[login]==1)
        $morefunctions= "| <span><A HREF=/index.php?command=upload class=bottombar>Upload a Card</a></span> | <span><A HREF=/index.php?command=browse&mode=user class=bottombar>My Cards</a></span> | <span><A HREF=/index.php?command=viewbio class=bottombar>View user bio</a></span> | <span><A HREF=/index.php?command=browse&mode=viewFavorites class=bottombar>Favorites </a></span> | <span><A HREF=\"/index.php?command=adressBook\" class=\"bottombar\">Adresses ";       
        else
        $morefunctions = "| <span><A HREF=/index.php?command=viewbio class=bottombar>View user bio</a></span>";
        
    #- Parse the appropriate template
    $tpl = new XTemplate("$template"); 
    $tpl->assign("BODY",$body);
    $tpl->assign("TITLE",$title);
    $tpl->assign("WELCOME",$welcome);
    $tpl->assign("COOKIENAME",$cookiename);
    $tpl->assign("NIVEL",$nivel);	
    $tpl->assign("LOGINFO",$loginfo);	
    $tpl->assign("FOOTER",$footer);
    $tpl->assign("POPUP",$popup);
    $tpl->assign("MOREFUNCTIONS",$morefunctions);
    
    #the user must login before uploading cards	
    
    if ($mycookie != "nocookie")
    $tpl->assign("UPLOADLINK","/index.php?command=upload");	
    else	
    $tpl->assign("UPLOADLINK","/index.php?command=login");	
   

    $tpl->parse("main");
    $tpl->out("main");  
    return(1);
    
  }#- End show page



#------------------------------------------#
# Given a file, load it and parse it with  #
# the supplied hash                        #
# Return as a string to do with as we wish #
#------------------------------------------#
function lib_parseform($template, $arr)
  {
  
    $str = lib_file($template);
    $str = lib_parse($str,$arr);
    return($str);
  }#end parseform


#------------------------------------------#
# Given an array, parse a string with vars #
# in the format {VARIABLE}		   #
#------------------------------------------#
function lib_parse($string="",$vals="")
  {
    if(!is_array($vals) || !$string)
      return 0;
     
     #DEBUG
     #print_r($vals);
     #ENDDEBUG
     
     #- get the key and value for each hash element    
     while(list($k, $v) = each($vals)) 
       {
          $string = eregi_replace("\{$k\}","$v",$string);
       }#wend
          
    return($string);  
  }#end parse


#------------------------------------------#  
# Gets any file and returns it as a string #
#------------------------------------------#
function lib_file($file)
  {
    $file = join("",file($file));
    return $file;  	
  }#end int_file 

   
#-------------------------------#
# Retrieve all records from a table #
#-------------------------------#
function lib_getall($table="",$more="")
      {
	 global $sql;
        
        $db = new phpDB();
        $db->connect();

        $sql = "SELECT * FROM $table $more";
        $result = $db->execute($sql);

	$rows = $result->getNumOfRows();
	if( $rows == 0)
         {
           $result->close();
           return 0;
         }#fi result  
        
          $x=0;
        do   
          {
            $retval[$x] = $result->fields;
            $result->nextRow();
            $x++;
          } while(!$result->EOF);
           
        $result->close();       
        
        return $retval; 
                         
       }
#-------------------------------#
# Retrieve records from a table #
#-------------------------------#
function lib_getrecords($table="",$key="",$value="",$more="")
      {
        global $sql;
        
        $db = new phpDB();
        $db->connect();

        $sql = "SELECT * FROM $table WHERE $key=\"$value\" $more";
        $result = $db->execute($sql);
        $rows = $result->getNumOfRows();
        
        if( $rows == 0)
          {
            $result->close();
            return 0;
          }#fi result  
        
        $x=0;
        do   
          {
            $retval[$x] = $result->fields;
            $result->nextRow();
            $x++;
          } while(!$result->EOF);
           
        $result->close();       
        
        return $retval; 
                         
      }#fi get_records

#-------------------------------#
# Retrieve records from a table #
#-------------------------------#
function lib_getsql($query="")
      {
        global $sql;

        $db = new phpDB();
        $db->connect();

        $sql = $query;
        $result = $db->execute($sql);
        $rows = $result->getNumOfRows();

        if( $rows == 0)
          {
            $result->close();
            return 0;
          }#fi result

        $x=0;
        do
          {
            $retval[$x] = $result->fields;
            $result->nextRow();
            $x++;
          } while(!$result->EOF);

        $result->close();

        return $retval;

      }#fi get_records



#-------------------------------#
# Insert records in a table     #
#-------------------------------#
function lib_insert($table="", $arr="")
  {
    global $sql;
    if(!is_array($arr))
      return(0);
   
     while(list($k, $v) = each($arr))   
       {
         $cols[] = $k;
         $vals[] = $v;
       }#-wend 
       
     $names = join(",",$cols);
     $vals  = '"' . join('","',$vals) . '"';
     
     $retval = 0;
     $db = new phpDB();
     $db->connect();
     $sql = "INSERT INTO $table ($names) VALUES($vals)";
     $result = $db->execute($sql);
     
     #DEBUG
     #print $sql;
     #END DEBUG
     
     #- Get the last insert id
     $result->close();
     $sq = "SELECT LAST_INSERT_ID() FROM $table";
     $result = $db->execute($sq);
     $retval = $result->fields[0];
     $result->close();                 
     
     return($retval);  
  }#-end lib_insert



#-------------------------------#
# Do a simple db update         #
#-------------------------------#
function lib_update($table="", $key_col="", $key_val="", $arr="")
  {
    global $sql;
    
    if(!$table || !$key_col || !$key_val || !$arr)
      {
        return(0);
      }#fi
      
     while(list($k, $v) = each($arr))   
         $pairs[] = "$k=\"$v\"";

     $cols = join(",", $pairs);        
     
     $retval=0;
     
     $db = new phpDB();
     $db->connect();
     $sql = "UPDATE $table SET $cols WHERE $key_col=\"$key_val\"";
     $result = $db->execute($sql);
     $result->close();

     
    return(0);  
  }#end lib_update


#-------------------------------#
# delete some record(s)         #
#-------------------------------#
function lib_delete($table="", $key_col="", $key_val="")
  {
    global $sql;
    
    if(!$table || !$key_col || !$key_val)
        return(0);
      
    $db = new phpDB();
    $db->connect();
    $sql = "DELETE FROM $table WHERE $key_col=\"$key_val\"";
    $result = $db->execute($sql);
    $result->close();

    #DEBUG
    #print "$sql <br>";
    #ENDDEBUG
     
    return(0);  
  }#end lib_delete


#-------------------------------#
# count records			#
#-------------------------------#

function lib_countrecords($key1="", $table="", $key2="", $value="", $more=""){
	
	global $sql;

	$db = new phpDB();
    $db->connect();

	#- How many rows are actually there?
	$sql = "SELECT count($key1) FROM $table WHERE $key2=\"$value\" $more";
	$result = $db->execute($sql);
    $num = $result->fields[0];              
	$result->close(); 
	
	return $num;

} #end of lib_countrecords()


#-------------------------------#
# Some miscellenous functions   #
#-------------------------------#

#- Return an HT List, given an array of items
#- and a default value. Does not return the name or anything
#- returns something like: 
#- Does not deal with option values yet!
function lib_htlist($items="", $default="")
  {
    $retval = "";

	for($x=0; $x< count($items) ;$x++)
      {
        if($default == $items[$x])
          $retval .= "\t<option selected>$items[$x]</option>\n";
        else
          $retval .= "\t<option>$items[$x]</option>\n";  
      }
    return($retval);  
  }#end lib_htlist


#- Prepare an iso formatted date
function lib_isodate($year=0,$month=0,$day=0)
  {
     #- Return today's ISO formatted date
     if($year == 0)
	   {
	     $date = time();
	     $day = date("j",$date);
		 $year = date("Y",$date);
		 $month = date("n",$date);			     
	   }
	   
	 #- Put together the iso date
	 $iso = sprintf("%04d%02d%02d", $year, $month, $day);
	 return($iso);   
  }


#- Clean up a text field for entry into the db
function lib_dbencode($text)
  {
     $text = str_replace("''","&quot;",$text);
     $text = str_replace('\"',"&quot;",$text);
     $text = str_replace('"',"&quot;",$text);
     return($text);
  }


#- Decode a string from the db for editing purposes
function lib_dbdecode($text)
  {
     $text = str_replace("&quot;",'"',$text);
     $text = str_replace("\'","'",$text);
    return($text);
  }

#- Decode a string from the db for displaying purposes
function lib_dbdisplay($text)
  {
    $text = nl2br($text);
    return ($text);
  }

  
#- These functions are self explanatory, but...
#  Convert to UNIX epoch date
function lib_toepoch($m,$d,$y)
  {
    $date = mktime(0, 0, 0, $m, $d, $y);
	return($date);
  }


# Convert to standard date  
function lib_frepoch($epoch)
  {
    $date = getdate($epoch);
    $date = $date['mon'] . "-" . $date['mday'] . "-" . $date['year'];
    return($date);
  }


#- Put together some input fields with dates
#- will make 3 fields:
#  $fieldname_month, $fieldname_day, $fieldname_year
#  Will make all into drop down lists
#  USEAGE:
#  No date - will return today's date
#  A MYSQL date will return that date
#  An epoch date - will return that date



function lib_dateinput($name="date",$date=0)
   {
     if($date==0)
	   {
	     $date = time();
	     $curr_day = date("j",$date);
		 $curr_year = date("Y",$date);
		 $curr_month = date("n",$date);		 
	   }
	 else
	   {
	     #- Check if this thing is in the MYSQL format
		 if(strlen($date) == 8)
		 {
		       $curr_day = substr($date,6,2);
			 $curr_year = substr($date,0,4);
			 $curr_month = substr($date,4,2);
		 }
		 
		 if(strlen($date) > 8)
	       {
			for($x=0; $x<8 ;$x++)
			   {
 		     		$curr_day = date("j",$date);
	                  $curr_year = date("Y",$date);
			      $curr_month = date("n",$date);		 		   
		         }
		 }  
	   }
	 
       $max_days=31;
	 
	 #- Account for years long past
	 if($curr_year < date("Y"))
	   {
	     $this_year = date("Y");
		 $years = $this_year - $curr_year;
		 $y=0;
		
	 for($x=$curr_year; $x <= $this_year; $x++)
		   {
		     $year[$y] = $x;
			 $y++;
		   }
	   }
	 else
	   {
    	   $year[0] = date ("Y");
	   $year[1] = $year[0] + 1;	   
	   $year[2] = $year[1] + 1;
	   }  

	 #- Setup months
	 $month[1]="January";
	 $month[2]="February";
	 $month[3]="March";
	 $month[4]="April";
	 $month[5]="May";
	 $month[6]="June";
	 $month[7]="July";
	 $month[8]="August";
	 $month[9]="September";
	 $month[10]="October";
	 $month[11]="November";
	 $month[12]="December";
	 
	 #- Prepare monthpart
	 $monthpart = "\t<select name=$name". $month . ">\n";
 
	for($x=1;$x<13;$x++)
	   {
	   if($curr_month == $x)
	      $monthpart .= "\t\t<option value=$x selected>$month[$x]</option>\n";
	   else
		$monthpart .= "\t\t<option value=$x>$month[$x]</option>\n";		 	
	   }#-rof

	 $monthpart .= "\t</select>\n";
	 
	 #- Prepare datepart
	 $datepart = "\t<select name=$name". $day . ">\n";
	 $x=0;
	 for($x=1; $x<=$max_days; $x++)
	   {
	     if($curr_day == $x)
		   $datepart .= "\t\t<option value=$x selected>$x</option>\n";
		 else
		   $datepart .= "\t\t<option value=$x>$x</option>\n";  
	   }
	 $datepart .= "\t</select>\n";  
	 
	 #- Prepare yearpart
	 $yearpart = "\t<select name=$name". $year . ">\n";
	 for($x=$year[0]; $x <= $year[count($year)-1]; $x++)
	    {
		  if($curr_year == $x)
		     $yearpart .= "\t\t<option value=$x selected>$x</option>\n";
		  else
		     $yearpart .= "\t\t<option value=$x>$x</option>\n";		     	 
		}#-rof
	 $yearpart .= "\t</select>";
	 
	 $parts = $monthpart . $datepart . $yearpart;
	 return($parts);
   }#- end function

   

#- Prepare a ranking HT list
function lib_rank($default=1)
   {
	 $ranklist = "";
	 for($x=0;$x < 10; $x++)
	   {
		 if($default == $x)
		   $ranklist .= "\t\t<option value=$x selected>$x</option>\n";
		 else
		   $ranklist .= "\t\t<option value=$x>$x</option>\n";		   
	   }
	 return($ranklist);  
   }

   
#- Prepare a status HT list
function lib_status($default= "Pending")
   {
         $array[0] = "Pending";
	 $array[1] = "Published";
	 $array[2] = "Pre-Published";
	 $array[3] = "Rejected";
	 $statuslist = "";
	 
	 for($x=0; $x < count($array); $x++)
	   {
	     if($default == $array[$x])
		    $statuslist .= "\t\t<option value=\"$array[$x]\" selected>$array[$x]</option>\n";
		 else
		    $statuslist .= "\t\t<option value=\"$array[$x]\">$array[$x]</option>\n";		    	
	   }#- rof
	   
	 return($statuslist);  
   }   

#- Prepare a HT list of templates
function lib_template($default=1)
   {
         $array[0] = "News";
         $array[1] = "Classified";
	 $array[2] = "Directory";
	 $array[3] = "Events";
	 $array[4] = "Personals";
	 $array[5] = "Online Store";
	 
	 $statuslist = "";
	 
	 for($x=0; $x < count($array); $x++)
	   {
	     $y = $x+1;
	     
	     if($default == $y)
		    $statuslist .= "\t\t<option value=\"$y\" selected>$array[$x]</option>\n";
		 else
		    $statuslist .= "\t\t<option value=\"$y\">$array[$x]</option>\n";		    	
	   }#- rof
	   
	 return($statuslist);  
   }   
   
   
#- Return a HT List, or array of categories
#- Defaults to array
function lib_table($table="",$array=0, $default=0)
   {
     $sql = "SELECT * FROM $table ORDER BY name";
     
	 $clist = "";
	 $alist = "";
	 $x = 0;
	 
	 $db = new phpDB();
         $db->connect();    	 
	 $result=$db->execute($sql);		  
	 do
	   {
             $name = $result->fields["name"];
	     $id   = $result->fields["id"];
	     if($default == $id)
	        $clist .= "\t\t<option value=$id selected>$name</option>\n";
	     else
	        $clist .= "\t\t<option value=$id>$name</option>\n";
					 		
             $alist[$x] = $result->fields;
	     $x++;
	     $result->nextRow();	
           }while(!$result->EOF);   
	 	 
	 $result->close();		 
		 
	 if($array) 
	    return $alist;
	 else
	    return $clist;		 
   }#- End lib_categories



#- Return a category from the db
function lib_getcategory($categoryid = 0)
   {
    $sql = "SELECT name FROM categories WHERE id=$categoryid";
    $db = new phpDB();
    $db->connect();    	 	 
    $result=$db->execute($sql);		     	
    $retval = $result->fields['name'];
    $result->close();

    return($retval);     
   }



#- Return a HT list, or array of countries worldwide, or from the caribbean
#- Defaults to the caribbean
#- Also defaults to HT List
#- $default is the country that will be selected
function lib_country($caribbean=1, $array=0, $default="")
   {
     #- Prepare a HT-List of countries
     if($caribbean == 1)
	$sql  = "SELECT * FROM countries WHERE carib=1 order by name";
     else
	$sql  = "SELECT * FROM countries order by name";
	 
     $db = new phpDB();
     $db->connect();    	 
	 	 
     $result=$db->execute($sql);		  
     $clist = "";
     $alist = "";
     $x = 0;
     
     do
       {
         $name = $result->fields["name"];
	     $id   = $result->fields["id"];
		 if($default == $id)
	        $clist .= "\t\t<option value=$id selected>$name</option>\n";
	     else
	        $clist .= "\t\t<option value=$id>$name</option>\n";
					  		
		 $alist[$x] = $result->fields;
		 $x++;
         $result->nextRow();	
      }while(!$result->EOF);   

	 $result->close();
	 
	 if($array)
	   return($alist);
	 else
	   return($clist);
	   
   }#- End lib_country


   
#- Return the name of a country
function lib_getcountry($id=0)
  {
    $sql = "SELECT name FROM countries WHERE id='$id'";
    $db = new phpDB();
    $db->connect();    	 	 
    $result=$db->execute($sql);		     	
	$retval = $result->fields[0];
	$result->close();

	return($retval);
  }

#- Check an e-mail address

function lib_checkemail($email) { 

if (eregi("^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,3}$", $email, $check)) { 

	if ( getmxrr(substr(strstr($check[0], '@'), 1), $validate_email_temp) ) { 
	return TRUE; 
															} 
// THIS WILL CATCH DNSs THAT ARE NOT MX. 
	if(checkdnsrr(substr(strstr($check[0], '@'), 1),"ANY")){ 
	return TRUE; 
	} 
} 
return FALSE; 
}#-end 
?>
