<?php

function relogin($firstTime, $error_message, $referer_page = '') {
    global $error, $msg;
    if ($_REQUEST['url'] != '') {
        $referer_page = $_REQUEST['url'];
    }
    ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <div class="container content">
        <form method="post" action="">
            <table id="form"  cellpadding="5" cellspacing="5" border="0"  style=" font-size:12px; color:#000; width:900px">
                <tr>
                    <td>&nbsp;</td>
                    <td height="60" valign="top"><span style="float:left; font-size:26px; font-weight:bold; color:#00F">My Member Login</span> </td>
                </tr>
                <?php
                if (isset($msg)) {
                    echo '<tr>
                    <td colspan="2">
                    <div class="success">' . $msg . '

                    </div></td>
                    </tr>';
                }
                if (!empty($error)) {
                    echo '<tr>
                    <td colspan="2">
                    <div class="error">' . implode('<br>', $error) . '

                    </div></td>
                    </tr>';
                }
                ?>
                <tr>
                    <td width="150" align="right"><b>Your Email Address</b></td>
                    <td ><input type="text" name="user" value="<?php echo $user; ?>" style="width:250px; height:25px; border:1px solid #000" required /></td>
                    <td rowspan="5" valign="top">
                        <span style="float:right; width: 250px;  font-size:14px; font-weight:bold; color:#00F; text-align:center;">
                            <a href="JoinMember1.php" class="button btn1 btn-primary">NOT A MEMBER<br />Click Here to Register for an Account</a>

                        </span>
                    </td>
                </tr>
                <tr>
                    <td align="right"><b>Password</b></td>
                    <td><input type="password" name="password" value=""  style="width:250px; height:25px; border:1px solid #000" required /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><div class="g-recaptcha" data-sitekey="<?php echo GOOGLE_RECAPTCHA_SITE_KEY; ?>"></div>

                    </td>
                </tr>
                <tr>
                    <td >&nbsp;</td>
                    <td ><br>
                        <input type="hidden" name="url" value="<?php echo $referer_page; ?>">
                        <input type="submit" name="button" value="Sign into my Account" class="button btn1  btn-primary" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><a href="forgetPassword.php" class="btn1 btn-warning">? Click Here if you forgot your Password.</a></td>
                </tr>
            </table>
        </form>
    </div>

    <?php
}
?>