<?php
// User settings
$to = "support@chronsystems.com";
$subject = "Westcarb Enterprises Contact Form";
$extra = array(
	"form_subject"	=> true,
	"form_cc"		=> true,
	"ip"			=> true,
	"user_agent"	=> true
);

$action = isset($_POST["action"]) ? $_POST["action"] : "";
if(empty($action)) {
	// Send back the contact form HTML
	$output = "<div style='display:none'>
	<div class='contact-top'></div>
	<div class='contact-content'>
		<h1 class='contact-title'>Contact Us:</h1>
		<div class='contact-loading' style='display:none'></div>
		<div class='contact-message' style='display:none'></div>
		<form action='#' style='display:none'>
			<label for='contact-name'>*Name:</label>
			<input type='text' id='contact-name' class='contact-input' name='name' tabindex='1001' />
			<label for='contact-email'>*Email:</label>
			<input type='text' id='contact-email' class='contact-input' name='email' tabindex='1002' />";

	if ($extra["form_subject"]) {
		$output .= "
			<label for='contact-subject'>Subject:</label>
			<input type='text' id='contact-subject' class='contact-input' name='subject' value='' tabindex='1003' />";
	}
	$output .= "
			<label for='contact-message'>*Message:</label>
			<textarea id='contact-message' class='contact-input' name='message' cols='40' rows='4' tabindex='1004'></textarea>
			<br/>";

	if ($extra["form_cc"]) {
		$output .= "
			<label>&nbsp;</label>
			<input type='checkbox' id='contact-cc' name='cc' value='1' tabindex='1005' /> <span class='contact-cc'>Send me a copy</span>
			<br/>";
	}
	$output .= "
			<label>&nbsp;</label>
			<button type='submit' class='contact-send contact-button' tabindex='1006'>Send</button>
			<button type='submit' class='contact-cancel contact-button simplemodal-close' tabindex='1007'>Cancel</button>
			<br/>
			<input type='hidden' name='token' value='" . smcf_token($to) . "'/>
		</form>
	</div>
</div>";
	echo $output;
}
else if ($action == "send") {
	// Send the email
	$name 		= isset($_POST["name"]) ? $_POST["name"] : "";
	$email 		= isset($_POST["email"]) ? $_POST["email"] : "";
	$subject 	= isset($_POST["subject"]) ? $subject . ' - ' .$_POST["subject"] : $subject;
	$email_subject 	= isset($_POST["subject"]) ? $_POST["subject"] : "";
	$message 	= isset($_POST["message"]) ? $_POST["message"] : "";
	$cc 		= isset($_POST["cc"]) ? $_POST["cc"] : "";
	$token 		= isset($_POST["token"]) ? $_POST["token"] : "";

	// make sure the token matches
	if ($token === smcf_token($to)) {
		smcf_send($name, $email, $subject, $email_subject, $message, $cc);
		echo "Your message was successfully sent.";
	}
	else {
		echo "Unfortunately, your message could not be verified.";
	}
}


function EmailText($info)
{
	$email_body = '<table rules="all" style="border-color: #666;" cellpadding="10">
	  <tr style="background: #eee;">
		<td valign="top"><strong>Name:</strong></td>
		<td>'.$info['name'].'</td>
	  </tr>
	  <tr>
		<td valign="top"><strong>Email:</strong></td>
		<td>'.$info['email'].'</td>
	  </tr>
	  <tr>
		<td valign="top"><strong>Subject:</strong></td>
		<td>'.$info['subject'].'</td>
	  </tr>
	  <tr>
		<td valign="top"><strong>Message:</strong></td>
		<td>'.nl2br($info['message']).'</td>
	  </tr>';
	  
	if(isset($info['ip']))
	{
		$email_body .='
		<tr>
		  <td valign="top"><strong>IP Address:</strong></td>
		  <td>'.nl2br($info['ip']).'</td>
		</tr>';
	}
	if(isset($info['user_agent']))
	{
		$email_body .='
		<tr>
		  <td valign="top"><strong>User Agent:</strong></td>
		  <td>'.nl2br($info['user_agent']).'</td>
		</tr>';
	}
	
	$email_body .= '</table>';
	
	return $email_body;
}

function smcf_token($s) {
	return md5("smcf-" . $s . date("WY"));
}


function smcf_send($name, $email, $subject, $email_subject, $message, $cc) {
	global $to, $extra;

	// Filter and validate fields
	$name = smcf_filter($name);
	$subject = smcf_filter($subject);
	$email_subject = smcf_filter($email_subject);
	$email = smcf_filter($email);
	if (!smcf_validate_email($email)) {
		$subject .= " - invalid email";
		$message .= "\n\nBad email: $email";
		$email = $to;
		$cc = 0; // do not CC "sender"
	}
	
	$emailinfo = array(
			'name'=>$name,
			'email'=>$email,
			'subject'=>$email_subject,
			'message'=>$message
			);

	// Add additional info to the message
	if ($extra["ip"]) {
		$emailinfo['ip']=$_SERVER["REMOTE_ADDR"];
	}
	if ($extra["user_agent"]) {
		$emailinfo['user_agent']=$_SERVER["HTTP_USER_AGENT"];
	}

	// Set and wordwrap message body	
	$message = '<html><body>';
	$message .= '<img src="http://westcarbenterprise.chrondev.com/images/WCE_logo_-_modified1.jpg" alt="'.$subject.'" />';
	$message .= EmailText($emailinfo);
	$message .= "</body></html>";	

	// Build header
	$headers = 'From: '.$email. "\r\n" .
				'Reply-To: '.$email . "\r\n" .
				'Return-Path: <'.$email . ">\r\n";
	if($cc == 1){
		$headers .= "Cc: $email\r\n";
	}			
	$headers .= 'X-Mailer: PHP/' . phpversion();	
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	// UTF-8
	if (function_exists('mb_encode_mimeheader')) {
		$subject = mb_encode_mimeheader($subject, "UTF-8", "B", "\n");
	}
	

	// Send email
	@mail($to, $subject, $message, $headers) or 
		die("Unfortunately, a server issue prevented delivery of your message.");
}

// Remove any un-safe values to prevent email injection
function smcf_filter($value) {
	$pattern = array("/\n/","/\r/","/content-type:/i","/to:/i", "/from:/i", "/cc:/i");
	$value = preg_replace($pattern, "", $value);
	return $value;
}

function smcf_validate_email($email) {
	$at = strrpos($email, "@");
	if ($at && ($at < 1 || ($at + 1) == strlen($email)))
		return false;
	if (preg_match("/(\.{2,})/", $email))
		return false;
	$local = substr($email, 0, $at);
	$domain = substr($email, $at + 1);

	$locLen = strlen($local);
	$domLen = strlen($domain);
	if ($locLen < 1 || $locLen > 64 || $domLen < 4 || $domLen > 255)
		return false;

	if (preg_match("/(^\.|\.$)/", $local) || preg_match("/(^\.|\.$)/", $domain))
		return false;

	if (!preg_match('/^"(.+)"$/', $local)) {
		if (!preg_match('/^[-a-zA-Z0-9!#$%*\/?|^{}`~&\'+=_\.]*$/', $local))
			return false;
	}
	if (!preg_match("/^[-a-zA-Z0-9\.]*$/", $domain) || !strpos($domain, "."))
		return false;	

	return true;
}

exit;

?>