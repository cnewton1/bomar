<footer>

    <div id="center">
        <div class="vlbdr"></div>

        <div class="contact">

            <div class="call">
                <a href="https://www.facebook.com/westcarb" target="_blank"><img src="images/facebook.png" style="width: 32px;"></a>
            </div>
            <div class="call">
                <div class="hdr">Call Us</div>
                <div class="no" id="salsePhoneNumFt">1-866-507-1576</div>
            </div>
            <div class="email">
                <div class="hdr">Email Us</div>
                <p><a href="/contactUs.php">Click to send us an email</a></p>
            </div>
        </div>

        <div class="vrbdr"></div>

        <nav>
            <ul>
                <li>Customer Service</li>
                <li><a href="help.php">Help</a></li>
                <li><a href="contactUs.php">Contact Us</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="terms_condition.php">Terms &amp; Conditions</a></li>
                <li><a href="shipping_returns.php">Shipping &amp; Returns</a></li>
            </ul>
            <ul>
                <li>Account Information</li>
                <li><a href="order_status.php">Order Status</a></li>
                <li><a href="shopping_cart.php">Shopping Cart</a></li>
            </ul>
        </nav>

    </div>
    <!-- end div center -->
    <div class="hbdr"></div>

    <p id="copyright">Copyright &#169; 2002-2021 Westcarb Enterprises Company. All Rights Reserved.
        <br />
        <strong>Shop With Confidence - 30 Day Satisfaction Guarantee</strong>
        <br />
    </p>

    <div id="node">60</div>

</footer>
</div>

<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Bootstrap Dropdown Hover JS -->
<script src="bootstrap/js/bootstrap-dropdownhover.min.js"></script>
<script type="text/javascript" src="js/slick/slick.min.js"></script>

<script>
    $(document).ready(function() {
        $('.slider-main').slick({
            infinite: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 2000,
        });
    })
</script>