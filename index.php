<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
?>


<div id="home_body">
    <div class="container">
        <div class="slider-main">
            <?php
            $imagedata = array();
            $imagequery = mysql_query("SELECT * FROM `mainimage` LIMIT 0,4");
            if (mysql_num_rows($imagequery)) {

                while ($row = mysql_fetch_array($imagequery)) {
                    echo '<div>';
                    $url = ($row['url'] != '') ? $row['url'] : '';
                    if ($url != '') {
                        echo '<a href="' . $url . '" target="_blank"><img style="width:100%" src="mainimage/' . $row['imagename'] . '"></a>';
                    } else {
                        echo '<img style="width:100%" src="mainimage/' . $row['imagename'] . '">';
                    }
                    echo '</div>';
                }
            }
            ?>
        </div>

        <div class="topsell">
            <div style="text-align: right;font-size: 25px;color: blue;">Contact Us: 1-866-507-1576</div>
            <ul class="prod">

                <?php
                $product_query = mysql_query("SELECT * FROM `product2` WHERE `position` != '' AND `position` != '0' ORDER BY `position` DESC LIMIT 0,18");
                if (mysql_num_rows($product_query)) {
                    while ($row = mysql_fetch_array($product_query)) {
                        $product_id = $row['id'];
                        $header = $row['description'];


                        $NameofPicture = $row['image_hyperlink'];

                        if (!isURL($NameofPicture) && ($NameofPicture != '')) {
                            $POSX = strpos($NameofPicture, ".jpg");
                            if ($POSX == 0) {
                                $NameofPicture .= ".jpg";
                            }
                            $row['image_hyperlink'] = 'ProductImage/' . $NameofPicture;
                        }


                        if (!isURL($row['image_hyperlink'])) {
                            $thumbnail_url = 'ProductImage/' . basename($row['image_hyperlink']);
                        } else {
                            $thumbnail_url = $row['image_hyperlink'];
                        }
                        $image_get_url = getimagesize($thumbnail_url);

                        if (!is_array($image_get_url)) {
                            $thumbnail_url = 'images/ImageNotAvailable.jpg';
                        }

                        //$newSize = myResize($thumbnail_url,125,125);
                        $price = $row['CUSTOMER'];
                        $detail_link = 'product_details.php?id=' . $product_id;
                        $product_name = $row['description'];
                        $detail_link = 'product_details.php?id=' . $product_id;
                        $thumbnail_url = str_replace("http:", "https:", $thumbnail_url);
                        // print "<br>" . $thumbnail_url;
                        echo '<li>
				  <div class="item">
                  <a href="' . $detail_link . '">' . $row['prodname'] . '</a><br><br>    
                   <p class="itemno"><a href="' . $detail_link . '" >' . $row['mfgpart'] . '</a></p> 
									   
					<div class="prod_img" style="display:flex">
						<a href="' . $detail_link . '" >
						<img src="' . $thumbnail_url . '"  style="max-width:100%; max-height:122px; align-self: center" border="0">
					  </a>
					</div>
					
				  </div>
				  
				  <div class="info">
					  <p class="title"><a index="0" class="addClickInfo" title="' . htmlentities($product_name) . '" href="' . $detail_link . '">' . $product_name . '</a></p>
					    
					  <p class="itemno"><a href="' . $detail_link . '" >' . $row['mfgpart'] . '</a></p> 
				  </div>
	  
				  <div class="pricing">
					<div class="prices">';
                        if ($row['CUSTOMER'] != '') {
                            //	echo '<p class="price">$'.number_format(str_replace('$','',$row['CUSTOMER']),2).'</p>';
                            echo '<p class="price">$' . display_price($price) . '</p>';
                        }
                        echo '
						
						<p class="mailin_rebate">&nbsp;</p>
					  <p class="soldpkg">&nbsp;</p>             
					</div>
			
				  </div>
				  </li>';
                    }
                }
                ?>
            </ul>
        </div>


        <?php //display_footer_shop_by_category(); 
        ?>

        <div class="container hidden-xs">
            <div class="industrial-features">
                <a href="search.php?subcategory=+CLAMPING+PRODUCTS&category=+FASTENERS+CLAMPS+%26+STRAPS">
                    <div class="industrial-features__feature-3col">
                        <div class="sliding-text">
                            <h3 class="industrial-features__title">Industrial</h3>
                        </div>
                        <div class="image-wrapper">
                            <img src="images/INDUSTRIAL.jpg" alt="Industrial">
                        </div>

                    </div>
                </a>
                <a href="search.php?subcategory=+MANUAL+WELDING&category=+WELDING+SUPPLIES">
                    <div class="industrial-features__feature-3col">
                        <div class="sliding-text">
                            <h3 class="industrial-features__title">Welding</h3>
                        </div>
                        <div class="image-wrapper">
                            <img src="images/WELDING.jpg" alt="Welding">
                        </div>

                    </div>
                </a>
                <a href="search.php?subcategory=+FIRE+GAS+%26+WATER+PROTECTION&category=+SAFETY+%26+SECURITY">
                    <div class="industrial-features__feature-3col">
                        <div class="sliding-text">
                            <h3 class="industrial-features__title">Safety</h3>
                        </div>
                        <div class="image-wrapper">
                            <img src="images/SAFETY.jpg" alt="Safety">
                        </div>

                    </div>
                </a>
                <a href="search.php?subcategory=+EXTENSION+%26+POWER+CORDS&category=+ELECTRICAL+%26+LIGHTING">
                    <div class="industrial-features__feature-3col">
                        <div class="sliding-text">
                            <h3 class="industrial-features__title">Energy</h3>
                        </div>
                        <div class="image-wrapper">
                            <img src="images/ENERGY.jpg" alt="Energy">
                        </div>

                    </div>
                </a>
                <a href="search.php?subcategory=CLEANING+PRODUCTS&category=JANITORIAL+EQUIPMENT">
                    <div class="industrial-features__feature-3col">
                        <div class="sliding-text">
                            <h3 class="industrial-features__title">Janitorial</h3>
                        </div>
                        <div class="image-wrapper">
                            <img src="images/JANITORIAL.jpg" alt="Jansan">
                        </div>

                    </div>
                </a>
                <a href="search.php?subcategory=+LADDERS+PLATFORMS+%26+SCAFFOLDING&category=+MATERIAL+HANDLING">
                    <div class="industrial-features__feature-3col">
                        <div class="sliding-text">
                            <h3 class="industrial-features__title">Construction</h3>
                        </div>
                        <div class="image-wrapper">
                            <img src="images/CONSTRUCTIONS.jpg" alt="Construction">
                        </div>

                    </div>
                </a>
            </div>
        </div>
    </div>
</div>






<?php include 'footer.php'; ?>



</body>

</html>