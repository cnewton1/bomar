<?php

//-------------------------------------------
// Format a date we can use in a database
//-------------------------------------------
function getTodayDate()
{
 $my_t=getdate(date("U"));
 $tMonth="01";
 
 if ($my_t[month] == "January")
      $tMonth="01";
      
 if ($my_t[month] == "Febuary")
      $tMonth="02";
      
 if ($my_t[month] == "March")
      $tMonth="03";
      
 if ($my_t[month] == "April")
      $tMonth="04";
      
 if ($my_t[month] == "May")
      $tMonth="05";
      
 if ($my_t[month] == "June")
      $tMonth="06";
      
 if ($my_t[month] == "July")
      $tMonth="07";
      
 if ($my_t[month] == "August")
      $tMonth="08";
      
 if ($my_t[month] == "September")
      $tMonth="09";
      
 if ($my_t[month] == "October")
      $tMonth="10";   
      
 if ($my_t[month] == "November")
      $tMonth="11";    
      
 if ($my_t[month] == "December")
      $tMonth="12";                                                                   
      
 $dateprint="$my_t[year]-$tMonth-$my_t[mday]";
 
 return $dateprint;
}


//-------------------------------------------
// Format a date we can use in a database
//-------------------------------------------
function getTodayDatePlus()
{
 $my_t=getdate(date("U"));
 $dateprint="$my_t[year]-$my_t[month]-$my_t[mday] $my_t[hours]:$my_t[minutes]:$my_t[seconds]";
 return $dateprint;
}


?>