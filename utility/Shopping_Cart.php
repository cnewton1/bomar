<?php

function addToCart($userid, $product_id, $item_number, $header, $price, $options, $qty, $product_details) {
//	$price = number_format(str_replace('$','',$price),2);
    $select_existing_product_query = mysql_query("SELECT * FROM `cart` WHERE `userid`= '$userid' AND `product_id`= '$product_id'");
    if (mysql_num_rows($select_existing_product_query) > 0) {
        $calculate_total_qty = mysql_fetch_array(mysql_query("SELECT sum( `qty` ) AS `totalQty` FROM `cart` WHERE `userid`= '$userid' AND `product_id`= '$product_id'"));
        $totalQty = $calculate_total_qty['totalQty'] + $qty;
        $update_total_qty = mysql_query("UPDATE `cart` SET `qty`= '$totalQty' WHERE `userid`= '$userid' AND `product_id`= '$product_id'");
    } else {
        $free_shipping = (strtolower($product_details['FOB']) == 'yes') ? '1' : '0';
        $insert_cart_query = mysql_query("INSERT INTO `cart` (`userid`, `product_id`, `item_number`, `description`, `price`, `options`, `qty`, `free_shipping`, `length`, `width`, `height`, `weight`) 
            VALUES ('$userid', '$product_id', '$item_number', '$header', '$price', '$options', '$qty', '$free_shipping', '$product_details[PROD_LENGTH]', '$product_details[PROD_WIDTH]', '$product_details[PROD_HEIGHT]', '$product_details[PROD_WEIGHT]')");
        if ($insert_cart_query) {
            return mysql_insert_id();
        } else {
            return false;
        }
    }
}

function PaypalAddToCart_All_Items_Link($business, $ORDERNUMBER, $add = '1', $currency_code = 'USD') {
    $host = $_SERVER['HTTP_HOST'];
    $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $site_url = "http://$host$path";

    $resultID = mysql_query("SELECT * FROM `orderhistory` WHERE `order_number` = '$ORDERNUMBER'");
    $row = mysql_fetch_array($resultID);
    $first_name = $row['first_name'];
    $last_name = $row['last_name'];
    $email = $row['email'];
    $phone = $row['phone'];
    $notes = $row['notes'];
    $orderstatus = $row['orderstatus'];
    $order_number = $row['order_number'];
    $cause_id = $row['cause_id'];
    $cust_notes = $row['cust_notes'];
    $date = $row['date'];
    $coupon_code = $row['coupon_code'];
    $discount = $row['discount'];
    $shipping = $row['shipping'];
    $sales_tax = $row['sales_tax'];

    $items_array = array();
    $cart_query = mysql_query("SELECT `item_order`.*, `product2`.`image_hyperlink` AS `image_name` 
	FROM `item_order` 
	LEFT JOIN `product2` ON (`item_order`.`product_id` = `product2`.`id`) 
	WHERE `item_order`.`order_id` = '$ORDERNUMBER'");
    if (mysql_num_rows($cart_query)) {
        $count = 0;
        while ($row = mysql_fetch_array($cart_query)) {
            $count++;
            $cartid = $row['id'];
            $item_number = $row['item_number'];
            $description = $row['description'];
            $subheader = $row['subheader'];
            $price = $row['unit_price'];
            $options = $row['options'];
            $size = $row['size'];
            $qty = $row['qty'];
            $image_name = basename($row['image_name']);
            $image_full_path = '';
            if ($image_name != '') {
                $image_path = 'ProductImage/' . $image_name;
                $image_full_path = $site_url . '/' . $image_path;
            }

            $image_get_url = getimagesize($thumbnail_url);

            if (!is_array($image_get_url)) {
                $thumbnail_url = $site_url . '/images/ImageNotAvailable.jpg';
            }

            if ($image_full_path != '') {
                $return[] = '<img src="' . $image_full_path . '" width="100" border="0"><br>';
            }

            $item_name = $description . ' - ' . $subheader . ' (Size: ' . $size . ')';
            $item_number = $item_number;
            $amount = trim(str_replace(array('$', ','), array('', ''), $price));
            $quantity = $qty;

            $total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
            $qty_item_row[$cartid] = $qty;
            $items_array[] = '
			&item_name_' . $count . '=' . urlencode(htmlspecialchars($item_name)) . '&item_number_' . $count . '=' . $item_number . '&amount_' . $count . '=' . $amount . '&quantity_' . $count . '=' . $quantity;
        }
        if ($coupon_code != '') {
            $discount_percent = $discount;

            $discount_amount = number_format(((array_sum($total_item_row) * $discount_percent) / 100), 2, '.', '');
        }
    }

    $Paypal_Cart_Link = 'https://www.paypal.com/cgi-bin/webscr?upload=1
&cmd=_cart
&business=' . $business . '
' . implode("", $items_array) . '
&discount_amount_cart=' . $discount_amount . '
&handling_cart=' . $shipping . '
&tax_cart=' . $sales_tax . '
&no_shipping=0
&no_note=1
&currency_code=' . $currency_code . '
&lc=US';

    $link_str = '<a href="' . $Paypal_Cart_Link . '" target="_blank" title="Click here to Make Payment"><img src="http://bomar.chrondev.com/images/InvoicePaymentButton.png" border="0"></a>';

    return $link_str;
}

function PaypalAddToCart_All_Items($business, $ORDERNUMBER, $add = '1', $currency_code = 'USD') {
    $host = $_SERVER['HTTP_HOST'];
    $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $site_url = "http://$host$path";

    $resultID = mysql_query("SELECT * FROM `orderhistory` WHERE `order_number` = '$ORDERNUMBER'");
    $row = mysql_fetch_array($resultID);
    $first_name = $row['first_name'];
    $last_name = $row['last_name'];
    $email = $row['email'];
    $phone = $row['phone'];
    $notes = $row['notes'];
    $orderstatus = $row['orderstatus'];
    $order_number = $row['order_number'];
    $cause_id = $row['cause_id'];
    $cust_notes = $row['cust_notes'];
    $date = $row['date'];
    $coupon_code = $row['coupon_code'];
    $discount = $row['discount'];
    $shipping = $row['shipping'];
    $sales_tax = $row['sales_tax'];

    $items_array = array();
    $cart_query = mysql_query("SELECT `item_order`.*, `product2`.`image_hyperlink` AS `image_name` 
	FROM `item_order` 
	LEFT JOIN `product2` ON (`item_order`.`product_id` = `product2`.`id`) 
	WHERE `item_order`.`order_id` = '$ORDERNUMBER'");
    if (mysql_num_rows($cart_query)) {
        $count = 0;
        while ($row = mysql_fetch_array($cart_query)) {
            $count++;
            $cartid = $row['id'];
            $item_number = $row['item_number'];
            $description = $row['description'];
            $subheader = $row['subheader'];
            $price = $row['unit_price'];
            $options = $row['options'];
            $size = $row['size'];
            $qty = $row['qty'];
            $image_name = basename($row['image_name']);
            $image_full_path = '';
            if ($image_name != '') {
                $image_path = 'ProductImage/' . $image_name;
                $image_full_path = $site_url . '/' . $image_path;
            }

            $image_get_url = getimagesize($thumbnail_url);

            if (!is_array($image_get_url)) {
                $thumbnail_url = $site_url . '/images/ImageNotAvailable.jpg';
            }
            if ($image_full_path != '') {
                $return[] = '<img src="' . $image_full_path . '" width="100" border="0"><br>';
            }

            $item_name = $description . ' - ' . $subheader . ' (Size: ' . $size . ')';
            $item_number = $item_number;
            $amount = trim(str_replace(array('$', ','), array('', ''), $price));
            $quantity = $qty;

            $total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
            $qty_item_row[$cartid] = $qty;
            $items_array[] = '
			<input type="hidden" name="item_name_' . $count . '" value="' . htmlspecialchars($item_name) . '">
			<input type="hidden" name="item_number_' . $count . '" value="' . $item_number . '">
			<input type="hidden" name="amount_' . $count . '" value="' . $amount . '">	
			<input type="hidden" name="quantity_' . $count . '" value="' . $quantity . '">
			';
        }
        if ($coupon_code != '') {
            $discount_percent = $discount;

            $discount_amount = number_format(((array_sum($total_item_row) * $discount_percent) / 100), 2, '.', '');
        }
    }

    $paypal_form = '<form method="post" id="paypal_cart" action="https://www.paypal.com/cgi-bin/webscr" target="paypal">
	<input type="hidden" name="cmd" value="_cart">
	<input type="hidden" name="upload" value="' . $add . '">
	<input type="hidden" name="business" value="' . $business . '">
	<input type="hidden" name="currency_code" value="' . $currency_code . '">';
    $paypal_form .= implode("\n", $items_array);

    $paypal_form .= '
	<input type="hidden" name="discount_amount_cart" value="' . $discount_amount . '">
	<input type="hidden" name="handling_cart" value="' . $shipping . '">
	<input type="hidden" name="tax_cart" value="' . $sales_tax . '">
	';


    $paypal_form .= '
	<input type="hidden" name="return" value="' . $site_url . '">
	<input type="image" src="btn_buynow.gif" border="0" name="submit" alt="Buy Now">
	</form>';

    return $paypal_form;
}

function getProductImage($product_id) {
    $host = $_SERVER['HTTP_HOST'];
    $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $site_url = "http://$host$path";

    $product_query = mysql_query("SELECT `image_hyperlink` FROM `product2` WHERE `id` = '$product_id'");
    if (mysql_num_rows($product_query)) {
        $row = mysql_fetch_array($product_query);
        if (!isURL($row['image_hyperlink'])) {
            $image_path = 'ProductImage/' . basename($row['image_hyperlink']);
            $thumbnail_url = $site_url . '/' . $image_path;
        } else {
            $thumbnail_url = $row['image_hyperlink'];
        }

        $image_get_url = getimagesize($thumbnail_url);
        if (!is_array($image_get_url)) {
            $thumbnail_url = $site_url . '/images/ImageNotAvailable.jpg';
        }
    } else {
        $thumbnail_url = $site_url . '/images/ImageNotAvailable.jpg';
    }
    return $thumbnail_url;
}

function parseProductImageURL($image) {
    $host = $_SERVER['HTTP_HOST'];
    $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $site_url = "http://$host$path";
    if ($image != '') {
        if (!isURL($image)) {
            $image_path = 'ProductImage/' . basename($image);
            $thumbnail_url = $site_url . '/' . $image_path;
        } else {
            $thumbnail_url = $image;
        }

        $image_get_url = getimagesize($thumbnail_url);
        if (!is_array($image_get_url)) {
            $thumbnail_url = $site_url . '/images/ImageNotAvailable.jpg';
        }
    } else {
        $thumbnail_url = $site_url . '/images/ImageNotAvailable.jpg';
    }
    return $thumbnail_url;
}

function Showcart($userid, $style = '') {
    $return = array();
    $cart_query = mysql_query("SELECT * FROM `cart` WHERE `userid` = '$userid'");
    if (mysql_num_rows($cart_query)) {
        if ($style == '') {
            $return[] = '<div id="contents">
						<form target="_self" id="cart" method="post" action="addToCart.php?action=update">
							<font size="2" face="Arial">
								<div>
									<font size="2">
										<button type="submit">
											<img width="105" height="22" border="0" src="images/BUpdatecart.gif">
										</button>&nbsp;
										<a target="_self" href="Checkout1.php"><img width="91" height="22" border="0" src="images/BCheckout.gif"></a>
										<a target="_self" href="index.php"><img width="163" height="22" border="0" src="images/BContinueShopping.gif"></a>
									</font>
								</div>
								<font size="2">
									<font size="2"></font>
								</font>';
        }


        $return[] = '<table width="900" cellspacing="0" cellpadding="2" bordercolor="#FF0000" border="1" id="AutoNumber1" style="border-collapse: collapse"  fgcolor="#FFFFFF">
									<tbody>
										<tr>';
        if ($style == '') {
            $return[] = '<td width="100" bgcolor="#000000">
															  <b>
																  <font size="2" color="#FFFFFF">Action</font>
															  </b>
														  </td>	';
        }

        $return[] = '<td bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Description</font>
												</b>
											</td>
											
											<td width="250" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Item No.</font>
												</b>
											</td>
											
											<td width="100" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Price</font>
												</b>
											</td>
											
											<td width="50"bgcolor="#000000">
												<b>
													<font size="1" color="#FFFFFF">Qty</font>
												</b>
											</td>
											
											<td width="100" bgcolor="#000000">
												<b>
													<font size="1" color="#FFFFFF">EXT PRICE</font>
												</b>
											</td>
											
										</tr>';

        while ($row = mysql_fetch_array($cart_query)) {
            $cartid = $row['id'];
            $product_id = $row['product_id'];
            $item_number = $row['item_number'];
            $description = $row['description'];
            $price = $row['price'];
            $options = $row['options'];
            $qty = $row['qty'];
            $allId = $item_number . ',';

            $image_thumbnail = getProductImage($product_id);

            $return[] = '<tr>';
            if ($style == '') {
                $return[] = '<td align="center">
				<img src="' . $image_thumbnail . '" style="max-height:100px; max-widht:100px;"><br>
				<a class="r" href="addToCart.php?action=delete&cartid=' . $cartid . '">
				<font size="1" target="_self">Remove Item</font></a>
				<input type="hidden" value="' . $cartid . '" name="cartid[]">
				</td>';
            }
            $return[] = ' <td>
                            <font size="2" color="#000000">
                                <b>' . $description . '
                                </b>
                            </font>
                        </td>
                        <td>
                            <font size="2" color="#000000">
                                ' . $item_number . '
                            </font>
                        </td>
                       
                      
						
						<td align="right">
                            <font size="2" color="#000000">$' . str_replace('$', '', $price) . '</font>
                        </td>
						
                        <td align="center">
                            <font size="2" color="#000000">';
            if ($style == '') {
                $return[] = '<input type="text" maxlength="3" size="3" value="' . $qty . '" name="qty[' . $cartid . ']">';
            } else {
                $return[] = $qty;
            }
            $total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
            $qty_item_row[$cartid] = $qty;

            $return[] = ' </font>
                        </td>
						
						 <td align="right">
                            <font size="2" color="#000000">$' . number_format($total_item_row[$cartid], 2) . '</font>
                        </td>
						
                    </tr>';
        }
        $allId = substr($allId, 0, -1);
        $colspan = ($style == '') ? '4' : '3';
        $subtotal = number_format(array_sum($total_item_row), 2);


        $TOTAL_ITEMS_SUBTOTAL = array_sum($qty_item_row);
        $TOTAL_COST_SUBTOTAL = number_format(array_sum($total_item_row), 2);

        define('TOTAL_ITEMS_SUBTOTAL', $TOTAL_ITEMS_SUBTOTAL);
        define('TOTAL_COST_SUBTOTAL', $TOTAL_COST_SUBTOTAL);
        define('GRAND_TOTAL', $TOTAL_COST_SUBTOTAL);



        $return[] = '<tr>
						<td  colspan="' . $colspan . '" align="right"><strong>Subtotal</strong></td>
						<td  align="center">' . array_sum($qty_item_row) . '</td>
						<td  align="right">$' . number_format(array_sum($total_item_row), 2) . '</td>
					</tr>';

        $return[] = '</tbody>
				  </table>';
        if ($style == '') {
            $return[] = '<div>
					  <font size="2">
						  <font size="2">
							  <font size="2">
								  <button type="submit">
									  <img width="105" height="22" border="0" src="images/BUpdatecart.gif">
								  </button>&nbsp;
								  <a target="_self" href="Checkout1.php"><img width="91" height="22" border="0" src="images/BCheckout.gif"></a>
								  <a target="_self" href="index.php"><img width="163" height="22" border="0" src="images/BContinueShopping.gif"></a>
							  </font>
						  </font>
					  </font>
				  </div>
				  <font size="2">
					  <font size="2">
						  <font size="2">
							  <input type="hidden" value="' . $allId . '" name="allId">
						  </font>
					  </font>
				  </font>
			  </font>
		  </form>
	  </div>';
        }
    } else {
        $return[] = 'Shopping Cart is empty.';
    }
    return implode('', $return);
}

function showOrderDetail_FrontEnd($OrderId, $notes = '', $cause_id = '', $status = '') {
    $host = $_SERVER['HTTP_HOST'];
    $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $site_url = "http://$host$path";
    $site_url = str_replace("/manage", "", $site_url);

    $return = array();
    $select_order_details = mysql_fetch_array(mysql_query("SELECT * FROM `orderhistory` WHERE `order_number`= '$OrderId'"));
    $email = $select_order_details['email'];
    $shipping_options = $select_order_details['shipping_options'];
    $two_day_shipping = $select_order_details['two_day_shipping'];
    $select_taxrate = mysql_fetch_array(mysql_query('SELECT * FROM `tax_rate` WHERE `name`="salestax"'));
    $sales_tax_rate = $select_taxrate['rate'];

    if ($select_order_details['shipping'] > 0) {
        $shipping = $select_order_details['shipping'];
    } else {
        $shipping = '0.00';
    }
    $sales_tax = $select_order_details['sales_tax'];
    $coupon_code = $select_order_details['coupon_code'];
    $discount = $select_order_details['discount'];

    $select_shipping_option = mysql_query("SELECT `detail` FROM `shipping_options` WHERE `id`='$shipping_options' ");
    if (mysql_num_rows($select_shipping_option)) {
        $shipping_options_result = mysql_fetch_array($select_shipping_option);
        $shipping_details = $shipping_options_result['detail'];
    } else {
        $shipping_details = '<em>Not Selected</em>';
    }

    $cart_query = mysql_query("SELECT `item_order`.*, `product2`.`image_hyperlink` AS `image_name` 
	FROM `item_order` 
	LEFT JOIN `product2` ON (`item_order`.`product_id` = `product2`.`id`) 
	WHERE `item_order`.`order_id` = '$OrderId'");
    if (mysql_num_rows($cart_query)) {
        $return[] = '<table width="900" cellspacing="0" cellpadding="2" bordercolor="#000000" border="1" id="cart_display" style="border-collapse: collapse"  bgcolor="#FFFFFF">
									<tbody>
										<tr>';
        $return[] = '
											<td width="100" bgcolor="#000000">
															  <b>
																  <font size="2" color="#FFFFFF">&nbsp;</font>
																  <input type="hidden" name="OrderId" value="' . $OrderId . '">
															  </b>
											</td>	
											<td width="200" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Description</font>
												</b>
											</td>
											
											<td width="100" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Item No.</font>
												</b>
											</td>
											<td width="70" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Price</font>
												</b>
											</td>
											
											<td width="50"bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Qty</font>
												</b>
											</td>
											
											<td width="80" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">EXT PRICE</font>
												</b>
											</td>
											
										</tr>';

        while ($row = mysql_fetch_array($cart_query)) {
            $cartid = $row['id'];
            $item_number = $row['item_number'];
            $description = $row['description'];
            $subheader = $row['subheader'];
            $price = $row['unit_price'];
            $options = $row['options'];
            $size = $row['size'];
            $qty = $row['qty'];
            //$allId = $item_number . ',';
            $image_name = basename($row['image_name']);

            $image_full_path = '';
            if (!isURL($row['image_name'])) {
                $image_full_path = $site_url . '/ProductImage/' . basename($row['image_name']);
            } else {
                $image_full_path = $row['image_name'];
            }

            $image_get_url = getimagesize($image_full_path);

            if (!is_array($image_get_url)) {
                $image_full_path = $site_url . '/images/ImageNotAvailable.jpg';
            }




            $return[] = '<tr>';
            $return[] = ' <td>';
            if ($image_full_path != '') {
                $return[] = '<img src="' . $image_full_path . '" width="100" border="0"><br>';
            }
            $return[] = '<input type="hidden" value="' . $cartid . '" name="cartid[]">
                        </td>';


            $return[] = ' <td style="vertical-align: top;">
                            <font size="2" color="#000000">
                                <b>' . $description . '
                                </b>
                            </font>
                        </td>
                        <td style="vertical-align: top;">
                            <font size="2" color="#000000">
                                ' . $item_number . '
                            </font>
                        </td>
						
						<td align="right" style="vertical-align: top;">
                            <font size="2" color="#000000">$' . str_replace('$', '', $price) . '</font>
                        </td>
                        <td  style="vertical-align: top;">
                            <font size="2" color="#000000">';

            if ($status == 'clean') {
                $return[] = $qty;
            } else {
                $return[] = '<input type="text" name="qty[' . $cartid . ']" value="' . $qty . '" size="4">';
            }


            $total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
            $qty_item_row[$cartid] = $qty;

            $return[] = ' </font>
                        </td>
						
						 <td align="right"  style="vertical-align: top;">
                            <font size="2" color="#000000">$' . number_format($total_item_row[$cartid], 2) . '</font>
                        </td>
						
                    </tr>';
        }
        //$allId = substr($allId,0,-1);
        $colspan = '4';
        $subtotal = number_format(array_sum($total_item_row), 2);

        $return[] = '<tr>
						<td  colspan="' . $colspan . '" align="right"><strong>Subtotal</strong></td>
						<td  align="center">' . array_sum($qty_item_row) . '</td>
						<td  align="right">$' . number_format(array_sum($total_item_row), 2) . '</td>
					</tr>';

        if ($sales_tax > 0) {
            $return[] = '<tr>
						<td  colspan="' . $colspan . '" align="right"><strong>Sales Tax</strong></td>
						<td  align="center"></td>
						<td  align="right">$' . number_format($sales_tax, 2) . '</td>
					</tr>';
        }

        if ($sales_tax == '0.00') {
            $sales_tax = number_format((($sales_tax_rate / 100) * $subtotal), 2);
            $applySalesTaxCheckedStr = '';
            $salesTaxForGrandTotal = '0.00';
        } else {
            $applySalesTaxCheckedStr = 'checked';
            $salesTaxForGrandTotal = number_format($sales_tax, 2);
        }



        /* $return[] ='<tr>
          <td  colspan="'.($colspan-1).'"  align="right">';

          if($status == 'clean')
          {
          $return[] = '';
          }
          else
          {
          $return[] = '<strong>Apply Sales Tax:</strong> <input type="checkbox" name="applySalesTax" id="applySalesTax" onClick="javascript:showTax();" '. $applySalesTaxCheckedStr .'>';
          }
          $return[] = '	</td>
          <td  align="right"><strong>Sales Tax</strong></td>
          <td  align="center"><input type="hidden"  id="sales_tax" name="sales_tax" value="'.$sales_tax.'" ></td>
          <td  align="right"><span id="showSalesTax">$'.$salesTaxForGrandTotal.'</span></td>
          </tr>';
         */
        $two_day_shipping_checked = ($two_day_shipping > 0) ? 'checked' : '';
        $two_day_shipping_disabled = ($status == 'clean') ? 'disabled' : '';
        $return[] = '<tr>
						<td colspan="' . ($colspan) . '"   align="right">
						
						
						<div style="float:right"><strong>Standard Shipping:</strong></div>
						
						</td>
						<td  align="center"></td>
						<td  align="right">
							<input type="hidden"  id="total_price" name="total_price" value="' . number_format(array_sum($total_item_row), 2) . '" >';
        if ($status == 'clean') {
            $return[] = '$' . $shipping;
        } else {
            $return[] = '$' . number_format($shipping, 2);
            //$return[] = '<input type="text"  id="shipping" name="shipping" value="'.$shipping.'" size="4" onBlur="javascript:display_total();">';
            $return[] = '<input type="hidden"  id="shipping" name="shipping" value="' . $shipping . '">';
        }

        $return[] = '	</td>
								</tr>';

        if ($coupon_code != '') {
            $discount_percent = $discount;

            $discount_amount = number_format(((array_sum($total_item_row) * $discount_percent) / 100), 2, '.', '');
        }


        if ($shipping != '') {
            $grand_total = number_format((($shipping + array_sum($total_item_row) + $salesTaxForGrandTotal) - $discount_amount), 2);
        } else {
            $grand_total = number_format(((array_sum($total_item_row) + $salesTaxForGrandTotal) - $discount_amount), 2);
        }



        if ($discount_amount > 0) {
            $return[] = '<tr>
						<td  colspan="' . $colspan . '" align="right"><strong>Discount:</strong></td>
						<td  align="center">&nbsp;</td>
						<td  align="right">$<span id="discount_amount" >' . number_format($discount_amount, 2) . '</span></td>
					</tr>';
        }

        $return[] = '<tr>
						<td  colspan="' . $colspan . '" align="right"><strong>Grand Total</strong></td>
						<td  align="center">
						<input type="hidden" id="discount_percent" name="discount_percent" value="' . $discount_percent . '">
						<input type="hidden" name="grand_total_temp" value="' . $grand_total . '">
						</td>
						<td  align="right">$<span id="grand_total" >' . $grand_total . '</span></td>
					</tr>';


        $return[] = '</tbody>
				  </table>';
        if ($status != 'clean') {
            $return[] = '<br><div align="center"  style="width:900;">
						<input type="submit" name="emailCopy"  value="Email Copy">
					</div>';

            $return[] = '<br><div align="right"  style="width:900;">
						<input type="submit" name="update"  value="Update Cart Quantity">
					</div>';
        }


        /* $return[] = '<br><br>
          <div align="left"  style="width:900;">
          <strong>NOTES:</strong><br>';

          if($status == 'clean')
          {
          $return[] = nl2br($notes);
          }
          else
          {
          $return[] ='<textarea name="notes" rows="5" cols="100">'.$notes.'</textarea>
          </div>';
          } */

        if ($status != 'clean') {
            $return[] = '<br><br>
					  <div align="right"  style="width:900;">
						  <input type="submit" name="submit" value="Save Changes">
					  </div>';
        }
    } else {
        $return[] = 'Shopping Cart is empty.';
    }
    return implode('', $return);
}

function updateCart($userid, $cartid, $options, $qty) {
    $update_cart_query = mysql_query("UPDATE `cart` SET `qty` = '$qty' WHERE `userid` = '$userid' AND `id` = '$cartid'");
    if ($update_cart_query) {
        return true;
    } else {
        return false;
    }
}

function deleteCartItem($userid, $cartid, $item_number='') {
    $delete_cart_item = mysql_query("DELETE FROM `cart` WHERE `userid` = '$userid' AND `id` = '$cartid'");
    if ($delete_cart_item) {
        return true;
    } else {
        return false;
    }
}

function deleteCart($userid) {
    $delete_cart = mysql_query("DELETE FROM `cart` WHERE `userid` = '$userid'");
    if ($delete_cart) {
        return true;
    } else {
        return false;
    }
}

function insertProductOrder($userid, $OrderNumber, $dataobj) {
    $return = array();
    $cart_query = mysql_query("SELECT * FROM `cart` WHERE `userid` = '$userid'");
    if (mysql_num_rows($cart_query)) {
        $allId = '';
        while ($row = mysql_fetch_array($cart_query)) {
            $cartid = $row['id'];
            $product_id = $row['product_id'];
            $item_number = $row['item_number'];
            $description = $row['description'];
            $price = $row['price'];
            $options = $row['options'];
            $qty = $row['qty'];
            $length = $row['length'];
            $width = $row['width'];
            $height = $row['height'];
            $weight = $row['weight'];
            $distance_unit = $row['distance_unit'];
            $mass_unit = $row['mass_unit'];
            $free_shipping = $row['free_shipping'];
            $rate_object_id = (isset($dataobj['shipping_item'][$cartid])) ? $dataobj['shipping_item'][$cartid] : '';
            $allId .= $item_number . ',';
//			$total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
            $total_item_row[$cartid] = trim(str_replace(array('$', ','), array('', ''), $price));
            $qty_item_row[$cartid] = $qty;
            $Totalcost = ($total_item_row[$cartid] * $qty_item_row[$cartid]);

            //$insert_value_string .= "('$OrderNumber', '$item_number'),";

            $insert_item_order_value_string .= " ('$OrderNumber', '$product_id', '$item_number', '$description', '$options', '$qty', '$total_item_row[$cartid]', '$Totalcost', '$length', '$width', '$height', '$weight', '$distance_unit', '$mass_unit', '$rate_object_id', '$free_shipping'),";
        }
        $allId = substr($allId, 0, -1);
        $insert_item_order_value_string = substr($insert_item_order_value_string, 0, -1);


        $insert_item_order_query = mysql_query("INSERT INTO `item_order` (`order_id`, `product_id`, `item_number`, `description`, `options`, `qty`, `unit_price`, `cost`, `length`, `width`, `height`, `weight`,`distance_unit`,`mass_unit`, `rate_object_id`, `free_shipping`) VALUES $insert_item_order_value_string ");


        //$subtotal= number_format(array_sum($total_item_row), 2);
        //$insert_product_order_query = mysql_query("INSERT INTO `product_order` (`items`, `subtotal`, `shipping`, `tax`, `total`, `user_email`) VALUES  ('$allId',' $subtotal', 0.00, 0.00, '$subtotal', '$userid')");
    }
}

function shipping_address_info($id, $user_email='') {
    $return = array();
    $check_shipping_record = mysql_query("SELECT * FROM `order_shipping_address` WHERE `id` = '$id'");
    if (mysql_num_rows($check_shipping_record)) {
        $order_shipping_address_row = mysql_fetch_array($check_shipping_record);
        $return['status'] = true;
        $return['id'] = $order_shipping_address_row['id'];
        $return['user_email'] = $order_shipping_address_row['user_email'];
        $return['s_address1'] = $order_shipping_address_row['s_address1'];
        $return['s_address2'] = $order_shipping_address_row['s_address2'];
        $return['s_city'] = $order_shipping_address_row['s_city'];
        $return['s_state'] = $order_shipping_address_row['s_state'];
        $return['s_province'] = $order_shipping_address_row['s_province'];

        $return['s_zip'] = $order_shipping_address_row['s_zip'];

        $return['s_country'] = $order_shipping_address_row['s_country'];

        $return['s_phone'] = $order_shipping_address_row['s_phone'];
        $return['shipping_option_id'] = $order_shipping_address_row['shipping_option_id'];
        $return['is_residential'] = $order_shipping_address_row['is_residential'];
        $return['is_commercial'] = $order_shipping_address_row['is_commercial'];
    } else {
        $check_shipping_record = mysql_query("SELECT * FROM `member` WHERE `email` = '$user_email'");
        if (mysql_num_rows($check_shipping_record)) {
            $order_shipping_address_row = mysql_fetch_array($check_shipping_record);
            $return['status'] = true;
            $return['id'] = $order_shipping_address_row['id'];
            $return['user_email'] = $order_shipping_address_row['email'];
            $return['s_address1'] = $order_shipping_address_row['saddress1'];
            $return['s_address2'] = $order_shipping_address_row['saddress2'];
            $return['s_city'] = $order_shipping_address_row['scity'];
            $return['s_state'] = $order_shipping_address_row['sstate'];
            $return['s_province'] = $order_shipping_address_row['sprovince'];

            $return['s_zip'] = $order_shipping_address_row['szip'];

            $return['s_country'] = $order_shipping_address_row['scountry'];

            $return['s_phone'] = $order_shipping_address_row['sphone'];
            $return['shipping_option_id'] = $order_shipping_address_row['shipping_options_id'];
        } else {
            $return['status'] = false;
        }
    }

    return $return;
}

function lastest_order_shipping_address($user_email) {
    $return = array();

    $check_shipping_record = mysql_query("SELECT * FROM `order_shipping_address` 
	WHERE `user_email` = '$user_email' ORDER BY `updated` DESC LIMIT 0,1 ");
    if (mysql_num_rows($check_shipping_record)) {
        $order_shipping_address_row = mysql_fetch_array($check_shipping_record);
        $return['status'] = true;
        $return['id'] = $order_shipping_address_row['id'];
        $return['user_email'] = $order_shipping_address_row['user_email'];
        $return['s_address1'] = $order_shipping_address_row['s_address1'];
        $return['s_address2'] = $order_shipping_address_row['s_address2'];
        $return['s_city'] = $order_shipping_address_row['s_city'];
        $return['s_state'] = $order_shipping_address_row['s_state'];
        $return['s_province'] = $order_shipping_address_row['s_province'];
        $return['s_country'] = $order_shipping_address_row['s_country'];
        $return['s_zip'] = $order_shipping_address_row['s_zip'];
        $return['s_phone'] = $order_shipping_address_row['s_phone'];
        $return['shipping_option_id'] = $order_shipping_address_row['shipping_option_id'];
        $return['is_residential'] = $order_shipping_address_row['is_residential'];
        $return['is_commercial'] = $order_shipping_address_row['is_commercial'];
    } else {
        $return['status'] = false;
    }
    return $return;
}

function order_shipping_address($user_email, $s_address1, $s_address2, $s_city, $s_state, $s_province, $s_zip, $s_phone, $shipping_option_id, $is_residential, $is_commercial, $FCountry, $FSCountry) {
    $return = array();

    if ($is_residential != '' || $is_commercial != '') {
        $is_address_str = "AND `is_residential` = '$is_residential' AND `is_commercial` = '$is_commercial'";
    }

    $check_shipping_record = mysql_query("SELECT * FROM `order_shipping_address` 
	WHERE `user_email` = '$user_email' AND `s_address1` = '$s_address1' AND `s_address2` = '$s_address2' AND `s_city` = '$s_city' AND `s_state` = '$s_state' AND `s_province` = '$s_province' AND `s_zip` = '$s_zip' AND `s_phone` = '$s_phone' AND `s_country` = '$FSCountry' AND `shipping_option_id` = '$shipping_option_id' $is_address_str ORDER BY `updated` DESC LIMIT 0,1 ");
    if (mysql_num_rows($check_shipping_record)) {
        $order_shipping_address_row = mysql_fetch_array($check_shipping_record);
        $return['status'] = true;
        $return['id'] = $order_shipping_address_row['id'];
        $return['user_email'] = $user_email;
        $return['s_address1'] = $s_address1;
        $return['s_address2'] = $s_address2;
        $return['s_city'] = $s_city;
        $return['s_state'] = $s_state;
        $return['s_province'] = $s_province;
        $return['s_zip'] = $s_zip;
        $return['s_country'] = $s_country;
        $return['s_phone'] = $s_phone;
        $return['shipping_option_id'] = $shipping_option_id;
        $return['is_residential'] = $order_shipping_address_row['is_residential'];
        $return['is_commercial'] = $order_shipping_address_row['is_commercial'];
    } else {
        $created = date('Y-m-d H:i:s');
        $insert_order_shipping_address = mysql_query("INSERT INTO `order_shipping_address` 
		(`user_email`, `s_address1`, `s_address2`, `s_city`, `s_state`, `s_province`, `s_zip`, `s_phone`, `shipping_option_id`, `is_residential`, `is_commercial`, `created`, `s_country`) 
		VALUES 
		('$user_email', '$s_address1', '$s_address2', '$s_city', '$s_state', '$s_province', '$s_zip', '$s_phone', '$shipping_option_id', '$is_residential', '$is_commercial', '$created', '$FSCountry')");
        if ($insert_order_shipping_address) {
            $return['status'] = true;
            $return['id'] = mysql_insert_id();
            $return['user_email'] = $user_email;
            $return['s_address1'] = $s_address1;
            $return['s_address2'] = $s_address2;
            $return['s_city'] = $s_city;
            $return['s_state'] = $s_state;
            $return['s_province'] = $s_province;
            $return['s_zip'] = $s_zip;
            $return['s_phone'] = $s_phone;
            $return['shipping_option_id'] = $shipping_option_id;
            $return['is_residential'] = $is_residential;
            $return['is_commercial'] = $is_commercial;
            $return['s_country'] = $s_country;
        } else {
            $return['status'] = false;
        }
    }
    return $return;
}

function shipping_preference($id) {
    $result = mysql_fetch_array(mysql_query("SELECT * FROM `shipping_options` WHERE `id` = '$id'"));
    return $result['detail'];
}

function member_info($email) {
    $result = mysql_query("SELECT * FROM `member` WHERE `email` = '$email'");
    if (mysql_num_rows($result)) {
        $row = mysql_fetch_array($result);
        return $row;
    } else {
        return false;
    }
}

function shopping_cart_buttons() {
    echo '<br>
	<img src="images/ConfirmShippingA.png">
	<img src="images/FinalReviewA.png">
	<img src="images/OrderCompleteA.png">
	<br>';
}

function shopping_cart_buttons1() {
    echo '<br>
	<img src="images/ConfirmShippingA.png">
	<img src="images/FinalReviewB.png">
	<img src="images/OrderCompleteB.png">
	<br>';
}

function shopping_cart_buttons2() {
    echo '<br>
	<img src="images/ConfirmShippingB.png">
	<img src="images/FinalReviewA.png">
	<img src="images/OrderCompleteB.png">
	<br>';
}

function shopping_cart_buttons3() {
    echo '<br>
	<img src="images/ConfirmShippingB.png">
	<img src="images/FinalReviewB.png">
	<img src="images/OrderCompleteA.png">
	<br>';
}

function PaypalAddToCart_buy_with_Address_Link($business, $item_name, $item_number, $amount, $add = '1', $currency_code = 'USD', $address_info) {
    $data = array(
        'cmd' => '_xclick',
        'add' => $add,
        'business' => $business,
        'item_name' => $item_name,
        'item_number' => $item_number,
        'amount' => $amount,
        'currency_code' => $currency_code,
        'quantity' => '1',
        'first_name' => $address_info['first_name'],
        'last_name' => $address_info['last_name'],
        'address1' => $address_info['address1'],
        'address2' => $address_info['address2'],
        'city' => $address_info['city'],
        'state' => $address_info['state'],
        'zip' => $address_info['zip'],
        'email' => $address_info['email'],
        'landing_page' => 'Billing'
    );
    $payment_link = 'https://www.paypal.com/cgi-bin/webscr?' . http_build_query($data);
    return $payment_link;
}

function PaypalAddToCart_buy_with_Address($business, $item_name, $item_number, $amount, $add = '1', $currency_code = 'USD', $address_info) {
    $paypal_form = '<form method="post" id="paypal_cart" action="https://www.paypal.com/cgi-bin/webscr" target="paypal">
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="add" id="add" value="' . $add . '">
	<input type="hidden" name="business" value="' . $business . '">
	<input type="hidden" name="item_name" id="item_name" value="' . htmlspecialchars($item_name) . '">
	<input type="hidden" name="item_number" id="item_number" value="' . $item_number . '">
	<input type="hidden" name="amount" value="' . $amount . '">
	<input type="hidden" name="currency_code" value="' . $currency_code . '">
	<input type="hidden" name="quantity" id="quantity" value="1">
	<input type="hidden" name="first_name" id="first_name" value="' . $address_info['first_name'] . '">
	<input type="hidden" name="last_name" id="last_name" value="' . $address_info['last_name'] . '">
	<input type="hidden" name="address1" id="address1" value="' . $address_info['address1'] . '">
	<input type="hidden" name="address2" id="address2" value="' . $address_info['address2'] . '">
	<input type="hidden" name="city" id="city" value="' . $address_info['city'] . '">
	<input type="hidden" name="state" id="state" value="' . $address_info['state'] . '">
	<input type="hidden" name="zip" id="zip" value="' . $address_info['zip'] . '">
	<input type="hidden" name="email" id="email" value="' . $address_info['email'] . '">
	<input type="hidden" name="landing_page" value="Billing" />
	<input type="hidden" name="return" value="">
	<input type="image" src="btn_buynow.gif" border="0" name="submit" alt="Buy Now">
	</form>';

    return $paypal_form;
}

function order_place_preview($userid, $shipping_info, $style = '') {
    $return = array();
    $cart_query = mysql_query("SELECT * FROM `cart` WHERE `userid` = '$userid'");
    if (mysql_num_rows($cart_query)) {
        //Shippo::setApiKey(API_KEY);
        $UPS_Rates = new UPS_Rates;
        if ($style == '') {
            $return[] = '<div id="contents">
            <form target="_self" id="cart" method="post" action="addToCart.php?action=update">
            <div>
            <button type="submit">
                    <img width="105" height="22" border="0" src="images/BUpdatecart.gif">
            </button>&nbsp;
            <a target="_self" href="Checkout1.php"><img width="91" height="22" border="0" src="images/BCheckout.gif"></a>
            <a target="_self" href="index.php"><img width="163" height="22" border="0" src="images/BContinueShopping.gif"></a>

            </div>';
        }
        $return[] = '<table width="100%" cellspacing="0" cellpadding="2" bordercolor="#FF0000" border="0" id="AutoNumber1" style="border-collapse: collapse"  fgcolor="#FFFFFF">
        <tbody><tr>
        <td>
        ';
        while ($row = mysql_fetch_array($cart_query)) {
            $cartid = $row['id'];
            $product_id = $row['product_id'];
            $item_number = $row['item_number'];
            $description = $row['description'];
            $price = $row['price'];
            $options = $row['options'];
            $qty = $row['qty'];
            $allId = $item_number . ',';

            $image_thumbnail = getProductImage($product_id);

            $return[] = '<table class="order-item-table" width="100%" cellspacing="5" cellpadding="5" >
                <tr>';
            $return[] = '<td align="center" style="width:150px" valign="top">
            <img src="' . $image_thumbnail . '" style="max-height:100px; max-widht:100px;"><br>
            <input type="hidden" value="' . $cartid . '" name="cartid[]">
                <a href="previewData.php?action=delete&cartid=' . $cartid . '" style="color:red">Remove this item from the order</a>
            </td>';
            $return[] = ' <td valign="top" width="400px">
                <font size="2" color="#000000">
                    <b>' . $description . '</b>
                    <br>
                    Item #: ' . $item_number . '
                    <br>Price: $' . str_replace('$', '', $price) . '
                    <br>Quantity: ' . $qty . '
                    <br><br>Dimension <br>
                    Depth: ' . ($row['length'] + 0) . '<br>
                    Width: ' . ($row['width'] + 0) . '<br>
                    Height: ' . ($row['height'] + 0) . '<br>
                    Weigth: ' . ($row['weight'] + 0) . '<br>
                </font>
                <br>
            </td>';
            $return[] = '<td valign="top">';
            if ($row['free_shipping'] == '1') {
                $return[] = '<p style="font-weight:bold;color:red;">FREE SHIPPING ON THIS ITEM</p>';
            } else {
                $parcels = array();
                for ($i = 0; $i < $qty; $i++) {
                    $parcels[] = array(
                        'length' => $row['length'],
                        'width' => $row['width'],
                        'height' => $row['height'],
                        'distance_unit' => 'in',
                        'weight' => $row['weight'],
                        'mass_unit' => 'lb',
                    );
                }
                $rates = array();
                /*
                  try {
                  $shipment = Shippo_Shipment::create(
                  array(
                  'address_from' => $shipping_info['from_address'],
                  'address_to' => $shipping_info['to_address'],
                  'parcels' => $parcels,
                  'async' => false,
                  )
                  );
                  $rates = $shipment['rates'];
                  } catch (Exception $ex) {
                  // $return[] = $ex->getMessage();
                  }

                 */
                $error = array();
                $getRatesResult = $UPS_Rates->getRates($shipping_info['from_address'], $shipping_info['to_address'], $parcels);
                if ($getRatesResult['status']) {
                    $rates = $getRatesResult['data'];
                } else {
                    $error[] = $getRatesResult['data'];
                }

                $return[] = 'Choose a delivery option:<br>';

                /*
                  if (!empty($shipment['messages'])) {
                  foreach ($shipment['messages'] as $message) {
                  $return[] = '<p style="line-height:18px; color:blue;"><b>Source: ' . $message['source'] . ' </b>- ' . $message['text'] . '</p>';
                  }
                  }
                 * 
                 */
                if (!empty($error)) {
                    foreach ($error as $message) {
                        $return[] = '<p style="line-height:18px; color:blue;"><b>Alert: ' . $message . ' </b></p>';
                    }
                }
                if (!empty($rates)) {
                    foreach ($rates as $rate) {
                        $return[] = '<div class="shipping-rates"><input class="rate" type="radio" name="shipping_item[' . $cartid . ']" value="' . $rate['amount'] . '-' . $rate['days'] . '" data-amount="' . $rate['amount'] . '"> 
                    ' . date('l , M. d', strtotime("+" . $rate['days'] . " days")) . '<br>
                    $ <span class="shipping-amount">' . $rate['amount'] . '</span>
                    </div>';
                    }
                }
            }





            $return[] = '</td>';

            $total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
            $qty_item_row[$cartid] = $qty;

            $return[] = '</tr></table>';
        }
        $allId = substr($allId, 0, -1);
        $colspan = ($style == '') ? '4' : '3';
        $subtotal = number_format(array_sum($total_item_row), 2);


        $TOTAL_ITEMS_SUBTOTAL = array_sum($qty_item_row);
        $TOTAL_COST_SUBTOTAL = number_format(array_sum($total_item_row), 2);


        $DISCOUNT = getDiscount($TOTAL_COST_SUBTOTAL, $userid);
        $discount_msg = '';
//        if (isFirstOrder($userid)) {
//            $discount_msg .= '<span style="float:left;color:#fff;font-weight:bold; background:#000; padding: 0px 10px;line-height:30px;  text-align:right">10% New-Member Discount Applied</span>';
//        }
//        if ($DISCOUNT > 0) {
//            $discount_msg .= '<span style="color:#000;font-weight:bold; line-height:20px; text-align:left">Applied Discount amount saved    - $' . number_format($DISCOUNT, 2) . '</span>';
//        }

        $GRAND_TOTAL = $TOTAL_COST_SUBTOTAL - $DISCOUNT;

        define('TOTAL_ITEMS_SUBTOTAL', $TOTAL_ITEMS_SUBTOTAL);
        define('TOTAL_COST_SUBTOTAL', $TOTAL_COST_SUBTOTAL);
        define('DISCOUNT', $DISCOUNT);
        define('GRAND_TOTAL', $GRAND_TOTAL);

        $return[] = '</table>
        </td>
        <td width="350px" valign="top">
        
        <div style="margin-bottom:20px;">
        <input type="hidden" name="DISCOUNT_AMOUNT" id="DISCOUNT_AMOUNT" value="' . $DISCOUNT . '">

        <div id="discount_msg">' . $discount_msg . '</div>
        <input type="text" name="discount_code" id="discount_code"><input style="padding:3px" type="button" id="apply_discount" name="apply_discount" value="Apply Discount">
        </div>
        Order Summary<br>
        <table class="order-summary">
        <tr>
        <td>Items(' . array_sum($qty_item_row) . '):</td>
        <td>$ ' . $TOTAL_COST_SUBTOTAL . '
        <input type="hidden" name="TOTAL_COST_SUBTOTAL" id="TOTAL_COST_SUBTOTAL" value="' . $TOTAL_COST_SUBTOTAL . '">
            
            </td>
        </tr>
        ';
        if ($DISCOUNT > 0) {
            $return[] = ' <tr>
        <td>Discount Applied:</td>
        <td>- $' . number_format($DISCOUNT, 2) . '</td>
        </tr>';
        }
        $return[] = '
        
        <tr>
        <td>Shipping & handling:</td>
        <td>$ <span id="TOTAL_SHIPPING_AMOUNT_HTML">0</span>
        <input type="hidden" name="TOTAL_SHIPPING_AMOUNT" id="TOTAL_SHIPPING_AMOUNT" value="' . $TOTAL_SHIPPING_AMOUNT . '">

        
        </td>
        </tr>
        <tr>
        <td>Order Total:</td>
        <td>$ <span id="GRAND_TOTAL_HTML">' . $GRAND_TOTAL . '</span>
        <input type="hidden" name="GRAND_TOTAL" id="GRAND_TOTAL" value="' . $GRAND_TOTAL . '">
            
            </td>
        </tr>
        
        </table>
        </td>        
        ';

        $return[] = '</tbody></table>';
        if ($style == '') {
            $return[] = '<div>
            <button type="submit">
                    <img width="105" height="22" border="0" src="images/BUpdatecart.gif">
            </button>&nbsp;
            <a target="_self" href="Checkout1.php"><img width="91" height="22" border="0" src="images/BCheckout.gif"></a>
            <a target="_self" href="index.php"><img width="163" height="22" border="0" src="images/BContinueShopping.gif"></a>
                           
            </div>
                            <input type="hidden" value="' . $allId . '" name="allId">
            </form>
            </div>';
        }
    } else {
        $return[] = 'Shopping Cart is empty.';
    }
    return implode('', $return);
}

function isFirstOrder($userid) {
    $check_user_exists = mysql_query("SELECT `id`, `email`
		FROM `member` 
		WHERE 
		BINARY `member`.`id` = '$userid'");
    if (mysql_num_rows($check_user_exists)) {
        $user_info = mysql_fetch_assoc($check_user_exists);
        $email = $user_info['email'];
        $result = mysql_fetch_array(mysql_query("SELECT COUNT(`orderhistory`.`id`) AS `num`
		FROM `orderhistory` 
		WHERE 
		BINARY  `orderhistory`.`email` = '$email'"));
        if ($result['num'] > 0) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function getDiscount($amount, $userid) {
    $amount = str_replace(',', '', $amount);
    $discount_per = 0;

    $cart_query = mysql_query("SELECT DISTINCT(`discount`) AS `discount` FROM  `cart` WHERE `userid` = '$userid' AND `discount` > 0");
    if (mysql_num_rows($cart_query)) {
        $cart_info = mysql_fetch_array($cart_query, MYSQL_ASSOC);
        if ($cart_info['discount'] > 0) {
            $discount_per = $cart_info['discount'];
        }
    }
//    if (isFirstOrder($userid)) {
//        $discount_per = $discount_per + 10;
//    }
    $discount = ($amount * $discount_per) / 100;
    return $discount;
}

function getCartDiscountCode($userid) {
    $discount_code = '';

    $cart_query = mysql_query("SELECT DISTINCT(`discount_code`) AS `discount_code` FROM  `cart` WHERE `userid` = '$userid' AND `discount_code` != ''");
    if (mysql_num_rows($cart_query)) {
        $cart_info = mysql_fetch_array($cart_query, MYSQL_ASSOC);
        $discount_code = $cart_info['discount_code'];
    }
    return $discount_code;
}

?>