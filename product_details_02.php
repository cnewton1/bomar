<?php
$page_name = 'product_details';
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
?>
<?php
$category = $_REQUEST['category'];
$mfgname = $_REQUEST['mfgname'];
$q = (isset($_REQUEST['q'])) ? mysql_real_escape_string(trim($_REQUEST['q'])) : '';

$where_condition = "";
$query_parm = array();

if ($q != '') {
    $where_condition = $where_condition . " AND (
		`WESTCARB_PART_NUMBER` LIKE '%$q%' OR 	
		`mfgname` LIKE '%$q%' OR 
		`upc` LIKE '%$q%')";

    $query_parm['q'] = $q;
}
if (trim($_REQUEST['category']) != '') {
    $category = mysql_real_escape_string(trim($_REQUEST['category']));
    $where_condition = $where_condition . " AND `category` LIKE '%$category%'";
    $query_parm['category'] = $category;
}
if (trim($_REQUEST['mfgname']) != '') {
    $mfgname = mysql_real_escape_string(trim($_REQUEST['mfgname']));
    $where_condition = $where_condition . " AND `mfgname` = '$mfgname'";
    $query_parm['mfgname'] = $mfgname;
}

if (isset($_POST['action']) && $_POST['action'] == 'send_email') {

    $data = $_POST['data'];

    ob_start();
    product_details_email_content($data);

    $email_content = ob_get_clean();

    $to = $data['to_email'];

    $returnaddress = $FROM;
    $subject = 'Product Suggestion';
    $message = $email_content;

    $headers = 'From: ' . $FROM . "\r\n" .
        'Reply-To: ' . $returnaddress . "\r\n" .
        'Return-Path: <' . $returnaddress . ">\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    $mail = mail($to, $subject, $message, $headers);
    if (!$mail) {
        $error[] = 'Email Sending Failed!.';
    } else {
        $msg = 'Email successfully sent';
        header('Location: product_details.php?id=' . $data['id'] . '&msg=' . $msg);
        exit();
    }
}
?>
<script type="text/javascript">
    function submit_form(form_name) {
        document.getElementById("addtocart").submit();
        // body...
    }
</script>

<style>
    .product_description {
        max-width: 800px;
    }

    .prodInfo_txt ul li {
        background: none;
        line-height: 13px;
        margin-bottom: 3px;
        padding-left: 0px !important;
    }

    .btns a.email {
        background-position: left;
        background-repeat: no-repeat;
        padding: 5px 10px 5px 25px;
        display: inline;
    }

    .btns a.email {
        background-image: url(images/ico_email.png);
    }

    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 99999999999;
        /* Sit on top */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto;
        /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
        /* Could be more or less, depending on screen size */
    }

    /* The Close Button */
    .close {
        color: #aaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }

    div.error {
        padding: 2px 4px;
        margin: 0px;
        border: solid 1px #FBD3C6;
        background: #FDE4E1;
        color: #CB4721;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
    }

    div.success {
        padding: 2px 4px;
        margin: 0px;
        border: solid 1px #C0F0B9;
        background: #D5FFC6;
        color: #48A41C;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
    }
</style>


<div id="home_body">


    <div id="midsec">
        <?php
        if (isset($_GET['msg']) && $_GET['msg'] != '') {
            $msg = $_GET['msg'];
        }
        if (isset($msg)) {
            echo '
	<div class="success">' . htmlspecialchars($msg) . '
	
	</div>';
        }
        if (!empty($error)) {
            echo '
	<div class="error">' . implode('<br>', $error) . '
	
	</div>';
        }
        ?>


        <?php
        if ($_REQUEST['id'] != '') {
            $FREE_SHIPPING = "NO";
            $ID = mysql_real_escape_string($_REQUEST['id']);
            $product_query = mysql_query("SELECT * FROM `product2` WHERE `id` = '$ID'");
            if (mysql_num_rows($product_query)) {
                $row = mysql_fetch_array($product_query);
                $row = array_map('trim', $row);
                $category = $row['category'];
                $subcategory = $row['subcategory'];
                $header = $row['header'];
                $subheader = $row['subheader'];
                $description = $row['description'];
                $comment = $row['comment'];
                $colorstext = $row['colorstext'];
                // MORE
                $UNIT_OF_ISSUE = $row['UNIT_OF_ISSUE'];
                $PACKAGE_QTY = $row['PACKAGE_QTY'];
                $PROD_WIDTH = $row['PROD_WIDTH'];
                $PROD_HEIGHT = $row['PROD_HEIGHT'];
                $PROD_LENGTH = $row['PROD_LENGTH'];
                // EVEN MORE
                $UL_LISTED = $row['UL_LISTED'];
                $ADA = $row['ADA'];
                $JWOD = $row['JWOD'];
                $Clearance = $row['clearance'];
                $FOB = $row['FOB'];

                //KEYWORDS BREAK UP
                $KEYWORDS = $row['KEYWORD'];
                $GSA = "N\A";
                $GSAAPROVE = "N\A";
                $USMADE = "";
                if (strpos($KEYWORDS, "GSA APPROVED") > 1) {
                    $GSAAPROVE = "YES";
                } else {
                    $GSAAPROVE = "N\A";
                }

                if (strpos($KEYWORDS, "MADE IN USA") > 1) {
                    $USMADE = "YES";
                }

                if ($FOB == "YES") {
                    $FREE_SHIPPING = "YES";
                }

                //

                $key_features_array = array();
                if (!empty($row['b1'])) {
                    $key_features_array[] = $row['b1'];
                }
                if (!empty($row['b2'])) {
                    $key_features_array[] = $row['b2'];
                }
                if (!empty($row['b3'])) {
                    $key_features_array[] = $row['b3'];
                }
                if (!empty($row['b4'])) {
                    $key_features_array[] = $row['b4'];
                }
                if (!empty($row['b5'])) {
                    $key_features_array[] = $row['b5'];
                }
                if (!empty($row['b6'])) {
                    $key_features_array[] = $row['b6'];
                }
                if (!empty($row['b7'])) {
                    $key_features_array[] = $row['b7'];
                }


                $NameofPicture=$row['image_hyperlink'];

                if (strpos($row['image_hyperlink'],".jpg") < 0) 
                {
                    $row['image_hyperlink']='ProductImage/' . $NameofPicture . ".jpg";
                }

                if (!isURL($row['image_hyperlink'])) {
                    $thumbnail_url = 'ProductImage/' . basename($row['image_hyperlink']);
                    if (!file_exists($thumbnail_url)) {
                        $thumbnail_url = 'images/ImageNotAvailable.jpg';
                    }
                } else {
                    $thumbnail_url = $row['image_hyperlink'];
                    $image_get_url = getimagesize($thumbnail_url);
                    if (!is_array($image_get_url)) {
                        $thumbnail_url = 'images/ImageNotAvailable.jpg';
                    }
                }

                //$newSize = myResize($thumbnail_url,400,400);
                $price = $row['price'];

                $thumbnail_url = str_replace("http:", "https:", $thumbnail_url);


                RecordAccess($EMAIL_CC, "Product Details");  //2018
                $product_info = array('product_id' => $row['id'], 'item_number' => $row['WESTCARB_PART_NUMBER'], 'item_title' => mysql_real_escape_string($row['prodname']));
                insertProductAccessLog($_SESSION['CURRENT_SESSION_UID'], $product_info);
                $prev_result = mysql_fetch_assoc(mysql_query("SELECT `id` FROM `product2` WHERE `id` > '$ID' $where_condition ORDER BY `id` ASC LIMIT 0,1"));
                $next_result = mysql_fetch_assoc(mysql_query("SELECT `id` FROM `product2` WHERE `id` < '$ID' $where_condition ORDER BY `id` DESC LIMIT 0,1"));
            } else {
                header('Location: index.php');
                exit();
            }
        } else {
            header('Location: index.php');
            exit();
        }
        ?>


        <div class="picgroup_topsec">
            <div>
                <?php
                if (!empty($prev_result)) {
                    echo '<a href="product_details.php?id=' . $prev_result['id'] . '&' . http_build_query($query_parm) . '" class="btn-bs btn-primary">Previous</a></td>';
                }
                if (!empty($next_result)) {
                    echo '<a style="margin-left:20px;" href="product_details.php?id=' . $next_result['id'] . '&' . http_build_query($query_parm) . '" class="btn-bs btn-primary">Next</a></td>';
                }

                if ($row['pdf_file'] != '' && file_exists('ProductImage/' . $row['pdf_file'])) {
                    echo '<a download href="ProductImage/' . $row['pdf_file'] . '" class="btn-bs btn-danger" style="margin-left:250px;">PRODUCT PDF</a>';
                }
                ?>
            </div>
            <div class="picgroup">
                <div class="prod_img" style="display: flex; flex-wrap: wrap; margin-left:5px; height: auto;">
                    <div>
                        <?php echo '<img id="zoom_02" src="' . $thumbnail_url . '" class="prod-img-responsive" border="0" data-zoom-image="' . $thumbnail_url . '" >'; ?>

                        <div style="padding-bottom:10px; font-weight: bold;">Current Price Subject to change</div>
                    </div>

                </div>
                <div class="info">
                    <h1 class="title"><?php echo $row['prodname']; ?></h1>
                    <p>

                        <div class="prodInfo_txt">

                            <div class="btns">
                                <a class="email " id="myBtn" href="javascript:void(0);" rel="nofollow">Email</a>
                            </div>
                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- Modal content -->
                                <div class="modal-content">
                                    <span class="close">&times;</span>

                                    <div class="emailcart_popup" id="emailCartPopup">
                                        <div class="closeBtn">Email Product</div>
                                        <div class="body" id="popup">
                                            <!-- start error message -->
                                            <div class="error_sec hide" origin="logIn">
                                                <div class="error_icon"></div>
                                                <div class="error">
                                                    <span id="reqFldHlt">Required fields are highlighted.</span>

                                                    <span id="customErrMsg"></span>

                                                </div>
                                            </div>

                                            <!-- end error message -->
                                            <div class="form_2col">
                                                <form action="" name="emailProduct" method="POST">
                                                    <input type="hidden" name="data[id]" value="<?php echo $row['id']; ?>" />
                                                    <legend>Enter Requested Information Below:</legend>
                                                    <label id="friendNameLabel">Recipient's Name:</label><input type="text" id="friendName" name="data[to_name]" onblur="clearHilighted(this.id)" size="35" maxlength="100" required /><br />
                                                    <label id="friendEmailLabel">Recipient's Email
                                                        Address:</label><input type="email" id="friendEmail" name="data[to_email]" onblur="clearHilighted(this.id)" size="35" maxlength="100" required /><br />
                                                    <label id="yourNameLabel">Your Name:</label><input type="text" id="yourName" name="data[from_name]" size="35" onblur="clearHilighted(this.id)" maxlength="100" value="" required /><br />
                                                    <label id="yourEmailLabel">Your Email Address:</label><input type="email" id="yourEmail" name="data[from_email]" size="35" maxlength="100" onblur="clearHilighted(this.id)" value="" required /><br />
                                                    <input type="hidden" name="action" value="send_email">
                                                    <input type="submit" class="btn submit" value='Submit' />
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                            <script type="text/javascript">
                                // Get the modal
                                var modal = document.getElementById('myModal');

                                // Get the button that opens the modal
                                var btn = document.getElementById("myBtn");

                                // Get the <span> element that closes the modal
                                var span = document.getElementsByClassName("close")[0];

                                // When the user clicks on the button, open the modal 
                                btn.onclick = function() {
                                    modal.style.display = "block";
                                }

                                // When the user clicks on <span> (x), close the modal
                                span.onclick = function() {
                                    modal.style.display = "none";
                                }

                                // When the user clicks anywhere outside of the modal, close it
                                window.onclick = function(event) {
                                    if (event.target == modal) {
                                        modal.style.display = "none";
                                    }
                                }
                            </script>


                            <?php
                            if ($row['UNIT_OF_ISSUE'] == "EA") {
                                $UNITTYPE = "Each";
                            }
                            print "<B>PART NUMBER: </B>" . $row['mfgpart'] . "</B>";

                            $ProductDescription = $row['EXPANDED_PRODUCT_DESCRIPTION'];

                            $ProductDescription = str_replace("877-694-4932", "866-507-1576", $ProductDescription);

                            // NC18
                            if ($FREE_SHIPPING == "YES") {
                                $ProductDescription = $ProductDescription . "<P><BR><span style=\"color: rgb(255, 0, 0);\"><B> FREE SHIPPING ON THIS ITEM.";
                            }

                            $SHOWCART = "YES";
                            $PLEASECALL = "<img style=\"width: 573px; height: 30px;\" alt=\"PLEASE CALL US AT 866-507-1576 FOR BEST PRICE ON THIS ITEM\" title=\"PLEASE CALL US AT 866-507-1576 FOR BEST PRICE ON THIS ITEM\" src=\"./images/CallBest.jpg\"></span> <br>";

                            // See if wieght is > that 70lb
                            $REAL_WEIGHT = $row['PROD_WEIGHT'];
                            /*
                          if (($REAL_WEIGHT > 70) and ($FREE_SHIPPING == "NO"))
                          {
                          $SHOWCART="NO";
                          $ProductDescription = $ProductDescription . $PLEASECALL;
                          }
                          else
                          {
                          $ProductDescription = $ProductDescription . "</span>";
                          }
                         */
                            echo '<div  style="margin-top:20px;">' . $ProductDescription . '</div>';

                            echo '<div style="display:block;width:100%">';
                            echo '<div style="display:inline-block;width:50%;">';
                            $product_review_link = 'product_review.php?id=' . $row['id'];
                            echo '<p><a href="' . $product_review_link . '"  style="color: white; line-height: 7px; text-decoration: none; font-size: 13px;" class="btn1 btn-info">Customer Reviews</a></p>';

                            $product_reviews_count = product_reviews_count($row['id']);
                            if ($product_reviews_count > 0) {
                                echo '<span style="padding:5px; font-weight:bold; color:blue">' . $product_reviews_count . ' Reviews Posted</span>';
                            }
                            echo '</div>';
                            echo '<div style="display:inline-block;width:50%">';
                            if ($SHOWCART == "YES") {
                                echo '<form method="GET" action="addToCart.php" name="addtocart" id="addtocart">
    	                      <div class="prodSpec" style="margin-top:20px;"><ul>
                                <li style="background:none"><span style="font-size:18px; font-weight:bold; width:100%; text-align:right; height:30px; vertical-align:middle; color:#FF0000">Price:&nbsp; $' . display_price($row['CUSTOMER']) . '&nbsp;' . $UNITTYPE . '</span>
                                
                                <div style="color:#000; font-weight:bold; font-size:1.2em; float:right;clear:both; text-align:right;">
                                Quantity: <input type="number" name="qty" value="1" style="width:100px; padding:5px;"><br><br>
                                <input type="hidden" name="action" value="add">
                                <input type="hidden" name="ID" value="' . $row['id'] . '">

                                <input type="button" name="add" value="&nbsp" class="btn addtocart" style="background-position: 0 -568px;height: 27px;width: 124px;"  onclick="submit_form(\'addtocart\');">
                                </div> </li></ul> </div></form>';
                            }
                            echo '<div style="text-align:right">
                            <a style="text-decoration: none; font-size: 13px;" class="btn1" href="my_wishlist.php?action=add&product_id=' . $row['id'] . '">Add to Wishlist</a>
                                </div>
                                ';
                            echo '</div>';
                            echo '</div>';
                            $ProductDescription = "";
                            ?>
                        </div>
                    </p>

                    <br>

                </div>
            </div>

            <div style="clear:both; " class="prodInfo_txt product_description">
                <?php
                echo '<div class="prodSpec">
					<p>Product Details</p>    
	    			<ul>
                                <li><span>WESTCARB PART NUMBER</span><span>' . $row['WESTCARB_PART_NUMBER'] . '</span></li>';
                if ($row['mfgname'] != '') {
                    echo '<li><span>MANUFACTURER</span><span>' . $row['mfgname'] . '</span></li>';
                }
                if ($row['UNIT_OF_ISSUE'] != '') {
                    echo '<li><span>Unit of Issue</span><span>' . $row['UNIT_OF_ISSUE'] . '</span></li>';
                }
                if ($row['PACKAGE_QTY'] != '') {
                    echo '<li><span>Package Quantity</span><span>' . $row['PACKAGE_QTY'] . '</span></li>';
                }
                if ($row['PROD_WIDTH'] != '') {
                    echo '<li><span>Product Width</span><span>' . $row['PROD_WIDTH'] . '</span></li>';
                }
                if ($row['PROD_HEIGHT'] != '') {
                    echo '<li><span>Product Heigth</span><span>' . $row['PROD_HEIGHT'] . '</span></li>';
                }
                if ($row['PROD_LENGTH'] != '') {
                    echo '<li><span>Product Length</span><span>' . $row['PROD_LENGTH'] . '</span></li>';
                }
                if ($row['colorstext'] != '') {
                    echo '<li><span>COLOR FINISH</span><span>' . $row['colorstext'] . '</span></li>';
                }
                echo '</ul>';
                echo '
                <p>MAKE & MODEL</p>
                <ul>';

                if ($row['UNSPSC'] != '') {
                    echo '<li><span>UNSPSC</span><span>' . $row['UNSPSC'] . '</span></li>';
                }
                if ($row['mfgname'] != '') {
                    echo '<li><span>Brand</span><span>' . $row['mfgname'] . '</span></li>';
                }
                if ($row['mfgpart'] != '') {
                    echo '<li><span>MANUFACTURER PART NUMBER</span><span>' . $row['mfgpart'] . '</span></li>';
                }
                echo '</ul>
                <p>Specifications</p>
                <ul>';
                if ($row['current_country_of_origin'] != '') {
                    echo '<li><span>Country of Origin</span><span>' . $row['current_country_of_origin'] . '</span></li>';
                }
                if ($row['current_country_of_origin'] != '') {
                    echo '<li><span>BTU Rating</span><span>' . $row['current_country_of_origin'] . '</span></li>';
                }
                if ($USMADE != '') {
                    echo '<li><span>Made in the USA</span>' . $USMADE . '<span></span></li>';
                }

                echo '</ul>
                <p>Packaging Dimensions</p>
                <ul>';
                if ($row['PROD_LENGTH'] != '') {
                    echo '<li><span>Depth</span><span>' . $row['PROD_LENGTH'] . '</span></li>';
                }
                if ($row['PROD_HEIGHT'] != '') {
                    echo '<li><span>Height</span><span>' . $row['PROD_HEIGHT'] . '</span></li>';
                }
                if ($row['PROD_WEIGHT'] != '') {
                    echo '<li><span>Weigth</span><span>' . $row['PROD_WEIGHT'] . '</span></li>';
                }
                if ($row['PROD_WIDTH'] != '') {
                    echo '<li><span>Width</span><span>' . $row['PROD_WIDTH'] . '</span></li>';
                }
                echo '</ul>
                <p>Product Features</p>
                <ul>';
                if ($row['FOB'] != '') {
                    echo '<li><span>FOB</span><span>' . $row['FOB'] . '</span></li>';
                }
                if ($JWOD != '') {
                    echo '<li><span>JWOD</span><span>' . $JWOD . '</span></li>';
                }
                echo '<li><span>Clearance</span><span>' . 'NO' . '</span></li>';
                if ($row['upc'] != '') {
                    echo '<li><span>UPC Code</span><span>' . $row['upc'] . '</span></li>';
                }

                echo '</ul>
                <p>Certification & Standards</p>
                <ul>
                <tr><td style = "vertical-align: top;"><img
                style = "width: 52px; height: 45px;" src = "ULLOGO.jpg"><br>
                </td>
                <li><span><src = "ULLOGO.jpg"></span><span>' . ' ' . '</span></li>
                <li><span>ADA Approved</span><span>' . $ADA . '</span></li>
                <li><span>GSA Approved</span><span>' . $GSAAPROVE . '</span></li>
                <li><span>UL Listed</span><span>' . $UL_LISTED . '</span></li>
                </ul>



                </div>';
                ?>
            </div>
        </div>

        <div style="clear:both"></div>
        <?php display_related_category_product($row['id'], $row['category'], $row['subcategory'], $limit = 4); ?>

    </div>
</div>


<?php //display_footer_shop_by_category(); ?>

<?php include 'footer.php'; ?>
<script src='js/jquery.elevatezoom.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#zoom_02').elevateZoom({
            cursor: "crosshair",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750,
            zoomWindowPosition: 7
        });
    });
</script>
</body>

</html>