<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
require_once('recaptcha-config.php');
?>


<?php
function isEmail($email)
{
	return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
}

function extract_email_from_string($string)
{
	$pattern = '/([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
	'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)/i';
	
	preg_match ($pattern, $string, $matches);
	return $matches[0];
}

function check_exists_member_email($email, $user_id = '')
{
	if($user_id != '')
	{
		$qry_add = "AND `id` != '$user_id'";
	}
	else
	{
		$qry_add = '';		
	}
	$result = mysql_fetch_array(mysql_query("SELECT COUNT(`id`) AS `num` FROM `member` WHERE `email` = '$email' $qry_add"));
	if($result['num']>0)
	{
		return true;		
	}
	else
	{
		return false;		
	}	
}

function country_dropdown($country)
{
	$result_country=mysql_query("SELECT `country_name` FROM `country` ORDER BY `country_name`");
	while($row_country=mysql_fetch_array($result_country))
	{
		$selected_str = ($country == $row_country['country_name'])?'selected':'';
		echo '<option value="'.$row_country['country_name'].'" '.$selected_str.'>'.$row_country['country_name'].'</option>';
	}
}

function state_dropdown($state)
{
	$query=mysql_query("SELECT `state_name` FROM `state` ORDER BY `state_name`");
	while($row=mysql_fetch_array($query))
	{
		$selected_str = (strtolower($state) == strtolower($row['state_name']))?'selected':'';
		echo '<option value="'.$row['state_name'].'" '.$selected_str.'>'.$row['state_name'].'</option>';
	}
}

$country = $scountry = 'United States';


function Quote($string)
{
	return addslashes($string);	
}

function EmailText($info)
{
	$email_body = '<table rules="all" style="border-color: #666;" cellpadding="10">
	  <tr style="background: #eee;">
	  <td colspan="2" style="font-size:18px;"><strong>Personal Info</strong></td>
	  </tr>
	  <tr>
		<td class="label"><strong>Your Login ID (Email):</strong></td>
		<td>'.$info['email'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Password:</strong></td>
		<td>'.$info['password'].'</td>
	  </tr>
	  
	  <tr>
		<td class="label"><strong>First Name:</strong></td>
		<td>'.$info['name'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Last Name:</strong></td>
		<td>'.$info['last'].' </td>
	  </tr>
	  
	  <tr>
		<td class="label"><strong>Company Name:</strong></td>
		<td>
		'.$info['company'].'
		&nbsp;&nbsp;&nbsp;&nbsp;<strong>Your Company Tax ID:</strong> '.$info['company_tax_id'].'
		
		</td>
	  </tr>
	  
	  
	  <tr>
		<td class="label"><strong>Address1:</strong></td>
		<td>'.$info['address1'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Address2:</strong></td>
		<td>'.$info['address2'].'</td>
	  </tr>
	  
	  <tr>
		<td class="label"><strong>City:</strong> </td>
		<td>'.$info['city'].'</td>
	  </tr>	  
	  <tr>
		<td class="label"><strong>State:</strong></td>
		<td>'.$info['state'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Province:</strong></td>
		<td>'.$info['province'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Zip:</strong> </td>
		<td>'.$info['zip'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Country: </strong> </td>
		<td>'.$info['country'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Phone:</strong></td>
		<td>'.$info['phone'].'</td>
	  </tr>
	  
	  <tr  style="background: #eee;">
	  <td colspan="2" style="font-size:18px;"><strong>Shipping Address</strong></td>
	  </tr>
	 
	  <tr>
		<td class="label"><strong>Address1:</strong></td>
		<td>'.$info['saddress1'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Address2:</strong></td>
		<td>'.$info['saddress2'].'</td>
	  </tr>
	  
	  <tr>
		<td class="label"><strong>City:</strong> </td>
		<td>'.$info['scity'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>State:</strong></td>
		<td>'.$info['sstate'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Province:</strong></td>
		<td>'.$info['sprovince'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Zip:</strong> </td>
		<td>'.$info['szip'].'</td>
	  </tr>
	  <tr>
		<td class="label"><strong>Country:</strong>  </td>
		<td>'.$info['scountry'].'
		</td>
	  </tr>	
	  <tr>
		<td class="label"><strong>Phone:</strong></td>
		<td>'.$info['sphone'].'</td>
	  </tr>
	</table>';
	
	return $email_body;
}

if(isset($_POST['submit']))
{
	$email 		= $_POST['email'];
	$password 	= $_POST['password'];
	$status 	= $_POST['status'];
	$calltype 	= $_POST['calltype'];
	
	$name 		= $_POST['name'];
	$last 		= $_POST['last'];
	$company 	= $_POST['company'];
	$address1 	= $_POST['address1'];
	$address2 	= $_POST['address2'];
	$city 		= $_POST['city'];
	$state 		= $_POST['state'];
	$province	= $_POST['province'];
	$zip 		= $_POST['zip'];
	$country 	= $_POST['country'];
	$phone 		= $_POST['phone'];
	$saddress1 	= $_POST['saddress1'];
	$saddress2 	= $_POST['saddress2'];
	$scity 		= $_POST['scity'];
	$sstate 	= $_POST['sstate'];
	$sprovince	= $_POST['sprovince'];
	$szip 		= $_POST['szip'];
	$scountry 	= $_POST['scountry'];
	$sphone 	= $_POST['sphone'];
	
	if(empty($email))
	{
		$error[]= "You did not enter Login ID. ";
	}
	else
	{
		if(!isEmail($email))
		{
			$error[]= "Enter A Valid Email Address in Login ID";
		}
		else if(check_exists_member_email($email))
		{
			$error[]= "Login Name $email is already registered.";
		}
	}
	if(empty($password))
	{
		$error[]= "You did not enter password.";
	}
	else
	{
		/*if(strlen($password)<8)
		{
			$error[]= "Enter atleast 8 characters password.";
		}*/
	}
	
	if(empty($password))
	{
		$error[]= "You did not enter password.";
	}
	
	$recaptcha=$_POST['g-recaptcha-response'];
  
	if(!empty($recaptcha))
	{
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret=GOOGLE_RECAPTCHA_SECRET_KEY;
		$ip=$_SERVER['REMOTE_ADDR'];
		$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
		$res=getCurlData($url);
		$res= json_decode($res, true);
		if($res['success'])
		{
		}
		else 
		{
		  $error[] = "Please re-enter your reCAPTCHA.";
		}
	}
	else
	{
		$error[] = "Please re-enter your reCAPTCHA.";
	}

	if(empty($email) || empty($password) || empty($name) || empty($last) || empty($company) || empty($address1) || empty($city) || empty($state) || empty($zip) || empty($country) || empty($phone) || empty($saddress1) || empty($scity) || empty($sstate) || empty($szip) || empty($scountry))
	{
		$error[] = 'Please fill all required fields.';
		
	}
	
	
	if(empty($error))
	{
		$resultID = mysql_query("INSERT INTO `member` 
		(`email`, `password`, `name`, `last`, `company`, `address1`, `address2`, `city`, `state`, `province`, `zip`, `country`, `phone`, `saddress1`, `saddress2`, `scity`, `sstate`, `sprovince`, `szip`, `scountry`, `sphone`, `status`) 
		VALUES 
		('$email', '$password', '$name', '$last', '$company', '$address1', '$address2', '$city', '$state', '$province', '$zip', '$country', '$phone', '$saddress1', '$saddress2', '$scity', '$sstate', '$sprovince', '$szip', '$scountry', '$sphone', 'NOT ACTIVE')");	   
		if($resultID)
		{
			$emailinfo = $_POST;
			
			$to      = $email;
			//$to = 'mdadilsiddiqui@gmail.com';
			$subject = 'You are registered in '.$COMPANYNAME;
			$message = '<html><body>';
			$message .= '<img src="http://bomar.chrondev.com/images/logo.jpg" alt="'.$COMPANYNAME.'" />';
			$message .= EmailText($emailinfo);
			$message .= "</body></html>";
			
			$FROM = extract_email_from_string($FROM);
			$COMPEMAIL = extract_email_from_string($COMPEMAIL);
			
			$headers = 'From: '.$FROM. "\r\n" .
				'Reply-To: '.$FROM . "\r\n" .
				'Return-Path: <'.$FROM . ">\r\n" .
				'Cc: '.$COMPEMAIL."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			
			
			$mail = mail($to, $subject, $message, $headers);
			if(!$mail){
				$error[] = 'Email Sending Failed!.';
			}
			
			$msg = "We will contact you by phone for account verification to complete the dealer registration process.";
			header('Location: JoinMember1.php?msg='.$msg);
			exit();
		}
		else
		{
			$error[] = 'DB Error: '. mysql_error();
		}
	}
}
?>

<style>
div.error{
	padding:2px 4px;
    margin:0px;
    border:solid 1px #FBD3C6;
    background:#FDE4E1;
    color:#CB4721;
    font-family:Arial, Helvetica, sans-serif;
    font-size:14px;
    font-weight:bold;
    text-align:center; 
}
div.success{
	padding:2px 4px;
	margin:0px;
	border:solid 1px #C0F0B9;
	background:#D5FFC6;
	color:#48A41C;
	font-family:Arial, Helvetica, sans-serif; font-size:14px;
	font-weight:bold;
	text-align:center; 
}

input[type="text"], input[type="password"], select 
{
	width:250px; 
	height:25px; 
	border:1px solid #000;
}

td .label
{
	text-align:right;
	
}
.required
{
	color:#F00;
	font-weight:bold;
	
}
</style>

<script type="text/javascript">
function copyBillingAddress()
{
	var chkStatus1 = document.getElementById("sameasabove");
	 
	if (chkStatus1.checked) {
		
		document.getElementById('saddress1').value = document.getElementById('address1').value;
		document.getElementById('saddress2').value = document.getElementById('address2').value;
		document.getElementById('scity').value = document.getElementById('city').value;
		document.getElementById('sprovince').value = document.getElementById('province').value;
		document.getElementById('szip').value = document.getElementById('zip').value;
		document.getElementById('sphone').value = document.getElementById('phone').value;	
		
		var state_index=document.getElementById("state");
		var valueToSelect_state = state_index.value;
		
		var state_element = document.getElementById('sstate');
		state_element.value = valueToSelect_state;
		
			
		var y=document.getElementById("country");
		var valueToSelect_country = y.value;
		
		var element = document.getElementById('scountry');
		element.value = valueToSelect_country;
	}
}
</script>    

<script src="https://www.google.com/recaptcha/api.js" async defer></script>


    <div id="home_body">
        

        <div id="midsec">
  
<table width="100%" cellpadding="0" cellspacing="5" border="0"  style=" font-size:12px;">
<?php
$msg = $_REQUEST['msg'];
if(isset($msg))
{
	echo '<tr>
	<td colspan="2">
	<div class="success">'.$msg.'
	
	</div></td>
  </tr>';
  exit();
  
  
}
if(!empty($error))
{
	echo '<tr>
	<td colspan="2">
	<div class="error">'.implode('<br>',$error).'</div>
	</td>
  </tr>';
}
?>
                  
<tr>
<td  colspan="2">

<form method="POST" action="">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
<colgroup>
<col width="200" />
</colgroup>
  <tr>
  <td colspan="2" style="font-size:18px;">Personal Info</td>
  </tr>
  <tr>
    <td class="label">Your Login ID (Email)<span class="required">*</span>:</td>
    <td><input type="text" name="email"  id="email" size="50" value="<?php echo $email;?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (must be a valid email id)</td>
  </tr>
  <tr>
    <td class="label">Password<span class="required">*</span>:</td>
    <td><input type="password" name="password" id="password" size="20" value="<?php echo $password;?>"></td>
  </tr>
  
  <tr>
    <td class="label">First Name<span class="required">*</span>:</td>
    <td><input type="text" name="name"  id="name" size="50"  value="<?php echo $name;?>" ></td>
  </tr>
  <tr>
    <td class="label">Last Name<span class="required">*</span>:</td>
    <td><input type="text" name="last"  id="last" size="50"  value="<?php echo $last;?>" ></td>
  </tr>
  
  <tr>
    <td class="label">Company Name<span class="required">*</span>:</td>
    <td>
    <div style="float:left">
    <input type="text" name="company"  id="company" size="50"  value="<?php echo $company;?>" >
    </div>
   
    </td>
  </tr>
  
  
  <tr>
    <td class="label">Address1<span class="required">*</span>:</td>
    <td><input type="text" name="address1"  id="address1" size="50"  value="<?php echo $address1;?>"></td>
  </tr>
  <tr>
    <td class="label">Address2:</td>
    <td><input type="text" name="address2"  id="address2" size="50"  value="<?php echo $address2;?>"></td>
  </tr>
  
  <tr>
    <td class="label">City<span class="required">*</span>: </td>
    <td><input type="text" name="city"  id="city" size="50"  value="<?php echo $city;?>"></td>
  </tr>
  <tr>
    <td class="label">State<span class="required">*</span>:</td>
    <td>
    <select id="state" name="state">
    <option value=""></option>
    <?php state_dropdown($state);?>
    </select></td>
  </tr>
  <tr>
    <td class="label">Province: </td>
    <td><input type="text" name="province"  id="province" size="50"  value="<?php echo $province;?>"></td>
  </tr>
  <tr>
    <td class="label">Zip<span class="required">*</span>: </td>
    <td><input type="text" name="zip"  id="zip" size="50"  value="<?php echo $zip;?>"></td>
  </tr>
  <tr>
    <td class="label">Country<span class="required">*</span>:  </td>
    <td>
    <select id="country" name="country">
    <?php country_dropdown($country);?>
    </select>
    </td>
  </tr>
  <tr>
    <td class="label">Phone<span class="required">*</span>:</td>
    <td><input type="text" name="phone"  id="phone" size="50"  value="<?php echo $phone;?>" ></td>
  </tr>
  
  <tr>
  <td colspan="2" style="font-size:18px;">Shipping Address</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  <td ><input type="checkbox" name="sameasabove" id="sameasabove" value="1" onClick="copyBillingAddress();">Shipping Address is same as above</td>
  </tr>
  <tr>
    <td class="label">Address1<span class="required">*</span>:</td>
    <td><input type="text" name="saddress1"  id="saddress1" size="50"  value="<?php echo $saddress1;?>"></td>
  </tr>
  <tr>
    <td class="label">Address2:</td>
    <td><input type="text" name="saddress2"  id="saddress2" size="50"  value="<?php echo $saddress2;?>"></td>
  </tr>
  
  <tr>
    <td class="label">City<span class="required">*</span>: </td>
    <td><input type="text" name="scity"  id="scity" size="50"  value="<?php echo $scity;?>"></td>
  </tr>
  <tr>
    <td class="label">State<span class="required">*</span>:</td>
    <td>
    <select id="sstate" name="sstate">
    <option value=""></option>
    <?php state_dropdown($sstate);?>
    </select>
    </td>
  </tr>
  <tr>
    <td class="label">Province: </td>
    <td><input type="text" name="sprovince"  id="sprovince" size="50"  value="<?php echo $sprovince;?>"></td>
  </tr>
  <tr>
    <td class="label">Zip<span class="required">*</span>: </td>
    <td><input type="text" name="szip"  id="szip" size="50"  value="<?php echo $szip;?>"></td>
  </tr>
  <tr>
    <td class="label">Country<span class="required">*</span>:  </td>
    <td>
    <select id="scountry" name="scountry">
    <?php country_dropdown($scountry);?>
    </select>
    </td>
  </tr>
  <tr>
    <td class="label">Phone<span class="required">*</span>:</td>
    <td><input type="text" name="sphone"  id="sphone" size="50"  value="<?php echo $sphone;?>" ></td>
  </tr>
  <tr>
  <td></td>
  <td>
  	<div class="g-recaptcha" data-sitekey="<?php echo GOOGLE_RECAPTCHA_SITE_KEY;?>"></div>
  	
  </td>
  </tr>
  <tr>
    <td>&nbsp; </td>
    <td>
    <input type="hidden" name="status" value="NOT ACTIVE">
    <input type="hidden" name="calltype" size="20" value="A">
    <input type="submit" name="submit" value="Submit" style="background:#CCC; padding:5px; font-family:Arial, Helvetica, sans-serif; font-weight:bold" />
    <input type="button" name="cancel" value="Cancel" style="background:#CCC; padding:5px; font-family:Arial, Helvetica, sans-serif; font-weight:bold" onClick="window.location='index.php';" />
    </td>
  </tr>
</table>
</form>

</td>
</tr>

</table>


        </div>
    </div>

    




<?php include 'footer.php';?>


    
  </body>
</html>