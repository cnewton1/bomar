<?php

//----------------------------------------------
// function section
//----------------------------------------------

//----------------------------------------------
// Mod the date
//----------------------------------------------
function allaDate($chronDate) {
 if ($chronDate <= 0) return 0;

 $chronDate+=15;
 return $chronDate;
}


//----------------------------------------------
// key signature
//----------------------------------------------
function makeRegister($prodKey) {

 if (strlen($prodKey) < 17) {
   return "";
 }

// get first part of string
// make sure it conforms
 $firstPart=substr($prodKey,0,10);

//------------------------------------------------
// Test all merchant keys
//------------------------------------------------
// Try Chronsystem Gerneral Product 
 $scomp=strcmp($firstPart,"AMP95X21YJ");
 
// Try Custom Built Computers Product
 $CBCcomp=strcmp($firstPart,"CBC95X21YJ");

// Try Brooklyn NetCafe Product
 $BNCcomp=strcmp($firstPart,"BNC95X21YJ");

// Try Consultant and Education Service 
 $CEScomp=strcmp($firstPart,"CES95X21YJ");

// Try LGI Product
 $LGIcomp=strcmp($firstPart,"LGI95X21YJ");

// Try CYC Product
 $CYCcomp=strcmp($firstPart,"CYC95X21YJ");

//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
//-----------------------------------------------
// Test and make sure that at lest one of the keys 
// are correct
//------------------------------------------------
 if (($scomp != 0) && ($CBCcomp != 0) && ($BNCcomp != 0) && ($CEScomp != 0) && ($LGIcomp != 0) && ($CYCcomp != 0))
 {
     return "";
 }


// ok we have a good key
 $firstPart="AMA96048YJ";



// Get the Midpart
$midPart=substr($prodKey,10,5); // offset, length

// Make last part
srand((int) time * 1000000);
$stargen = rand(1,99999);

//---------------------------------------------------------
// Format display
//---------------------------------------------------------
return $firstPart . allaDate($midPart) . $stargen . "D";

}

?>

