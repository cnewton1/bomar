<?php
$page_name = 'product_details';
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
?>

<?php

//page_protect();

$user_query = mysql_query("SELECT * FROM  `member` WHERE `id` = '$_SESSION[USERID]'");
if (mysql_num_rows($user_query)) {
    $row = mysql_fetch_array($user_query);

    $first_name         = trim($row['first_name']);
    $last_name             = trim($row['last_name']);
} else {
    // header('Location: login.php');
    // exit();

}

$product_id = mysql_real_escape_string($_REQUEST['id']);
$error = array();
if (isset($_POST['calltype'])) {
    page_protect();

    $calltype = $_POST['calltype'];
    $reviewForm = $_POST['reviewForm'];

    if ($reviewForm['review_title'] == '') {
        $error[] = 'Please enter Title of your review ';
    }
    if ($reviewForm['review_description'] == '') {
        $error[] = 'Please enter Your review ';
    }

    if (empty($error)) {
        $product_id         = $reviewForm['product_id'];
        $user_id             = $_SESSION['USERID'];
        $rating             = $reviewForm['rating'];
        $review_title         = mysql_real_escape_string($reviewForm['review_title']);
        $review_description = mysql_real_escape_string($reviewForm['review_description']);
        $pros                 = mysql_real_escape_string($reviewForm['pros']);
        $cons                 = mysql_real_escape_string($reviewForm['cons']);
        $status             = '1';

        $resultID = mysql_query("INSERT INTO `product_review` (`product_id`, `user_id`, `rating`, `review_title`, `review_description`, `pros`, `cons`, `status`) VALUES ('$product_id', '$user_id', '$rating', '$review_title', '$review_description', '$pros', '$cons', '$status')");
        if ($resultID) {
            $msg = 'Your Review Successfully Added.';
            header('Location: product_review.php?id=' . $product_id . '&msg=' . $msg);
            exit();
        } else {
            $error[] = mysql_error();
        }
    }
}
?>

<link rel="stylesheet" href="review.css" />
<link href="src/rateit.css" rel="stylesheet" type="text/css">

<link href="content/bigstars.css" rel="stylesheet" type="text/css">
<link href="content/antenna.css" rel="stylesheet" type="text/css">
<link href="content/svg.css" rel="stylesheet" type="text/css">
<!-- syntax highlighter -->
<link href="sh/shCore.css" rel="stylesheet" type="text/css">
<link href="sh/shCoreDefault.css" rel="stylesheet" type="text/css">

<script src="src/jquery.rateit.js" type="text/javascript"></script>


<style>
    div.error {
        padding: 2px 4px;
        margin: 0px;
        border: solid 1px #FBD3C6;
        background: #FDE4E1;
        color: #CB4721;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
    }

    div.success {
        padding: 2px 4px;
        margin: 0px;
        border: solid 1px #C0F0B9;
        background: #D5FFC6;
        color: #48A41C;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
    }
</style>


<div id="home_body">


    <div id="midsec">


        <?php
        $product_info = load_product($product_id);
        if (empty($product_info)) {
            header('Location: index.php');
            exit();
        }
        ?>






        <div class="container content">

            <table id="form" width="100%" height="<?php echo $table_height; ?>" align="center" border="0" bgcolor="#FFFFFF" cellpadding="5" cellspacing="5">
                <TR>

                    <td valign="top" style="padding:20px; ">
                        <div style="background:#FFF; color:#000; padding:10px; width:100%;  min-height:320px;  border-radius:10px;">

                            <form method="post" action="">
                                <table width="100%" cellpadding="0" cellspacing="5" border="0" style=" font-size:12px;">
                                    <?php
                                    if ($_REQUEST['msg'] != '') {
                                        $msg = $_REQUEST['msg'];
                                    }
                                    if (isset($msg)) {
                                        echo '<tr>
                                            <td colspan="2">
                                            <div class="success">' . htmlspecialchars($msg) . '
                                            
                                            </div></td>
                                        </tr>';
                                    }
                                    if (!empty($error)) {
                                        echo '<tr>
                                            <td colspan="2">
                                            <div class="error">' . implode('<br>', $error) . '
                                            
                                            </div></td>
                                        </tr>';
                                    }
                                    ?>

                                    <tr>
                                        <td colspan="2">

                                            <table width="100%" cellpadding="5" cellspacing="5" border="0">
                                                <tr>
                                                    <td>


                                                        <div id="writereview" class="row">
                                                            <div class="col-md-12 col-sm-12">

                                                                <?php display_product_box($product_info); ?>

                                                                <div class="section">
                                                                    <div id="rating-scroll"></div>
                                                                    <h2><strong>Your review</strong></h2>

                                                                    <div class="form-group">
                                                                        <label for="reviewForm_rating" class="required">How would you rate
                                                                            <?php echo $product_info['header']; ?></label>

                                                                        <select id="reviewForm_rating" name="reviewForm[rating]" required="required" data-focus-element="rating-scroll" class="form-control">
                                                                            <option value="" selected="selected">Select
                                                                                a rating</option>
                                                                            <?php rating_dropdown($reviewForm['rating']); ?>
                                                                        </select>
                                                                        <div id="review_stars">
                                                                            <div class="rateit bigstars" id="rateit" data-rateit-starwidth="32" data-rateit-starheight="32" data-rateit-backingfld="#reviewForm_rating" data-rateit-resetable="false" data-rateit-min="0" data-rateit-max="5">
                                                                            </div>
                                                                            <div id="rate-hover">
                                                                                <span class="visible-xs">Click to
                                                                                    rate</span>
                                                                                <span class="hidden-xs">Click stars to
                                                                                    start rating</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        var c = [];
                                                                        var n = ["Terrible", "Bad", "OK", "Good",
                                                                            "Excellent"
                                                                        ];
                                                                        var h = $("#rate-hover span:visible").text();
                                                                        $("#rateit").bind("rated", function(p, q) {
                                                                            h = n[q - 1];
                                                                            $("#rate-hover span:visible").text(h);
                                                                            var o = $("#reviewForm_rating").parent(
                                                                                ".form-group");
                                                                            o.removeClass("has-error");
                                                                            o.find("p.help-block").remove();
                                                                            window.onbeforeunload = a;
                                                                            g("reviewForm[rating]")
                                                                        });
                                                                        $("#rateit").bind("over", function(o, p) {
                                                                            $("#rate-hover span:visible").text(p ?
                                                                                n[p - 1] : h)
                                                                        });
                                                                    </script>



                                                                    <div class="form-group"><label class="required" for="reviewForm_title">Title of your review
                                                                        </label><input type="text" class="form-control" required="required" name="reviewForm[review_title]" id="reviewForm_title" value="<?php echo htmlspecialchars($reviewForm['review_title']); ?>">
                                                                        <p class="quiet">Example: This is perfect! Can't
                                                                            live without it.</p>
                                                                    </div>

                                                                    <div class="form-group has-error">
                                                                        <label class="required" for="reviewForm_textOverall">Your review
                                                                        </label>

                                                                        <textarea class="form-control" rows="4" required="required" name="reviewForm[review_description]" id="reviewForm_textOverall" style="overflow: hidden; word-wrap: break-word; resize: none; height: 110px;"><?php echo htmlspecialchars($reviewForm['review_description']); ?></textarea>
                                                                    </div>

                                                                    <div class="form-group"><label for="reviewForm_textGood">Pros: what did you
                                                                            like about it? <span class="small quiet">(optional)</span></label><input type="text" class="form-control" name="reviewForm[pros]" id="reviewForm_textGood" value="<?php echo htmlspecialchars($reviewForm['pros']); ?>">
                                                                        <p class="quiet">Example: "reliable,
                                                                            inexpensive, energy efficient". Leave empty
                                                                            if you liked nothing.</p>
                                                                    </div>
                                                                    <div class="form-group"><label for="reviewForm_textBad">Cons: What was
                                                                            wrong with it? <span class="small quiet">(optional)</span></label><input type="text" class="form-control" name="reviewForm[cons]" id="reviewForm_textBad" value="<?php echo htmlspecialchars($reviewForm['cons']); ?>">
                                                                        <p class="quiet">Example: "expensive, bad
                                                                            warranty service". Leave empty if nothing
                                                                            was wrong."</p>
                                                                    </div>

                                                                    <input type="hidden" class="form-control" name="reviewForm[product_id]" value="<?php echo $product_id; ?>">

                                                                </div>

                                                                <button data-gae="{&quot;act&quot;:&quot;Review&quot;, &quot;cat&quot;:&quot;ClickSubmit&quot;, &quot;lab&quot;: &quot;298993&quot;}" data-loading-text="Sending Review..." class="btn btn-lg btn-primary gae" type="submit" name="submit">Submit your review</button>

                                                            </div>

                                                        </div>

                                                        <?php

                                                        reviews_list($product_id);

                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>











                                        </td>
                                    </tr>



                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <br>
                                            <input type="hidden" name="calltype" size="20" value="U">

                                        </td>
                                    </tr>
                                </table>

                            </form>
                        </div>

                        <div style="clear:both; margin-top:20px;"></div>
                    </td>
                </TR>
            </table>

        </div>

    </div>
</div>


<?php //display_footer_shop_by_category(); ?>

<?php include 'footer.php'; ?>
</body>

</html>