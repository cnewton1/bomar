<!DOCTYPE html>
<html>
	<head>
		<?php include 'global_head.php';?>
        
		<!-- <hs:title> -->
        
		<title>Westcarb Enterprises | Springfield, MA 01108</title>
	
		
	</head>
	<body id="element1" onunload="" scroll="auto">
		<noscript>
			<img height="40" width="373" border="0" alt="" src="images/javascript_disabled.gif">
		</noscript>
        
        
<div class="lpxcenterpageouter">
	<div class="lpxcenterpageinner">
		<!-- <hs:bodyinclude> -->
		<?php include 'head_top.php';?>
		
		
		<!-- </hs:bodyinclude> -->
		<!-- <hs:element48> -->
		<div id="element48" style="position: absolute; top: 404px; left: 99px; width: 465px; height: 19px; z-index: 1000;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size12 Tahoma12">Construction &amp; Services<br>
				</font>
			</div>
		</div>
		<!-- </hs:element48> -->
		<!-- <hs:element49> -->
		<div id="element49" style="position: absolute; top: 432px; left: 101px; width: 243px; height: 451px; z-index: 1001;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">WCE Facilities Operation Support Services include, but are not limited to the following:<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size8 Tahoma8"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Clean Room Maintenance<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• CMMS Programs<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Construction Project Management<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Electrical Systems<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Energy Management<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Facility Maintenance<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• HVAC<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Landscaping<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Lighting Services<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Maintenance Scheduling &amp; Planning<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Mechanical Systems<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Operations Support<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Preventive/Predictive Maintenance<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Production Maintenance &amp; Support<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Project Construction &amp; Management<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Reliability-Centered Maintenance<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Snow Removal<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Warehouse &amp; Storeroom Services<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Waste Management<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Disaster Recovery Clean Up<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• Inventory Management<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">• MRO Supplies<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10"><br>
				</font>
			</div>
		</div>
		<!-- </hs:element49> -->
		<!-- <hs:element50> -->
		<div id="element50" style="position: absolute; top: 130px; left: 100px; width: 773px; height: 29px; z-index: 1002;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#112c45" class="size18 Tahoma18">Westcarb Enterprises Services<br>
				</font>
			</div>
		</div>
		<!-- </hs:element50> -->
		<!-- <hs:element51> -->
		<div id="element51" style="position: absolute; top: 166px; left: 100px; width: 469px; height: 224px; z-index: 1003;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">At Westcarb Enterprises, we take care to provide our customers high quality services to meet their unique needs. Our Integrated Project Team, combined with Operations Management, strives to meet and exceed customer expectations.<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">Westcarb has performed in many capacities since its inception. This has facilitated the growth of the company and the development of its personnel. While Westcarb is known primarily for its Maintenance, Repair, Operations (MRO) services, the company has also performed many services in a Prime Contractor capacity. Some of the services provided include:<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">Design Build, Furniture Design and Layout, Low Voltage Cabling,<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">Facility Maintenance, Contract Management, Product Distribution,<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">Administrative Services, Construction Management, Telecommunications,<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size10 Tahoma10">Project Management, General Construction, and Subject Matter Expertise.<br>
				</font>
			</div>
		</div>
		<!-- </hs:element51> -->
		<!-- <hs:element62> -->
		<div id="element62" style="position: absolute; top: 413px; left: 373px; width: 141px; height: 112px; z-index: 1004;">
			<div style="overflow: hidden; height: 112px; width: 141px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="112" width="141" style="display: block; border-radius: 0px;" title="" alt="" src="images/Services~~element62.jpg"/>
			</div>
		</div>
		<!-- </hs:element62> -->
		<!-- <hs:element66> -->
		<div id="element66" style="position: absolute; top: 539px; left: 371px; width: 563px; height: 312px; z-index: 1005;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size12 Tahoma12">Specialized Services NAICS Codes/Classifications<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size10 Tahoma10"><b></b><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size9 Tahoma9">Construction of Buildings<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 236115<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>New Single-Family Housing Construction (except Operative Builders) <br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 236116<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>New Multifamily Housing Construction (except Operative Builders)<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 236210<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Industrial Building Construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 236220<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Commercial and Institutional Building Construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size9 Tahoma9">Heavy and Civil Engineering Construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 237110<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Water and Sewer Line and Related Structures Construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 237130<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Telecommunications, Door Tagging, Disconnects, Drops, Underground construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 237310<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Highway, Street, and Bridge Construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 237990<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Other Heavy and Civil Engineering Construction<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size9 Tahoma9">Specialty Trade Contractors<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 238210<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Electrical Contractors and Other Wiring Installation Contractors<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 238220<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Plumbing, Heating, and Air-Conditioning Contractors<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 238910<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Site Preparation Contractors<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000066" class="size9 Tahoma9">Administrative and Support Services<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 561210<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Facilities Support Services<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 561720<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Janitorial Services<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#000000" class="size9 Tahoma9"> 561730<img src="images/tp.gif" alt border="0" width="30" class="lpxtab"/>Landscaping Services<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
		</div>
		<!-- </hs:element66> -->
		<!-- <hs:element67> -->
		<div id="element67" style="position: absolute; top: 173px; left: 590px; width: 282px; height: 199px; z-index: 1006;">
			<div style="overflow: hidden; height: 199px; width: 282px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="199" width="282" style="display: block; border-radius: 0px;" title="" alt="" src="images/DOJ_Const_site.jpg"/>
			</div>
		</div>
		<!-- </hs:element67> -->
		<!-- <hs:element68> -->
		<div id="element68" style="position: absolute; top: 414px; left: 558px; width: 129px; height: 111px; z-index: 1007;">
			<div style="overflow: hidden; height: 111px; width: 129px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="111" width="129" style="display: block; border-radius: 0px;" title="" alt="" src="images/imagesCAC3ZS5C.jpg"/>
			</div>
		</div>
		<!-- </hs:element68> -->
		<!-- <hs:element69> -->
		<div id="element69" style="position: absolute; top: 417px; left: 724px; width: 145px; height: 109px; z-index: 1008;">
			<div style="overflow: hidden; height: 109px; width: 145px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="109" width="145" style="display: block; border-radius: 0px;" title="" alt="" src="images/construction-site.jpg"/>
			</div>
		</div>
		<!-- </hs:element69> -->
        
        
      <?php include 'footer.php';?>
        
        
        
	</div>
</div>



		
		
	</body>
</html>
