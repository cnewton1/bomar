/* jQuery(document).ready(function ($) {

  $(".qty_update").live('change keyup keydown blur focus click', function(event) {
		  $(this).parent().find('div').show();  
  	});
  });*/
 
$(document).on("keyup",".qty_update , .lengthCut ", function(e) {
	var tdClass  = $(this).attr("itemDisplay");
	var vendorKey = $(this).attr("vendorKey");
	
	if( ($(this).val() == "") || ($(this).val() == $(this).attr("origVal")) ){
		$('td.'+tdClass+'_'+vendorKey).find('div.updDiv').hide();
	}else{
		$('td.'+tdClass+'_'+vendorKey).find('div.updDiv').show();
	} 		
});
		
  // *** save item for later ***
  $(document).on('click','.keeplater', function(event) {		
		var keys = $(this).attr('id').split('_');
		var itemKey = keys[0];
		
		var olc = keys[1];
		
		if (itemKey > 0 && olc > 0) {
			saveItemForLater(itemKey, olc);
		}
	});
  function saveItemForLater(itemKey, itemLineNumber){
	$.ajax({
		type:"post",
		url:'/saveItemForLater', 
		cache: false,
		data: { 
				'itemKey':itemKey,
				'itemLineNumber':itemLineNumber, 
				'ajaxDate':getAjaxDate()
		},
		success: function(data)
		{ 
			if (data.indexOf('pcs-error-messages') < 0) {
   				window.location.href = '/viewCart';
		
			} else {
				// an error occurred
			}
		}
	});
  }

  // *** delete from cart ***
  //$(document).on('click', '.delete', function(event) {
  $(document).on('click', '[id^="savedDelete_"]', function(event) {
	  	event.preventDefault();
		event.stopPropagation();
		var tmp = $(this).attr('id');
		if (tmp && tmp.indexOf('savedDelete_') >= 0 ) {
			var item_id = $(this).attr('id').replace('savedDelete_','');
			var curFormId = 'addSavedItemForLaterToCart_' + item_id;			
			var itemId = null;	
			if ($('#itemId_'+item_id).val()) {
				itemId = $('#itemId_'+item_id).val();
			}			
			var itemKey = $('#item_'+item_id).val();
			if (!itemKey){
				itemKey = item_id.substr(0,8);
			}
			if (itemKey > 0 ) {
				var obj = new Object();
				obj.url= "/removeItemForLater";
				obj.data= "itemKey=" + itemKey;
				if (itemId && itemId > 0) {
					obj.data= "itemKey=" + itemKey + "&itemId=" + itemId;
				}
				obj.successFunction= "cartDisplay"; 
				obj.errorFunction = "cartError";
				createAjaxCall_Post(obj);
			}		
		}
  });
  
  $(document).on('click', '[id^="cartDelete_"]', function(event) {
		event.preventDefault();
		event.stopPropagation();
		var tmp = $(this).attr('id');
	
		if (tmp && tmp.indexOf('cartDelete_') >= 0 ) {	
			var item_id = $(this).attr('id').replace('cartDelete_','');
			if (item_id > 0) {
				var obj = new Object();
				obj.url= "/removeFromFloatingCart";
				obj.data= "i=" + item_id;
				obj.successFunction= "updateCartSuccessFun";
				obj.errorFunction = "cartError";
				createAjaxCall(obj, true);
			}
		}
	}); 
			
	$(document).on('click', '[id^="floatingCartDelete_"]', function(event) {
		event.preventDefault();
		event.stopPropagation();
		var tmp = $(this).attr('id');
		if (tmp && tmp.indexOf('floatingCartDelete_') >= 0 ) {			
			var item_id = $(this).attr('id').replace('floatingCartDelete_','');
			if (item_id > 0) {
				var obj = new Object();
				obj.url= "/removeFromFloatingCart";
				obj.data= "i=" + item_id;
				obj.successFunction= "updateFloatingCart";
				obj.errorFunction = "updateFloatingCartError";
				createAjaxCall(obj);
			}
		}
  });
	
  
  // *** add to cart from saved item area ***
  // ******This is used by PicGroup Page and ProdList Component **********
  //.addtocart and .customize is coming from picgrid page
  $(document).on('click', '.addtocart, .add, .customize ', function (event) {
	//from QuickView Cutomize
	  if($(this).attr('id') == 'quickvwCustomize' || $(this).attr('id') == 'recentlyVw' ){
		}else{
	  	event.preventDefault();
		event.stopPropagation();
		clickPos = $(window).scrollTop();
		
		if ($(this).attr("rel") == "pg") {
			if($(this).attr("class") == "btn customize"){
				window.location.href = $(this).attr("href");
			}else{
				addItemToCartFromPGRow($(this).attr("itemKey"), true);
				var itemKey = $(this).attr("itemKey");
				if( $('#quantityList_disp_'+itemKey).val() > 0){
					$('#pgCheckout').show();
					$('#pgGridCheckout').show();
  				}else{
					$('#pgCheckout').hide();
					$('#pgGridCheckout').hide();
				}
				return false;
			}
		} 
		
		if ($(this).attr("rel") == "pg_customize") {
			addItemToCartFromPGRow($(this).attr("itemKey"), true);
			return false;
		}
		
		if ($(this).attr("rel") == "quickview") {
			addItemToCartFromQuickView($(this).attr("itemKey"));
			return false;
		}
		
		if ($(this).attr("rel") == "prodlist" || $(this).attr("rel") == "prodlistcart" || $(this).attr("rel") == "accessories" || $(this).attr("rel") == "savdfrLtr") {
			
			var itemKey = $(this).attr("itemkey");
			var qty = $('#qty_'+itemKey);
			var fc = $(this).attr("fc");
			//var fc = "yes";
			var minQty = 1;
			
			/*if (qty.length > 0) {
								
				// check if entered qty is less than 1
				if (qty.val() < minQty ) {
					alert("Minimum Qty Required = " + minQty);
					return false;
				}
			}*/
			
			if (fc == 'yes') {
				if( $(this).attr("rel") == "prodlistcart" ) {
					//(itemKey, qty, customize, fc, uom, updateFcMarkup)
					if ($("#itemadded_"+itemKey).length > 0) {
						$("#itemadded_"+itemKey).hide();
					}
					addItemToShoppingCart(itemKey, qty.val(), false , true , false, true);
				} 
				else if ($(this).attr("rel") == "accessories") {
					//not using the arguments uom, updateFcMarkup hence passing empty string
					addItemToShoppingCart(itemKey, qty.val(), false , true,"","",'Y');
				}
				else if ($(this).attr("rel") == "prodlist") {
					addItemToShoppingCart(itemKey, qty.val(), false , true , null, null, null,null,null,true);
				} 
				else if ($(this).attr("rel") == "savdfrLtr") {
					var savedItemId = $(this).attr('id');
					addItemToShoppingCart(itemKey, 1, false , true , null, null, null,null,null,null,true,savedItemId);
				} else {
					addItemToShoppingCart(itemKey, qty.val(), false , true );
				
				}
			} else {
				addItemToShoppingCart(itemKey, qty.val(), false , false );
			}
			
			return false;
		}
		
		var tmp = $(this).attr('id');
		if (tmp && tmp.indexOf('savedAdd_') >= 0) {
			var key = tmp.replace('savedAdd_','');
			var curFormId = 'addSavedItemForLaterToCart_' + key;
			
			var pz = null;
			var itemId = null;			
			//var ttt = $('#'+curFormId).find("input[name='item']").attr('value');
			if ($('#pz_'+key).val()) {
				pz = $('#pz_'+key).val();				
			}
			if ($('#itemId_'+key).val()) {
				itemId = $('#itemId_'+key).val();
			}
			var itemKey = $('#item_'+key).val();
			
			if (itemId && itemId > 0) {
				$.ajax({
					type: "post",
					cache: false,
					url: "/removeItemForLater",
					data: { 
						'itemKey':itemKey,
						'itemId':itemId,
						'ajaxDate':getAjaxDate()
						
					},
					//success: function(data){
					//	addItemSavedForLaterToCartTemp(key);						
				//	},
					success: function(data){
						refreshPage();
					},
					error: function(xhr, ajaxOptions, thrownError){
						if(xhr.readyState == 0 || xhr.status == 0) {
							return;  // it's not really an error This happens due to Response already commited 
						}
						alert("System Experienced a Failure !");
					}
				});
			} else {
				$.ajax({
					type: "post",
					cache: false,
					url: "/removeItemForLater", 
					data: { 
						'itemKey'    :itemKey,
						'ajaxDate'   :getAjaxDate()
					},
				/*	success: function(data){
						addItemSavedForLaterToCartTemp(key);
					},*/
					success: function(data){
						refreshPage();
					},
					error: function(xhr, ajaxOptions, thrownError){
						if(xhr.readyState == 0 || xhr.status == 0) {
							return;  // it's not really an error This happens due to Response already commited 
						}
						alert("System Experienced a Failure !");
					}
				});
			}
		} 
	}
  }); 
  
   function refreshPage(){
	  window.location.reload(); 
  }
  
  function addItemSavedForLaterToCartTemp(id){	  
	  $('#addSavedItemForLaterToCart_' + id).action = '/addToCart';
	  $('#addSavedItemForLaterToCart_' + id).serialize();
	  $('#addSavedItemForLaterToCart_' + id).submit();	  
  }
  
  function addRemoveItemsSavedForLater(obj){
	  var tmp = obj.getAttribute('itemId');
		if (tmp && tmp.indexOf('savedAdd_') >= 0) {
			var key = tmp.replace('savedAdd_','');
			var curFormId = 'addSavedItemForLaterToCart_' + key;
			
			var pz = null;
			var itemId = null;			
			//var ttt = $('#'+curFormId).find("input[name='item']").attr('value');
			if ($('#pz_'+key).val()) {
				pz = $('#pz_'+key).val();				
			}
			if ($('#itemId_'+key).val()) {
				itemId = $('#itemId_'+key).val();
			}
			var itemKey = $('#item_'+key).val();
			
			if (itemId && itemId > 0) {
				$.ajax({
					type: "post",
					cache: false,
					url: "/removeItemForLater",
					data: { 
						'itemKey':itemKey,
						'itemId':itemId,
						'ajaxDate':getAjaxDate()
						
					},
					//success: function(data){
					//	addItemSavedForLaterToCartTemp(key);						
				//	},
					error: function(xhr, ajaxOptions, thrownError){
						if(xhr.readyState == 0 || xhr.status == 0) {
							return;  // it's not really an error This happens due to Response already commited 
						}
						alert("System Experienced a Failure !");
					}
				});
			} else {
				$.ajax({
					type: "post",
					cache: false,
					url: "/removeItemForLater", 
					data: { 
						'itemKey'    :itemKey,
						'ajaxDate'   :getAjaxDate()
					},
				/*	success: function(data){
						addItemSavedForLaterToCartTemp(key);
					},*/
					success: function(data){
						refreshPage();
					},
					error: function(xhr, ajaxOptions, thrownError){
						if(xhr.readyState == 0 || xhr.status == 0) {
							return;  // it's not really an error This happens due to Response already commited 
						}
						alert("System Experienced a Failure !");
					}
				});
			}
		} 
  }
  
  
  function cartDisplay() {
		window.location.href = '/viewCart';
  }
  function cartError(data) {
	  alert("cartError: " + data);
  }
  function createAjaxCall_Post(obj)
  {
	  createAjaxCallPost(obj);
  }
  
  // *** add add-on item into the cart. *** 
  // ******** This method is also being invoked from PicGroup Page...*****************
  $(document).on('click', '.addtocart_items', function(event) {
	  //event.preventDefault();
	  //event.stopPropagation();
	  var topPos = $(window).scrollTop();
	  if( $(this).attr("rel") == "pg") {
		  var fc = $(this).attr("fc");
		  addItemsToCartFromPicGroupAddons(fc);
		  return false;
	  }
	  
	  
	  if ($(this).attr('id') == 'addonSubmit') {
		  var aform = document.createElement("form");
		  document.body.appendChild(aform);
		  aform.action = $(this).attr('href');
		  aform.method = "post";
		  
		  var numOfItems = 0;
		  $('.addons ul.prod li div.btns, .addonsCartSlider ul.prod li div.btns').each(function() {
			  $(this).children(':input').each(function() {
				  var inputId = $(this).attr('id');
				  if (inputId.indexOf('qty_') == 0) {					  
					  var qty = $(this).attr('value');					  
					  if (qty && qty > 0) {					
						  var itemKey = inputId.replace('qty_','');					 	  
						  //alert('itemkey=' + itemKey + ", qty =" + qty);
					 	  numOfItems = numOfItems = + 1;
					 	  
						  var itemKeyField = document.createElement("input");						  
						  itemKeyField.setAttribute("type", "hidden");
						  itemKeyField.setAttribute("name", "item");
						  itemKeyField.setAttribute("value", itemKey);
						  aform.appendChild(itemKeyField);
						  	
						  var qtyField = document.createElement("input");
						  qtyField.setAttribute("type", "hidden");
						  qtyField.setAttribute("name", "qty");
						  qtyField.setAttribute("value", qty);
						  aform.appendChild(qtyField);
					  }
				  }
			  });
		  });
		  if (numOfItems > 0) {
			  aform.submit();			  
		  }
	  }
	  if($(this).attr('id') == 'addProductsOrdersToCart'){
			
		var cartForm = $('#addCartForm');
		var numChecked = 0;
		$(".sub_checkbox:checked").each(function() {
			if ($(this).is(':checked')) {
				numChecked++;
				var itemKey = $(this).attr("id").replace('checkboxList_', '');

				var itemKeyField = document.createElement("input");
				itemKeyField.setAttribute("type", "hidden");
				itemKeyField.setAttribute("name", "item");
				itemKeyField.setAttribute("value", itemKey);
				$(cartForm).append(itemKeyField);
					
				var qtyField = document.createElement("input");
				qtyField.setAttribute("type", "hidden");
				qtyField.setAttribute("name", "qty");
				qtyField.setAttribute("value", "1");
				$(cartForm).append(qtyField);
			  }
				//chkArray.push(($(this).attr("id")).replace(/checkboxList_/g, ""));
		});
		
		if (numChecked == 0) {
		   // showBBoxMessage('Add Items To Cart', 'Please select at least one item', {'width':325});
		   // alert("Please select at least one item");
			//$('#addToCartItem').trigger('click');
			showWindow("#alertShopCartWarningPop",topPos);
		  } else {
			cartForm.submit();
		  }
	  }
	  return false;
  });
  
  // *** add warranty to cart ***
  $(document).on('click','.warrantyBtn', function(event) {	
		var keys = $(this).attr('id');
		if (keys.indexOf("warranty_") >= 0) {
			var len = keys.indexOf('_');
			var key = keys.substring(len + 1);
			if (document.getElementById('selectedWarranty')) {
				document.getElementById('selectedWarranty').value = key;
			}
					
			//alert ("key = " + key);
			
			if($("#warranty_selection").length > 0) {
				$("#warranty_selection").val(key);
			}
		  } else if ($(this).attr('origin') == "Add_To_Cart"){
				 warrantyAddPlansCart();				 
		  }
  });
	
  $(document).on('click', '#addWarrantyBtn', function(event) {
	  if ($(this).attr("origin") == "Add_Product_Details") {
		  if ($("#warranty_selection").val() == ""){
			  $("#warranty_selection").val($(this).attr("itemKey"));
		  }
		  //submitProduct(false, $("#itemKey_Main").val() , 0);
		  addItemToShoppingCart($('#itemKey_Main').val(), $('#itemQty').val(), false, false);
		  return false;
		  
	  } else if ($(this).attr("origin") == "Add_To_Cart") {
			warrantyAddPlansCart();
	  }else if ($(this).attr("origin") == "Checkout_Cart_Warr_Add") {
		  warrantyAddPlans('checkout','2');
	  }else {
		  warrantyAddPlans('cart','2');
	  }
  });
  function warrantyNoThanks(action)
  {
		var cartForm = document.getElementById('cartUpdateForm');
		if(!cartForm)
		{
			window.location.href = "/viewCart";
			return;
		}
		
		if(action == "cart")
		{
			hideBBox();
		}
		else if(action == "paypal")
		{
			cartForm.action='/updateCart';
			document.getElementById('scNextAction').value = 'checkoutPayPal';
			cartForm.submit();
		}
		else if(action == "checkout")
		{
			cartForm.action='/updateCart';
			document.getElementById('scNextAction').value = 'checkout';
			cartForm.submit();
		}
		else if(action == "checkoutMember")
		{
			cartForm.action='/updateCart';
			document.getElementById('scNextAction').value = 'checkoutMember';
			cartForm.submit();
		}
  }

  function warrantyAddPlans(action,checkoutVersion)
  {	
		$('warrantyButtonsEnabled').hide();
		$('warrantyButtonsDisabled').show();
		$.ajax({
			type: "get",
			url: '/warrantySave?ajaxDate='+getAjaxDate(), 
			cache: false,
			data: $("#warrantyForm").serialize(),
			success: function(data){
				if (data.indexOf('pcs-error-messages') < 0) {
					//warrantyAddPlansAfterSubmit(action, checkoutVersion);	
					if(action == "checkout"){
						var aform = document.createElement("form");
						  document.body.appendChild(aform);
						  aform.action = "/updateCart";
						  aform.method = "post";
						  var itemKeyField = document.createElement("input");						  
						  itemKeyField.setAttribute("type", "hidden");
						  itemKeyField.setAttribute("name", "nextAction");
						  itemKeyField.setAttribute("value", action);
						  aform.appendChild(itemKeyField);
						  aform.submit();
					}else{
					window.location.href = "/updateCart";
					}
				} else {
					warrantyNoThanks(action);
				}
			},
			error: function(xhr, ajaxOptions, thrownError){
				if(xhr.readyState == 0 || xhr.status == 0) {
					return;  // it's not really an error This happens due to Response already commited 
				}
				warrantyNoThanks(action);
			}
		});
		
  }
  /** called from addToCart.jsp. **/
  function warrantyAddPlansCart() {
	  var tmp = $('#newQty').text();
	  var qty = 0;
	  if (!isNaN(parseInt(tmp))) {
		  qty = parseInt(tmp);
	  }
	  if (qty > 0){
		  $('#added_qty').val(tmp);
		  $.ajax({
				type: "get",
				url: '/warrantySave?ajaxDate='+getAjaxDate(),
				cache: false,
				data: $("#warrantyForm").serialize(),
				success: function(data){
					//
				},
				error: function(xhr, ajaxOptions, thrownError){
					if(xhr.readyState == 0 || xhr.status == 0) {
						return;  // it's not really an error This happens due to Response already commited 
					}
				}
			});
	  }
  }
  function warrantyAddPlansAfterSubmit(action, checkoutVersion)
  {	
		var cartForm = document.getElementById('cartUpdateForm');
		if(!cartForm)
		{
			if(action == "checkout")
			{
				if (checkoutVersion=="v2"){
					window.location.href = "/checkout/signin";
				} else {
					window.location.href = "/checkout/guest";
				}
			}
			else if(action == "checkoutMember")
			{
				if (checkoutVersion=="v2"){
					window.location.href = "/checkout/signin";
				} else { 			
					window.location = "/checkout/member";
				}
			}
			else if(action == "cart"){
				window.location.href = "/checkout/view";
			}
			
			return;
		}
		
		cartForm.action='/updateCart';
		
		if(action == "cart")
		{
			document.getElementById('scNextAction').value = '';
		}
		else if(action == "paypal")
		{
			document.getElementById('scNextAction').value = 'checkoutPayPal';
		}
		else if(action == "checkout")
		{
			document.getElementById('scNextAction').value = 'checkout';
		}
		else if(action == "checkoutMember")
		{
			document.getElementById('scNextAction').value = 'checkoutMember';
		}
		
		cartForm.submit();
  }

  function showWarrantyDescriptionOfCoverage()
  {
	document.getElementById("fadeBg").style.zIndex = 200;
	document.getElementById("bbox").style.display = "none";
	var warrantyPopup = document.getElementById('warrantyCoveragePopup');
	warrantyPopup.style.display = "";
	centerBBox('warrantyCoveragePopup');
  }
	
  function hideWarrantyDescriptionOfCoverage()
  {
	$('warrantyCoveragePopup').hide();
	document.getElementById("bbox").style.display = "";
	document.getElementById("fadeBg").style.zIndex = 10;	
  }
	
  // *** cart calculate shipping ***
  $(document).on("click", "a.shipping", function(event) {
		event.preventDefault();
		var myId = $(this).attr('id');
		$('#totalzip').hide();
		if (myId == 'cartCalShip') {
			
			$.ajax({
				url: '/calculateShippingPage?ajaxDate='+getAjaxDate(), 
				cache: false,
				success: function(returnData) {
					$("#enterzip").html(returnData);
					$("#enterzip").css("display", "block");
				},
				error: function(xhr, ajaxOptions, thrownError){
					if(xhr.readyState == 0 || xhr.status == 0) {
						return;  // it's not really an error This happens due to Response already commited 
					}
					alert("error in calculate shipping: " + e);
				}
			});
		}		
	});

  function calculateShippingFromCart()
  {
		var obj = new Object();
		obj.url = $("#shipCalcCartForm").attr('action');
		obj.data = $("#shipCalcCartForm").serialize();
		obj.successFunction = "updateShippingPrice";
		obj.errorFunction = "calculateShippingErrorFun";
		createAjaxCall(obj);
		return false;
  }
function getSelectedCountry()
{
	var country = $(".cntry");
	var countryKey = 0;
	if(country.length == 1 && country[0].id == "countryKey") {
		countryKey = country[0].value;
	} else {
		country = document.getElementById("countryKey");
		countryKey = country.options[country.selectedIndex].value;
	}
	// alert("countryKey: "+ countryKey);
	return countryKey;
}
var shipDesc = "";
function getSelectedShippingMethod()
{
	var countryKey = getSelectedCountry();
	var ship = document.getElementById("shipMet_"+countryKey);
	var shipSel = "";
	shipDesc = "";
	if (ship) {
		shipSel = $("#shipMetCsKey_"+countryKey).attr("value");
		shipDesc = document.getElementById("shipMet_"+countryKey).innerHTML;

	} else {
		shipSel = $("#shippingMethods_"+countryKey+" option:selected").attr("value");
		var ar = document.getElementById("shippingMethods_"+countryKey);
		shipDesc = ar.options[ar.selectedIndex].text;
		//alert("3: shipSel = " + shipSel + ", shipDesc =" + shipDesc +", " + ar.options[ar.selectedIndex].value);
	}
	// alert("shipSel = " +shipSel + ", shipDesc =" + shipDesc);
	return shipSel;
}
  $(document).on('click', '#calculateBtn_cart', function(event) {
	  event.preventDefault();
	  //event.stopPropagation();
	  if (document.getElementById("shippingCostZip").value == "") {
		$("#shippingCostZip").addClass("highlight");
		$("div.error_sec").removeClass("hide");
	}
	  
	else {
		$("#ajaxIndicator").show();
		$(".disableMask").show();
		$("#enterzip").hide();
	  
	  var zip = $("#shippingCostZip").attr('value');
	  
	  if (zip) {
		  var shipSel = getSelectedShippingMethod();
		  var countryKey = getSelectedCountry();
		  
		  $.ajax({
			  url : '/calculateShipping',
			  type: 'post',
			  cache: false,
			  data: { 
					'zip'				    : zip,
					'countryKey'	        : countryKey,
					'carrierServiceKey'		: shipSel,
					'ajaxDate'              : getAjaxDate()
						   
		       },
		  	  success: function(returnData) {
				  $("#ajaxIndicator").hide();
				  $(".disableMask").hide();
		  		 // $(this).parent("div").hide();
              if (returnData.indexOf("validation-messages") > -1) {
					  highLightFormField(returnData);
					  $("#enterzip").show();
					  //alert(returnData);
				  } else {
					  
			  		  document.getElementById("shipMethod").innerHTML = shipDesc;
			  		  document.getElementById("shipCostZip").innerHTML = zip;
			  		  updateShippingPrice(returnData);
				  }
		  	  },
		  	error: function(xhr, ajaxOptions, thrownError) {
		  		if(xhr.readyState == 0 || xhr.status == 0) {
					return;  // it's not really an error This happens due to Response already commited 
				}
		  		calculateShippingErrorFun(xhr.responseText);
		  	}
		  });
	  }
	}
  });
  
  function updateShippingPrice(data) {
		
		if((data.indexOf('pcs-error-messages') < 0) && (data.indexOf('canada-postal-alert') < 0) && (data.indexOf('usa-postal-alert') < 0))
		{
			if (data.indexOf("{") > 0) {
				//var jsonStr = $.parseJSON(data);
				//alert(jsonStr);
				eval("var jsonStr = " + data);
				
				var shippingCostZipSection = document.getElementById("shippingCostZipSection");
				var shippingCostZip = document.getElementById("shippingCostZip");
				
				var zipToDisplay = jsonStr.zip;
				/*if(shippingCostZipSection && shippingCostZip && zipToDisplay != "")
				{
					shippingCostZip.innerHTML = jsonStr.zip;
					shippingCostZipSection.style.display = "";
				}*/
				
		   		var shippingOnCartPage = document.getElementById("shippingOnCartPage");
		   		if(shippingOnCartPage)
		   		{
		   			shippingOnCartPage.innerHTML = jsonStr.shippingText;
		   		}
		   		
		   		var totalOnCartPage = document.getElementById("totalOnCartPage");
		   		if(totalOnCartPage)
		   		{
		   			totalOnCartPage.innerHTML = jsonStr.cartTotal;
		   		}
		   		var shipMethod = jsonStr.shipMethod;
		   		if (shipMethod) {
		   			document.getElementById("shipMethod").innerHTML = shipMethod;
		   		}
		   		var calculationDetailsSection = document.getElementById("calculationDetailsSection");
		   		if(calculationDetailsSection)
		   		{
		   			calculationDetailsSection.innerHTML = jsonStr.calculationDetails;
		   		}
		
				gaEvent('Shipping', 'Calculate', 'Shopping Cart', 0);
		
					if(jsonStr.showQuoteAlert)
				{
			
					$('#calculateshippingpopupid').trigger( "click" );
					
				}
		   		else
		   		{
		   			//hideBBox();
		   		}
		   		$("span.enter").hide();
		   		$('#enterzip').hide();
				$("#totalzip").css("display", "block");
				$('ziploading').css("display", "none");
			} 
			
		} else if (html.indexOf('canada-postal-alert') >= 0)
		{
			//showBBoxText('Canada Website', html);
		}
		else if (html.indexOf('usa-postal-alert') >= 0)
		{
			//showBBoxText('USA Website', html);
		}		   			
		else {
			// error
			$('#shippingCalcResult').hide();
			$('#calcShippingErrors').innerHTML = data;
			$('#calcShippingErrorsSection').show();	
		}
  }
	
  function calculateShippingErrorFun(data){
	  	alert("error: " + data);
		$("#calcShippingErrors").html(data);
		$("#calcShippingErrorsSection").show();
  }
  function toggleShippingMethodList(countryKey)
  {
  	if(countryKey == 2)
  	{
  		document.getElementById("shippingMethods").style.display = "none";
  		document.getElementById("shippingMethodsCanada").style.display = "";
  	}
  	else
  	{
  		document.getElementById("shippingMethodsCanada").style.display = "none";
  		document.getElementById("shippingMethods").style.display = "";
  	}
  }
  
  function toggleShippingMethodList_New(countryKey)
  {
  	$('.shippingMethods_New').each(function(){
		$(this).hide();
	});
	$('#divShippingMethods_'+countryKey).show();
  }
  function setShipMethodDesc(){
	  var ship = $("#shipMet");
	  var shipSel = "";
	  
	  if (ship) {
		  shipSel = $("#shipMet").innerHTML;
	  } else {
		  var keySel = $("#shippingMethods option:selected").attr("value");
		  shipSel = $('#'+keySel).innerHTML;
		  var ar = document.getElementById("shippingMethods");
		  var tmp = ar.options[ar.selectedIndex].text;
		  //alert("shipSel = " + shipSel + ", tmp =" + tmp);
	  }
	  document.getElementById("shipMethod").innerHTML = shipSel;
  }
  function calculateShippingAjax(zip){
	  if (zip != null && zip != '') {
		  $.ajax({
			  url : '/calculateShippingAjax',
			  type: 'get',
			  cache: false,
              data: { 
			       'zip'				: zip,
				   'ajaxDate'           : getAjaxDate()					
		      },		  	
      		success: function(returnData) {
				  $("#ajaxIndicator").hide();
				  $(".disableMask").hide();
	              if (returnData.indexOf("validation-messages") > -1) {
						  highLightFormField(returnData);
						  $("#enterzip").show();
				  } else {
					  //document.getElementById("shipMethod").innerHTML = shipDesc;
			  		  document.getElementById("shipCostZip").innerHTML = zip;
			  		  updateShippingPrice(returnData);
				  }
			  },
			  error: function(returnData) {
		  		//calculateShippingErrorFun(returnData);
		  	  }
		  });
	  }
	
  }
  // *** product rule key change ***/
  var productRuleActionValuesInputBox
  function onProdRuleActionChange(selectBox, tdClass) {
	
	 	  $('td.'+tdClass).find('div.updDiv').show();
	  
	if (selectBox)
	{	
		var id = selectBox.id;
		
		var productRuleActionKeysInputBox = document.getElementById('productRuleActionKeys_'+selectBox.id);
		productRuleActionValuesInputBox = document.getElementById('productRuleActionValues_'+selectBox.id);
		
		var tmpVal = selectBox[selectBox.selectedIndex].value; // selectBox.val();
		
		divProductRuleAction =  document.getElementById('divProductRuleAction_'+selectBox.id);
		var selectedProductRuleActionKey =  tmpVal;
		var actionResp = '';
		if (tmpVal.indexOf('_') > 0) {
			selectedProductRuleActionKey = tmpVal.substring(0, tmpVal.indexOf('_')); //selectBox[selectBox.selectedIndex].value;
			actionResp = tmpVal.substring(tmpVal.indexOf('_') + 1);
			divProductRuleAction.innerHTML = "<label>"+actionResp+":</label> <input id=\""+id+"\" type=\"text\" value=\"\" onchange=\"getKeyCode(this.value,id);\" />";
		} else {
			divProductRuleAction.innerHTML = '';
		}		
		productRuleActionKeysInputBox.value = selectedProductRuleActionKey;
		productRuleActionValuesInputBox.value = '';
	}
  }
	
  function getKeyCode(tvalue, id) {
	  
	  productRuleActionValuesInputBox.value = tvalue;
	  document.getElementById('productRuleActionValues_'+id).value = tvalue;
  }
  
  var applyingCartPromotion = false;
  function applyCartDiscount()
  {
  	if(applyingCartPromotion)
  	{
  		return false;
  	}
  	applyingCartPromotion = true;
  	
  	var discountCode = $("#discountCode").val();
  	if(!discountCode || discountCode == null || discountCode == undefined || discountCode == '' || discountCode == 'key code')
  	{
  		applyingCartPromotion = false;
  		return false;
  	}

  	var obj = new Object();
	obj.url = $("#applyCartDiscountForm").attr('action');
	obj.data = $("#applyCartDiscountForm").serialize();
	obj.successFunction = "applyCartDiscountSuccessFunc";
	createAjaxCall_Post(obj);
	return false;
  	
  }
  function applyCartDiscountSuccessFunc(data){
	  if (data.indexOf("validation-messages") > -1) {
			$("#cartApplyDiscountMsgDiv").html( data);
			applyingCartPromotion = false;
			setTimeout(function() {
			$("#cartApplyDiscountMsgDiv").fadeIn(250);
			$("#cartApplyDiscountMsgDiv").hide('blind', {}, 250);
		}, 3000);
		}else{
			eval("var jsonResult = " + data);
			$("#cartApplyDiscountMsgDiv").html( jsonResult.promoResultText);
			$("#cartApplyDiscountMsgDiv").fadeIn(250);
		}
  }
  // from viewSavedCarts page
  function deleteCartConfirm(cartKey)
  {
  	document.getElementById('deleteCartId').value = cartKey;
  }

  function deleteCart()
  {
	  $.ajax({
			type: "GET",
			url: "/deleteSavedCart",
			data: {			
			'cartId'   : document.getElementById('deleteCartId').value,
            'ajaxDate' : getAjaxDate()		
			},

			success: function(data){
				document.location.reload();
			},
			error: function(xhr, ajaxOptions, thrownError) {
		  		if(xhr.readyState == 0 || xhr.status == 0) {
					return;  // it's not really an error This happens due to Response already commited 
				}
		  	}
		});
  }
  
//customzie redirect url from search results and picgrid quickview  
  function redirectUrlForCustomize(url){
  	window.location.href = url;
  }

  

  // ---