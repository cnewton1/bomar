<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';

include 'pager.php';
?>

<?php
$where_condition = "1";
$page = ($_REQUEST['page'] > 1) ? $_REQUEST['page'] : 1;
$page_name = 'articles.php';
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');
$page = ($_GET['page'] > 1) ? $_GET['page'] : 1;
$pagestring = '?' . $req_parameters . '&page=';
$adjacents = 3;
$totalitems = mysql_fetch_array(mysql_query("SELECT COUNT(DISTINCT `id`) AS `num` FROM `articles` WHERE $where_condition"));

$total = $totalitems['num'];
$limit = 10;
$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;
$limit = $pager->limit;
$page = $pager->page;
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

<div id="home_body">


    <div id="midsec">




        <div class="container">
            <div class="row">

                <div class="topsell products">
                    <h1 style="font-size:28px; font-weight: bold; color: white; background:#024d86; padding: 5px;">Our Specials</h1>
                    <h1></h1>
                    <div style="clear: both" >
                        <?php
                        echo getPaginationString($page, $total, $limit, $adjacents, $page_name, $pagestring);
                        ?>    
                    </div>
                    <ul class="prod">

                        <?php
                        $query = mysql_query("SELECT * FROM `articles` WHERE $where_condition ORDER BY `id` DESC LIMIT $offset, $limit");
                        if (mysql_num_rows($query)) {
                            while ($row = mysql_fetch_array($query)) {
                                $article_id = $row['id'];
                                $title = $row['title'];
                                 $description = htmlspecialchars_decode(html_entity_decode($row['description']));
                                 $description = strip_tags($description);
                                 $description = (strlen($description) > 500)?substr($description,0,500).'...':$description;
                                 $imagename = $row['imagename'];
                                
                                echo '<div style="width:100%; clear:both">

                                    <div style="width:30%; float:left">
                                    &nbsp;
                                    ';
                                    if ($imagename != '' && file_exists("mainimage/$imagename")) {
                                        echo "<img src=\"mainimage/$imagename\" style=\"max-width:100%;\"><br><br>";
                                    }

                                echo '

                                    </div>
                                    <div  style="width:70%; float:left; padding:10px;">
                                    <h3 style="font-weight:bold; font-size:22px; line-height:30px;">'.$title.'</h3>
                                    <p style="line-height:20px; text-align:justify">'.$description.'</p>
                                    <p><a href="article.php?id='.$article_id.'" style="line-height:20px; font-weight:bold; color:blue;">More...</a></p>

                                    </div>

                                </div>';
                            }
                        }
                        ?>




                    </ul>
                    <div style="clear: both" >
                        <?php
                        echo getPaginationString($page, $total, $limit, $adjacents, $page_name, $pagestring);
                        ?>    
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>






<?php include 'footer.php'; ?>



</body>
</html>