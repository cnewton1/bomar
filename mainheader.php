<?php
include 'users.php';
include 'connect.php';
require_once('global_functions.php');
if (in_array($_SERVER['HTTP_HOST'], array('westcarb.com', 'www.westcarb.com'))) {
    mysql_set_charset('utf8', $linkID);
}
ob_get_clean();
ob_start();
$css_version = '20210823';

$meta['title'] = 'Westcarb Inc..com - Material Handling Equipment|Workbenches|Furniture|Tools|Motors|HVAC';
$meta['keywords'] = 'Material Handling, Pallet Jack, Industrial Supplies, Storage, Shelving, Carts, Furniture, Workbench, Janitorial, Fans, Heaters, Air Conditioners, Ladders, Pallet Racks, Cabinets, Chairs, Desks, Bins, Containers, Lighting, Lockers, Cleaning Supplies, Garbage Cans';
$meta['description'] = 'Westcarb Inc is a Leading Distributor of Material Handling Equipment, Storage Solutions, Workbenches, Office Furniture, Safety Equipment, Tools, Motors, HVAC Equipment and more, Carrying over 1,000,000 Commercial and Industrial Products at low prices.';

if (isset($page_name) && $page_name == 'product_details') {
    $ID = mysql_real_escape_string($_REQUEST['id']);
    $product_query = mysql_query("SELECT * FROM `product2` WHERE `id` = '$ID'");
    if (mysql_num_rows($product_query)) {
        $row = mysql_fetch_array($product_query);
        $meta['title'] = $row['prodname'] . ' | ' . $meta['title'];
        $meta['keywords'] = htmlspecialchars($row['prodname'] . ', ' . $row['mfgpart']) . ', ' . $meta['keywords'];
        $meta['description'] = htmlspecialchars($row['description']) . ' ' . $meta['description'];
    }
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

    <title><?php echo $meta['title']; ?></title>


    <meta name="keywords" content="<?php echo $meta['keywords']; ?>" />

    <meta name="description" content="<?php echo $meta['description']; ?>" />
    <link rel="canonical" href="/" />
    <meta name="robots" content="index, follow" />

    <!--[if IE 8]>
            <script src="styles/js/html5.js" type="text/javascript"></script>
        <![endif]-->

    <!--[if IE 7]>
         <script src="styles/js/modernizr.custom.IE7.js" type="text/javascript"></script>
        <![endif]-->

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="styles/reset.css?1414752269324" />
    <link rel="stylesheet" type="text/css" href="styles/common.css?1414752269336" />
    <link rel="stylesheet" type="text/css" href="styles/btns.css?var=<?php echo $css_version; ?>" />
    <link rel="stylesheet" type="text/css" href="styles/header.css?20160831" />
    <link rel="stylesheet" type="text/css" href="styles/leftnav.css?1414752279650" />
    <link rel="stylesheet" type="text/css" href="styles/footer.css?20160801" />
    <link rel="stylesheet" type="text/css" href="styles/prod.css?var=<?php echo $css_version; ?>" />
    <link rel="stylesheet" type="text/css" href="styles/sort.css?1414752279688" />
    <link rel="stylesheet" type="text/css" href="styles/layouts.css?1414752279702" />
    <link rel="stylesheet" type="text/css" href="styles/sitemapShopCat.css?1414752279719" />
    <link rel="stylesheet" type="text/css" href="styles/slider.css?1414752279736" />
    <link rel="stylesheet" type="text/css" href="styles/featureProductsTabs.css?1414752279751" />
    <link rel="stylesheet" type="text/css" href="styles/shopByTabs.css?1414752279765" />
    <link rel="stylesheet" type="text/css" href="styles/picGroupGrid.css?1414752279777" />
    <link rel="stylesheet" type="text/css" href="styles/cart.css?1414752279792" />
    <link rel="stylesheet" type="text/css" href="styles/cart_prog.css?1414752279803" />
    <link rel="stylesheet" type="text/css" href="styles/checkout.css?1414752279816" />
    <link rel="stylesheet" type="text/css" href="styles/account.css?1414752279831" />
    <link rel="stylesheet" type="text/css" href="styles/popUps.css?1414752279843" />

    <link rel="stylesheet" type="text/css" href="js/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="js/slick/slick-theme.css?v=2333332333" />



    <script type="text/javascript">
        var contentPath = '';
    </script>


    <script src="bootstrap/js/jquery-1.11.3.min.js"></script>
    <script src="styles/js/jquery-ui.min.js?1414752279876" type="text/javascript"></script>
    <script src="styles/js/modules.js?1414752279883" type="text/javascript"></script>
    <script src="styles/js/popup.js?1414752279895" type="text/javascript"></script>
    <script src="styles/js/cart.js?1414752279907" type="text/javascript"></script>
    <script src="styles/js/jquery.jcarousel.min.js?1414752279917" type="text/javascript"></script>
    <!-- the mousewheel plugin - provides mousewheel support -->
    <script src="styles/js/jquery.mousewheel.js?1414752279928"></script>
    <script src="styles/js/mwheelIntent.js?1414752279939"></script>
    <!-- the mousewheel plugin-->
    <script src="styles/js/jquery.jscrollpane.min.js?1414752279951" type="text/javascript"></script>
    <script src="styles/js/jquery.scrollTo.min.js?1414752280036"></script>


    <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
    <link rel="stylesheet" href="login_box.css" />
    <link rel="stylesheet" href="font-awesome-4.2.0/css/font-awesome.min.css" />



    <style>
        div.pagination {
            padding: 3px;
            margin: 3px;
            text-align: center;
            color: #000;
            font-size: 16px;
        }

        div.pagination a {
            border: 1px solid #909090;
            margin-right: 3px;
            padding: 2px 5px;
            background-image: url('bar.gif');
            background-position: bottom;
            text-decoration: none;
            color: #000;
            font-size: 16px;
        }

        div.pagination a:hover,
        div.meneame a:active {
            border: 1px solid #f0f0f0;
            background-image: url(invbar.gif);
            background-color: #404040;
            color: #000;
            font-size: 16px;
        }

        div.pagination span.current {
            margin-right: 3px;
            padding: 2px 5px;
            border: 1px solid #ffffff;
            font-weight: bold;
            background-color: #606060;
            color: #000;
            font-size: 16px;
        }

        div.pagination span.disabled {
            margin-right: 3px;
            padding: 2px 5px;
            border: 1px solid #606060;
            color: #000;
            font-size: 16px;
        }

        .page_title {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16px;
            color: #00F;
            font-weight: bold;
            padding: 10px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="css/default.css?var=<?php echo $css_version; ?>" />
    <link rel="stylesheet" type="text/css" href="css/component.css?var=<?php echo $css_version; ?>" />
    <script src="js/modernizr.custom.js"></script>
    <script>
        $(document).ready(function() {

            function calculate_total_order_amount() {
                var shipping_amount = 0;

                $('.shipping-rates input[type=radio]').each(function() {
                    var $this = $(this);
                    var shipping_rate = $this.data('amount');

                    if ($(this).prop('checked')) {
                        console.log(shipping_rate);
                        shipping_amount = shipping_amount + parseFloat(shipping_rate);
                    }

                });
                shipping_amount = shipping_amount.toFixed(2);
                $('#TOTAL_SHIPPING_AMOUNT_HTML').html(shipping_amount);
                $('#TOTAL_SHIPPING_AMOUNT').val(shipping_amount);

                var TOTAL_COST_SUBTOTAL = $('#TOTAL_COST_SUBTOTAL').val();
                var DISCOUNT_AMOUNT = $('#DISCOUNT_AMOUNT').val();
                var grand_total = parseFloat(TOTAL_COST_SUBTOTAL) + parseFloat(shipping_amount) - parseFloat(DISCOUNT_AMOUNT);
                grand_total = grand_total.toFixed(2);
                $('#GRAND_TOTAL_HTML').html(grand_total);
                $('#GRAND_TOTAL').val(grand_total);
            }
            $('.shipping-rates input[type=radio]').click(function() {
                calculate_total_order_amount();
            });


            $('#apply_discount').click(function() {
                $('#discount_msg').hide();
                var discount_code = $('#discount_code').val();
                if (discount_code != '') {
                    $.ajax({
                            method: "POST",
                            url: "ajax_coupon.php",
                            data: {
                                discount_code: discount_code
                            }
                        })
                        .done(function(msg) {
                            if (msg == 'success') {
                                location.reload();
                            } else {
                                $('#discount_msg').show();
                                $('#discount_msg').html(msg);
                            }
                        });

                } else {
                    $('#discount_msg').show();
                    $('#discount_msg').html('Please enter discount code.');
                }
            });
            $(".dropdown").hover(
                function() {
                    $(".dropdown-menu .dropdown").removeClass('open');
                    // $(this).addClass("open");
                    var dropdownMenu = $(this).children(".dropdown-menu");
                    dropdownMenu.parent().addClass("open");
                    if (dropdownMenu.is(":visible")) {
                        //dropdownMenu.parent().toggleClass("open");
                    }

                },
                function() {
                    // $(this).removeClass("open");
                }
            );
            $(".dropdown-menu").hover(
                function() {},
                function() {
                    $(this).parent().removeClass("open");
                }
            );

        });
    </script>
    <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" href="ie8-and-down.css" />
        <![endif]-->


    <style>
        @media (min-width: 1200px) {
            .container {
                width: 1300px;
                margin: 0px auto;
            }

        }

        .cbp-hsinner {
            background: #7a7fbe;
        }

        .slick-next,
        .slick-prev {
            content: "";
            z-index: 999;
        }

        .slick-prev,
        .slick-next,
        .slick-prev:hover,
        .slick-next:hover {
            background: #7a7fbe;

        }

        .slick-dots {
            list-style-type: none !important;
            list-style-position: initial;
            list-style-image: initial;
            text-align: center;
            position: absolute;
            padding-top: 0px;
            z-index: 999;
            bottom: 30px;
            margin-top: 0px;
            margin-right: auto;
            left: 0px;
            right: 0px;
        }

        .slick-dots li {
            display: inline-block;
            margin-top: 2px;
            list-style-type: none !important;
            list-style-position: initial;
            list-style-image: initial;
        }

        .slick-dots li button {
            font-size: 0px;
            width: 40px;
            height: 12px;
            background-color: rgb(255, 255, 255);
        }

        .slick-dots li.slick-active button {
            background-color: #7a7fbe;
        }

        .dropup,
        .dropdown {
            position: relative;
            z-index: 99999;
        }

        .dropdown-toggle:focus {
            outline: 0;
        }

        .dropdown-menu {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 160px;
            padding: 5px 0;
            margin: 2px 0 0;
            font-size: 14px;
            text-align: left;
            list-style: none;
            background-color: #7a7fbe;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: 4px;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            white-space: inherit;
        }

        .dropdown-menu.pull-right {
            right: 0;
            left: auto;
        }

        .dropdown-menu .divider {
            height: 1px;
            margin: 9px 0;
            overflow: hidden;
            background-color: #7a7fbe;
        }

        .dropdown-menu>li>a {

            display: flex;
            justify-content: space-between;
            padding: 3px 20px;
            clear: both;
            font-weight: normal;
            line-height: 1.42857143;
            color: #fff;
            white-space: nowrap;
        }

        .dropdown-menu>li>a:hover,
        .dropdown-menu>li>a:focus {
            color: #000;
            text-decoration: none;
            background-color: #fff;
        }

        .dropdown-menu>.active>a,
        .dropdown-menu>.active>a:hover,
        .dropdown-menu>.active>a:focus {
            color: #fff;
            text-decoration: none;
            background-color: #000;
            outline: 0;
        }

        .dropdown-menu>.disabled>a,
        .dropdown-menu>.disabled>a:hover,
        .dropdown-menu>.disabled>a:focus {
            color: #777;
        }

        .dropdown-menu>.disabled>a:hover,
        .dropdown-menu>.disabled>a:focus {
            text-decoration: none;
            cursor: not-allowed;
            background-color: transparent;
            background-image: none;
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        }

        .open>.dropdown-menu {
            display: block;
        }

        .open>a {
            outline: 0;
        }

        .dropdown-menu-right {
            right: 0;
            left: auto;
        }

        .dropdown-menu-left {
            right: auto;
            left: 0;
        }

        .dropdown-header {
            display: block;
            padding: 3px 20px;
            font-size: 12px;
            line-height: 1.42857143;
            color: #777;
            white-space: nowrap;
        }

        .dropdown-backdrop {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 990;
        }

        .pull-right>.dropdown-menu {
            right: 0;
            left: auto;
        }

        .dropup .caret,
        .navbar-fixed-bottom .dropdown .caret {
            content: "";
            border-top: 0;
            border-bottom: 4px dashed;
            border-bottom: 4px solid \9;
        }

        .dropup .dropdown-menu,
        .navbar-fixed-bottom .dropdown .dropdown-menu {
            top: auto;
            bottom: 100%;
            margin-bottom: 2px;
        }

        @media (min-width: 768px) {
            .navbar-right .dropdown-menu {
                right: 0;
                left: auto;
            }

            .navbar-right .dropdown-menu-left {
                right: auto;
                left: 0;
            }
        }

        .dropdown-menu>li.open>a {
            background-color: #fff !important;
            color: #000 !important;
        }
    </style>

    <link href="bootstrap/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
</head>

<body>
    <?php
    $totalItems = 0;
    if ($_SESSION['user'] != '' || $_COOKIE['newuser'] != '') {
        if ($_SESSION['user'] != '') {
            $current_user = $_SESSION['user'];
        } else {
            $current_user = $_COOKIE['newuser'];
        }
        $count_cart_items = mysql_fetch_array(mysql_query("SELECT sum(`qty`) AS `total_qty` FROM `cart` WHERE `userid`= '$current_user'"));
        $totalItems = ($count_cart_items['total_qty'] != '') ? $count_cart_items['total_qty'] : '0';
    }
    $current_userid = getCurrentuserid();
    $count_cart_items = count_cart_items($current_userid);
    ?>
    <div class="responsive">

        <div class="container">
            <header class="clearfix">
                <h1><a href="http://www.westcarb.com"><img src="images/logo.jpg"></a></h1>
                <h1 class="header-additional-img"><a href="gsa.php"><img src="GSALOGO.png"></h1></a>
                <h1 class="header-additional-img"><img src="images/FreeShipImage.jpg"></h1>
                <nav>
                    <div style="text-align: left;font-size: 30px;color: blue;">You Click! WE Ship!</div>
                    <?php

                    if ($_SESSION['username'] != '') {
                        echo '<div  style="display:block; text-align:right;">Welcome ' . $_SESSION['username'] . '</div>';
                        echo '<span style="float:left" > <a href="contactus.php" class="btn1">Contact Us</a> </span>';
                        echo '<span style="float:left" ><a href="myProfile.php" class="btn1">My Account</a></span>';
                    } else {
                        echo '<span style="float:left" > <a href="contactus.php" class="btn1">Contact Us</a> </span>';
                        echo '<span style="float:left" ><a href="JoinMember1.php" class="btn1">Account Registration</a></span>';
                    }
                    ?>
                    <span style="float:left; margin-left: 10px;"><a href="addToCart.php" class="btn1" style="color:#fff"><i class="fa fa-shopping-cart"></i> View Cart - <?php echo $count_cart_items; ?> Item(s)</a></span>
                </nav>
            </header>
            <div class="main">
                <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                    <div class="cbp-hsinner">
                        <ul class="cbp-hsmenu">
                            <li class="dropdown">
                                <a href="#">Categories <span class="caret"></span></a>

                                <?php
                                $category_name = $_GET['category'];
                                $subcategory_name = $_GET['subcategory'];

                                $category_data = [];
                                $resultID = mysql_query("SELECT `category`,`subcategory` FROM `product2` 
                                WHERE 
                                `category` != '' AND `subcategory` != ''                                       
                                GROUP BY `category`, `subcategory` ORDER BY `category`, `subcategory`");

                                while ($row = mysql_fetch_assoc($resultID)) {
                                    if (!isset($category_data[$row['category']][$row['subcategory']])) {
                                        $category_data[$row['category']][$row['subcategory']] = true;
                                    }
                                    $category_data[$row['category']][$row['subcategory']] = array_filter($category_data[$row['category']][$row['subcategory']]);
                                }

                                if (!empty($category_data)) {
                                    echo '<ul class="dropdown-menu">';
                                    foreach ($category_data as $category_manu => $subcat_arr) {

                                        echo '<li class="dropdown">
                                        <a href="#">' . $category_manu . '<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                        ';

                                        foreach ($subcat_arr as $subcat_menu => $subcat2_arr) {

                                            echo '<li><a href="search.php?subcategory=' . urlencode($subcat_menu) . '&category=' . urlencode($category_manu) . '">' . $subcat_menu . '</a></li>';
                                        }
                                        echo '</ul></li>';
                                    }
                                    echo '</ul>';
                                }
                                ?>

                            </li>
                            <li>
                                <a href="FaceMask.php">Face Mask Protection</a>
                            </li>
                            <li>
                                <a href="IHardware.php">Industrial Hardware</a>

                            </li>

                            <li class="dropdown">
                                <a href="video_hurricane.php">Hurricane Protection <span class="caret"></span></a>
                            </li>

                            <li>
                                <a href="https://www.p4i.com/accountKey/2130916">Industrial Supplies</a>
                            </li>
                            <li>
                                <a href="articles.php">Our Specials</a>

                            </li>

                            <?php
                            if (!isset($_SESSION['user'])) {

                                print '<li><a href="AccountLogin1.php" >My Account</a></li>';
                            } else {
                                print '<li><a href="AccountLogout.php" >Log Out</a></li>';
                            }
                            ?>
                            <li>
                                <a href="gsa.php">GSA Catalog</a>

                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="container">
            <header>
                <form id="searchForm" action="search.php" method="get">
                    <input type="hidden" id="searchBox_hidden" name="searchBox" />
                    <div class="search">
                        <span style="font-weight:bold"><big><big>Enter Search Text or Item Number Here:</big></big></span>
                        <input type="text" id="searchInput" autocomplete="off" name="q" value="<?php echo $_REQUEST['q']; ?>" />
                        <input type="submit" class="btn search_nav" value="" alt="Search" />
                        <big style="color: rgb(82, 83, 228); font-style: italic;"><big><span style="font-weight: bold;">WE cut the FAT out of our PRICES!</span></big></big><br>
                    </div>
                </form>
            </header>

        </div>
        <div style="clear:both"></div>



        <script type="text/javascript">
            $(document).ready(function() {
                // $("#modal_trigger, #modal_trigger1").leanModal({
                //     top: 200,
                //     overlay: 0.6,
                //     closeButton: ".modal_close"
                // });

                // Calling Login Form
                $("#login_form").click(function() {
                    $(".forgot_password").hide();
                    $(".user_login").show();
                    $(".header_title").text('Login into Your Account');
                    return false;
                });

                // Calling Register Form
                $("#forgot_password").click(function() {
                    $(".user_login").hide();
                    $(".forgot_password").show();
                    $(".header_title").text('Forgot Password?');
                    return false;
                });

                // Going back to Social Forms
                $(".back_btn").click(function() {
                    $(".user_login").show();
                    $(".user_register").hide();
                    $(".forgot_password").hide();
                    $(".header_title").text('Login into Your Account');
                    return false;
                });

            });
        </script>