<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'pager.php';
include 'functions.php';
?>

<?php
$page_name = 'search.php';
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');
$page = ($_GET['page'] > 1) ? $_GET['page'] : 1;


if ($_REQUEST['q'] != '') {
    $q = mysql_real_escape_string(trim($_REQUEST['q']));
}

$where_condition = "1";

$searchby = $_REQUEST['searchby'];

$mSearch = "Search for: ";


$query_parm = array();

if ($q != '') {
    $where_condition = $where_condition . " AND (
		`PRODUCT` LIKE '%$q%' OR 	
		`WESTCARB_PART_NUMBER` LIKE '%$q%' OR 	
		`mfgname` LIKE '%$q%' OR 
		`subcategory` LIKE '%$q%' OR 
		`upc` LIKE '%$q%')";
    $query_parm['q'] = $q;
}

if (trim($_REQUEST['category']) != '') {
    $category = mysql_real_escape_string(trim($_REQUEST['category']));
    $where_condition = $where_condition . " AND TRIM(`category`) LIKE '$category'";
    $query_parm['category'] = $category;
}

if (trim($_REQUEST['subcategory']) != '') {
    $subcategory = mysql_real_escape_string(trim($_REQUEST['subcategory']));
    $where_condition = $where_condition . " AND TRIM(`subcategory`) LIKE '$subcategory'";
    $query_parm['subcategory'] = $subcategory;
}

if (trim($_REQUEST['mfgname']) != '') {
    $mfgname = mysql_real_escape_string(trim($_REQUEST['mfgname']));
    $where_condition = $where_condition . " AND `mfgname` = '$mfgname'";
    $query_parm['mfgname'] = $mfgname;
}

$page = ($_REQUEST['page'] > 1) ? $_REQUEST['page'] : 1;
$searchText = $_GET['searchText'];
$page_name = 'search.php';
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');
$page = ($_GET['page'] > 1) ? $_GET['page'] : 1;
$pagestring = '?' . $req_parameters . '&page=';
$adjacents = 3;
$totalitems = mysql_fetch_array(mysql_query("SELECT COUNT(DISTINCT `id`) AS `num` FROM `product2` WHERE $where_condition"));

$total = $totalitems['num'];
$limit = 16;
$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;
$limit = $pager->limit;
$page = $pager->page;
?>
<div id="home_body">
    <div id="midsec">
        <div class="topsell">
            <div class="hdr">Search result for <?php echo $q; ?></div>
            <div style="clear: both" >
                <?php
                echo getPaginationString($page, $total, $limit, $adjacents, $page_name, $pagestring);


                // PageNavigation($page, $pager, $page_name, $req_parameters); 
                ?>    
            </div>
            <ul class="prod">

                <?php
                $mSearch = $mSearch . $q;
                RecordAccess($EMAIL_CC, $mSearch);  //2018
///2018
                $product_query = mysql_query("SELECT * FROM `product2` WHERE $where_condition GROUP BY `id` ORDER BY `id` DESC LIMIT $offset, $limit");
                $number1=mysql_num_rows($product_query);
                if (mysql_num_rows($product_query)) 
                {
                    while ($row = mysql_fetch_array($product_query, MYSQL_ASSOC)) 
                    {
                        $product_id = $row['id'];
                        $header = $row['description'];

                        $NameofPicture=$row['image_hyperlink'];

                        if (strpos($NameofPicture,".jpg") < -1) {
                            $row['image_hyperlink']='ProductImage/' . $NameofPicture . ".jpg";
                        }
        

                        if (!isURL($row['image_hyperlink'])) {
                            $thumbnail_url = 'ProductImage/' . basename($row['image_hyperlink']);
                            if (basename($row['image_hyperlink']) == '' || !file_exists($thumbnail_url)) {
                                $thumbnail_url = 'images/ImageNotAvailable.jpg';
                            }
                        } else {
                            $thumbnail_url = $row['image_hyperlink'];
                            $image_get_url = getimagesize($thumbnail_url);
                            if (!is_array($image_get_url)) {
                                $thumbnail_url = 'images/ImageNotAvailable.jpg';
                            }
                        }
                        $newSize = myResize($thumbnail_url,125,125);

                        $product_name = $row['description'];
                        $price = extract_price($row['CUSTOMER']);
                        $detail_link = 'product_details.php?id=' . $product_id . '&' . http_build_query($query_parm);
                        echo '<li>
                        <div class="item">
                        <a href="' . $detail_link . '">' . $row['prodname'] . '</a><br><br>                                        
                            <div class="prod_img" style="display:flex">

                                <a href="' . $detail_link . '" >
                                <img src="' . $thumbnail_url . '" style=' . $newSize . ' align-self: center" border="0">
                            </a>
                            </div>
                            
                        </div>
                        
                        <div class="info">
                            <p class="title"><a index="0" class="addClickInfo" title="' . htmlentities($product_name) . '" href="' . $detail_link . '">' . $product_name . '</a></p>

                            <p class="soldby"><a href="' . $detail_link . '">' . $row['mfgname'] . '</a></p>     
                            <p class="itemno"><a href="' . $detail_link . '">' . $row['mfgpart'] . '</p></a>
                        </div>
            
                        <div class="pricing">
                            <div class="prices">';
                                if ($row['CUSTOMER'] != '') {
                                    echo '<p class="price">$' . display_price($price) . '</p>';
                                }
                                echo '
                                
                                <p class="mailin_rebate">&nbsp;</p>
                            <p class="soldpkg">&nbsp;</p>             
                            </div>
                    
                        </div>
                        </li>';
                    }
                }
                ?>




            </ul>
            <div style="clear: both" >
                <?php
                echo getPaginationString($page, $total, $limit, $adjacents, $page_name, $pagestring);

                //PageNavigation($page, $pager, $page_name, $req_parameters); 
                ?>    
            </div>
        </div>








    </div>
</div>






<?php include 'footer.php'; ?>



</body>
</html>