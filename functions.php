<?php
require_once('global_functions.php');

function isURL($url)
{
    if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) {
        return true;
    } else {
        return false;
    }
}

function display_4_side_images()
{
    $sideimagedata = array();
    $sideimagequery = mysql_query("SELECT * FROM `fp_side_images` LIMIT 0,4");
    if (mysql_num_rows($sideimagequery)) {
        $count = 0;
        echo '<div class="dealalert">';
        while ($row = mysql_fetch_array($sideimagequery)) {
            $count++;
            $row['url'] = ($row['url'] != '') ? $row['url'] : '#';
            echo '<a href="' . $row['url'] . '" style="font-weight:bold;">
            <img src="mainimage/' . $row['imagename'] . '" style="max-width:285px; max-height:160px" />
            ' . $row['title'] . '
            </a>';
            if ($count == '2') {
                echo '</div>
				<div class="dealalert_ext">';
            }
        }
        echo '</div>';
    }
}

function display_related_category_product($product_id, $category, $subcategory, $limit = 4)
{
    $category = trim($category);
    $subcategory = trim($subcategory);
?>

    <?php
    $product_query = mysql_query("SELECT * FROM `product2` WHERE TRIM(`category`) = '$category' AND TRIM(`subcategory`) = '$subcategory' AND `id` != '$product_id' ORDER BY RAND() LIMIT 0,$limit");
    if (mysql_num_rows($product_query)) {
    ?>
        <div class="row" style="margin-top:20px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div style="background: #7a7fbe; padding: 5px; font-weight: bold;">
                    <font color="#FFFFFF" face="Arial"> Related products in this Category</font>
                </div>
            </div>

        </div>
        <div class="row rowWithFullWidth" id="related_products">

            <?php
            while ($row = mysql_fetch_array($product_query)) {
                echo ' <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 product_box">';
                $product_id = $row['id'];
                $header = $row['shortddesc'];
                if (!isURL($row['image_hyperlink'])) {
                    $thumbnail_url = 'ProductImage/' . basename($row['image_hyperlink']);
                } else {
                    $thumbnail_url = $row['image_hyperlink'];
                }

                $image_get_url = getimagesize($thumbnail_url);

                if (!is_array($image_get_url)) {
                    $thumbnail_url = 'images/ImageNotAvailable.jpg';
                }
                $NewSize = myResize($thumbnail_url, 200, 200);
                $thumbnail_url = str_replace("http:", "https:", $thumbnail_url);
                $product_name = $row['prodname'];

                $description = $row['description'];
                $detail_link = 'product_details.php?id=' . $product_id;
                $price = $row['CUSTOMER'];

                print "<br><strong><font color=\"#3366FF\">" . $product_name . "</font></strong></a><br>";
                print "<a href=\"$detail_link\">
                        <img  border=\"0\" src=\"$thumbnail_url \" $NewSize >";

                echo '<br><font color="#0000FF">' . $row['mfgpart'] . '</font>';
                print "<br><font color=\"#0000FF\"> $ " . display_price($price) . "</font></a>";

                $Slen = strlen($description);
                if ($Slen > 56) {
                    $description = substr(strip_tags($description), 0, 56) . '...';
                }

                print "<br><font color=\"#000000\"> " . $description . "</font></a>";
                echo '</div>';
            }
            ?>
        </div>
    <?php
        /*
                while ($row = mysql_fetch_array($product_query)) {
                    $product_id = $row['id'];
                    $header = $row['shortddesc'];
                    if (!isURL($row['image_hyperlink'])) {
                        $thumbnail_url = 'ProductImage/' . basename($row['image_hyperlink']);
                    } else {
                        $thumbnail_url = $row['image_hyperlink'];
                    }

                    $image_get_url = getimagesize($thumbnail_url);

                    if (!is_array($image_get_url)) {
                        $thumbnail_url = 'images/ImageNotAvailable.jpg';
                    }

                    //$newSize = myResize($thumbnail_url,125,125);
                    $price = $row['CUSTOMER'];
                    $detail_link = 'product_details.php?id=' . $product_id;
                    $product_name = $row['description'];
                    $price = $row['CUSTOMER'];
                    $detail_link = 'product_details.php?id=' . $product_id;

                    $thumbnail_url = str_replace("http:", "https:", $thumbnail_url);

                    echo '<li>
                      <div class="item">
                        
                        
                                           
                        <div class="prod_img">
                            <a href="' . $detail_link . '" >
                            <img src="' . $thumbnail_url . '" width="125" border="0">
                          </a>
                        </div>
                        
                      </div>
                      
                      <div class="info">
                          <p class="title"><a index="0" class="addClickInfo" title="' . htmlentities($product_name) . '" href="' . $detail_link . '">' . $product_name . '</a></p>
                          <p class="soldby">Manufacturer : ' . $row['mfgname'] . '
                          </p>     
                          <p class="itemno">Item No: ' . $row['mfgpart'] . '</p> 
                          <p class="itemno">Price: $' . display_price($price) . '</p> 
                      </div>
          
                      <div class="pricing">
                        <div class="prices">';
                    if ($row['CUSTOMER'] != '') {
                        echo '<p class="price">$' . number_format(str_replace('$', '', $row['CUSTOMER']), 2) . '</p>';
                    }
                    echo '
                            
                            <p class="mailin_rebate">&nbsp;</p>
                          <p class="soldpkg">&nbsp;</p>             
                        </div>
                
                      </div>
                      </li>';
                }
                */
    }
    ?>




<?php
}

function display_footer_shop_by_category()
{
?>
    <div id="mainShopByContainer">

        <link rel="stylesheet" href="css/ihwy-2012.css" type="text/css" media="screen" charset="utf-8" />
        <link rel="stylesheet" href="css/jquery.listnav-2.1.css" type="text/css" media="screen" charset="utf-8" />

        <style type="text/css" media="screen" charset="utf-8">
            #tabNav ul li a {
                background: #ffffff;
                /* Old browsers */
                background: -moz-linear-gradient(top, #ffffff 0%, #dadada 4%, #e5e5e5 7%, #e8e8e8 33%, #ececec 37%, #ececec 44%, #f0f0f0 48%, #f0f0f0 59%, #f5f5f5 63%, #f5f5f5 74%, #fafafa 78%, #f5f5f5 81%, #fafafa 85%, #fafafa 93%, #fdfdfd 96%, #fdfdfd 100%);
                /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffffff), color-stop(4%, #dadada), color-stop(7%, #e5e5e5), color-stop(33%, #e8e8e8), color-stop(37%, #ececec), color-stop(44%, #ececec), color-stop(48%, #f0f0f0), color-stop(59%, #f0f0f0), color-stop(63%, #f5f5f5), color-stop(74%, #f5f5f5), color-stop(78%, #fafafa), color-stop(81%, #f5f5f5), color-stop(85%, #fafafa), color-stop(93%, #fafafa), color-stop(96%, #fdfdfd), color-stop(100%, #fdfdfd));
                /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, #ffffff 0%, #dadada 4%, #e5e5e5 7%, #e8e8e8 33%, #ececec 37%, #ececec 44%, #f0f0f0 48%, #f0f0f0 59%, #f5f5f5 63%, #f5f5f5 74%, #fafafa 78%, #f5f5f5 81%, #fafafa 85%, #fafafa 93%, #fdfdfd 96%, #fdfdfd 100%);
                /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, #ffffff 0%, #dadada 4%, #e5e5e5 7%, #e8e8e8 33%, #ececec 37%, #ececec 44%, #f0f0f0 48%, #f0f0f0 59%, #f5f5f5 63%, #f5f5f5 74%, #fafafa 78%, #f5f5f5 81%, #fafafa 85%, #fafafa 93%, #fdfdfd 96%, #fdfdfd 100%);
                /* Opera 11.10+ */
                background: -ms-linear-gradient(top, #ffffff 0%, #dadada 4%, #e5e5e5 7%, #e8e8e8 33%, #ececec 37%, #ececec 44%, #f0f0f0 48%, #f0f0f0 59%, #f5f5f5 63%, #f5f5f5 74%, #fafafa 78%, #f5f5f5 81%, #fafafa 85%, #fafafa 93%, #fdfdfd 96%, #fdfdfd 100%);
                /* IE10+ */
                background: linear-gradient(to bottom, #ffffff 0%, #dadada 4%, #e5e5e5 7%, #e8e8e8 33%, #ececec 37%, #ececec 44%, #f0f0f0 48%, #f0f0f0 59%, #f5f5f5 63%, #f5f5f5 74%, #fafafa 78%, #f5f5f5 81%, #fafafa 85%, #fafafa 93%, #fdfdfd 96%, #fdfdfd 100%);
                /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a6bcbcbc', endColorstr='#00000000', GradientType=0);
                /* IE6-9 */
                float: left;
                height: 34px;
                padding: 0 20px 0 20px;
                border: 1px solid #007AA9;
                border-top: none;
                font-size: 16px;
                line-height: 35px;
                margin-left: 5px;
                font-weight: bold;
                color: #004962;
                margin-bottom: 3px;
                cursor: pointer;
            }

            #tabNav ul li a.selected {
                cursor: default;
                margin-bottom: 0;
                background: none;
                background-color: #007aa9;
                color: white;
                height: 38px;
                border: none;
                filter: none !important;
            }
        </style>

        <script type="text/javascript" src="js/jquery-1.3.2.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.cookie.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.idTabs.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.listnav-2.1.js" charset="utf-8"></script>


        <script type="text/javascript" charset="utf-8">
            $(function() {
                if (top.location.href.toLowerCase() == self.location.href.toLowerCase())
                    $('#docLink').show();

                $("#tabNav ul").idTabs("tab1");

                $('#demoOne').listnav({
                    initLetter: 'a'
                });

            });
        </script>


        <div id="tabNav">
            <ul>
                <li><a href="#tab1">Shop by Manufacture</a></li>
                <li><a href="#tab2">Shop by Categories</a></li>
                <li><a href="#tab3">Shop by Sub-Categories</a></li>
            </ul>
            <div class="clr"></div>
        </div>
        <div id="tabs" style="border: 1px solid #1b5790;">
            <div id="tab1" class="tab">

                <div id="demo1" style="margin:20px;">

                    <div id="demoOne-nav" class="listNav"></div>

                    <ul id="demoOne" class="demo grid-list">
                        <?php
                        $query = mysql_query("SELECT DISTINCT `mfgname` FROM `product2` WHERE `mfgname` != '' ORDER BY `mfgname`");
                        if (mysql_num_rows($query)) {
                            $mfg_count = 0;
                            while ($row = mysql_fetch_array($query)) {
                                echo '<li><a href="search.php?mfgname=' . $row['mfgname'] . '">' . $row['mfgname'] . '</a></li>';
                            }
                        }
                        ?>

                    </ul>
                </div>
            </div>


            <div id="tab2" class="tab">

                <div id="demo2" style="margin:20px">


                    <ul class="demo grid-list">
                        <?php
                        $category_query = mysql_query("SELECT DISTINCT `Name` AS `category` FROM `subcategory` ORDER BY `name`");
                        if (mysql_num_rows($category_query)) {
                            while ($row = mysql_fetch_array($category_query)) {
                                echo '<li ><a href="search.php?category=' . $row['category'] . '">' . $row['category'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>

                </div>
            </div>


            <div id="tab3" class="tab">

                <div id="demo2" style="margin:20px">


                    <ul class="demo grid-list">
                        <?php
                        $category_query = mysql_query("SELECT DISTINCT `Name` AS `category` FROM `category` ORDER BY `category`");
                        if (mysql_num_rows($category_query)) {
                            while ($row = mysql_fetch_array($category_query)) {
                                echo '<li ><a href="search.php?category=' . $row['category'] . '">' . $row['category'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>

                </div>
            </div>







        </div>













    </div>
<?php
}

function product_details_email_content($data)
{
    global $COMPANYNAME, $FROM, $MAINURL;

    $baseURL = ($_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http') . "://{$_SERVER['SERVER_NAME']}" . str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

    $product_url = $baseURL . '/product_details.php?id=' . $data['id'];

    $product_query = mysql_query("SELECT * FROM `product2` WHERE `id` = '$data[id]'");
    if (mysql_num_rows($product_query)) {
        $row = mysql_fetch_array($product_query);
        if (!isURL($row['image_hyperlink'])) {
            $thumbnail_url = $baseURL . '/ProductImage/' . basename($row['image_hyperlink']);
        } else {
            $thumbnail_url = $row['image_hyperlink'];
        }
    }
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>

    <head>
        <META http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $COMPANYNAME; ?></title>
    </head>

    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="10">
            <tr>
                <td>
                    <table border="0" cellspacing="0" width="500" cellpadding="0">
                        <tr>
                            <td width="300" rowspan="2" valign="top"><img border="0" src="http://westcarbcom.chrondev.com/images/logo.jpg"></td>
                            <td align="right" valign="top" nowrap="nowrap" width="100%">
                                <font face="Arial, Helvetica, sans-serif" size="3" color="#000066"><strong>Call To Order:
                                        1-727-474-1327</strong></font>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="bottom">
                                <font face="Arial, Helvetica, sans-serif" size="3" color="#000066"><strong>Product
                                        Suggestion&nbsp;</strong></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="500" border="0" cellspacing="0" cellpadding="1" bgcolor="#f7fafb">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="6" bgcolor="#FFFFFF">
                        <tr>
                            <td>
                                <font face="Arial, Helvetica, sans-serif" size="2"><strong>Hello
                                        <?php echo $data['to_name']; ?>,<br>
                                        <br>
                                        Your friend <?php echo $data['from_name']; ?> thought you might be interested in
                                        this product found at <?php echo $COMPANYNAME; ?>.</strong></font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" bgcolor="#e4eaed">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="#f7fafb">
                                                <tr>
                                                    <td width="220"><a href="<?php echo $product_url; ?>" target="_blank"><img src="<?php echo $thumbnail_url; ?>" border="0" width="220" height="220"></a></td>
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                                            <tr>
                                                                <td colspan="2">
                                                                    <font face="Arial, Helvetica, sans-serif" size="2">
                                                                        <strong><a href="<?php echo $product_url; ?>" target="_blank"><?php echo $row['prodname']; ?></a></strong>
                                                                    </font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <font face="Arial, Helvetica, sans-serif" size="2">
                                                                    </font>
                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td nowrap>
                                                                    <font face="Arial, Helvetica, sans-serif" size="2" color="#990000"><strong>Price:</strong></font>
                                                                </td>
                                                                <td>
                                                                    <font face="Arial, Helvetica, sans-serif" size="2" color="#990000"><?php echo $row['CUSTOMER']; ?>
                                                                    </font>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <font face="Arial, Helvetica, sans-serif" size="1"><strong>We encourage you to visit our
                                        website in the future at <a href="<?php echo $MAINURL; ?>" target="_blank"><?php echo $MAINURL; ?></a>
                                        <br>
                                        Call Us: 1-727-474-1327</strong></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </td>
        </tr>
        </table>
    </body>

    </html>
<?php
}

function extract_price($price)
{
    $price = trim(str_replace(array('$', ','), array('', ''), $price));
    return $price;
}

function display_price($price)
{
    $price = extract_price($price);
    return number_format($price, 2);
}

function load_product($id)
{

    $query = mysql_query("SELECT * FROM `product2` WHERE `id` = '$id'");
    if (mysql_num_rows($query)) {
        $row = mysql_fetch_assoc($query);
        return $row;
    }
}


function product_reviews_count($product_id)
{
    $result = mysql_fetch_array(mysql_query("SELECT COUNT(`id`) AS `num` FROM `product_review` WHERE `product_id` = '$product_id'"), MYSQL_ASSOC);
    return $result['num'];
}



function rating_dropdown($rating)
{
    $query = mysql_query("SELECT * FROM `rating_config` ORDER BY `rating`");
    if (mysql_num_rows($query)) {
        while ($row = mysql_fetch_array($query)) {
            $selected_str = ($row['rating'] == $rating) ? 'selected' : '';
            echo '<option value="' . $row['rating'] . '">' . $row['value'] . '</option>';
        }
    }
}
function display_product_box($product_info)
{
    $title = $product_info['prodname'];
    $description = html_entity_decode($product_info['description']);
    $product_link = 'product_details.php?id=' . $product_info['id'];


    if (!isURL($product_info['image_hyperlink'])) {
        $thumbnail_url = 'ProductImage/' . basename($product_info['image_hyperlink']);
        if (!file_exists($thumbnail_url)) {
            $thumbnail_url = 'images/ImageNotAvailable.jpg';
        }
    } else {
        $thumbnail_url = $product_info['image_hyperlink'];
        $image_get_url = getimagesize($thumbnail_url);
        if (!is_array($image_get_url)) {
            $thumbnail_url = 'images/ImageNotAvailable.jpg';
        }
    }


    echo '<div class="reviewed-product section" >
                                            <a href="' . $product_link . '" class="reviewed-product-image">
                            <img src="' . $thumbnail_url . '" alt="' . htmlspecialchars($title) . '" style="width:250px;">
                        </a>
                                        <div class="reviewed-product-summary">
                        <h5><a href="' . $product_link . '">' . $title . '</a></h5>
                        <p>' . $description . '</p>
                    </div>
                </div>';
}



function reviews_list($product_id)
{

?>
    <div class="reviews11">

        <?php
        $query = mysql_query("SELECT `product_review`.*, `member`.`name`, `member`.`last`, `member`.`city`, `member`.`state`, `member`.`registrationDate` AS `join_date` FROM 
            `product_review` 
            INNER JOIN `member`  ON (`product_review`.`user_id` = `member`.`id`) 	
            WHERE `product_review`.`product_id` = '$product_id' AND `product_review`.`status` = '1' ORDER BY `product_review`.`id` DESC ");

        if (mysql_num_rows($query)) {
            while ($row = mysql_fetch_array($query)) {
        ?>
                <div itemtype="http://schema.org/Review" itemscope="" itemprop="review">
                    <div id="review569925" class="review-wrapper">
                        <div class="row">
                            <div class="user col-lg-2 col-md-3 col-sm-2 hidden-xs">

                                <h5 class="username"><a href="#"><?php echo $row['name'] . ' ' . $row['last']; ?></a>
                                </h5>

                                <dl class="user-mini-profile">
                                    <dt>Member since:</dt>
                                    <dd><?php echo date('M d, Y', strtotime($row['join_date'])); ?></dd>

                                    <dt>Location:</dt>
                                    <dd><?php echo $row['city'] . ', ' . $row['state']; ?></dd>
                                </dl>
                            </div>

                            <div class="review col-lg-10 col-md-9 col-sm-10">


                                <h2><span itemprop="name"><?php echo $row['review_title']; ?></span></h2>

                                <div class="rating-small">
                                    <div itemtype="http://schema.org/Rating" itemscope="" itemprop="reviewRating">
                                        <div class="stars">
                                            <div class="rateit" data-rateit-value="<?php echo $row['rating']; ?>" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                                        </div>
                                        <p style="display:none">-</p>
                                    </div>
                                </div>

                                <p class="quiet">
                                    <span itemprop="author"><?php echo $row['name'] . ' ' . $row['last']; ?></span>
                                    posted this on
                                    <meta content="<?php echo date('Y-m-d', strtotime($row['timestamp'])); ?>" itemprop="datePublished"><?php echo date('M d, Y', strtotime($row['timestamp'])); ?>
                                </p>

                                <div class="review-content">
                                    <?php echo html_entity_decode(nl2br($row['review_description'])); ?>
                                </div>


                                <div class="pros-cons">
                                    <?php
                                    if ($row['pros'] != '') {
                                    ?>
                                        <p class="pros">
                                            <i class="icon-plus"></i>
                                            <?php echo $row['pros']; ?>
                                        </p>
                                    <?php
                                    }
                                    ?>
                                    <?php
                                    if ($row['cons'] != '') {
                                    ?>

                                        <p class="cons">
                                            <i class="icon-minus"></i>
                                            <?php echo $row['cons']; ?>
                                        </p>
                                    <?php
                                    }
                                    ?>

                                </div>





                            </div>
                        </div>
                    </div>
                </div>
        <?php
            }
        }
        ?>

    </div>
<?php
}
?>