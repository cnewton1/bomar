<?php
include "../globals.php"; 
include "SubCategoryBoxFunction.php";
include "ActiveBoxFunction.php";
include "../MyImageFunction.php";
include "adminheader.php";
adminTitle(" Modify Advertisement ");

//-----------------------------
// Must use get in this case
//-----------------------------

$Catname = $HTTP_GET_VARS['Catname'];
$Catnameorig = $Catname; // They are the same at first
$UpdateStatus = $HTTP_GET_VARS['Update'];

print "<div style=\"position: absolute; top: 130px; left: 10px; width: 700px;>\"";

//==============================================================================
// (1) FIRST READ ALL THE POINTERS AND GET ALL THE SUBCAT NAMES
// STORE IT IN A ARRAY FOR COMPARE. BASE OF THE CATNAME WHICH IS THE PRODUCTID
//===============================================================================

$resultID = mysql_query("Select subcategory from adPointer where productid='$Catname' order by category, subcategory", $linkID);

while ($row = mysql_fetch_row($resultID))
{
   $adCount++;
   $fieldCount=1;
   foreach ($row as $field)
   {
     // Save the Category Name Only
     if ($fieldCount==1)
      { 
        $SubCategoryArray[$adCount]=$field;
      }     
     $fieldCount++;    
   }
}

//=================================================================================
// (2) READ THE PRODUCT
//=================================================================================
$resultID = mysql_query("Select id, category, subcategory, header, description, item_num, price, filename, comment, subheader, name, phone, address1, address2, city, state, zip from product where id='$Catname'", $linkID);

$m=0;
$row = mysql_fetch_row($resultID); // get the first row

if ($row > 0)
{
   $fieldCount=1;
   foreach ($row as $field)
   {
     changeRowColor($m);
     // Save the Category Name Only
     if ($fieldCount==1)
         $ID = $field;
          
     if ($fieldCount==2)
         $Catname = $field;
         
     if ($fieldCount==3)
         $SubCat = $field;
         
     if ($fieldCount==4)
         $PHeader = $field;
         
     if ($fieldCount==5)
         $PDescription = $field;

     if ($fieldCount==6)
         $PItemNum = $field;         
         
     if ($fieldCount==8)
         $PImageName = $field;
         
     if ($fieldCount==9)
         $PComment = $field;
         
     if ($fieldCount==10)
        { $PSubHeader = $field;
        }
         
     if ($fieldCount==11)
         $FullName = $field;                                                                                 

     if ($fieldCount==12)
         $Phone = $field;             
       // Get the next field  

     if ($fieldCount==13)
         $PAddress1 = $field;             
       // Get the next field  

     if ($fieldCount==14)
         $PAddress2 = $field;             
       // Get the next field  

    if ($fieldCount==15)
         $City = $field;             
       // Get the next field         
       
    if ($fieldCount==16)
        { $State = $field;                      
        }
  
      if ($fieldCount==17)
        { $Zip = $field;                      
        }      
       // Get the next field   
     $fieldCount++; 
   } // Foreach look
}  
else 
{    
 print "Error 52...Ad not found in DB";   
}

//------------------------------------------------
print "<body>";
print "<b><font face=\"Arial\" size=\"5\">Edit Ad Content</font></b>";

$UpdateStatus = "Update";

if ($UpdateStatus == "Update")
{
  print "<b><font face=\"Arial\" size=\"3\" font color=\"#0000FF\" ><br>Enter new values in the field below and click on Save Changes to complete.</font></b>";
}

print "<form method=\"GET\" action=\"DBProduct.php\">";
print "<table border=\"0\" cellpadding=\"4\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\"";
print "bordercolor=\"#111111\" width=\"50%\" id=\"AutoNumber1\" height=\"155\">";
print "<tr>";

print "<td width=\"10%\" height=\"150\" style=\"\border-style: none; border-width: medium\">";

$THEIMAGE="$MAINURL//ProductImage//". basename($PImageName);
$NewSize=myResize($THEIMAGE,400,400);   
  
print "<img border=\"0\" src=\"$MAINURL//ProductImage//". basename($PImageName) . "\"" . $NewSize . ">"; 
print "</font></p>";
  
print "</td>";
// Subcategory section ---------------------------
print "<td width=\"50%\" height=\"150\" style=\"border-style: none; border-width: medium\">";

print "<font face=\"Arial\" align=\"left\" size=\"2\"><b>SubCategory:<br></font>";
// SelectSubCategoryCheckBoxes($linkID,$Catname,$SubCat);
// AllCategoryBox($linkID,$Catname);

AllCategoryBoxSelect($linkID,$Catname,$SubCategoryArray);
//-------------------------------------------
print "</td>";
// Status ----------------------------
print "<td width=\"20%\" height=\"150\" valign=\"top\" style=\"border-style: none; border-width: medium\" colspan=\"4\">";
print "<font face=\"Arial\"><font size=\"2\"><b>Status:<br></font>";
ActiveCheckBoxes($linkID,$ID);
print "</td>";
//----------------------------
print "</tr></table>";


// Display Form
print "<div style=\"background-color: #E1F0FF\">";
//***********************************************************
// TITLE AND URL
print "<font face=\"Arial\"><font size=\"2\"><b>";
print "Ad Title: (for Main Listing): <br></b></font><b><input type=\"text\" name=\"FHeader\" size=\"40\"  value=\"$PHeader\"></b></font>";

//URL
print "&nbsp;&nbsp;<font face=\"Arial\"><b><font size=\"2\">Advertiser's Web Site:&nbsp;</font>";
print "<input type=\"text\" name=\"FSubheader\" size=\"42\" value=\"$PSubHeader\"></b></font>";

//=*****************************************************************************
// CONTACT FULL NAME
print "<br><br><font face=\"Arial\"><b><font size=\"2\">";
print "Contact Name:&nbsp; </font><input type=\"text\" name=\"FName\" size=\"23\" value=\"$FullName\"><font size=\"2\">";

print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

// BUSINESS PHONE
print "<font face=\"Arial\"><b><font size=\"2\">Business Phone:&nbsp; </font>";
print "<input type=\"text\" name=\"FPhone\" size=\"25\" value=\"$Phone\"></b></font>";

//=*****************************************************************************
// Address 1 and 2
print "<br><font face=\"Arial\"><b><font size=\"2\">";
print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address1:&nbsp; </font><input type=\"text\" name=\"FAddress1\" size=\"23\" value=\"$PAddress1\"><font size=\"2\">";

print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

print "<font face=\"Arial\"><b><font size=\"2\">Address2:&nbsp; </font>";
print "<input type=\"text\" name=\"FAddress2\" size=\"25\" value=\"$PAddress2\"></b></font>";

//=*****************************************************************************
// City and State 
print "<br><font face=\"Arial\"><b><font size=\"2\">";
print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City:&nbsp; </font><input type=\"text\" name=\"FCity\" size=\"23\" value=\"$City\"><font size=\"2\">";

print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

print "<font face=\"Arial\"><b><font size=\"2\">State:&nbsp; </font>";
print "<input type=\"text\" name=\"FState\" size=\"2\" value=\"$State\"></b></font>";

print "&nbsp;&nbsp;&nbsp;&nbsp;<font face=\"Arial\"><b><font size=\"2\">Zip:&nbsp; </font>";
print "<input type=\"text\" name=\"FZip\" size=\"15\" value=\"$Zip\"></b></font>";

//*****************************************************


print "<div style=\"background-color: #E1F0FF\">";
print "<br><font face=\"Arial\"><b><font size=\"2\">Extended Description:<br> </font>";
print "<textarea rows=\"9\" name=\"FDescription\" cols=\"46\">$PDescription</textarea></b></font></div>";

/*
print "<font face=\"Arial\" color=\"#FF0000\" size=\"2\"><b>Upload Image File:</font>&nbsp;<input type=\"file\" name=\"FUpload\" size=\"40\"></div>";
*/

print "<div style=\"background-color:#D9FFDC\">";
print "<input name=\"DisplayImage\" size=\"100\" value=\"$target_path\" type=\"hidden\">";
print "<input name=\"Catname\" size=\"100\" value=\"$Catname\" type=\"hidden\">";     
print "<input name=\"calltype\" size=\"100\" value=\"U\" type=\"hidden\">";
print "<input name=\"PID\" size=\"100\" value=\"$ID\" type=\"hidden\">";
print "<font face=\"Arial\"><b><input type=\"submit\" value=\"Save Changes\" name=\"B1\">";
print "<input type=\"reset\" value=\"Clear Form\" ";
print "name=\"B2\"></b></font></div>";

print "</form></body>";
include "../gfooter.php";

?>

