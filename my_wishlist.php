<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
include 'utility/Shopping_Cart.php'; //Inluding Sooping Cart Function file
?>

<?php
$session_user = $_SESSION['user'];
$select_user_details = "select * from member where email='$session_user'";
$select_user_sql = mysql_query($select_user_details, $linkID) or die(mysql_error());
if (mysql_num_rows($select_user_sql) > 0) {
    $user_result = mysql_fetch_assoc($select_user_sql);
    $userid = $user_result['id'];
} else {
    header('Location:AccountLogin1.php');
    exit();
}

if (isset($_GET['action']) && $_GET['action'] == 'remove') {
    $id = intval($_GET['id']);
    if ($id > 0) {
        $delete = mysql_query("DELETE FROM `wishlist` WHERE `userid` = '$userid' AND `id` = '$id'");
        if ($delete) {
            $msg = 'Item successfully removed from the wishlist';
            header('Location: my_wishlist.php?msg=' . urlencode($msg));
            exit();
        }
    }
}
if (isset($_GET['action']) && $_GET['action'] == 'add') {
    $product_id = intval($_GET['product_id']);
    if ($product_id > 0) {
        $check_exists_result = mysql_fetch_assoc(mysql_query("SELECT COUNT(`id`) AS `num` FROM `wishlist` WHERE `userid` = '$userid' AND `product_id` = '$product_id'"));
        if ($check_exists_result['num'] > 0) {
            $msg = 'Item already added into wishlist';
            header('Location: my_wishlist.php?msg=' . urlencode($msg));
            exit();
        } else {
            $insert = mysql_query("INSERT INTO `wishlist` (`userid`, `product_id`) VALUES ('$userid', '$product_id')");
            if ($insert) {
                $msg = 'Item successfully added into wishlist';
                header('Location: my_wishlist.php?msg=' . urlencode($msg));
                exit();
            }
        }
    }
}
if (isset($_GET['msg']) && $_GET['msg'] != '') {
    $msg = strip_tags(urldecode($_GET['msg']));
}
?>


<div id="home_body">


    <div id="midsec">
        <h1 style="font-size:28px; font-weight: bold; text-align: center;">My Wishlist</h1>
        <?php
        if (isset($msg)) {
            ?>
            <div ><?php echo $msg; ?></div>

            <?php
        }
        ?>

        <form method="POST" action="" target="_self">
            <table class="table table-striped table-bordered" style="border-collapse: collapse"  >
                <thead>
                    <tr>
                        <th width="50">Sr. No.</th>
                        <th >Product</th>
                        <th width="200">Price</th>
                        <th width="200">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = mysql_query("SELECT `wishlist`.*, 
                    `product2`.`mfgpart`, 
                    `product2`.`description`, 
                    `product2`.`CUSTOMER` AS `price`,
                    `product2`.`image_hyperlink` AS `image`
                    
                    FROM `wishlist` 
                    INNER JOIN `product2` ON (`wishlist`.`product_id` = `product2`.`id`)
                    WHERE `wishlist`.`userid` = '$userid'");
                    if (mysql_num_rows($query)) {
                        $count = 0;
                        while ($row = mysql_fetch_assoc($query)) {
                            $count++;


                            $header = $row['description'];
                            $image_url = parseProductImageURL($row['image']);
                            $product_url = 'product_details.php?id=' . $row['product_id'] . '';

                            echo '<tr>
                            <td valing="top">' . $count . '</td>
                            <td valing="top"><a href="'.$product_url.'"><img src="' . $image_url . '" width="150"><br>' . $header . '</a></td>
                            <td valing="top">' . $row['price'] . '</td>
                            <td valing="top">
                            <a style="font-weight:bold;" target="_blank" href="addToCart.php?action=add&ID=' . $row['product_id'] . '">Add to cart</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a style="font-weight:bold;" href="my_wishlist.php?action=remove&id=' . $row['id'] . '" onclick="return confirm(\'Are you sure to remove this item from wishlist?\');">Remove</a></td>
                        </tr>';
                        }
                    } else {
                        echo '<tr><td colspan="4" align="center">Your wishlist is empty.</td></tr>';
                    }
                    ?>
                </tbody>

            </table>
        </form>



        <div style="clear:both"></div>





    </div>
</div>






<?php include 'footer.php'; ?>



</body>
</html>