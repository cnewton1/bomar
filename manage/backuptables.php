<?php

include "../users.php";
if ($_SESSION['session_username'] == '') {
    header('Location: index.php');
    exit();
}
set_time_limit(0);
ini_set('memory_limit', '1024M');



$exclude_tables = array('access_log', 'product2', 'product2_copy', 'product2_copy20171116', 'product_access_log');
$backup_all = backup_tables($SERVER, $ADMINUSER, $ADMINPASSWD, $DBNAME, '*', $exclude_tables);
if ($backup_all) {
    $backup_product2_table = backup_tables($SERVER, $ADMINUSER, $ADMINPASSWD, $DBNAME, 'product2', array(), 'db-product2-');
    if ($backup_product2_table) {
        header('Location: BackupDB.php?msg=Database Backup Successfully Completed');
        exit();
    }
}

exit;

/* backup the db OR just a table */

function backup_tables($host, $user, $pass, $name, $tables = '*', $exclude_tables = array(), $file_name_prefix = "db-backup-") {
    $link = mysql_pconnect($host, $user, $pass);
    mysql_select_db($name, $link);
    //get all of the tables
    if ($tables == '*') {
        $tables = array();
        $result = mysql_query("SHOW TABLES", $link);
        while ($row = mysql_fetch_row($result)) {
            if (!in_array($row[0], $exclude_tables)) {
                $tables[] = $row[0];
            }
        }
    } else {
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

    //cycle through
    foreach ($tables as $table) {
        $result = mysql_query('SELECT * FROM ' . $table);
        $num_fields = mysql_num_fields($result);

        $return .= 'DROP TABLE ' . $table . ';';
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
        $return .= "\n\n" . $row2[1] . ";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = mysql_fetch_row($result)) {
                $return .= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                    if (isset($row[$j])) {
                        $return .= '"' . $row[$j] . '"';
                    } else {
                        $return .= '""';
                    }
                    if ($j < ($num_fields - 1)) {
                        $return .= ',';
                    }
                }
                $return .= ");\n";
            }
        }
        $return .= "\n\n\n";
    }
    //save file
    // $handle = fopen('./BACKUP/db-backup-'.date().'-'.(md5(implode(',',$tables))).'.sql','w+');
    $file_name = $file_name_prefix . date('Y-m-d') . '-' . time() . '-' . (md5(implode(',', $tables))) . '.sql';
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $handle = fopen('BACKUP/' . $file_name, 'w+');
    if (fwrite($handle, $return)) {
        fclose($handle);
        $file_size = filesize('BACKUP/' . $file_name);
        $insert_record = mysql_query("INSERT INTO `db_backup` (`file_name`, `file_size`, `user`, `date`, `time`) VALUES ('$file_name', '$file_size', '$_SESSION[session_username]', '$date', '$time')");
        if ($insert_record) {
            return true;
        }
    }
    fclose($handle);
}

?>