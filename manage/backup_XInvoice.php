<?php
include "../users.php";
include "../connect.php";
           
include "../globals.php"; 
include "CategoryBoxFunction.php";
include "SubCategoryBoxFunction.php";
include "makeIconFunction.php";
include "adminheader.php";
include "../MyImageFunction.php";
include "customerparser.php";
include "myDateFunction.php";
include "addressparser.php";


$filename = "./invoice.txt";

$PATH = $_SERVER['DOCUMENT_ROOT']; #get server path
$save_path=$PATH; // . $filename; #define target path

$SHIP_TO=" ";
// $save_path=$save_path . $filename;

$save_path = $filename;

// OPEN FILE FOR WRITING
$fp = fopen($save_path, "w", 0); #open for writing

//=====================================
// CREATE FIRST CSV ROW
// $HEADERROW="Invoice Number\tInvoice Date\tSales Order\tDepartment\tService_Agency\tMajor_Command\tCustomer\tItem\tFinancial Account\tDescription\tQuantity\tPrice\tItem Tax Group\tSalesperson\n";

$HEADERROW="Record #\tCustomer\tTransaction Date\tRef # PO Number\tTerms\tClass\tTemplateName\tTo Be PrintedShip Date\tBill Line 1\tBill Line 2\tBill Line 3\tBill Line 4\tBill To City\tBill To State\tBill To Postal Code\tShip To Country\tShip Line 1\tShip Line 2\tShip Line 3\tShip  Line 4\tShipTo City\tShip To State\tShipTo Postal Code\tBill To Country\tPhone\tFax\tEmail\tContact Name\tFirstLast\tRep\tDue Date\tShip Method\tCustomer Message\tMemo\tItem\tQuantity\tDescription\tPrice\tIs Pending\tItem Line Class\tService Date\tFOB\tCustomer Account #\tSales Tax Item\tTo Be Mailed\tOther\tAR AccountSales Tax Code\n";

fputs($fp, $HEADERROW); #write all of $data to our opened file
//=====================================

//===============================
// Get DATABASE
//===============================

$sql="SELECT * FROM invoice";
$result=mysql_query($sql); 

//header("Content-type: application/octet-stream");
//header("Content-Disposition: attachment; filename=\"myProducts.csv\"");
// FIRST ROW.
// echo $HEADERROW;
$recordCount=0;

while($row=mysql_fetch_array($result))
{
	$trans = $row['translink'];

	$major_command = $row['major_command'];
	$position1 = strpos($major_command, "<br>");

	if($position1==0)
	$major_command = substr($major_command,2);

	$major_command = str_replace("<br>",";",$major_command);
	$major_command = str_replace("\n",";",$major_command);
	$major_command = str_replace("\r",";",$major_command);
	$major_command = str_replace(",",";",$major_command);

	$description = $row['description'];
	$description = str_replace("<br>",";",$description);
	$description = str_replace("\n",";",$description);
	$description = str_replace("\r",";",$description);
	$description = str_replace(",",";",$description);

	$customer = $row['customer'];

	$customer = str_replace("<br>",";",$customer);
	$customer = str_replace("\n",";",$customer);
	$customer = str_replace("\r",";",$customer);
	$customer = str_replace(",",";",$customer);

	$DepartmentName = getDepartment($customer);
	$Activity_Code = getActiveCode($customer);
	$Last_Name = getLastName($customer);
	$First_Name = getFirstName($customer);
	$Email = getEmail($customer);
	$Phone_Number = getPhone($customer);

	$recordCount++;


	$date1 = $row['invoice_date'];
	$date_arr = explode(' ',$date1);
	$date_updated = $date_arr[0];

$New_DATE = myDateAdd($date_updated);

  $ORDERNUMBER = $row['invoice_number'];
  $MOMSNUMBER=   $row['sales_order'];       

    $translink=getTransLink($MOMSNUMBER);
    $BILL_TO=getBILLTO($translink);  // This will also return the SHIP_TO in the global scope.
    
//===============================================================================    
// NOW PARSE THE ADDRESSES
//===============================================================================

$SHIP_TO_ADDRESS = $SHIP_TO;
// PARSE THE ADDRESSES DATA HERE Both billing and shipping

   $SHIP_TO_ZIP=getTagZip($SHIP_TO_ADDRESS);
   $SHIP_TO_STATE=getTagState($SHIP_TO_ADDRESS);     
   $SHIP_TO_CITY=getTagCity($SHIP_TO_ADDRESS);       
   $SHIP_TO_STREET=getTagStreet($SHIP_TO_ADDRESS);     
   $SHIP_TO_STREET=str_replace("\n"," ",$SHIP_TO_STREET);  

	if (strlen(stristr($SHIP_TO_ADDRESS,"<br>"))==0)
		$SHIP_TO_ADDRESS = nl2br($SHIP_TO_ADDRESS);

    $SHIP_TO_ADDRESS=str_replace("\n"," ",$SHIP_TO_ADDRESS);           
   	$ship_to_arr = explode('<br />',$SHIP_TO_ADDRESS);

// Now do the shipping
    
$SHIP_TO_ADDRESS = nl2br($SHIP_TO_ADDRESS);
$ship_to_arr = explode('<br />',$SHIP_TO_ADDRESS);
       
$ship_to_arr[0] = str_replace("\n"," ",$ship_to_arr[0]);
$ship_to_arr[1] = str_replace("\n"," ",$ship_to_arr[1]);
$ship_to_arr[2] = str_replace("\n"," ",$ship_to_arr[2]);
$ship_to_arr[3] = str_replace("\n"," ",$ship_to_arr[3]);
$ship_to_arr[4] = str_replace("\n"," ",$ship_to_arr[4]);     

$SHIP_TO_1 =  $ship_to_arr[1];
$SHIP_TO_2 =  $ship_to_arr[2];    
$SHIP_TO_3 =  $ship_to_arr[3];    
$SHIP_TO_4 =  $ship_to_arr[4];        
                                        
// FIX.
 
// TEST CLEAR BILL
//=================================================================================

 if ($SHIP_TO_2 != "")
  if ($SHIP_TO_CITY != "")    
    if (strstr($SHIP_TO_2,$SHIP_TO_CITY)  != "") 
       {$SHIP_TO_2=" ";  
       }
 
if ($SHIP_TO_2 != "")        
  if ($SHIP_TO_STATE != "")  
    if (strstr($SHIP_TO_2,$SHIP_TO_STATE) != "")
       {$SHIP_TO_2=" ";
       }
    
if ($SHIP_TO_2 != "")  
  if ($SHIP_TO_ZIP != "")      
   if (strstr($SHIP_TO_2,$SHIP_TO_ZIP)   != "")
   {  $SHIP_TO_2=" ";
   }
   
// Remove Country if US  
if ($SHIP_TO_2 != "")      
   if (strstr($SHIP_TO_2," US") != "")
   {  $SHIP_TO_2=" ";
   }   
      
//+++++++++++++++++
if ($SHIP_TO_3 != "")
  if ($SHIP_TO_CITY != "")    
    if (strstr($SHIP_TO_3,$SHIP_TO_CITY)  != "") 
       {$SHIP_TO_3=" ";  
       }
 
if ($SHIP_TO_3 != "")        
  if ($SHIP_TO_STATE != "")  
    if (strstr($SHIP_TO_3,$SHIP_TO_STATE) != "")
       {$SHIP_TO_3=" ";
       }
    
if ($SHIP_TO_3 != "")  
  if ($SHIP_TO_ZIP != "")      
   if (strstr($SHIP_TO_3,$SHIP_TO_ZIP)   != "")
   {  $SHIP_TO_3=" ";
   }
   
// Remove Country if US  
if ($SHIP_TO_3 != "")      
   if (strstr($SHIP_TO_3," US") != "")
   {  $SHIP_TO_3=" ";
   }    
 
 
//+++++++++++++++++
if ($SHIP_TO_4 != "")
  if ($SHIP_TO_CITY != "")    
    if (strstr($SHIP_TO_4,$SHIP_TO_CITY)  != "") 
       {$SHIP_TO_4=" ";  
       }
 
if ($SHIP_TO_4 != "")        
  if ($SHIP_TO_STATE != "")  
    if (strstr($SHIP_TO_4,$SHIP_TO_STATE) != "")
       {$SHIP_TO_4=" ";
       }
    
if ($SHIP_TO_4 != "")  
  if ($SHIP_TO_ZIP != "")      
   if (strstr($SHIP_TO_4,$SHIP_TO_ZIP)   != "")
   {  $SHIP_TO_4=" ";
   }
   
// Remove Country if US  
if ($SHIP_TO_4 != "")      
   if (strstr($SHIP_TO_4," US") != "")
   {  $SHIP_TO_4=" ";
   }    
  
// NOW FIX THE BILLTO ADDRESS

$BILLING_ADDRESS = $BILL_TO;

// NOW do the billing   
   
   $BILLING_TO_ZIP=getTagZip($BILLING_ADDRESS);
   $BILLING_TO_STATE=getTagState($BILLING_ADDRESS);     
   $BILLING_TO_CITY=getTagCity($BILLING_ADDRESS);       
   $BILLING_TO_STREET=getTagStreet($BILLING_ADDRESS);     
   $BILLING_TO_STREET=str_replace("\n"," ",$BILLING_TO_STREET);

    if (strlen(stristr($BILLING_ADDRESS,"<br>"))==0)
    $BILLING_ADDRESS = nl2br($BILLING_ADDRESS);

       $billing_arr = explode('<br />',$BILLING_ADDRESS);
  
    
$billing_arr[0] = str_replace("\n"," ",$billing_arr[0]);
$billing_arr[1] = str_replace("\n"," ",$billing_arr[1]);
$billing_arr[2] = str_replace("\n"," ",$billing_arr[2]);
$billing_arr[3] = str_replace("\n"," ",$billing_arr[3]);       
$billing_arr[4] = str_replace("\n"," ",$billing_arr[4]);

$BILL_TO_1 =  $billing_arr[1];    
$BILL_TO_2 =  $billing_arr[2];   
$BILL_TO_3 =  $billing_arr[3];  
$BILL_TO_4 =  $billing_arr[4];   

// TEST CLEAR BILL
//=================================================================================

 if ($BILL_TO_2 != "")
  if ($BILLING_TO_CITY != "")    
    if (strstr($BILL_TO_2,$BILLING_TO_CITY)  != "") 
       {$BILL_TO_2=" ";  
       }
 
if ($BILL_TO_2 != "")        
  if ($BILLING_TO_STATE != "")  
    if (strstr($BILL_TO_2,$BILLING_TO_STATE) != "")
       {$BILL_TO_2=" ";
       }
    
if ($BILL_TO_2 != "")  
  if ($BILLING_TO_ZIP != "")      
   if (strstr($BILL_TO_2,$BILLING_TO_ZIP)   != "")
   {  $BILL_TO_2=" ";
   }
   
// Remove Country if US  
if ($BILL_TO_2 != "")      
   if (strstr($BILL_TO_2," US") != "")
   {  $BILL_TO_2=" ";
   }   
      
//+++++++++++++++++
 if ($BILL_TO_3 != "")
  if ($BILLING_TO_CITY != "")    
    if (strstr($BILL_TO_3,$BILLING_TO_CITY)  != "") 
       {$BILL_TO_3=" ";  
       }
 
if ($BILL_TO_3 != "")        
  if ($BILLING_TO_STATE != "")  
    if (strstr($BILL_TO_3,$BILLING_TO_STATE) != "")
       {$BILL_TO_3=" ";
       }
    
if ($BILL_TO_3 != "")  
  if ($BILLING_TO_ZIP != "")      
   if (strstr($BILL_TO_3,$BILLING_TO_ZIP)   != "")
   {  $BILL_TO_3=" ";
   }   

// Remove Country if US  
if ($BILL_TO_3 != "")      
   if (strstr($BILL_TO_3," US") != "")
   {  $BILL_TO_3=" ";
   }   
   
//+++++++++++++++++   
 if ($BILL_TO_4 != "")
  if ($BILLING_TO_CITY != "")    
    if (strstr($BILL_TO_4,$BILLING_TO_CITY)  != "") 
       {$BILL_TO_4=" ";  
       }
 
if ($BILL_TO_4 != "")        
  if ($BILLING_TO_STATE != "")  
    if (strstr($BILL_TO_4,$BILLING_TO_STATE) != "")
       {$BILL_TO_4=" ";
       }
    
if ($BILL_TO_4 != "")  
  if ($BILLING_TO_ZIP != "")      
   if (strstr($BILL_TO_4,$BILLING_TO_ZIP)   != "")
   {  $BILL_TO_4=" ";
   }
   
// Remove Country if US  
if ($BILL_TO_4 != "")      
   if (strstr($BILL_TO_4," US") != "")
   {  $BILL_TO_4=" ";
   }
      

    
     
//===============================================================================    
// NOW PARSE THE ADDRESSES   END
//===============================================================================	
$data = $recordCount .  "\t" .  $First_Name." ".$Last_Name . "\t" .  $date_updated . "\t" . $ORDERNUMBER . "\t" . "Net 10" . "\t" . $DepartmentName . "\t" . $row['service_agency'] . "\t" . $New_DATE . "\t";
   
$data = $data . $BILL_TO_1 . "\t" . $BILL_TO_2 . "\t" . $BILL_TO_3 . "\t" . $BILL_TO_4 . "\t" . $BILLING_TO_CITY . "\t" . $BILLING_TO_STATE . "\t" . $BILLING_TO_ZIP . "\t";

$data = $data . "US" . "\t" . $SHIP_TO_1 . "\t" . $SHIP_TO_2 . "\t" . $SHIP_TO_3 . "\t" . $SHIP_TO_4 . "\t" . $SHIP_TO_CITY . "\t" . $SHIP_TO_STATE . "\t" . $SHIP_TO_ZIP . "\t" . "US" . "\t" . $Phone_Number . "\t" . " " . "\t" . $Email . "\t" . $First_Name . "\t" . $Last_Name . "\t" . " " . "\t" . $New_DATE . "\t" . "Truck" . "\t" . " " . "\t" . " " . "\t" . $row['item_number'] . "\t" . $row['quantity'] . "\t" . $description . "\t" . $row['price'] . "\t" . "No" . "\t" . " " . "\t" . $New_DATE . "\t" . " " . "\t" . " " . "\t" . " " . "\t" . "Yes" . "\t" . " " . "\t" . " " ."\n";

   fputs($fp, $data);  
 //  echo $data;
  }
//===============================  
//EXIT DB  
//===============================
  fclose($fp); #close the file


//=============================================
// Create the download link for the file
//=============================================

print "<p><font face=\"Arial\">";
print "<b><a href=\"./invoice.txt\">";
print "CLICK HERE TO DOWNLOAD SPREADSHEET</a></b></font></p>";

include "adminfooter.php";


//-------------------------------------------------------------
// GET the translink from orderhistory given the MOMS Number
//-------------------------------------------------------------
function getTransLink($ORDERNUMBER)
{
$select_supplier_query = mysql_query("SELECT `translink` FROM `orderhistory` WHERE `purchase_order`='$ORDERNUMBER' ");
$number = mysql_num_rows($select_supplier_query);

 if($number == 0)
 { 
    $translink = '000000';
 }
 else
 {
    $result_vendor = mysql_fetch_assoc($select_supplier_query);
    $translink = $result_vendor['translink'];
 }    

  return $translink;
}



//-------------------------------------------------------------
// GET the BILLTO from orderhistory given the the translink
//-------------------------------------------------------------
function getBILLTO($TRANSLINK)
{
 global $SHIP_TO;
 
$select_supplier_query = mysql_query("SELECT `billing_address` , `ship_to` FROM `dodaccounts` WHERE `translink`='$TRANSLINK' ");
$number = mysql_num_rows($select_supplier_query);

if($number == 0)
    $BILL_TO = '000000';
   else
   {
    $result_vendor = mysql_fetch_assoc($select_supplier_query);
    $BILL_TO = $result_vendor['billing_address'];
    $SHIP_TO = $result_vendor['ship_to'];   
    }    
  return $BILL_TO;
}


?>