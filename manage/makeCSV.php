<?php

include "../globals.php";
include "../pager.php";
include "adminheader.php";

function filter_value($val) {
    $val = str_replace(",", ";", $val);
    $val = preg_replace("/\s+/", " ", $val);
    return trim($val);
}

$page = (isset($_REQUEST['page']) && $_REQUEST['page'] > 1) ? $_REQUEST['page'] : 1;
$limit = 5000;

$result = mysql_fetch_assoc(mysql_query("SELECT COUNT(`id`) AS `num` FROM `product2`"));
$total = $result['num'];

$result2 = mysql_fetch_assoc(mysql_query("SELECT COUNT(`id`) AS `num` FROM `product2` WHERE `FOB` != ''"));
$total2 = $result2['num'];


$result_global = mysql_fetch_assoc(mysql_query("SELECT COUNT(`id`) AS `num` FROM `product2` WHERE `supplier` LIKE '%GLOBAL%'"));
$total_global = $result_global['num'];


$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;

$pager2 = Pager::getPagerData($total2, $limit, $page);
$offset2 = $pager2->offset;

$pager_global = Pager::getPagerData($total_global, $limit, $page);
$offset_global = $pager_global->offset;

echo '<div style="width:700px; margin:0px auto; text-align:center; padding:40px;">';
echo '<table>
    <tr>
    <td valign="top" align="center" style="border-right:1px solid">
    ';
echo '<h1 style="color:blue; margin-bottom:30px;">EXPORT ALL PRODUCTS AS CSV</h1>';
for ($i = 1; $i <= $pager->numPages; $i++) {
    print "<p><font face=\"Arial\">";
    print "<b><a href=\"makeCSV.php?action=download&page=$i\">";
    print "Download CSV $i</a></b></font></p>";
}
echo '</td>
    <td valign="top"  align="center"  style="border-right:1px solid">
        ';

echo '<h1 style="color:blue; margin-bottom:30px;">EXPORT FOB ITEMS ONLY</h1>';
for ($j = 1; $j <= $pager2->numPages; $j++) {
    print "<p><font face=\"Arial\">";
    print "<b><a href=\"makeCSV.php?action=downloadFOB&page=$j\">";
    print "Download CSV $j</a></b></font></p>";
}
echo '</td>
<td valign="top"  align="center">
';

echo '<h1 style="color:blue; margin-bottom:30px;">EXPORT GLOBAL ITEMS ONLY</h1>';
for ($j = 1; $j <= $pager_global->numPages; $j++) {
print "<p><font face=\"Arial\">";
print "<b><a href=\"makeCSV.php?action=downloadGLOBAL&page=$j\">";
print "Download CSV $j</a></b></font></p>";
}
echo '</td>

         </tr>
         <tr >
         <td colspan="3">
         <hr style="border:solid 1px blue">
         <p style="margin-top:20px;"><a href="makeCSV.php?action=downloadORSPrice">Download Price Match Report CSV</a></p>
         </td>
         </tr>
         </table>
         
         ';
echo '</div>';

if ($_GET['action'] == 'downloadORSPrice') {
    $filename = "csv_export/PriceMatchReport.csv";
    $save_path = $filename;

    $fields = array('List_Price',
        'Potential Profit',
        'Item_Discount',
        'Part_Number',
        'Item_Description',
        'Minimum_Order_Quantity',
        'On-line Price',
        'Level_3_Customized'
    );
    $fp = fopen($save_path, "w"); #open for writing
//    $column_query = mysql_query("SHOW COLUMNS FROM `ORSDATA`");
//    while ($column_row = mysql_fetch_array($column_query)) {
//        if ($column_row['Field'] != 'id') {
//            $column_name = $column_row['Field'];
//            $columns[] = $column_name;
//        }
//    }
    if (!empty($fields)) {
        fputcsv($fp, $fields);
    }
    $query = mysql_query("SELECT * FROM `ORSDATA`");
    if (mysql_num_rows($query)) {
        while ($row = mysql_fetch_assoc($query)) {
            $min_order = $row['Minimum_Selling_Quantity'] * $row['Minimum_Order_Quantity'];
            $recommended_price = $row['Level_3_Customized'] * $min_order;
            if ($recommended_price < 100) {
                $recommended_price = $recommended_price + 5;
            }
            $recommended_price = $recommended_price + ($recommended_price * 15 / 100);
            $potential_profits = $recommended_price - $row['Level_3_Customized'];

            $data = array(
                number_format($row['List_Price'],2),
                number_format($potential_profits,2),
                $row['Item_Discount'],
                $row['Part_Number'],
                $row['Item_Description'],
                $row['Minimum_Order_Quantity'],
                number_format($recommended_price,2),
                number_format($row['Level_3_Customized'],2)
            );
            fputcsv($fp, $data);
        }
    }
    fclose($fp); #close the file
    header('Location: ' . $save_path);
    exit();
}

if ($_GET['action'] == 'download' && $page > 0) {
    $filename = "csv_export/Products_$page.csv";
    $save_path = $filename;
    $fp = fopen($save_path, "w"); #open for writing
    $column_query = mysql_query("SHOW COLUMNS FROM `product2`");
    while ($column_row = mysql_fetch_array($column_query)) {
        if ($column_row['Field'] != 'id') {
            $column_name = $column_row['Field'];
            $columns[] = $column_name;
        }
    }
    if (!empty($columns)) {
        fputcsv($fp, $columns);
    }
    $sql = "SELECT * FROM `product2` LIMIT $offset, $limit";
    $result = mysql_query($sql);
    while ($row = mysql_fetch_assoc($result)) {
        unset($row['id']);
        fputcsv($fp, $row);
    }
    fclose($fp); #close the file
    header('Location: ' . $save_path);
    exit();
}
if ($_GET['action'] == 'downloadFOB' && $page > 0) {
    $filename = "csv_export/Products_FOB_$page.csv";
    $save_path = $filename;
    $fp = fopen($save_path, "w"); #open for writing
    $column_query = mysql_query("SHOW COLUMNS FROM `product2`");
    while ($column_row = mysql_fetch_array($column_query)) {
        if ($column_row['Field'] != 'id') {
            $column_name = $column_row['Field'];
            $columns[] = $column_name;
        }
    }
    if (!empty($columns)) {
        fputcsv($fp, $columns);
    }
    $sql = "SELECT * FROM `product2` WHERE `FOB` != '' LIMIT $offset, $limit";
    $result = mysql_query($sql);
    while ($row = mysql_fetch_assoc($result)) {
        unset($row['id']);
        fputcsv($fp, $row);
    }
    fclose($fp); #close the file
    header('Location: ' . $save_path);
    exit();
}

if ($_GET['action'] == 'downloadGLOBAL' && $page > 0) {
    $filename = "csv_export/Products_GLOBAL_$page.csv";
    $save_path = $filename;
    $fp = fopen($save_path, "w"); #open for writing   

    $columns = array(
        'description'=>'DESCRIPTION',
        'description1'=>'DESCRIPTION',
        'WESTCARB_PART_NUMBER'=>'PART_NUMBER',
        'EXPANDED_PRODUCT_DESCRIPTION'=>'DESCRIPTION', 
        'nsn'=>'SKU',     
        'mfgpart'=>'MFG_PART_NUMBER',
        'mfgname'=>'MANUFACTURE',
        'CUSTOMER'=>'CUSTOMER',
        'position'=>'POSITION',
        'UNIT_OF_ISSUE'=>'PRICE_UNIT',
        'PACKAGE_QTY'=>'PACKAGE',
        'leadtime'=>'LEAD_TIME',
        'CUSTOMER'=>'CUSTOMER',
        'KEYWORD'=>'KEYWORD',
        'current_country_of_origin'=>'COUNTRY_OF_ORIGIN',
        'category'=>'CATEGORY',
        'subcategory'=>'SUBCATEGORY',
        'ARTFILE_NAME'=>'ALT_IMAGE',
        'image_hyperlink'=>'IMAGE_URL',
        'FOB'=>'FOB',
        'PROD_LENGTH'=>'ITEM_LENGTH',
        'PROD_HEIGHT'=>'ITEM_HEIGHT',
        'PROD_WIDTH'=>'ITEM_WIDTH',
        'PROD_CUBE'=>'ITEM_CUBE',
        'PROD_WEIGHT'=>'ITEM_WIEGHT',
        'HAZARDOUS'=>'HAZMATPRODNAME',
        'vendpart'=>'VENDPART',
        'shortddesc'=>'SHORTDDESC',
        'longdesc'=>'LONGDESC',
        'isscode'=>'ISSCODE',
        'qty_unit'=>'QTY_UNIT',
        'qp_unit'=>'QP_UNIT',
        'weight'=>'WEIGHT',
        'sin'=>'SIN',
        'ppoint'=>'PPOINT',
        'incr_of'=>'INCR_OF',
        'p_www'=>'P_WWW',
        'warnumber'=>'WARNUMBER',
        'supplier'=>'SUPPLIER'
    );
    if (!empty($columns)) {
        fputcsv($fp, $columns, "\t");
    }
    $columns_select_str = '';
    foreach($columns as $col_name=>$col_alias){
        if($col_name == 'description1'){
            $col_name = 'description';
            $col_alias = 'description1';
        }
        else if($col_name == 'EXPANDED_PRODUCT_DESCRIPTION'){
            $col_alias = 'EXPANDED_PRODUCT_DESCRIPTION';
        }
        $columns_select_str .= "`$col_name` AS `$col_alias`, ";
    }
    $columns_select_str = substr($columns_select_str, 0, -2);
    $sql = "SELECT $columns_select_str FROM `product2` WHERE `supplier` LIKE '%GLOBAL%' LIMIT $offset_global, $limit";
    
    $result = mysql_query($sql);
    while ($row = mysql_fetch_assoc($result)) {
        fputcsv($fp, $row, "\t", '"');
    }
    fclose($fp); #close the file
    header('Location: ' . $save_path);
    exit();
}



include "adminfooter.php";
?>
