<?
 
include "../globals.php";
include "../DODMAIL/bodyparser.php";
include "updateStatusFunctions.php";

$STREET_ARRAY=array();   //mmm     

if(isset($_REQUEST['ordernum']))
$ordernumber = $_REQUEST['ordernum'];
else
$ordernumber = 0;

$TRANSLINK = $_REQUEST['TRANSLINK'];

// COPYRIGHT CHRONICLES SYSTEMS, INC.
  
  $THEDATE = date("c");
  
  $doc = new DOMDocument('1.0', 'UTF-8');
  $doc->formatOutput = true;
  
  
  $DTOP = $doc->createElement( "DOCTMP" ); 
  $doc->appendChild( $DTOP );
  
  $CXML = $doc->createElement( "cXML" ); 
  $doc->appendChild( $CXML );
  
  $TOP_COMMENT=$doc->createComment("This is a test comment"); 
  $doc->appendChild( $TOP_COMMENT );
  
 // Define top of XML Document.
 
  // Credential Attribute
  $A_PAYLOAD = $doc->createAttribute("payloadID");
  $V_DUNS = $doc->createTextNode("PO-Test@westcarb.com");
  $A_PAYLOAD->appendChild($V_DUNS);
  $CXML->appendChild($A_PAYLOAD);
  
  // Credential Attribute
  $A_TIME = $doc->createAttribute("timestamp");
  $V_STAMP = $doc->createTextNode($THEDATE); // <---------- FIX
  $A_TIME->appendChild($V_STAMP);
  $CXML->appendChild($A_TIME);
    

  // Credential Attribute
  $A_VERSION = $doc->createAttribute("version");
  $V_NUMBER = $doc->createTextNode("1.2.017");
  $A_VERSION->appendChild($V_NUMBER);
  $CXML->appendChild($A_VERSION);
      
  
  
    
  $Header = $doc->createElement( "Header" );
  $doc->appendChild( $Header );
  
// CONTINUE
//-----------------------------------------------
// FROM
//-----------------------------------------------
  $From = $doc->createElement( "From" );
  $Credential = $doc->createElement( "Credential" );
  
  // Credential Attribute
  $A_DOMAIN = $doc->createAttribute("domain");
  $Credential->appendChild($A_DOMAIN);
  // create attribute value node for domain
  $V_DUNS = $doc->createTextNode("DUNS");
  $A_DOMAIN->appendChild($V_DUNS);  
  
  
  $Identity = $doc->createElement( "Identity" );
  $Identity->appendChild( $doc->createTextNode( "107199999"));
  
  $Credential->appendChild($Identity);  
   
  $From->appendChild( $Credential);

//-----------------------------------------------  
// TO  
//-----------------------------------------------
  $To = $doc->createElement( "To" );
  $Credential = $doc->createElement( "Credential" );
  
     // Credential Attribute
  $A_DOMAIN = $doc->createAttribute("domain");
  // create attribute value node for domain
  $V_DUNS = $doc->createTextNode("DUNS");
  $A_DOMAIN->appendChild($V_DUNS);
  $Credential->appendChild($A_DOMAIN);  
  
  
  $Identity = $doc->createElement( "Identity" );
//  $Identity->appendChild( $doc->createTextNode( "606788404-T")); TEST IDENTITY
  $Identity->appendChild( $doc->createTextNode( "606788404"));  
  
  $Credential->appendChild($Identity);  
  

  
  $To->appendChild( $Credential);

//-----------------------------------------------
// Sender
//-----------------------------------------------
  $Sender = $doc->createElement( "Sender" );
  $Credential = $doc->createElement( "Credential" );
  
     // Credential Attribute
  $A_DOMAIN = $doc->createAttribute("domain");
  // create attribute value node for domain
  $V_DUNS = $doc->createTextNode("westcarb.com");
  $A_DOMAIN->appendChild($V_DUNS);
  $Credential->appendChild($A_DOMAIN);
  
  $Identity = $doc->createElement( "Identity" );
  $Identity->appendChild( $doc->createTextNode( "westcarb.com"));
  
  $SharedSecret = $doc->createElement( "SharedSecret" );
  $SharedSecret->appendChild( $doc->createTextNode( "westcarb.com"));
  
  
  $UserAgent = $doc->createElement( "UserAgent" );
  $UserAgent->appendChild( $doc->createTextNode( "CXML Purchase Order Request"));
  
  $Credential->appendChild($Identity);  
  $Credential->appendChild($SharedSecret);    
  
  $Sender->appendChild($Credential);
  $Sender->appendChild($UserAgent);  

//----------------------------------
// ADD EVERYTHING & END THE HEADER
//----------------------------------
  $Header->appendChild( $From );
  $Header->appendChild( $To );
  $Header->appendChild( $Sender );  
  
  $CXML->appendChild( $Header );    
//***********************************************************  
//===========================================================
// FORMAT THE TOP PART OF THE REQUEST
//===========================================================
//***********************************************************
// GET THE SHIPTO AND OTHER ADDRESS INFO FROM THE ACCOUNT TABLE

$select_ACCOUNT_query = "SELECT * FROM dodaccounts WHERE translink ='$TRANSLINK'";
$select_ACCOUNT_sql = mysql_query($select_ACCOUNT_query);
$select_ACCOUNT_number = mysql_num_rows($select_ACCOUNT_sql);

	while($select_account_result1 = mysql_fetch_array($select_ACCOUNT_sql))
	{ 
		$SHIP_ADD=$select_account_result1['ship_to'];
		$SHIP_PHONE=$select_account_result1['phone'];
		$SHIP_EMAIL=$select_account_result1['email'];	
                $SHIP_COMPANY=$select_account_result1['major_command'];        			
	        $SHIP_SERVICE_AGENCY=$select_account_result1['service_agency'];  
	                        	
		$SHIP_ADD  = trim($SHIP_ADD);    
		$SHIP_PHONE = trim($SHIP_PHONE);    
		$SHIP_EMAIL = trim($SHIP_EMAIL);    
                $SHIP_COMPANY = trim($SHIP_COMPANY);     
                $SHIP_SERVICE_AGENCY = trim($SHIP_SERVICE_AGENCY);                    
        		   
        }

// NOW PROCESS THE DATA
//print "<br>SHIP TO:";
//print $SHIP_ADD;

$THEZIP = getTagZip($SHIP_ADD);
$THEState = getTagState($SHIP_ADD);
$THECity = getTagCity($SHIP_ADD);       
$THEStreet =getTagStreet($SHIP_ADD);   

$TAGStreet = $THEStreet;
$THEStreet = str_replace("\r\n", ' ', $THEStreet);
$THEStreet = str_replace("\n", ' ', $THEStreet);

$THEName=  getTag($SHIP_ADD,"Attention:");
if ($THEName == "")
{
  $THEName=getTagBlankName($SHIP_ADD);       
}
/*
print "<br><b>Name: </b>";
print $THEName= trim($THEName);

print "<br><b>Street: </b>";
print $THEStreet= trim($THEStreet);

print "<br><b>City: </b>";
print $THECity= trim($THECity);

print "<br><b>State: </b>";
print $THEState= trim($THEState);

print "<br><b>ZipCode: </b>";
print $THEZIP = trim($THEZIP);
  
print "<br>=============================<br>";

*/

  $Request = $doc->createElement( "Request" );
  $doc->appendChild( $Request );
  
    $CXML->appendChild( $Request );
    
  $OrderRequest = $doc->createElement( "OrderRequest" );
  $doc->appendChild( $OrderRequest );
  
  $Request->appendChild( $OrderRequest );  
  
  // FORMAT THE ORDER REQUEST HEADER
  $OrderRequestHeader = $doc->createElement( "OrderRequestHeader" );
  $doc->appendChild( $OrderRequestHeader );  
  // ATTRIBUTES
  
  //++ orderNo Attribute
  $A_ORDERID = $doc->createAttribute("orderID");
  $ORDER_NUMBER=$ordernumber; // <-------- FIX
  $V_ORDNUM = $doc->createTextNode($ORDER_NUMBER);
  $A_ORDERID->appendChild($V_ORDNUM);
  $OrderRequestHeader->appendChild($A_ORDERID);  
  
  
   //++ ORDERDATE Attribute
  $A_ORDERDATE = $doc->createAttribute("orderDate");

  
  $V_TIME = $doc->createTextNode($THEDATE); //<-------------
  $A_ORDERDATE->appendChild($V_TIME);
  $OrderRequestHeader->appendChild($A_ORDERDATE);  
  
  //++ type New Attribute
  $A_TYPE = $doc->createAttribute("type");
  $V_NEW = $doc->createTextNode("new");
  $A_TYPE->appendChild($V_NEW);
  $OrderRequestHeader->appendChild($A_TYPE);  
  
  //++ typeType Regural New Attribute
  $A_ORDERTYPE = $doc->createAttribute("orderType");
  $V_REGULAR = $doc->createTextNode("regular");
  $A_ORDERTYPE->appendChild($V_REGULAR);
  $OrderRequestHeader->appendChild($A_ORDERTYPE);  
    
  $OrderRequest->appendChild( $OrderRequestHeader );  
     
  // TOTAL LINE FOR ALL
  $Total = $doc->createElement( "Total" );
  $doc->appendChild( $Total );
  

      $Money = $doc->createElement( "Money" ); 
// ADD THIS PART AT THE END  
//       $Money->appendChild( $doc->createTextNode($THEORDERTOTAL));
         
      //++ Money Currency New Attribute
       $A_CURRENCY = $doc->createAttribute("currency");
       $A_USD = $doc->createTextNode("USD");
       $A_CURRENCY->appendChild($A_USD);
       $Money->appendChild($A_CURRENCY); 
       
  $Total->appendChild( $Money );

  
  //==========================================================
  // SHIP TO *************************************************
  //==========================================================
  $ShipTo = $doc->createElement( "ShipTo" );
  $doc->appendChild( $ShipTo );  
  
    $Address = $doc->createElement( "Address" );
    $doc->appendChild( $Address ); 
    
    // Name SHIPTP
     $AddressName = $doc->createElement( "Name" );
     $doc->appendChild( $AddressName );  
     $Address->appendChild( $AddressName ); 
                    
     if ($SHIP_COMPANY == "")
     {
        $SHIP_COMPANY=$SHIP_SERVICE_AGENCY;
     }
                                    
          //++ text
                   $T_WCNAME = $doc->createTextNode($SHIP_COMPANY);       
        //  $T_WCNAME = $doc->createTextNode($THEName);     
       
                  $AddressName->appendChild( $T_WCNAME );
     //++ ARR                  
                   $A_ADDNAME = $doc->createAttribute("xml:lang");
                   $V_ADDNAME = $doc->createTextNode("en");
                   $A_ADDNAME->appendChild($V_ADDNAME);
                   $AddressName->appendChild($A_ADDNAME);    
     
     //PostAddress
     $PostAddress = $doc->createElement( "PostalAddress" );
     $doc->appendChild( $PostAddress );  
     $Address->appendChild( $PostAddress );      

       //===========================================
       //DeliverTo
       $DeliverTo = $doc->createElement( "DeliverTo" );
       $doc->appendChild( $DeliverTo );  
               $T_THEName = $doc->createTextNode($THEName);
               $DeliverTo->appendChild( $T_THEName );         
       $PostAddress->appendChild( $DeliverTo ); 
       
       
      // START OF FUNCTION CALL.
       
       AllStreets($TAGStreet);  //VX

      $max=count($STREET_ARRAY);  
      $j=1;
    
       while ($j < $max)
       {
      // XML Formatting goes here
      //  print "STREET = " .    $STREET_ARRAY[$j] . "<br>";
        
        $THEStreet = $STREET_ARRAY[$j];
        $j++;
      
       
       // NOW FOR THE XML PART
       $THEStreet= trim($THEStreet);  // Remove the spaces
       $Street = $doc->createElement( "Street" );
       $doc->appendChild( $Street );  
                $T_THEStreet = $doc->createTextNode($THEStreet);
                $Street->appendChild( $T_THEStreet );         
       $PostAddress->appendChild( $Street );      
       }
       
       
         
     // END OF WHILE
     
       $City = $doc->createElement( "City" );
       $doc->appendChild( $City );  
                $T_THECity = $doc->createTextNode($THECity);
                $City->appendChild( $T_THECity );          
       $PostAddress->appendChild( $City );      
     
            $THEState= trim($THEState); // Remove the spaces
       $State = $doc->createElement( "State" );
       $doc->appendChild( $State );  
                $T_TheState = $doc->createTextNode($THEState);
                $State->appendChild( $T_TheState );       
       $PostAddress->appendChild( $State );      

            $THEZIP= trim($THEZIP); // Remove the spaces
       $PostalCode = $doc->createElement( "PostalCode" );
       $doc->appendChild( $PostalCode );  
                $T_ZipCode = $doc->createTextNode($THEZIP);
                $PostalCode->appendChild( $T_ZipCode );     
       $PostAddress->appendChild( $PostalCode );    
   
       
       
       //====================================
       // COUNTRY NODE
       $Country = $doc->createElement( "Country" );
       $doc->appendChild( $Country );  
       $PostAddress->appendChild( $Country );      
       
         $T_CountryCode1 = $doc->createTextNode("United States");                  
         $Country->appendChild( $T_CountryCode1 );
  
                            //++ attr
                            $A_CountryCode1 = $doc->createAttribute("isoCountryCode");
                            $V_CountryCode1 = $doc->createTextNode("US");
                            $A_CountryCode1->appendChild($V_CountryCode1);
                            $Country->appendChild($A_CountryCode1);  
       // END                          
     
     
     // SHIPTO EMAIL NODE
      $TEmail = $doc->createElement( "Email" );
      $doc->appendChild( $TEmail );  
      $Address->appendChild( $TEmail );   
      

                  //++ text
                  $T_TEmail = $doc->createTextNode($SHIP_EMAIL);                  
                  $TEmail->appendChild( $T_TEmail );
  
                            //++ attr
                           $A_TEmail = $doc->createAttribute("name");
                           $V_TEmail = $doc->createTextNode("default");
                           $A_TEmail->appendChild($V_TEmail);
                           $TEmail->appendChild($A_TEmail);   
   
       
     
     //====================
     //main Phone NODE
      $TPhone = $doc->createElement( "Phone" );
      $doc->appendChild( $TPhone );  
      $Address->appendChild( $TPhone );   
      
                             //++ attr
                            $A_PHONE = $doc->createAttribute("name");
                            $V_PHONE = $doc->createTextNode("work");
                            $A_PHONE->appendChild($V_PHONE);
                            $TPhone->appendChild($A_PHONE);   
                            // END
      
      
               // Number
                $TelephoneNumber = $doc->createElement( "TelephoneNumber" );
                $doc->appendChild( $TelephoneNumber );  
                $TPhone->appendChild( $TelephoneNumber );   
      
               //======================
               // CountryCode
                  $CountryCode = $doc->createElement( "CountryCode" );
                  $doc->appendChild( $CountryCode );  
                  $TelephoneNumber->appendChild( $CountryCode );  
                  
                  //++ text
                  $T_CountryCode = $doc->createTextNode("1");                  
                  $CountryCode->appendChild( $T_CountryCode );
  
                            //++ attr
                            $A_CountryCode = $doc->createAttribute("isoCountryCode");
                            $V_CountryCode = $doc->createTextNode("US");
                            $A_CountryCode->appendChild($V_CountryCode);
                            $CountryCode->appendChild($A_CountryCode);   
                            // END
                            
                  
                //======================
                // Area Code
                  $Area = $doc->createElement( "AreaOrCityCode" );
                  $doc->appendChild( $Area );  
                  $TelephoneNumber->appendChild( $Area ); 
                  
                  
                  
                  
                  //END
               
                 //=========================================
                // Number
                  $FNumber = $doc->createElement( "Number" );
                  $doc->appendChild( $FNumber );  
                  $TelephoneNumber->appendChild( $FNumber ); 
                  
                  //++ text
                         $TX_Phone = $doc->createTextNode($SHIP_PHONE);
                         $FNumber->appendChild( $TX_Phone );  
                  // END
                  
                      
  
  $ShipTo->appendChild( $Address ); 
  
   //==================================
   // *********************************
   // *********************************   
   //==================================

  
  
  //==========================================================
  // BILL TO *************************************************
  //==========================================================
  $BillTo = $doc->createElement( "BillTo" );
  $doc->appendChild( $BillTo );  
  
    $Address = $doc->createElement( "Address" );
    $doc->appendChild( $Address ); 
    
    //=============================================
    // Name SHIPTP
     $AddressName = $doc->createElement( "Name" );
     $doc->appendChild( $AddressName );  
     
     //++ text
                   $T_WCNAME = $doc->createTextNode("WestCarb Enterprises");                  
                  $AddressName->appendChild( $T_WCNAME );
     //++ ARR                  
                   $A_ADDNAME = $doc->createAttribute("xml:lang");
                   $V_ADDNAME = $doc->createTextNode("en");
                   $A_ADDNAME->appendChild($V_ADDNAME);
                   $AddressName->appendChild($A_ADDNAME);      
                  
     $Address->appendChild( $AddressName ); 
     // end
     
     
     //PostAddress
     $PostAddress = $doc->createElement( "PostalAddress" );
     $doc->appendChild( $PostAddress );  
     $Address->appendChild( $PostAddress );      

       //===========================================
       // STREET 
       $Street = $doc->createElement( "Street" );
       $doc->appendChild( $Street );  
       $PostAddress->appendChild( $Street );   
                 //++text                 
                  $Street->appendChild( $doc->createTextNode("1717 E. Busch Blvd, Suite D"));   
 
       //===========================================
       // CITY NODE     
       $City = $doc->createElement( "City" );
       $doc->appendChild( $City );  
       $PostAddress->appendChild( $City );   
       
                          $City->appendChild( $doc->createTextNode("Tampa"));  

       //===========================================
       // STATE NODE      
       $State = $doc->createElement( "State" );
       $doc->appendChild( $State );  
       $PostAddress->appendChild( $State );   
          
                   $State->appendChild( $doc->createTextNode("Florida"));      
    
 
       //===========================================
       // POSTAL CODE NODE     
       $PostalCode = $doc->createElement( "PostalCode" );
       $doc->appendChild( $PostalCode );  
       $PostAddress->appendChild( $PostalCode );    
       
                  $PostalCode->appendChild( $doc->createTextNode("33612-8683"));     
       //===========================================
       // COUNTRY NODE       
       $Country = $doc->createElement( "Country" );
       $doc->appendChild( $Country );  
       $PostAddress->appendChild( $Country );     
                       
             $Country->appendChild( $doc->createTextNode("United States") );     
             
                         //++ attr
                            $A_CountryCode1 = $doc->createAttribute("isoCountryCode");
                            $V_CountryCode1 = $doc->createTextNode("US");
                            $A_CountryCode1->appendChild($V_CountryCode1);
                            $Country->appendChild($A_CountryCode1);  
                         // END
     
     // EMAIL NODE
     
      $TEmail = $doc->createElement( "Email" );
      $doc->appendChild( $TEmail );  
          //++ text
          $TX_EMAIL = $doc->createTextNode("orders@westcarb.com");
          $TEmail->appendChild( $TX_EMAIL );         
            //++ attr
         $A_EMAIL = $doc->createAttribute("name");
         $V_EMAILV = $doc->createTextNode("default");
         $A_EMAIL->appendChild($V_EMAILV);
         $TEmail->appendChild($A_EMAIL);  
         
     $Address->appendChild( $TEmail );  
     
     // PHONE NODE
     //WESTCARB Phone information 
      $TPhone = $doc->createElement( "Phone" );
      $doc->appendChild( $TPhone );  
      
            //++ attr
         $A_PHONE = $doc->createAttribute("name");
         $V_PHONEV = $doc->createTextNode("work");
         $A_PHONE->appendChild($V_PHONEV);
         $TPhone->appendChild($A_PHONE);        
      
      $Address->appendChild( $TPhone );   
      
               // Number
                $TelephoneNumber = $doc->createElement( "TelephoneNumber" );
                $doc->appendChild( $TelephoneNumber );  
                $TPhone->appendChild( $TelephoneNumber );   
      
               // CountryCode NODE
                  $CountryCode = $doc->createElement( "CountryCode" );
                  $doc->appendChild( $CountryCode );  
                  
                                              //++ attr
                            $A_CountryCode1 = $doc->createAttribute("isoCountryCode");
                            $V_CountryCode1 = $doc->createTextNode("US");
                            $A_CountryCode1->appendChild($V_CountryCode1);
                            $CountryCode->appendChild($A_CountryCode1);  
                            
                  //++ text
                  $T_CONCODE = $doc->createTextNode("1");                  
                  $CountryCode->appendChild( $T_CONCODE );
  
                            //++ attr
                            $A_CONCODE = $doc->createAttribute("isoCountryCode");
                            $V_CONCODE = $doc->createTextNode("US");
                            $A_CONCODE->appendChild($V_CONCODE);
                            $CountryCode->appendChild($A_CONCODE);   
                            
                  $TelephoneNumber->appendChild( $CountryCode );                            
                            
                
                // Area Code
                  $Area = $doc->createElement( "AreaOrCityCode" );
                  $doc->appendChild( $Area );  
                  $TelephoneNumber->appendChild( $Area ); 
             
                // Number
                  $FNumber = $doc->createElement( "Number" );
                  $doc->appendChild( $FNumber );  
                         //++ text
                         $TX_Phone = $doc->createTextNode("727-233-7067");
                         $FNumber->appendChild( $TX_Phone );        
                  
                  $TelephoneNumber->appendChild( $FNumber ); 
                  
                      
  
  $BillTo->appendChild( $Address ); 
  //// 
  
     $Fax = $doc->createElement( "Fax" );
     $doc->appendChild( $Fax );  
     
                            $A_Fax = $doc->createAttribute("name");
                            $V_Fax = $doc->createTextNode("work");
                            $A_Fax->appendChild($V_Fax);
                            $Fax->appendChild($A_Fax); 
     
     
               // Number
                $Fax_TelephoneNumber = $doc->createElement( "TelephoneNumber" );
                $doc->appendChild( $Fax_TelephoneNumber );  
                $Fax->appendChild( $Fax_TelephoneNumber );   
      
               // CountryCode
                  $CountryCode = $doc->createElement( "CountryCode" );
                  $doc->appendChild( $CountryCode );  
                  $Fax_TelephoneNumber->appendChild( $CountryCode );  
                  $T_CountryCode = $doc->createTextNode("1");
                  $CountryCode->appendChild( $T_CountryCode );     
                  
                            $A_CountryCode = $doc->createAttribute("isoCountryCode");
                            $V_CountryCode = $doc->createTextNode("US");
                            $A_CountryCode->appendChild($V_CountryCode);
                            $CountryCode->appendChild($A_CountryCode); 
                  
                
                // Area Code
                  $Area = $doc->createElement( "AreaOrCityCode" );
                  $doc->appendChild( $Area );  
                  $Fax_TelephoneNumber->appendChild( $Area ); 
             
                // Number
                  $FNumber = $doc->createElement( "Number" );
                  $doc->appendChild( $FNumber );  
                  $Fax_TelephoneNumber->appendChild( $FNumber ); 
                  
                         $T_FNumber = $doc->createTextNode("727-245-8989");
                         $FNumber->appendChild( $T_FNumber );                  
       

     $Address->appendChild( $Fax ); 
  

    // Payment After BillTo
    /*
       $Payment = $doc->createElement( "Payment" );
       $doc->appendChild( $Payment );  
       
                // Number
                  $PCard = $doc->createElement( "PCard" );
                  $doc->appendChild( $PCard );  
                  $Payment->appendChild( $PCard );       
                  
                    //++ attr
                            $A_PCARD = $doc->createAttribute("number");
                            $V_PCARD = $doc->createTextNode("4111111111111111");
                            $A_PCARD->appendChild($V_PCARD);
                            $PCard->appendChild($A_PCARD);    
                            
                            
 //++ attr experation
                            $A_PCARDX = $doc->createAttribute("expiration");
                            $V_PCARDX = $doc->createTextNode("2020-12-31");
                            $A_PCARDX->appendChild($V_PCARDX);
                            $PCard->appendChild($A_PCARDX);                                
       
 //++ attr name
                            $A_PCARDN = $doc->createAttribute("name");
                            $V_PCARDN = $doc->createTextNode("WestCarb Enterprises");
                            $A_PCARDN->appendChild($V_PCARDN);
                            $PCard->appendChild($A_PCARDN);           
      */ 
       
    // Comment After Payment
       $Comment = $doc->createElement( "Comments" );
       $doc->appendChild( $Comment );         
       
       $OrderRequestHeader->appendChild( $Total );  
       $OrderRequestHeader->appendChild( $ShipTo );   
       $OrderRequestHeader->appendChild( $BillTo );   
//       $OrderRequestHeader->appendChild( $Payment );            
       $OrderRequestHeader->appendChild( $Comment );            

       $OrderRequest->appendChild( $OrderRequestHeader ); 

//==============================================================================================================
// W H I L E    L O O P                          LINE ITEMS SECTION L O O P
//==============================================================================================================
//**************************************************************************
//**************************************************************************
//**************************************************************************
//==========================================================================
// NOW GET THE LINE ITEMS
//==========================================================================
$select_hitory_query = "SELECT * FROM orderhistory WHERE order_number='$ordernumber'";
// $select_hitory_sql = mysql_query($select_hitory_query);
$select_hitory_sql1 = mysql_query($select_hitory_query);
// $select_hitory_number = mysql_num_rows($select_hitory_sql);
$select_hitory_result = mysql_fetch_array($select_hitory_sql);

        $LINENUMBER=1; // to increament
	$total = 0;
	$THEORDERTOTAL = 0;
	while($select_hitory_result1 = mysql_fetch_array($select_hitory_sql1)) { 
		$LINE_QUANTITY=$select_hitory_result1['quantity'];
		$LINE_PARTNO=$select_hitory_result1['part_number'];
		$LINE_UOM=$select_hitory_result1['unit_of_measure'];
		$LINE_DESCRIPTION=$select_hitory_result1['description'];
		$LINE_UNITPRICE=$select_hitory_result1['unit_price'];
		
		$total +=$LINE_UNITPRICE;
		
		// Get the subtotal for this line to add at the end
		$QTYTOTAL = ($LINE_UNITPRICE * $LINE_QUANTITY);
		$THEORDERTOTAL +=$QTYTOTAL;
 
      // START OF LINE ITEM HEADER WITH ITEMOUT
      $ItemOut = $doc->createElement( "ItemOut" );
      $doc->appendChild( $ItemOut );  
      //+++++++++++++++++++++++++++++++++++
      //++ Qty of products in this line
         $A_Qty = $doc->createAttribute("quantity");
         $V_AmtNum = $doc->createTextNode($LINE_QUANTITY);
         $A_Qty->appendChild($V_AmtNum);
         $ItemOut->appendChild($A_Qty);  
         //++ Line Number 
         $A_LINE = $doc->createAttribute("lineNumber");
         $V_AmtNum = $doc->createTextNode($LINENUMBER);
         $A_LINE->appendChild($V_AmtNum);
         $ItemOut->appendChild($A_LINE); 
     
   
      // ITEMID NODE
               $ItemID = $doc->createElement( "ItemID" );
               $ItemOut->appendChild( $ItemID );  
               
                 $SupplierPartID = $doc->createElement( "SupplierPartID" );
                 $ItemID->appendChild( $SupplierPartID );  
                 $T_PARTNO = $doc->createTextNode($LINE_PARTNO);
                 $SupplierPartID->appendChild( $T_PARTNO );                   
               
                 $SupplierPartAuxiliary = $doc->createElement( "SupplierPartAuxiliary" );
                 $ItemID->appendChild( $SupplierPartAuxiliary );  
                 $T_APARTNO = $doc->createTextNode($LINE_PARTNO);                 
                 $SupplierPartAuxiliary->appendChild( $T_APARTNO );   
                 
                 
               
      // ITEM DETAIL NODE
               $ItemDetail = $doc->createElement( "ItemDetail" );
               $doc->appendChild( $ItemDetail );  
                             
                 //unit price
                 $UnitPrice = $doc->createElement( "UnitPrice" );
                 $ItemDetail->appendChild( $UnitPrice );  
 
                 
                 //++++ 
                   $UnitMoney = $doc->createElement( "Money" );
                   $UnitPrice->appendChild( $UnitMoney ); 
                         $A_CURR = $doc->createAttribute("currency");
                         $V_CUSD = $doc->createTextNode("USD");
                         $A_CURR->appendChild($V_CUSD);
                         $UnitMoney->appendChild($A_CURR); 
                   
                     $T_UNITAMOUNT = $doc->createTextNode($LINE_UNITPRICE);                 
                     $UnitMoney->appendChild( $T_UNITAMOUNT ); 
                     

                 //Description
                 $Description = $doc->createElement( "Description" );
                 $ItemDetail->appendChild( $Description );          
                 //+++
                         $A_DLANG = $doc->createAttribute("xml:lang");
                         $V_DEN = $doc->createTextNode("en");
                         $A_DLANG->appendChild($V_DEN);
                         $Description->appendChild($A_DLANG);                  
                 //+++++++++++
                 //++
                   $T_DESC = $doc->createTextNode($LINE_DESCRIPTION);                 
                   $Description->appendChild( $T_DESC );  
                 
                 
                 
                 //UnitOfMeasure
                 $UnitOfMeasure = $doc->createElement( "UnitOfMeasure" );
                 $ItemDetail->appendChild( $UnitOfMeasure );         
                 //+++++++++++
                 //++
                   $T_UOM = $doc->createTextNode($LINE_UOM);                 
                   $UnitOfMeasure->appendChild( $T_UOM );  
                 
                                          
               
                 //Classification
                 $Classification = $doc->createElement( "Classification" );
                 $ItemDetail->appendChild( $Classification );  
                         $J_APARTNO = $LINE_PARTNO; // text
                         $T_CVALUE = $doc->createTextNode($J_APARTNO);
                         $Classification->appendChild($T_CVALUE);   
                 
                          //+++ att
                         $A_CDOM = $doc->createAttribute("domain");
                         $V_SUPID  = $doc->createTextNode("SupplierPartID");
                         $A_CDOM->appendChild($V_SUPID);
                         $Classification->appendChild($A_CDOM);
                         
                         
                 
                 $ItemOut->appendChild( $ItemDetail );         
      
      $OrderRequest->appendChild( $ItemOut );     
      $LINENUMBER++;
 }  
 
 // Create the money branch to add the total for the order

       $Money->appendChild( $doc->createTextNode($THEORDERTOTAL));
   
  $theXMLData=$doc->saveXML();  
  
  print $theXMLData;  // Uncomment this before production
  
 
  $theXMLData = str_replace("<DOCTMP/>", "<!DOCTYPE cXML SYSTEM \"http://xml.cXML.org/schemas/cXML/1.2.017/cXML.dtd\">", $theXMLData);


      print "<h1>OfficeMax Purchase Order Process. </h1>";
      print "<h1>Please wait for completion message below. </h1>";   


 $MODE="PRODUCTION"; // SET THE MODE TO TEST OR PRODUCTION.
 
 if ($MODE=="TEST")
 {
     $ch = curl_init("https://real.qa.officemaxsolutions.com:5443/invoke/OMXBuyer/receive");    
 }
 else 
 {
     $ch = curl_init("https://real.officemaxsolutions.com:5443/invoke/OMXBuyer/receive");    
 }
 
    curl_setopt($ch, CURLOPT_HTTPHEADER,Array("Content-Type: text/xml")); 
    curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_POST, 1);    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $theXMLData); // use HTTP POST to send form data
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response. ###
    $resp = curl_exec($ch); //execute post and get results
    curl_close ($ch);

    print "<br><br>";  
    
    
// PROCESS RETURN
   $response_array = explode("\r\n", $resp);
   
    

   $i=count($response_array);
   
   $v=0;
  
   while ($v <$i + 1)
   {
     print "$response_array[$v]";
        
     if  (strpos( $response_array[$v]  , "<Status code=\"200\"" ) > 0)
     {
       print "<br><h1>Purchase Order was sent and received by OfficeMax Successfully</h1><br>";
       
       ChangeStatusOfOrder($ordernumber,"OMAX PO SENT");
     }
     
     if  (strpos( $response_array[$v]  , "<Status code=\"500\"" ) > 0)
     {
       print "<br><h1>Purchase Order Processing failed by OfficeMax</h1><br>";
       print "<br><h2>Return Code: 500 </h2><br>";   
       ChangeStatusOfOrder($ordernumber,"OMAX PO ERROR");
     }
     
      $v++;
   } 
    
  include "adminfooter.php";
  
  
  
//================================================================
//  TEST FUNCTION SECTION
//================================================================    
function AllStreets($SHIP)   
{
         global $STREET_ARRAY;
   $SHIP = str_replace("\n", "<br>", $SHIP);
  
   $ShipToZip= getTagZip($SHIP);
   
   //===================================== 
   // LOOK FOR ADDRESS FORMAT
   //===================================== 
   $STREET_ARRAY = explode("<br>", $SHIP);
}
 
 
function ChangeStatusOfOrder($ORDERNUMBERLOOKUP,$message)
{
 global $linkID;
 $resultID = mysql_query("update orderhistory set status = '$message' 
                          where order_number = '$ORDERNUMBERLOOKUP'",$linkID);                                 
                     
// Now update the status history                          
  updateStatusFunction($ORDERNUMBERLOOKUP,$linkID);
 
  print "<BR><h2>THE STATUS HAS BEEN UPDATED FOR THIS ORDER</h2><br>";     
 
  return $resultID; 
}  
 
  
?>