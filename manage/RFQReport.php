<?php
         include "../globals.php"; 
         include "../globalcss.php"; 
         include "../CSILib/CSIQuotesClass.php";

         $page = $_GET['page'];
         if ($page == NULL) 
         {  $page=0;
         }
	if(isset($_GET['mes']))
	$mes = $_GET['mes'];
	else
	$mes = "";
         
         print "<head><body>";
        //=======================================
        // MINIMUM DEFINITION for basic report  =
        //=======================================
        $Report=new CSIReport;
        $Report->tablename="QuotationRequest";  // define which table to work with
        $Report->columns="id,name,phone,email,company,itemname,partnumber"; // define which columns to display
        $Report->setCodition("order by id desc");
        
        //=========================================================
        // Optionals to define what to call for the action column =
        //=========================================================        
        $Report->setDeleteRow("DODQuotes.php");       // define delete and edit calls
        $Report->setEditRow("DODQuotes.php");
	if($mes == "delsuccess")
	$Report->setReportMessage("Quote Request Deleted Successfully");
	else
	$Report->setReportMessage(" ");

        $Report->setReportTitle("Requests : Quote for a Specific Product");
        
        //================================================
        // Optional what to call to add a new record     =
        //================================================            
       //   $Report->setAddFunction("Add new record","AddRecord.php");
                
        $limit=20; // How many rows to display3       
        $caller="RFQReport.php"; // Add the name of this function for pager control call back
        
        // Finally Display the report with page and limited rows  
        $Report->displayTable($page,$limit,$caller);

        
        print "</body></head>";
        
        exit;
        
?> 