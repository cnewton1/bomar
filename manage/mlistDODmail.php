<?php
         include "../globals.php"; 
         include "../globalcss.php"; 
//         include "../CSILib/CSIReportClass.php";
         include "../CSILib/CSIReportMailsClass.php";
         
         $page = $_GET['page'];
         if ($page == NULL) 
           $page=0;
         
         print "<head><body>";
        //=======================================
        // MINIMUM DEFINITION for basic report  =
        //=======================================
        $Report=new CSIMailsReport;
        $Report->tablename="dodraworder";  // define which table to work with
        $Report->columns="id,coded_message,translink"; // define which columns to display
        $Report->setCodition("order by id");
        //=========================================================
        // Optionals to define what to call for the action column =
        //=========================================================        
        //  $Report->setDeleteRow("DODOrderForm.php");       // define delete and edit calls
        
        $Report->setEditRow("mlistDODmailDetail.php");
        $Report->setEditTitle("Detail Record"); // What to call the link
        
        $Report->setReportTitle("DOD Mail Summary");
        
        //================================================
        // Optional what to call to add a new record     =
        //================================================            
       //   $Report->setAddFunction("Add new record","AddRecord.php");
                
        $limit=20; // How many rows to display3       
        $caller="mlistDODmail.php"; // Add the name of this function for pager control call back
        
        // Finally Display the report with page and limited rows  
        $Report->displayTable($page,$limit,$caller);

        
        print "</body></head>";
        
        exit;
        
?> 