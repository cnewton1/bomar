<?php
include "../globals.php";
include "adminheader.php";
include "../pager.php";
include "../MyImageFunction.php";
include 'image_functions.php';
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------
$page_name = 'Add Video Link';
$action = 'add';
$status = '1';
adminTitle("$page_name");

$allowedExtensions = array("jpg", "jpeg", "gif", "png");

function isAllowedExtension($fileName) {
    global $allowedExtensions;

    return in_array(strtolower(end(explode(".", $fileName))), $allowedExtensions);
}

function isEmail($email) {
    return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
}

function isURL($url) {
    if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) {
        return true;
    } else {
        return false;
    }
}

if ($_GET['action'] == 'edit' && $_GET['id'] != '') {
    $id = mysql_real_escape_string($_GET['id']);


    $video_query = mysql_query("SELECT * FROM `videos` WHERE `id` = '$id'");
    if (mysql_num_rows($video_query)) {

        $row = mysql_fetch_array($video_query);
        $video_title = $row['video_title'];
        $video_url = $row['video_url'];

        $page_name = 'Edit Video Link';
        $action = 'edit';
    }
}


if (isset($_POST['submit'])) {
    $action = $_POST['action'];

    $video_title = trim($_POST['video_title']);
    $video_url = trim($_POST['video_url']);

    $id = $_POST['id'];

    if ($video_title == '') {
        $error[] = 'Please enter video title';
    }
    if ($video_url == '') {
        $error[] = 'Please enter video url';
    } else if (!isURL($video_url)) {
        $error[] = 'Please enter valid video url';
    }

    if (empty($error)) {
        $video_title = mysql_real_escape_string($video_title);

        if ($action == 'add') {
            $query = mysql_query("INSERT INTO `videos` (`video_title`, `video_url`) VALUES ('$video_title', '$video_url')");
            if ($query) {
                header('Location: ManageTube.php');
                exit();
            } else {
                $error[] = mysql_error();
            }
        } else if ($action == 'edit') {
            $query = mysql_query("UPDATE `videos` SET `video_title` = '$video_title', `video_url` = '$video_url' WHERE `id` = '$id'");
            if ($query) {
                header('Location: ManageTube.php');
                exit();
            } else {
                $error[] = mysql_error();
            }
        }
    }
}
?>
<style>
    div.error{
        padding:2px 4px;
        margin:0px;
        border:solid 1px #FBD3C6;
        background:#FDE4E1;
        color:#CB4721;
        font-family:Arial, Helvetica, sans-serif;
        font-size:14px;
        font-weight:bold;
        text-align:center; 
    }
    div.success{
        padding:2px 4px;
        margin:0px;
        border:solid 1px #C0F0B9;
        background:#D5FFC6;
        color:#48A41C;
        font-family:Arial, Helvetica, sans-serif; font-size:14px;
        font-weight:bold;
        text-align:center; 
    }
    input[type="text"], input[type="password"], select 
    {
        width:450px; 
        height:25px; 
        border:1px solid #000;
    }
</style>


<div style="width: 900px;">
    <div style="font-size:22px; color:#00F; text-align:center; font-weight:bold; line-height:35px;"><?php echo $page_name; ?></div>

    <form method="post" action="">

        <div style="clear:both"></div>


        <table id="video_list" width="100%" cellpadding="10" cellspacing="10" border="0" style="border-collapse:collapse">
<?php
if (!empty($error)) {
    echo '<tr>
	<td colspan="2" style="color:red; font-weight:bold">' . implode("<br>", $error) . '</td>
	</tr>';
}
?>
            <tr>
                <td align="right"><strong>Video Title:</strong></td>
                <td><input type="text" name="video_title" value="<?php echo $video_title; ?>" required ></td>
            </tr>
            <tr>
                <td align="right"><strong>Your YouTube Video URL:</strong></td>
                <td><input type="text" name="video_url" value="<?php echo $video_url; ?>" required ></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="hidden" name="action" value="<?php echo $action; ?>">
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <input type="submit" name="submit" value="Submit">
                    <input type="button" name="cancel" value="Cancel" onClick="window.location = 'ManageTube.php';">
                </td>
            </tr>
        </table>


    </form>

<?php
include "adminfooter.php";
print "</div>";
?>



