<?php
//=================================================
// Function to Body Parse
//=================================================
/*
$message_body="my qweqe  aaatest:First
 BLOCK:
 LINE1
 LINE2
 LINE3
 
 psiodjf sdf:Second
 werwerwr ";

 */
// echo getTag($message_body,"aaatest");
// echo getTag($message_body,"sdf");


// echo getBlock($message_body,"BLOCK");

//----------------------------------------------------------------
// Read block tags
//----------------------------------------------------------------
function getBlock($message_body,$searchtag)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

  $thisBlock="";
  $FoundBlock=0;
  for ($x=0; $x<count($body); $x++)
  {
       // Break the body into each product line.
       if ($FoundBlock==0)
       {
         if (strlen(stristr($body[$x], $searchtag))>1) 
         { 
          $FoundBlock = 1;
        }
       }
       else
       {
         if (($body[$x] == "\r\n ") or ($body[$x] == "\r\n")) // must be "\r\n ";
         {
           $thisBlock = str_replace("\r\n", "<br>", $thisBlock);
           return $thisBlock;
         }
         if (strlen($body[$x]) < 5)
         {
          return $thisBlock;;
         }
         
           
         $thisBlock = $thisBlock .  $body[$x];
       }
   }// end for loop
   
   // Dont forget the last row.
    if ($FoundBlock==1)
    {
         if (strlen(stristr($body[$x], $searchtag))>1) 
         { 
          $FoundBlock = 1;
        }
    }
       
    return $thisBlock;
   
}



//----------------------------------------------------------------
// Read individual tags
//----------------------------------------------------------------
function getTag($message_body,$searchtag)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

           $mmCount = count($body);
           if ($mmCount < 2)
              {
              $body  = explode("<br>", $message_body);     
              }
           

  for ($x=0; $x<count($body); $x++)
  {
      // Break the body into each product line.
       if (strlen(stristr($body[$x], $searchtag))>1) 
       { 
                      return $inc_sec_off = substr(stristr($body[$x], ":"),1);               
       }
   }// end for loop
}


// Design to test the ShipToZip only
//----------------------------------------------------------------
function getTagZip($message_body)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

           $mmCount = count($body);
           if ($mmCount < 2)
              {
              $body  = explode("<br>", $message_body);     
                         $mmCount = count($body);   
              }
           

         $x = $mmCount - 2;
      // Break the body into each product line.

         $blen=strlen($body[$x]);
         $inc_sec_off = strrchr  ($body[$x]," ");      
         $inc_sec_off=  substr($inc_sec_off, 0, 6); 
         return $inc_sec_off;    

}

// Design to test the City only
//----------------------------------------------------------------
function getTagState($message_body)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

           $mmCount = count($body);
           if ($mmCount < 2)
              {
              $body  = explode("<br>", $message_body);     
                         $mmCount = count($body);   
              }
           

         $x = $mmCount - 2;
      // Break the body into each product line.

         $blen=strlen($body[$x]);
         $inc_sec_off = strrchr  ($body[$x],",");      
         $inc_sec_off=  substr($inc_sec_off, 1, 3); 
         return $inc_sec_off;    
}


// Design to test the City only
//----------------------------------------------------------------
function getTagCity($message_body)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

           $mmCount = count($body);
           if ($mmCount < 2)
              {
              $body  = explode("<br>", $message_body);     
                         $mmCount = count($body);   
              }
           

         $x = $mmCount - 2;
      // Break the body into each product line.

         $blen=strlen($body[$x]);
         $inc_sec_off = stripos   ($body[$x],",");      
         $inc_sec_off=  substr($body[$x],0,$inc_sec_off); 
         $inc_sec_off=  trim($inc_sec_off);     
         return $inc_sec_off;    
}


// Design to test the Street only
//----------------------------------------------------------------
function getTagStreet($message_body)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

           $mmCount = count($body);
           if ($mmCount < 2)
              {
              $body  = explode("<br>", $message_body);     
                         $mmCount = count($body);   
              }
           

         $x = 1;
      // Break the body into each product line.

         $inc_sec_off = $body[$x];
         $inc_sec_off .= $body[$x+1];     

         return $inc_sec_off;    
}

// Design to get the blank name tag without the attention 
//----------------------------------------------------------------
function getTagBlankName($message_body)
{
  //convert the body crlf to <br> tags if not done when the message was built
  if (strlen(stristr($message_body,"<br>"))==0)
  {
     $message_body = nl2br($message_body);
  }

           // take the body apart to get the individual elements
           $body  = explode("<br />", $message_body);

           $mmCount = count($body);
           if ($mmCount < 2)
              {
              $body  = explode("<br>", $message_body);     
                         $mmCount = count($body);   
              }
           

         $x = 0;
      // Break the body into each product line.

         $inc_sec_off = $body[$x];   

         return $inc_sec_off;    
}


?>