<?php
include "../globals.php"; 

include "adminheader.php";
adminTitle(" Discount Management ");

$pageTitle = "Add New Code";
$page= $_REQUEST['page'];
$calltype = 'A';

$allowedExtensions = array("jpg","jpeg","gif","png");

function isAllowedExtension($fileName) {
	global $allowedExtensions;
		$file_extension = strtolower(end(explode(".", $fileName)));
		return in_array($file_extension, $allowedExtensions);
		
	}

function check_exists_discount_code($code,$id)
{
	if($id != '')
	{
		$qry_add = "AND `id` != '$id'";
	}
	else
	{
		$qry_add = '';
	}
	$result = mysql_fetch_array(mysql_query("SELECT COUNT(*) AS `num` FROM `discount_code` WHERE `code` = '$code' $qry_add"));
	if($resut['num']>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

if($_GET['action']=='edit' && $_GET['id'] !='')
{
	$calltype = 'U';
	$id = intval($_GET['id']);
	$query = mysql_query("SELECT * FROM `discount_code` WHERE `id` = '$id'");
	if(mysql_num_rows($query))
	{
		$row = mysql_fetch_array($query);	
		$id 			= $row['id'];			
		$code 			= $row['code'];
		$discount 		= $row['discount'];
		$expiration_date = ($row['expiration_date'] != '0000-00-00')?date('m/d/Y', strtotime($row['expiration_date'])):'';
		$use_counter 	= $row['use_counter'];
		$max_order_count = $row['max_order_count'];

		
	}
	$pageTitle = "Edit Code";
}


if(isset($_POST['submit']))
{
	$calltype = $_POST['calltype'];
	$page= $_POST['page'];
	$id = $_POST['id'];
	$code 			= $_POST['code'];
	$discount 		= $_POST['discount'];
	$expiration_date 		= $_POST['expiration_date'];
	$use_counter 	= $_POST['use_counter'];
	$max_order_count = $_POST['max_order_count'];


	if($code == '')
	{
		$error[] = 'Please enter Code';		
	}
	else if(check_exists_discount_code($code,$id))
	{
		$error[] = 'Discount Code <strong>"'.$code.'" </strong> already exists';
	}
	if($discount == '')
	{
		$error[] = 'Please enter Discount';		
	}
	else if(!is_numeric($discount))
	{
		$error[] = 'Please enter correct discount (Numeric value)';		
	}
	
	if(empty($error))
	{
		$expiration_date_ymd = date('Y-m-d', strtotime($expiration_date));
		if($calltype == 'U')
		{
			$update_query = mysql_query("UPDATE `discount_code` SET `code` = '$code', `discount` = '$discount', `expiration_date` = '$expiration_date_ymd', `use_counter` = '$use_counter', `max_order_count` = '$max_order_count' WHERE `id` = '$id'");
			if($update_query)
			{
				header('Location: DiscountManagement.php?page='.$page);
				exit();		
			}
			else
			{
				$error[] = 'Databsae Error: '.mysql_error();
			}
		}
		else if($calltype == 'A')
		{
			$insert_query = mysql_query("INSERT INTO `discount_code` (`code`, `discount`, `expiration_date`, `use_counter`, `max_order_count` ) VALUES ('$code', '$discount', '$expiration_date_ymd', '$use_counter', '$max_order_count')");
			if($insert_query)
			{
				header('Location: DiscountManagement.php?page='.$page);
				exit();		
			}
			else
			{
				$error[] = 'Databsae Error: '.mysql_error();
			}
		}
	}//END Error Free
}

?>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
$(function() {
	$( "#expiration_date" ).datepicker({
		changeMonth: true,
		changeYear: true,

	});
});
</script>

<?php
print "<body>";
print "<p><b><font face=\"Arial\" size=\"4\">$pageTitle</font></b></p>";
print "<form method=\"POST\" action=\"\" enctype=\"multipart/form-data\">";
print "<p style=\"color:red\">".implode('<br>',$error)."</p>";
print "<table>";


$use_counter_checked_str = ($use_counter == '1')?'checked':'';

print "<tr><td width=\"200px\"><strong>Code:</strong></td><td><input type=\"text\" name=\"code\" value=\"$code\" style=\"width:200px;\" required ></td></tr>";

print "<tr><td width=\"200px\"><strong>Expiration Date:</strong></td><td><input type=\"text\" name=\"expiration_date\" id=\"expiration_date\" value=\"$expiration_date\" style=\"width:200px;\" required ></td>
<td >
<strong>Use Counter:</strong> <input type=\"checkbox\" name=\"use_counter\" id=\"use_counter\" value=\"1\" $use_counter_checked_str ></td>

</tr>";

print "<tr><td width=\"200px\"><strong>Discount Percentage:</strong></td><td><input type=\"text\" name=\"discount\" value=\"$discount\" style=\"width:200px;\" ></td>
<td >
<strong>Maximum Orders Count Down:</strong> <input type=\"text\" name=\"max_order_count\" id=\"max_order_count\" value=\"$max_order_count\"  style=\"width:60px;\"></td>

</tr>";


print "<tr><td></td><td>";

print "<p><font face=\"Arial\"><input type=\"submit\" value=\"Submit\" name=\"submit\"><input type=\"reset\"";
print "value=\"Reset\" name=\"B2\"></font></p>";
print '<input type="hidden" name="calltype" value="'.$calltype.'" />';
print "<input type=\"hidden\" name=\"id\" size=\"20\" value=\"$id\">";
print "<input type=\"hidden\" name=\"page\" size=\"20\" value=\"$page\">";
print "</td></tr>";
print "</table>";

print "</form></body>";

include "adminfooter.php";
?>

