<?php
//creating thumbnail
function createthumb($filename,$new_w,$new_h){
//echo $filename;
        if($filename != ''){
        $gd2=1;
//      $system=explode(".",$filename);

     $URL=$_SERVER['DOCUMENT_ROOT'];
     $name= $URL . "/catalog/ProductImage/" . $filename;
        $array=getimagesize($name);
        $system=explode("/",$array['mime']);
        
        if(preg_match("/jpg|jpeg/",$system[1])){
                $src_img=@imagecreatefromjpeg($name);
                if ($src_img == "")
                        return;
                }
        else if (preg_match("/gif/",$system[1])){
                $src_img=imagecreatefromgif($name);
        }
        else if (preg_match("/png/",$system[1])){
                $src_img=imagecreatefrompng($name);
        }
        else if (preg_match("/bmp/",$system[1])){
                $src_img=imagecreatefrombmp($name);
        }
        $dst_name = $URL . "/catalog/ProductImage/"  . "thumb_" .$filename; 
        
        $old_x=imageSX($src_img);
        $old_y=imageSY($src_img);
        
        if ($old_x > $old_y) {
                $thumb_w=$new_w;
                $thumb_h=$new_w*($old_y/$old_x);
        }
        else{
                $thumb_w=$new_h*($old_x/$old_y);
                $thumb_h=$new_h;
        }
        if ($gd2==1){
        
                $dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
                imagefill($dst_img,0,0,imagecolorallocate($dst_img,255,255,255));
                imagecopyresized($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
        }
        else{
                $dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
                imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
        }
        if (preg_match("/gif/",$system[1])){
                imagegif($dst_img,$dst_name);
        }
        else if(preg_match("/png/",$system[1])){
                imagepng($dst_img,$dst_name);
        }
        else if(preg_match("/bmp/",$system[1])){
                imagebmp($dst_img,$dst_name);
        }
        else{
                imagejpeg($dst_img,$dst_name);
                
        }
        chmod($dst_name,0755);
        imagedestroy($dst_img);
        imagedestroy($src_img);
        return $filename;
        }
        else{
                return 0;
        }
}
        
              
?>