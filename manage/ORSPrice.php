<?php
include "../globals.php";
include "adminheader.php";
include "../pager.php";
include "../MyImageFunction.php";
include 'image_functions.php';
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------
$page_name = 'ORS PRICE MATCHING CALCULATOR';

adminTitle("$page_name");
$keyword = '';
if (isset($_POST['submit'])) {
    $action = $_POST['action'];

    $keyword = trim($_POST['keyword']);

    if ($keyword == '') {
        $error[] = 'Please enter SKU or Part Number';
    }
}

$total_result = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) AS `num` FROM `ORSDATA`"));

?>
<style>
    div.error{
        padding:2px 4px;
        margin:0px;
        border:solid 1px #FBD3C6;
        background:#FDE4E1;
        color:#CB4721;
        font-family:Arial, Helvetica, sans-serif;
        font-size:14px;
        font-weight:bold;
        text-align:center; 
    }
    div.success{
        padding:2px 4px;
        margin:0px;
        border:solid 1px #C0F0B9;
        background:#D5FFC6;
        color:#48A41C;
        font-family:Arial, Helvetica, sans-serif; font-size:14px;
        font-weight:bold;
        text-align:center; 
    }
    input[type="text"], input[type="password"], select 
    {
        width:450px; 
        height:25px; 
        border:1px solid #000;
    }
   	.tbl-info span{
        font-weight: bold;

    }
</style>


<div style="width: 900px;">
    <div style="font-size:22px; color:#00F; text-align:center; font-weight:bold; line-height:35px;"><?php echo $page_name; ?></div>

    <form method="post" action="">

        <div style="clear:both"></div>


        <table width="100%" cellpadding="10" cellspacing="10" border="0" style="border-collapse:collapse">
            <?php
            if (!empty($error)) {
                echo '<tr>
                <td  style="color:red; font-weight:bold">' . implode("<br>", $error) . '</td>
                </tr>';
            }
            ?>
            <tr>
                <td align="left">
                    <strong>Enter SKU or Part Number:</strong>
                    <input style="max-width: 300px;" type="text" name="keyword" value="<?php echo htmlspecialchars($keyword); ?>" required >
                    <input type="submit" name="submit" value="Search" style="padding:4px;">
                    <span style="float:right; font-weight: bold; line-height: 25px;">No. of Items in Database: <?php echo number_format($total_result['num']);?></span>
                </td>
            </tr>
            <?php
            if ($keyword != '') {
                ?>
                <tr>
                    <td>
                        <hr style="border: 1px solid red">
                        <?php
                        $query = mysql_query("SELECT * FROM `ORSDATA` WHERE (`Part_Number` = '$keyword' OR `SKU` = '$keyword')");
                        if (mysql_num_rows($query)) {

                            $row = mysql_fetch_assoc($query);
                            $min_order = $row['Minimum_Selling_Quantity'] * $row['Minimum_Order_Quantity'];
                            $recommended_price = $row['Level_3_Customized'] * $min_order;
                            if($recommended_price < 100){
                            	$recommended_price = $recommended_price + 5;
                            }
                            $recommended_price = $recommended_price + ($recommended_price * 15/100);

                            $potential_profits =  $recommended_price - $row['Level_3_Customized'];

                            $potential_profits_label = 'Protential Profits';

                            if(isset($_REQUEST['price_match']) && $_REQUEST['price_match'] != ''){
                            	$potential_profits = $_REQUEST['price_match'] - $row['Level_3_Customized'];
                            	$potential_profits_label = 'Profit from Recommended Price';
                            }
                            ?>
                            <table class="tbl-info" width="100%" cellpadding="0" cellspacing="5">
                            	<tr>
                            		<td><span>On-line Recommended Price:</span> $<?php echo number_format($recommended_price,2);?></td>
                            		<td align="right"><span>Price Match:</span> <input style="max-width: 200px; background-color: #a9a9ff" type="text" name="price_match" value="<?php echo htmlspecialchars($_REQUEST['price_match']); ?>"  ></td>
                            	</tr>
                            	<tr>
                            		<td><span><?php echo $potential_profits_label;?>:</span> $<?php echo number_format($potential_profits,2);?></td>
                            		<td align="right"><input style="max-width: 200px;" type="submit" name="submit" value="Re-Calculate" ></td>
                            	</tr>

                            </table>
                            <hr style="border: 1px solid red">
                            <table class="tbl-info" width="100%" cellpadding="5" cellspacing="5">
                                <tr>
                                    <td><span>SKU:</span> <?php echo $row['SKU']; ?></td>
                                    <td><span>PART NUMBER:</span> <?php echo $row['Part_Number']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Brand Name:</span> <?php echo $row['Brand_Name']; ?></td>
                                    <td><span>Item Description:</span> <?php echo $row['Item_Description']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Minimum_Selling_Quantity:</span> <?php echo $row['Minimum_Selling_Quantity']; ?></td>
                                    <td><span>Minimum_Order_Quantity:</span> <?php echo $row['Minimum_Order_Quantity']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Multiples_Required:</span> <?php echo $row['Multiples_Required']; ?></td>
                                    <td><span>Sold_in_Std_Pkg:</span> <?php echo $row['Sold_in_Std_Pkg_']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Std_Pkg_UOM:</span> <?php echo $row['Std_Pkg_UOM']; ?></td>
                                    <td><span>Std_Pkg_Quantity:</span> <?php echo $row['Std_Pkg_Quantity']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Price_Unit:</span> <?php echo $row['Price_Unit']; ?></td>
                                    <td><span>Package:</span> <?php echo $row['Package']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>List_Price:</span> $<?php echo number_format($row['List_Price'],2); ?></td>
                                    <td><span>Your_ORS_Nasco_Cost:</span> $<?php echo number_format($row['Your_ORS_Nasco_Cost'],2); ?></td>
                                </tr>
                                <tr>
                                    <td><span>Level_3_Customized:</span> $<?php echo number_format($row['Level_3_Customized'],2); ?></td>
                                    <td><span>Item_Discount:</span> <?php echo $row['Item_Discount']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Item_Weight:</span> <?php echo $row['Item_Weight']; ?></td>
                                    <td><span>Item_Height:</span> <?php echo $row['Item_Height']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Item_Width:</span> <?php echo $row['Item_Width']; ?></td>
                                    <td><span>Item_Length:</span> <?php echo $row['Item_Length']; ?></td>
                                </tr>
                                <tr>
                                    <td><span>Item_Cube:</span> <?php echo $row['Item_Cube']; ?></td>
                                    <td><span>UNSPSC_Code:</span> <?php echo $row['UNSPSC_Code']; ?></td>
                                </tr>
                            </table>


                            <?php
                        } else {
                            echo '<p>No record found!</p>';
                        }
                        ?>

                    </td>
                </tr>
                <?php
            }
            ?>

        </table>


    </form>

    <?php
    include "adminfooter.php";
    print "</div>";
    ?>



