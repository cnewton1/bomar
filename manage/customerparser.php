<?
function getDepartment($name){
$name_array = explode(';',$name);
$department_details = explode(':',$name_array[1]);
$department = trim(str_replace('Activity Code','',$department_details[0]));
return $department;
}
function getActiveCode($name){
$name_array = explode(';',$name);
$department_details = explode(':',$name_array[1]);
$Activity_Code = trim($department_details[1]);
return $Activity_Code;
}
function getLastName($name){
$name_array = explode(';',$name);
$last_name = $name_array[2];
return $last_name;
}
function getFirstName($name){
$name_array = explode(';',$name);
$first_name = $name_array[3];
return $first_name;
}
function getEmail($name){
$name_array = explode(';',$name);
$email_details = explode(':',$name_array[4]);
$Email = trim($email_details[1]);
return $Email;
}
function getPhone($name){
$name_array = explode(';',$name);
$phone_details = explode(':',$name_array[5]);
$Phone = trim($phone_details[1]);
return $Phone;
}
?>