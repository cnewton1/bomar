<?php
include "../globals.php"; 
include "adminheader.php";
include "TableTypeDropDownFunction.php";
include "mlistTableFunction.php";
include "RedisplayManageTable.php";


$TabName = $_REQUEST["TabName"];
$NumCol = $_REQUEST['NumCol'];
$CallType = $_REQUEST['CallType'];
$ProductID=$_REQUEST['ID'];
$THEROW=$_REQUEST['record'];

//---------------------------
// Get all possible colunms
//---------------------------
$Column1=$_REQUEST['Colname_0'];
$Column2=$_REQUEST['Colname_1'];
$Column3=$_REQUEST['Colname_2'];
$Column4=$_REQUEST['Colname_3'];
$Column5=$_REQUEST['Colname_4'];
$Column6=$_REQUEST['Colname_5'];
$Column7=$_REQUEST['Colname_6'];
$Column8=$_REQUEST['Colname_7'];
$Column9=$_REQUEST['Colname_8'];
$Column10=$_REQUEST['Colname_9'];

if ($TabName==NULL)
{
$TabName = $HTTP_GET_VARS["TabName"];
$NumCol = $HTTP_GET_VARS['NumCol'];
$CallType = $HTTP_GET_VARS['CallType'];
$ProductID=$HTTP_GET_VARS['ID'];
$THEROW=$HTTP_GET_VARS['record'];    
}


adminTitle("Update Table in Progress");

//Create New Row
  if ($Column1!=NULL)
  {
    $NewRowData = $NewRowData  . $Column1;
  }

   if ($Column2!=NULL)
  {
    $NewRowData = $NewRowData . "|" . $Column2;
  }
  
    if ($Column3!=NULL)
  {
    $NewRowData =  $NewRowData . "|" . $Column3; 
  }
  
    if ($Column4!=NULL)
  {
    $NewRowData =  $NewRowData . "|" . $Column4;
  }
  
    if ($Column5!=NULL)
  {
    $NewRowData = "|" .$NewRowData  . "|" . $Column5; 
  }
  
    if ($Column6!=NULL)
  {
    $NewRowData = $NewRowData . "|" . $Column6; 
  }
  
    if ($Column7!=NULL)
  {
    $NewRowData = $NewRowData . "|" . $Column7; 
  }
  
    if ($Column8!=NULL)
  {
    $NewRowData = $NewRowData . "|" . $Column8; 
  }
  
    if ($Column9!=NULL)
  {
    $NewRowData = $NewRowData . "|" . $Column9; 
  }
   
  if ($Column10!=NULL)
  {
    $NewRowData = $NewRowData . "|" . $Column10; 
  }
  
  
// GET THE TABLE
  $CompDXX=GetTableData($ProductID,$TabName,$linkID);    
  
// PARSE THE OLD DATA and INSERT THE NEW DATA  
 if ($CallType=="I")
 {
 $CompDXX = InsertRowData($CompDXX,$ProductID,$THEROW,$NewRowData);
 } 
 else
 {
  if ($CallType=="D")
  {
   $CompDXX = DeleteRowData($CompDXX,$ProductID,$THEROW); 
  }
  else
  { 
   $CompDXX = RemakeData($CompDXX,$ProductID,$THEROW,$NewRowData); // Default is edit  
  }
 }

   SetUpdateTableData($ProductID,$CompDXX,$linkID);
// UPDATE THE TABLE

//--------------------------
// Redisplay the list
//--------------------------
print "<div style=\"position: absolute; top: 125px; left: 10px; width: 700px;>\"";
RedisplayManagerTableFunction($ProductID,$TabName,$linkID);
print "</div>";


// ******************************************************************
// ******************************************************************
exit;
// ******************************************************************
// ******************************************************************




function createFieldPair($ATTR,$NAME)
{
  $PART1="";
  $PART2="";
  if ($ATTR == "Description")
  {
     $PART2=">>s|";
  }
  
  if ($ATTR == "Plain Text")
  {
     $PART2=">>t|";
  }

  if ($ATTR == "Item Number")
  {
     $PART2=">>d|";
  }

  if ($ATTR == "Price")
  {
     $PART2=">>p|";
  }
  
  if ($ATTR == "[Volumme Price]")
  {
     $PART2=">>b|";
  }

  if ($ATTR == "[Discount Quantity]")
  {
     $PART2=">>q|";
  }

   $PART1=$NAME;
   $Out=$PART1 . $PART2;
   
   return $Out;
}


function InsertRowData($TableData,$ProductID,$RowID,$NewRowData)
{
   $RwArray=explode("~",$TableData);
   $m=count($RwArray);
   
   $NewData="";
   if ($m > 0)
   {
     for ($i=0;$i<$m;$i++)
     {
       // Replace the old row
       if ($i==$RowID)
       { 
          $NewData = $NewData . $RwArray[$i] . "~";     
          $NewData = $NewData . $NewRowData . "~";    
       } else
       {
        $NewData = $NewData . $RwArray[$i] . "~";       
       }
     }
   }
   return $NewData;
}


function RemakeData($TableData,$ProductID,$RowID,$NewRowData)
{
   $RwArray=explode("~",$TableData);
   $m=count($RwArray);
   
   $NewData="";
   if ($m > 0)
   {
     for ($i=0;$i<$m;$i++)
     {
       // Replace the old row
       if ($i==$RowID)
       { 
          $NewData = $NewData . $NewRowData . "~";    
       } else
       {
        $NewData = $NewData . $RwArray[$i] . "~";       
       }
     }
   }
   return $NewData;
}


function DeleteRowData($TableData,$ProductID,$RowID)
{
   $RwArray=explode("~",$TableData);
   $m=count($RwArray) - 1;
   
   $NewData="";
   if ($m > 0)
   {
     for ($i=0;$i<$m;$i++)
     {
       // Do not remake the deleted row
       if ($i!=$RowID)
       { 
         $NewData = $NewData . $RwArray[$i] . "~";       
       }
     }
   }
      return $NewData;      
}


function GetRowTableData($TableData,$RowNum,$ProductID)
{ 
  $TableRowArray=explode("~",$TableData);
  $TempRow=$TableRowArray[$RowNum];
  
  $currentRowArray=explode("|",$TempRow);
  $j=count($currentRowArray);
  
  if ($j > 0)
  {
    print "<tr>";
    
    $inEditMode=true;

    for ($i=0;$i<$j;$i++)
    {   
    
     print "<td>";        
     
     $COLNAME="Colname_" . $i; /// Build the name
     $VALUETEXT=$currentRowArray[$i]; 
     print "<b><input background=\"#FFFF00\" type=\"text\" name=$COLNAME size=\"50\" value=\"$VALUETEXT\"  style=\"color: #FF00FF\"></b>";
     print "</td>";          
    }    
    print "</tr>";  
  }
  
  return;
}


function GetTableData($ProductID,$TableName,$linkID) 
{
$resultID = mysql_query("Select table_data from tabledata where product_id ='$ProductID'", $linkID);
 if ($resultID)
 {
   while ($row = mysql_fetch_row($resultID))
   {
     $fieldCount=1;
     foreach ($row as $field)
     {
      if ($fieldCount==1)
         $data = $field;
         return $data;
 
      $fieldCount++; 
     } // Foreach look
   }    
 }
} 


function SetUpdateTableData($ProductID,$NewTableData,$linkID) 
{
$resultID = mysql_query("UPDATE tabledata SET table_data='$NewTableData' WHERE product_id ='$ProductID'", $linkID);

if ($resultID)
 {
  return true;
 }    
 else
 {       
   return false;   
 }
}     
?>

