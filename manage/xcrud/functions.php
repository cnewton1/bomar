<?php
require_once dirname(__FILE__) . '/../dblib.inc';
require_once dirname(__FILE__) . '/../lib.inc';

function publish_action($xcrud)
{
    if ($xcrud->get('primary')) {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE base_fields SET `bool` = b\'1\' WHERE id = ' . (int) $xcrud->get('primary');
        $db->query($query);
    }
}

function unpublish_action($xcrud)
{
    if ($xcrud->get('primary')) {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE base_fields SET `bool` = b\'0\' WHERE id = ' . (int) $xcrud->get('primary');
        $db->query($query);
    }
}

function exception_example($postdata, $primary, $xcrud)
{
    // get random field from $postdata
    $postdata_prepared = array_keys($postdata->to_array());
    shuffle($postdata_prepared);
    $random_field = array_shift($postdata_prepared);
    // set error message
    $xcrud->set_exception($random_field, 'This is a test error', 'error');
}

function test_column_callback($value, $fieldname, $primary, $row, $xcrud)
{
    return $value . ' - nice!';
}

function after_upload_example($field, $file_name, $file_path, $params, $xcrud)
{
    $ext = trim(strtolower(strrchr($file_name, '.')), '.');
    if ($ext != 'pdf' && $field == 'uploads.simple_upload') {
        unlink($file_path);
        $xcrud->set_exception('simple_upload', 'This is not PDF', 'error');
    }
}

function movetop($xcrud)
{
    if ($xcrud->get('primary') !== false) {
        $primary = (int) $xcrud->get('primary');
        $db = Xcrud_db::get_instance();
        $query = 'SELECT `officeCode` FROM `offices` ORDER BY `ordering`,`officeCode`';
        $db->query($query);
        $result = $db->result();
        $count = count($result);

        $sort = array();
        foreach ($result as $key => $item) {
            if ($item['officeCode'] == $primary && $key != 0) {
                array_splice($result, $key - 1, 0, array($item));
                unset($result[$key + 1]);
                break;
            }
        }

        foreach ($result as $key => $item) {
            $query = 'UPDATE `offices` SET `ordering` = ' . $key . ' WHERE officeCode = ' . $item['officeCode'];
            $db->query($query);
        }
    }
}

function movebottom($xcrud)
{
    if ($xcrud->get('primary') !== false) {
        $primary = (int) $xcrud->get('primary');
        $db = Xcrud_db::get_instance();
        $query = 'SELECT `officeCode` FROM `offices` ORDER BY `ordering`,`officeCode`';
        $db->query($query);
        $result = $db->result();
        $count = count($result);

        $sort = array();
        foreach ($result as $key => $item) {
            if ($item['officeCode'] == $primary && $key != $count - 1) {
                unset($result[$key]);
                array_splice($result, $key + 1, 0, array($item));
                break;
            }
        }

        foreach ($result as $key => $item) {
            $query = 'UPDATE `offices` SET `ordering` = ' . $key . ' WHERE officeCode = ' . $item['officeCode'];
            $db->query($query);
        }
    }
}

function show_description($value, $fieldname, $primary_key, $row, $xcrud)
{
    $result = '';
    if ($value == '1') {
        $result = '<i class="fa fa-check" />' . 'OK';
    } elseif ($value == '2') {
        $result = '<i class="fa fa-circle-o" />' . 'Pending';
    }
    return $result;
}

function custom_field($value, $fieldname, $primary_key, $row, $xcrud)
{
    return '<input type="text" readonly class="xcrud-input" name="' . $xcrud->fieldname_encode($fieldname) . '" value="' . $value .
        '" />';
}

function unset_val($postdata)
{
    $postdata->del('Paid');
}

function format_phone($new_phone)
{
    $new_phone = preg_replace("/[^0-9]/", "", $new_phone);

    if (strlen($new_phone) == 7)
        return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $new_phone);
    elseif (strlen($new_phone) == 10)
        return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $new_phone);
    else
        return $new_phone;
}

function before_list_example($list, $xcrud)
{
    var_dump($list);
}

function after_update_test($pd, $pm, $xc)
{
    $xc->search = 0;
}


function note_status_value($value, $fieldname, $primary_key, $row, $xcrud)
{
    if ($value == '') {
        return 'NONE';
    } else {
        return $value;
    }
}

function get_notes_status_arr($selected)
{
    if ($_SESSION['type'] == 'admin') {
        $in_array = [];
    } else if ($_SESSION['type'] == 'level 3') {
        $in_array = [
            'Approved',
            'Completed Final Call',
            'Reviewed',
            'Signed Complete'
        ];
    } else if ($_SESSION['type'] == 'level 2') {
        $in_array = [
            'Approved'
        ];
    } else {
        $in_array = [
            'Created',
            'Approved'
        ];
    }

    $where_condition_add = '';
    if (!empty($in_array)) {
        foreach ($in_array as $val) {
            $where_condition_add .= "'$val', ";
        }
        $where_condition_add = substr($where_condition_add, 0, -2);
        if ($where_condition_add != '') {
            $where_condition_add = " AND `status_name` IN ($where_condition_add)";
        }
        if ($selected != '') {
            $where_condition_add .= " OR `id` = '$selected'";
        }
    }
    $notes_status_arr = [0 => 'NONE'];

    $notes_status_result = lib_getrecords('note_status', 1, 1, " $where_condition_add ORDER BY `status_name` ASC");
    if (!empty($notes_status_result)) {
        foreach ($notes_status_result  as $row) {
            $notes_status_arr[$row['id']] = $row['status_name'];
        }
    }
    return $notes_status_arr;
}

function note_status_input($value, $field, $primary_key, $list, $xcrud)
{
    $array =  get_notes_status_arr($value);


    $str = '<select class="xcrud-input form-control form-control" data-type="select" name="' . $xcrud->fieldname_encode($field) . '" maxlength="11">
    <option value="">- none -</option>';
    foreach ($array as $key => $val) {
        $selected_str = ($key == $value) ? 'selected' : '';
        $str .= '<option value="' . $key . '" ' . $selected_str . '>' . $val . '</option>';
    }
    $str .= '</select>';
    return $str;
}

function note_view_link($value, $fieldname, $primary_key, $row, $xcrud)
{
    $value1 = substr(strip_tags($value), 0, 50);
    return '<a  href="javascript:;" data-header="Notes" data-content="'.htmlentities($value).'" class="note_modal">' . $value1 . '</a>';
}
