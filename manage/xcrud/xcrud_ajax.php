<?php

require_once dirname(__FILE__).'/../../users.php';
ob_get_clean();
require_once dirname(__FILE__).'/xcrud.php';

header('Content-Type: text/html; charset=' . Xcrud_config::$mbencoding);
if ($_POST['xcrud']['instance'] == 'customer_notes') {
    $_SESSION['customer_notes_limit'] = $_POST['xcrud']['limit'];
    $_SESSION['customer_notes_start'] = $_POST['xcrud']['start'];
} 
echo Xcrud::get_requested_instance();
