<?php

class CSVBuilder {

    public $delimiter = ",";
    public $enclosure = "\"";
    public $linefeed = "\r\n";

    public function __construct() {
        
    }

    public function sendCSVHeaders($filename = 'file.csv') {
        $filename = basename($filename);
        ob_get_clean();
        ob_start();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}");
        header("Pragma: no-cache");
        header("Expires: 0");
    }

    /**
     * Convert 2-dimensional array to CSV
     */
    public function arrayToCSV(array $array) {
        $csv = '';
        $count = 0;
        foreach ($array as $key => $val) {
            if ($count == 0) {
                $csv .= $this->CSVRow(array_keys($val));
            }
            if (is_array($val)) {
                $csv .= $this->CSVRow($val);
            } else {
                $csv .= $this->CSVRow(array($key, $val));
            }
            $count++;
        }
        return $csv;
    }

    public function CSVRow($row) {
        $rowtext = "";
        $first = true;
        foreach ($row as $col) {
            if (!$first) {
                $rowtext .= $this->delimiter;
            }
            $col = utf8_decode($col);
            $col = str_replace('"', '""', $col);
            $rowtext .= $this->enclosure . "$col" . $this->enclosure;
            $first = false;
        }
        $rowtext .= $this->linefeed;
        return $rowtext;
    }

}

?>