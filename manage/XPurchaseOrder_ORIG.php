<?php
include "../users.php";
include "../connect.php";
           
include "../globals.php"; 
include "CategoryBoxFunction.php";
include "SubCategoryBoxFunction.php";
include "makeIconFunction.php";
include "adminheader.php";
include "../MyImageFunction.php";    
include "addressparser.php";

$filename = "./PurchaseOrders.txt";

$PATH = $_SERVER['DOCUMENT_ROOT']; #get server path
$save_path=$PATH; // . $filename; #define target path

// $save_path=$save_path . $filename;

$save_path = $filename;

// OPEN FILE FOR WRITING
$fp = fopen($save_path, "w", 0); #open for writing

//=====================================
// CREATE FIRST CSV ROW
$HEADERROW="Vendor\tTransaction Date\tPO Number\tClass\tShip to Entity\tBill To 1\tBill To 2\tBill To 3\tBill To 4\tCity\tState\tPost Code\tBill To County\tShip To 1\tShip To 2\tShip To 3\tShip To 4\tCity\tState\tPostal Code\tBill To Country\tTerms\tDue Date\tShip Method\tFOB\tVendor Message\tMemo\tItem\tQuantity\tDescription\tRate\tCustomer\tExpected\tU/M\tManuf Part Number\n";
fputs($fp, $HEADERROW); #write all of $data to our opened file
//=====================================

//===============================
// Get DATABASE
//===============================

// $sql="SELECT * FROM purchase_orders";

$sql="select orderhistory.date, orderhistory.purchase_order, dodaccounts.billing_address, dodaccounts.ship_to, dodaccounts.email  , orderhistory.ship_via,  orderhistory.part_number,  orderhistory.quantity,  orderhistory.description, orderhistory.unit_price, dodaccounts.service_agency,  orderhistory.unit_of_measure,  orderhistory.aux_part_number from dodaccounts, orderhistory  where orderhistory.translink = dodaccounts.translink";

$result=mysql_query($sql); 

//header("Content-type: application/octet-stream");                           <h2></h2>
//header("Content-Disposition: attachment; filename=\"myProducts.csv\"");
// FIRST ROW.
// echo $HEADERROW;

while($row=mysql_fetch_array($result))
{
	$description = $row['description'];
	$description = str_replace(",",";",$description);
   
   // PARSE THE ADDRESS DATA HERE
   $SHIP_TO_ADDRESS=$row['ship_to'];
   
   $SHIP_TO_ZIP=getTagZip($SHIP_TO_ADDRESS);
   $SHIP_TO_STATE=getTagState($SHIP_TO_ADDRESS);     
   $SHIP_TO_CITY=getTagCity($SHIP_TO_ADDRESS);       
   $SHIP_TO_STREET=getTagStreet($SHIP_TO_ADDRESS);     
   $SHIP_TO_STREET=str_replace("\n"," ",$SHIP_TO_STREET);  
   
   
   $BILLING_ADDRESS=$row['billing_address'];    
   
   $BILLING_TO_ZIP=getTagZip($BILLING_ADDRESS);
   $BILLING_TO_STATE=getTagState($BILLING_ADDRESS);     
   $BILLING_TO_CITY=getTagCity($BILLING_ADDRESS);       
   $BILLING_TO_STREET=getTagStreet($BILLING_ADDRESS);     
   $BILLING_TO_STREET=str_replace("\n"," ",$BILLING_TO_STREET);    
   
   // Added by Sajith
$part_number = $row['part_number'];

$select_supplier_query = mysql_query("SELECT `supplier` FROM `product` WHERE `item_num`='$part_number'");
$number = mysql_num_rows($select_supplier_query);

	$date1 = $row['date'];
	$date_arr = explode(' ',$date1);
	$date_updated = $date_arr[0];

if($number == 0)
	$vendor = 'Unknown';
   else
   {
	$result_vendor = mysql_fetch_assoc($select_supplier_query);
	$vendor = $result_vendor['supplier'];
	}
   // end of update
   
   
   
                           
   // PARSE THE ADDRESS DATA HERE
   
   $data = $vendor  . "\t" . $row['date'] . "\t" . $row['purchase_order'] . "\t" . "Non 8(a)" . "\t" . "Westcarb Enterprises, Inc." . "\t" . $BILLING_TO_STREET  . "\t" . "  " . "\t" . " " . "\t" . " " . "\t" . $BILLING_TO_CITY . "\t" . $BILLING_TO_STATE . "\t" . $BILLING_TO_ZIP . "\t" . "US" . "\t" . $SHIP_TO_STREET . "\t" . "  " . "\t" . " " . "\t" . " " . "\t" . $SHIP_TO_CITY . "\t" . $SHIP_TO_STATE . "\t" . $SHIP_TO_ZIP . "\t" . "US" . "\t" . "Net 10" . "\t" . " " . "\t" . $row['ship_via'] . "\t" . "Destination" . "\t" . "NONE" . "\t" . "NONE" . "\t";
   
   $data = $data . $row['part_number'] . "\t" . $row['quantity']  . "\t" . $row['description'] . "\t" . $row['unit_price'] . "\t" . $row['email'] . "\t" . " " . "\t" . $row['unit_of_measure']  . "\t" .  $row['aux_part_number'] . "\n";
  
   $data = str_replace("SKU - ","",$data);
   $data = str_replace("UPC - ","",$data);
   $data = str_replace("Material...","",$data);
   $data = str_replace("Size.........","",$data);

   fputs($fp, $data);  
 //  echo $data;
  }
//===============================  
//EXIT DB  
//===============================
  fclose($fp); #close the file


//=============================================
// Create the download link for the file
//=============================================

print "<p><font face=\"Arial\">";
// print "<b><a href=\"$MAINURL/manage/PurchaseOrders.csv\">";
print "<b><a href=\"$MAINURL/manage/PurchaseOrders.txt\">";        
print "CLICK HERE TO DOWNLOAD SPREADSHEET</a></b></font></p>";

include "adminfooter.php";

?>