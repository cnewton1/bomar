<?php
include "../globals.php"; 
include "../pager.php";
include "../MyImageFunction.php";
include "../CategoryDropDownFunction.php";
include "../SubCategoryDropDownFunction.php";
include "adminheader.php";
include "functions.php";



$page_title = 'Create Batch Categories';
$filename = 'create_batch_categories.php';
$table_name = "product2";

$page = ($_REQUEST['page']>1)?$_REQUEST['page']:1;
$searchText = trim($_GET['searchText']);
$page_name = $filename;
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');



if($_REQUEST['action'] == 'start')
{
	$category_query = mysql_query("SELECT `category_description`, `major_category_description` FROM `product2` 
	WHERE 
	`category_description` != '' 
	AND `major_category_description` != '' 
	GROUP BY `category_description`, `major_category_description` 
	ORDER BY `category_description`, `major_category_description`");
	if(mysql_num_rows($category_query))
	{
		mysql_query("TRUNCATE category");
		mysql_query("TRUNCATE subcategory");
		
		while($row = mysql_fetch_array($category_query, MYSQL_ASSOC))
		{
			$category_data[$row['category_description']][] = $row['major_category_description'];
		}
		
	}
	
}
?>

<form method="get" action="" enctype="multipart/form-data">

<table width="900" cellpadding="5" cellspacing="5" border="0">

<tr>
<td class="page_heading" align="center"><?php echo $page_title;?> </td>
</tr>
<?php
if($msg != '')
{
	echo '<tr>
	<td style="color:green; font-weight:bold; font-size:14px;">'.$msg.'</td>
	</tr>';
	
}

if(!empty($category_data) && ($_REQUEST['action'] == 'start'))
{
	echo '
	<tr>
	<td>
		<table cellpadding="5" cellspacing="5" border="1" style="border-collapse:collapse">
		<tr>
		<td><strong>Category</strong></td>
		<td><strong>Subcategory</strong></td>
		</tr>
		';
	
		foreach($category_data as $category_name=>$subcategory_array)
		{
			$category_name = mysql_real_escape_string($category_name);
			mysql_query("INSERT INTO `category` (`Name`) VALUES ('$category_name')");
			foreach($subcategory_array as $subcategory_name)
			{
				$subcategory_name = mysql_real_escape_string($subcategory_name);
				mysql_query("INSERT INTO `subcategory` (`Name`, `Parent`) VALUES ('$subcategory_name', '$category_name')");
				
				echo '
				<tr>
				<td>'.$category_name.'</td>
				<td>'.$subcategory_name.'</td>
				</tr>';
			}
			
			
		}
		echo '
		</table>
	</td>
	</tr>';
	
	
}
else
{
	
	?>
	<tr>
	<td >
	<input type="hidden" name="action" value="start" />
	<input type="submit" name="submit" value="Click here to create batch categories" /></td>
	</tr>
	<?php
}
?>

</table>
</form>

