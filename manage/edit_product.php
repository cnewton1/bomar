<?php
include "../globals.php";
include "../pager.php";
include "../MyImageFunction.php";
include "../CategoryDropDownFunction.php";
include "../SubCategoryDropDownFunction.php";
include "adminheader.php";
include 'functions.php';
require_once('thumbnail.php');
?>


<?php
$allowedExtensions = array("jpg", "jpeg", "gif", "png");

function isAllowedExtension($fileName) {
    global $allowedExtensions;

    return in_array(strtolower(end(explode(".", $fileName))), $allowedExtensions);
}

$url = ($_REQUEST['url'] != '') ? $_REQUEST['url'] : $_SERVER['HTTP_REFERER'];
$table_name = 'product2';
$data = array();
if ($_GET['id'] != '' && $_GET['action'] == 'edit' && $_GET['table'] != '') {
    $table_name = mysql_real_escape_string($_REQUEST['table']);
    $id = mysql_real_escape_string($_GET['id']);
    $query = mysql_query("SELECT * FROM `$table_name` WHERE `id` = '$id'");
    if (mysql_num_rows($query)) {
        $data = mysql_fetch_assoc($query);
        $fields_array = array_keys($data);
    }
}
if (empty($data)) {
    $action = 'add';
    $page_title = 'Add Record';
    $fields_list = table_fields_list($table_name, '', array('id'));
    foreach ($fields_list as $key => $field_name) {
        $data[$field_name] = '';
    }
} else {
    $action = 'update';
    $page_title = 'Edit Record';
}
if ($_POST['action'] == 'add') {
    $url = ($_REQUEST['url'] != '') ? $_REQUEST['url'] : 'Product2Report.php';
    $table_name = $_POST['table'];
    $data = $_POST['data'];
    $values_str = $fields_str = '';
    foreach ($data as $field => $value) {
        if ($field == 'id' || $field == 'image_hyperlink' || $field == 'pdf_file') {
            continue;
        }
        $value = mysql_real_escape_string(stripslashes($value));
        $fields_str .= "`$field`, ";
        $values_str .= "'$value', ";
    }

    $fields_str = substr($fields_str, 0, -2);
    $values_str = substr($values_str, 0, -2);

    $file_name = $_POST['file_name'];
    $pdf_file_name = $_POST['data']['pdf_file'];

    if ($_FILES["file"]["size"] > 0) {
        $orig_file_name = $_FILES["file"]["name"];
        if (isAllowedExtension($orig_file_name)) {
            $file_size = $_FILES["file"]["size"];
            $ul_file_name = time() . "_" . $_FILES["file"]["name"];
            $upload_file = move_uploaded_file($_FILES["file"]["tmp_name"], "../ProductImage/" . $ul_file_name);
            if ($upload_file) {
                $file_name = $ul_file_name;
                $img_path = '../ProductImage/' . $file_name;
                $img_thumb_path = '../ProductImage/thumb_' . $file_name;
                if (file_exists($img_path)) {
                    $Thumbnailer = new Thumbnailer($img_path);
                    $thumbOptions = array('maxLength' => 300);
                    $Thumbnailer->createThumb($thumbOptions, $img_thumb_path);
                }
            } else {
                $error[] = 'File upload Failed!';
            }
        } else {
            $error[] = $orig_file_name . ' is an invalid file type! Only <strong>' . implode(",", $allowedExtensions) . '</strong> filetype is allowed.';
        }
    }
    if ($_FILES["pdf_file"]["size"] > 0) {
        $orig_file_name = $_FILES["pdf_file"]["name"];
        $allowedExtensions = array('pdf');
        if (isAllowedExtension($orig_file_name)) {
            $file_size = $_FILES["pdf_file"]["size"];
            $ul_file_name = time() . "_" . $_FILES["pdf_file"]["name"];
            $upload_file = move_uploaded_file($_FILES["pdf_file"]["tmp_name"], "../ProductImage/" . $ul_file_name);
            if ($upload_file) {
                $pdf_file_name = $ul_file_name;
            } else {
                $error[] = 'PDF File upload Failed!';
            }
        } else {
            $error[] = $orig_file_name . ' is an invalid file type! Only <strong>' . implode(",", $allowedExtensions) . '</strong> filetype is allowed.';
        }
    }

    $fields_str .= ", `image_hyperlink`, `pdf_file`";
    $values_str .= ", '$file_name', '$pdf_file_name'";

    $insert_query = mysql_query("INSERT INTO `$table_name` ($fields_str) VALUES ($values_str)");
    if ($insert_query) {
        $msg = 'Record successfully inserted';
        $header_location = urldecode($url);
        header('Location: ' . $header_location);
        exit();
    } else {
        $error = mysql_error();
    }
}
if ($_POST['action'] == 'update') {
    $url = ($_REQUEST['url'] != '') ? $_REQUEST['url'] : 'Product2Report.php';
    $table_name = $_POST['table'];
    $data = $_POST['data'];
    $update_str = '';
    foreach ($data as $field => $value) {
        if ($field == 'id' || $field == 'image_hyperlink'  || $field == 'pdf_file') {
            continue;
        }
        $value = mysql_real_escape_string(stripslashes($value));
        $update_str .= "`$field` = '$value', ";
    }

    $update_str = substr($update_str, 0, -2);

    $file_name = $_POST['file_name'];
    $pdf_file_name = $_POST['data']['pdf_file'];

    if ($_FILES["file"]["size"] > 0) {
        $orig_file_name = $_FILES["file"]["name"];
        if (isAllowedExtension($orig_file_name)) {
            $file_size = $_FILES["file"]["size"];
            $ul_file_name = time() . "_" . $_FILES["file"]["name"];
            $upload_file = move_uploaded_file($_FILES["file"]["tmp_name"], "../ProductImage/" . $ul_file_name);
            if ($upload_file) {
                $file_name = $ul_file_name;
                $img_path = '../ProductImage/' . $file_name;
                $img_thumb_path = '../ProductImage/thumb_' . $file_name;
                if (file_exists($img_path)) {
                    $Thumbnailer = new Thumbnailer($img_path);
                    $thumbOptions = array('maxLength' => 300);
                    $Thumbnailer->createThumb($thumbOptions, $img_thumb_path);
                }
            } else {
                $error[] = 'File upload Failed!';
            }
        } else {
            $error[] = $orig_file_name . ' is an invalid file type! Only <strong>' . implode(",", $allowedExtensions) . '</strong> filetype is allowed.';
        }
    }
    if ($_FILES["pdf_file"]["size"] > 0) {
        $orig_file_name = $_FILES["pdf_file"]["name"];
        $allowedExtensions = array('pdf');
        if (isAllowedExtension($orig_file_name)) {
            $file_size = $_FILES["pdf_file"]["size"];
            $ul_file_name = time() . "_" . $_FILES["pdf_file"]["name"];
            $upload_file = move_uploaded_file($_FILES["pdf_file"]["tmp_name"], "../ProductImage/" . $ul_file_name);
            if ($upload_file) {
                $pdf_file_name = $ul_file_name;
            } else {
                $error[] = 'PDF File upload Failed!';
            }
        } else {
            $error[] = $orig_file_name . ' is an invalid file type! Only <strong>' . implode(",", $allowedExtensions) . '</strong> filetype is allowed.';
        }
    }
    if (!isURL($file_name)) {
        $file_name = basename($file_name);
    }

    $update_str .= ", `image_hyperlink` = '$file_name', `pdf_file` = '$pdf_file_name'";
    $update_query = mysql_query("UPDATE `$table_name` SET $update_str WHERE `id` = '$data[id]'");
    if ($update_query) {
        $msg = 'Record successfully updated';
        if (isset($_POST['PREV_RECORD'])) {
            $header_location = 'edit_product.php?id=' . $_POST['prev_id'] . '&action=edit&table=' . $table_name . '&msg=' . $msg;
        } else if (isset($_POST['NEXT_RECORD'])) {
            $header_location = 'edit_product.php?id=' . $_POST['next_id'] . '&action=edit&table=' . $table_name . '&msg=' . $msg;
        } else {
            $header_location = urldecode($url);
        }

        header('Location: ' . $header_location);
        exit();
    } else {
        $error = mysql_error();
    }
}
?>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/sample.js" type="text/javascript"></script>

<script>
    function loadSubcategory(category) {
        if (category.length == 0) {
            document.getElementById("subcategory").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("subcategory").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "getSubcategory.php?category=" + category, true);
            xmlhttp.send();
        }
    }
</script>

<form method="POST" action="" enctype="multipart/form-data">

    <table width="900" cellpadding="5" cellspacing="5" border="0">

        <tr>
            <td class="page_heading" align="center"><?php echo $page_title; ?></td>
        </tr>
        <?php
        if ($msg == '') {
            $msg = $_REQUEST['msg'];
        }
        if ($msg != '') {
            echo '<tr>
            <td style="color:green; font-weight:bold; font-size:14px;">' . $msg . '</td>
            </tr>';
        }
        if (!empty($error)) {
            echo '<tr>
            <td style="color:red; font-weight:bold; font-size:14px;">' . implode('<br>', $error) . '</td>
            </tr>';
        }
        ?>
        <tr>
            <td align="center">
                <?php
                submit_update_button($table_name, $data['id']);
                ?>
            </td>
        </tr>
        <tr>
            <td >
                <?php
                print "<table border=\"0\" cellpadding=\"5\" cellspacing=\"5\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"layout\">";
                $NameofPicture=$data['image_hyperlink'];

                if (strpos($NameofPicture,".jpg") < -1) {
                    $data['image_hyperlink']= $NameofPicture . ".jpg";
                }              

                if (strpos($NameofPicture,"ProductImage") < -1) {
                    $data['image_hyperlink']='../ProductImage/' . $NameofPicture;
                }       

                echo '<tr>
                <td valign="top" width="150" align="right"><strong></strong></td>
                <td valign="top">';
                if ($data['image_hyperlink'] != '') {
                    echo '<img src = "' . $data['image_hyperlink'] . '" width = "300">';
                }
                echo '
                <br>
                <strong>Upload Image: </strong><input type="file" name="file">
                <input type="hidden" name="file_name" value="' . $data['image_hyperlink'] . '"></td>
                </tr>';
                echo '<tr>
                <td valign="top" width="150" align="right"><strong>PDF </strong></td>
                <td valign="top">';
                if ($data['pdf_file'] != '' && file_exists('../ProductImage/' . $data['pdf_file'])) {
                    echo '<a download href="../ProductImage/' . $data['pdf_file'] . '">'.basename($data['pdf_file']).'</a><br>';
                }
                echo '
                
                <input type="file" name="pdf_file">
                <input type="hidden" name="data[pdf_file]" value="' . $data['pdf_file'] . '"></td>
                </tr>';
                $textarea_fields = table_fields_list($table_name, 'text');
                foreach ($data as $key => $value) {
                    if ($key == 'id' || $key == 'pdf_file') {
                        continue;
                    }
                    if (in_array($key, $textarea_fields)) {
                        $type = 'textarea';
                        $style = "width:600px; height:100px";
                    } else {
                        $type = 'text';
                        $style = "width:600px";
                    }
                    $name = 'data[' . $key . ']';
                    echo '<tr>
                        <td width="150" align="right"><strong>' . $key . '</strong></td>
                        <td>';
                    if ($key == 'category') {
                        $category = $value;
                        echo '<select name="' . $name . '" onchange="loadSubcategory(this.value);" >';
                        echo ' <option value="">Select Category</option>';
                        category_dropdown($value);
                        echo '</select>';
                    } else if ($key == 'subcategory') {
                        echo '<select name="' . $name . '" id="subcategory" >';
                        echo ' <option value="">Select SubCategory</option>';
                        subcategory_dropdown($category, $value);
                        ;
                        echo '</select>';
                    } else {
                        echo input_box($type, $name, $value, $style);
                    }

                    echo '</td>
                    </tr>';
                }

                echo '
                <input type="hidden" name="url" value="' . $url . '">
                <input type="hidden" name="table" value="' . $table_name . '">
                <input type="hidden" name="action" value="' . $action . '">
                <input type="hidden" name="data[id]" value="' . $data['id'] . '">';

                print "</table>";
                ?>
                <script type="text/javascript">
				CKEDITOR.replace( 'data_description',
					{
						fullPage : false
					});
                    CKEDITOR.replace( 'data_EXPANDED_PRODUCT_DESCRIPTION',
					{
						fullPage : false
					});
			</script>

            </td>
        </tr>
        <tr>
            <td align="center">
                <?php
                submit_update_button($table_name, $data['id']);
                ?>
            </td>
        </tr>
    </table>
</form>

</body>
</html>