<?php

include "../globals.php";
include "adminheader.php";
include "../pager.php";


require_once 'csvbuilder.class.php';
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------

adminTitle("List Web Contacts");

$searchText = $_GET['searchText'];

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}



if ($_GET['action'] == 'export') {
    $csv_file = "csv_export/Contacts.csv";

    $resultID = mysql_query("SELECT         
        `name` AS `First Name`, 
        `last` AS `Last Name`, 
        `email` AS `Email`, 
        DATE(`date`) AS `Date`
        FROM `contactus`", $linkID);
    while ($row = mysql_fetch_assoc($resultID)) {
        $data[] = $row;
    }
    if (!empty($data)) {
        $Builder = new CSVBuilder;
        $csv_data = $Builder->arrayToCSV($data);
        file_put_contents($csv_file, $csv_data);
        $Builder->sendCSVHeaders($csv_file);
        echo $csv_data;
        exit();
    }
}
if ($_GET['print']) {
    ob_clean();
    ob_start();
}
print "<div style=\"width:100%; padding-top:10px; margin-bottom:10px;\">";
if (!$_GET['print']) {
    echo '<div style="float:left; margin-left:10px; width:120px;" class="styled-button-9"><a href="' . $_SERVER['REQUEST_URI'] . '?&print=true' . '" target="_blank">Print this Report</a></div>';
    echo '<div style="float:left; margin-left:10px; width:120px;" class="styled-button-9">
        <a href="mlistContacts.php?action=export" >Export to CSV</a>
            </div>';
}
echo '<div style="float:left; margin-left:100px;" ><font color="blue" size="+1">MANAGE CONTACTS</font></div>
<div style="clear:both"></div>';
print "</div>";




print "<div style=\"width: 700px;\">";

if ($searchText != '') {
    $result = mysql_query("Select * from contactus WHERE `last` LIKE '%$searchText%'", $linkID);
} else {
    $result = mysql_query("Select * from contactus", $linkID);
}


$total = mysql_num_rows($result);

$limit = 20;
$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;
$limit = $pager->limit;
$page = $pager->page;




if ($searchText != '') {
    $resultID = mysql_query("Select id,name,last,email,date  from contactus WHERE `last` LIKE '%$searchText%' order by date desc  limit $offset, $limit", $linkID);
} else {
    $resultID = mysql_query("Select id,name,last,email,date from contactus order by date desc  limit $offset, $limit", $linkID);
}


$m = 0;

$Catname = $_POST['Catname'];
$Catstatus = $_POST['Catstatus'];

print "<font face=\"Arial\">" . $msg . "</font>";


showPages($page, $pager);

print "<form name=\"form\" method=\"get\" action=\"\">";
print "<p align='center'><strong>Filter Customer By Lastname: </strong><input type='text' name='searchText' value='$searchText' > <input type='submit' name='search' value='Search'></p>";

print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">First Name</font></td>";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Last Name</font></td>";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1;\" width=\"40%\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Email</font></td>";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Date</font></td>";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Action</font></td>";


while ($row = mysql_fetch_row($resultID)) {
    print "<tr>";
    $fieldCount = 1;
    foreach ($row as $field) {
        if ($fieldCount == 1) {
            $saveField = $field;
            $fieldCount++;
            continue;
        }
        changeRowColor($m);
        if ($fieldCount == 2) {
            $first_name = $field;
            print "<font face=\"Arial\" size=\"2\">$field</td>";
            $fieldCount++;
            continue;
        }

        if ($fieldCount == 3) {
            $last_name = $field;
            print "<font face=\"Arial\" size=\"2\">$field</td>";
            $fieldCount++;
            continue;
        }

        if ($fieldCount == 4) {
            $company = $field;
            print "<font face=\"Arial\" size=\"2\">$field</td>";
            $fieldCount++;
            continue;
        }

        if ($fieldCount == 5) {
            $email = $field;
            print "<font face=\"Arial\" size=\"2\">$field</td>";
            $fieldCount++;
            continue;
        }

        if ($fieldCount == 6) {
            $status = $field;
            $fieldCount++;
            continue;
        }
        if ($fieldCount == 7) {
            $date = $field;
            print "<font face=\"Arial\" size=\"2\">" . date("m-d-Y", strtotime($date)) . "</font></td>";
            $fieldCount++;
            continue;
        }

        $fieldCount++;
    }
    changeRowColor($m);
    print "<font face=\"Arial\" size=\"2\">";
    /*
      if($status=='active')
      {
      print "<a href=\"MemberStatus.php?MemberNumber=$saveField&status=1\">deactivate<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      }
      else
      {
      print "<a href=\"MemberStatus.php?MemberNumber=$saveField&status=0\">activate<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      }
     */
    print "<a href=\"EditContacts.php?MemberNumber=$saveField\">Edit<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    print "<a href=\"UpdateContact.php?delete_id=$saveField\">Delete</td>";
    print "</tr>";

    if ($m == 1)
        $m = 0;
    else
        $m = 1;
}
print "</table></form>";

showPages($page, $pager);

if ($_GET['print']) {
    $page_contents = ob_get_clean();
    echo $page_contents;

    echo '<script type="text/javascript" > window.print();</script>';
    exit();
}

include "adminfooter.php";
print "</div>";

//**************************************************
// F U N C T I O N S
//**************************************************   
function showPages($page, $pager) {
    if ($page == 1)
        echo "Previous&nbsp; ";
    else
        echo "<a href=\"mlistContacts.php?searchText=" . $searchText . "&page=" . ($page - 1) . "\">Previous&nbsp;</a>";

    for ($i = 1; $i <= $pager->numPages; $i++) {
        echo " | ";
        if ($i == $pager->page)
            echo "<b>$i</b>";
        else
            echo "<a href=\"mlistContacts.php?searchText=" . $searchText . "&page=$i\">$i</a>";
    }

    if ($page == $pager->numPages)
        echo "|&nbsp;&nbsp;Next";
    else
        echo "<a href=\"mlistContacts.php?searchText=" . $searchText . "&page=" . ($page + 1) . "\">|&nbsp;&nbsp;Next</a>";
}
?>

