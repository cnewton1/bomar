<?php
include "../globals.php"; 
include "adminheader.php";
include "mlistAnnouncementsFunction.php";
include "../pager.php";

//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------
adminTitle("Manage Announcements");

if(isset($_GET['page']))
 {
 $page = $_GET['page']; 
 }
else
 {
 $page=1;
 }

listAnnouncementsFunction($linkID,$page);

?>