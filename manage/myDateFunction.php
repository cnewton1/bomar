<?php

// This function will add 10 days to the current date that is entered.
function myDateAdd($Date)
{
 $mm=strtotime($Date);           
 
 // Add 10 days, (1 day is 86,400)
 return date("Y-m-d", $mm + 864000);
  
}
  
?>
