<?php
include "../globals.php";
include "../pager.php";
include "../MyImageFunction.php";
include "../CategoryDropDownFunction.php";
include "../SubCategoryDropDownFunction.php";
include "adminheader.php";


//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------
adminTitle("Manage Admins");

?>
<link type="text/css" rel="stylesheet" media="all" href="../bootstrap/css/bootstrap.min.css" />

<?php
require_once dirname(__FILE__) . '/xcrud/xcrud.php';


echo '<div class="container">
<div class="row">
<div class="col-md-12">';
$upload_path = 'uploads';
$image_config_arr = array(
    'path' => $upload_path,
    'width' => 400
);
$instance_name = '';
$xcrud = Xcrud::get_instance($instance_name);

$xcrud->unset_csv();
$xcrud->unset_print();
//$xcrud->unset_remove();
//$xcrud->unset_view();

//$xcrud->hide_button('save_edit');
$xcrud->hide_button('save_new');

$xcrud->table_name('User Administration');
$xcrud->table('admin');
$columns = array(
    'username' => 'Username',
    'type' => 'User Type',
    'status' => 'Status'

);
$fields = array(
    'username' => 'Username',
    'password' => 'Password',
    'type' => 'User Type',
    'status' => 'Status'

);
$xcrud->change_type('type', 'select', 'admin', 'admin');
$xcrud->change_type('status', 'select', 'active', 'active,inactive');

$xcrud->columns(array_keys($columns));
$xcrud->label($fields);
$xcrud->fields($fields);
$xcrud->order_by('username', 'asc');

$xcrud->validation_required('username');
$xcrud->validation_required('password');
$xcrud->validation_required('type');

echo $xcrud->render();

echo '</div>
	</div>
   </div>';

print "</body>";

include "adminfooter.php";
print "</div>";

?>

