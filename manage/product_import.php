<?php
ob_start();
include "../globals.php";
include "adminheader.php";
include "../pager.php";

require_once dirname(__FILE__) . '/../vendor/autoload.php';


use Medoo\Medoo;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => CONFIG_DB_DBNAME,
    'server' => CONFIG_DB_HOST,
    'username' => CONFIG_DB_USERNAME,
    'password' => CONFIG_DB_PASSWORD
]);


function product_import_mapping_dropdown($id)
{
    global $db;
    $result = $db->select('product_import_mapping', ['id', 'name'], ['ORDER' => ['name' => 'ASC']]);
    foreach ($result as $row) {
        $selected_str = ($row['id'] == $id) ? 'selected' : '';
        echo '<option value="' . $row['id'] . '" ' . $selected_str . '>' . $row['name'] . '</option>';
    }
}
function filter_null_value($val)
{
    return ($val !== null);
}

function my_array_filter($array)
{
    return array_filter($array, 'filter_null_value');
}
$allowedExtensions = array('csv');

function isAllowedExtension($fileName)
{
    global $allowedExtensions;
    return in_array(strtolower(end(explode(".", $fileName))), $allowedExtensions);
}

function get_product_table_columns()
{
    global $db;
    $data = [];
    $result = $db->query("SHOW COLUMNS FROM product2 WHERE Field != 'id'")->fetchAll();

    foreach ($result as $row) {
        $data[] = $row['Field'];
    }
    sort($data);
    return $data;
}

function product_table_columns_drodown($column_name)
{
    $data = get_product_table_columns();

    foreach ($data as $key => $value) {
        $selected_str = ($column_name == $value) ? 'selected' : '';
        echo '<option value="' . $value . '" ' . $selected_str . '>' . $value . '</option>';
    }
}

$page_title = 'Master Spreadsheet Import: Step 1';


$inputFileName = '';
$mapping_name = '';
$mapping_id = '';
if (isset($_POST['submit'])) {


    $insert_count = 0;

    $option = $_POST['option'];
    $inputFileName = $_POST['inputFileName'];

    $update_existing_records = $_POST['update_existing_records'];
    $active_all_records = $_POST['active_all_records'];
    $mapping_id = $_POST['mapping_id'];

    if ($_FILES["file"]["size"] > 0) {


        $inputFileName = '';
        $orig_file_name = $_FILES["file"]["name"];
        if (isAllowedExtension($orig_file_name)) {
            $file_size = $_FILES["file"]["size"];
            $ul_file_name = time() . "_" . $_FILES["file"]["name"];
            if (!is_dir('uploads/webupload')) {
                if (!mkdir('uploads/webupload', 0777, true)) {
                    die('Directory creation failed: uploads/webupload');
                }
            }

            $upload_file_path = "uploads/webupload/" . $ul_file_name;
            $upload_file = move_uploaded_file($_FILES["file"]["tmp_name"], $upload_file_path);
            if ($upload_file) {
                $inputFileName = $upload_file_path;
            } else {
                $error[] = 'File upload Failed!';
            }
        } else {
            $error[] = $orig_file_name . ' is an invalid file type! Only <strong>' . implode(",", $allowedExtensions) . '</strong> filetype is allowed.';
        }
    }


    if ($inputFileName != '' && file_exists($inputFileName)) {
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);

        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        $highestCol = $worksheet->getHighestColumn();
        $first_rows = "1";
        $csv_first_row_data = $worksheet->rangeToArray("A1:$highestCol$first_rows", null, true, true, false);

        if (empty($csv_first_row_data)) {
            $error[] = 'CSV Parse Failed.';
        } else {
            $page_title = 'Master Spreadsheet Import: Step 2';
        }
    } else {
        $error[] = 'Please select file';
    }
}

if (isset($_POST['save_mapping'])) {
    $error = [];
    $columns = $_POST['columns'];
    $insert_count = 0;
    $inputFileName = $_POST['inputFileName'];
    $update_existing_records = $_POST['update_existing_records'];
    $active_all_records = $_POST['active_all_records'];
    $mapping_id = $_POST['mapping_id'];
    if (isset($_POST['save_mapping'])) {
        $mapping_name = trim($_POST['mapping_name']);
        if ($mapping_name != '') {
            $db->insert('product_import_mapping', ['name' => $mapping_name, 'mapping_data' => json_encode($columns)]);
            $mapping_id = $db->id();
        }
    }

    if ($inputFileName != '' && file_exists($inputFileName)) {
        /** Load $inputFileName to a Spreadsheet Object  * */
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        $highestCol = $worksheet->getHighestColumn();
        $first_rows = "1";
        $csv_first_row_data = $worksheet->rangeToArray("A1:$highestCol$first_rows", null, true, true, false);
        if (empty($csv_first_row_data)) {
            $error[] = 'CSV Parse Failed.';
        } else {
            $page_title = 'Master Spreadsheet Import: Step 2';
        }
    } else {
        $error[] = 'Please select file';
    }
}
if (isset($_POST['process'])) {
    $error = [];
    $columns = $_POST['columns'];
    $insert_count = 0;
    $inputFileName = $_POST['inputFileName'];
    $update_existing_records = $_POST['update_existing_records'];
    $active_all_records = $_POST['active_all_records'];
    $mapping_id = $_POST['mapping_id'];

    if (!in_array('category', $columns)) {
        $error[] = 'category mapping is missing';
    }

    if (!in_array('subcategory', $columns)) {
        $error[] = 'subcategory mapping is missing';
    }
    if (!in_array('mfgpart', $columns)) {
        $error[] = 'mfgpart mapping is missing';
    }
    if (empty($error)) {
        if ($inputFileName != '' && file_exists($inputFileName)) {
            /** Load $inputFileName to a Spreadsheet Object  * */
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();
            $highestCol = $worksheet->getHighestColumn();

            if (empty($error)) {
                $data = $worksheet->rangeToArray("A1:$highestCol$highestRow", null, true, true, false);
                if (!empty($data)) {
                    if ($option == 'remove') {
                        $db->query("TRUNCATE product2");
                        $db->query("TRUNCATE category");
                        $db->query("TRUNCATE subcategory");
                    }
                    $category_data = [];
                    for ($i = 1; $i < $highestRow; $i++) {
                        $item_row = [];

                        $category = $subcategory = '-';

                        foreach ($columns as $column_index => $table_field) {
                            if ($table_field != '') {
                                $item_row[$table_field] = $data[$i][$column_index];
                                if (in_array($table_field, ['price', 'price_2', 'price_3', 'price_4', 'price_5'])) {
                                    // $item_row[$table_field] = trim(str_replace(array('$', ',', ' '), array('', '', ''), $item_row[$table_field]));
                                    // $item_row[$table_field] = '$' . number_format($item_row[$table_field], 2);
                                }
                                if ($table_field == 'category') {
                                    $category = $data[$i][$column_index];
                                } else if ($table_field == 'subcategory') {
                                    $subcategory = $data[$i][$column_index];
                                } else if ($table_field == 'image_hyperlink') {
                                    $item_row[$table_field] = ($item_row[$table_field] != '') ? basename($item_row[$table_field]) : '';
                                }
                            }
                        }

                        $category_data[$category][$subcategory] = true;

                        $item_row = my_array_filter($item_row);
                        if (!empty($item_row)) {
                            $existing_record = [];
                            if (isset($active_all_records) && $active_all_records == '1') {
                                $item_row['active'] = '1';
                            }

                            if (isset($update_existing_records) && $update_existing_records == '1') {
                                $existing_record = $db->get(
                                    'product2',
                                    ['id'],
                                    [
                                        'category' => $item_row['category'],
                                        'subcategory' => $item_row['subcategory'],
                                        'mfgpart' => $item_row['mfgpart']
                                    ]
                                );
                            }
                            if (!empty($existing_record)) {
                                $db->update('product2', $item_row, ['id' => $existing_record['id']]);
                            } else {
                                $db->insert("product2", $item_row);
                            }
                            if ($db->error()[2] == '') {
                                $insert_count++;
                            } else {
                                echo "Row Number: $i - " . $db->error()[2];
                                exit();
                            }
                        }
                    }



                    if (!empty($category_data)) {
                        foreach ($category_data as $category => $subcat_arr) {
                            if ($category != '' && $category != '-') {
                                $cat_count = $db->count('category', ['Name' => $category]);
                                if ($cat_count == 0) {
                                    $db->insert('category', ['Name' => $category]);
                                }
                                foreach ($subcat_arr as $subcategory => $subcat2_arr) {
                                    if ($subcategory != '' && $subcategory != '-') {
                                        $subcat_count = $db->count('subcategory', ['Parent' => $category, 'Name' => $subcategory]);
                                        if ($subcat_count == 0) {
                                            $db->insert('subcategory', ['Name' => $subcategory, 'Parent' => $category]);
                                        }
                                    }
                                }
                            }
                        }
                    }


                    ob_get_clean();

                    $msg = $insert_count . ' record successfully inserted';
                    header('Location: product_import.php?msg=' . $msg);
                    exit();
                } else {
                    $error[] = 'Data file is empty';
                }
            }
        } else {
            $error[] = 'Please select file';
        }
    } else {
        if ($inputFileName != '' && file_exists($inputFileName)) {
            /** Load $inputFileName to a Spreadsheet Object  * */
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();
            $highestCol = $worksheet->getHighestColumn();
            $first_rows = "1";
            $csv_first_row_data = $worksheet->rangeToArray("A1:$highestCol$first_rows", null, true, true, false);
            if (empty($csv_first_row_data)) {
                $error[] = 'CSV Parse Failed.';
            } else {
                $page_title = 'Master Spreadsheet Import: Step 2';
            }
        } else {
            $error[] = 'Please select file';
        }
    }
}
adminTitle($page_title);


echo "<link rel='stylesheet' id='bootstrap-css' href=\"../bootstrap/css/bootstrap.min.css\" type='text/css' media='all' />";


echo '<div class="container">
<div class="row">
<div class="col-md-12">';

echo '<h2>' . $page_title . '</h2>';

?>
<?php
$msg = (isset($_GET['msg'])) ? $_GET['msg'] : '';
if ($msg != '') {
?>
    <div class="alert alert-success"><?php echo htmlspecialchars($msg); ?></div>
<?php
}
if (!empty($error)) {
?>
    <div class="alert alert-danger"><?php echo implode('<br>', $error); ?></div>
<?php
}
?>
<style>
    input[type=checkbox] {
        width: 18px;
        height: 18px;
    }
</style>

<form method="POST" action="" class="form-horizontal" style="margin-bottom:10px;" enctype="multipart/form-data">

    <?php
    if (empty($csv_first_row_data)) {
    ?>
        <div class="row">
            <div class="col-md-12">

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">CSV File: </label>
                    <div class="col-sm-3">
                        <input type="file" name="file" class="" required>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <h4 class="col-sm-12" style="color: blue; text-transform: uppercase;">Catalog Import Options: </h4>
                    <div class="col-sm-12" style="font-size: 18px; line-height:30px">
                        Update Existing Records: <input type="checkbox" name="update_existing_records" value="1">
                    </div>
                    <div class="col-sm-12" style="font-size: 18px; line-height:30px">
                        Active All Records: <input type="checkbox" name="active_all_records" value="1">
                    </div>

                </div>
            </div>
            <div class="col-md-6">

                <div class="form-group">

                    <div class="col-sm-12" style="font-size: 18px; line-height:30px">
                        <h4 style="color: blue; display: inline-block; text-transform: uppercase;">Use Mapping:</h4>

                        <select name="mapping_id">
                            <option value=""></option>
                            <?php product_import_mapping_dropdown($mapping_id); ?>

                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-sm-3">
                        <input class="btn btn-success" type="submit" name="submit" value="Continue">
                    </div>

                </div>
            </div>
        </div>
    <?php
    }
    ?>



    <?php
    if (!empty($csv_first_row_data)) {
        if(empty($columns) && $mapping_id != ''){
            $product_import_mapping = $db->get('product_import_mapping', ['mapping_data'], ['id'=>$mapping_id]);
            $columns = json_decode($product_import_mapping['mapping_data'], true);
        }
    ?>
        <div class="form-group text-right">
            SAVE MAP AS: <input type="text" name="mapping_name" value="<?php echo $mapping_name; ?>">
            <input type="hidden" name="mapping_id" value="<?php echo $mapping_id; ?>">

            <input type="submit" class="btn btn-success btn-sm" name="save_mapping" value="SAVE">
        </div>
        <div class="form-group">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>CSV Columns</th>
                        <th>Product Table Mapping</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($csv_first_row_data[0] as $csv_column_index => $csv_column_name) {
                        echo '<tr>';
                        echo '<td>' . $csv_column_name . '</td>';
                        echo '<td>';
                        echo '<select name="columns[' . $csv_column_index . ']">';
                        echo '<option value=""></option>';
                        product_table_columns_drodown($columns[$csv_column_index]);
                        echo '</select>';
                        echo '</td>';
                    }
                    ?>
                </tbody>
            </table>
        </div>


        <div class="form-group">
            <input type="hidden" name="inputFileName" value="<?php echo $inputFileName; ?>">
            <input type="hidden" name="update_existing_records" value="<?php echo $update_existing_records; ?>">
            <input type="hidden" name="active_all_records" value="<?php echo $active_all_records; ?>">
            <input class="btn btn-success btn-sm" type="submit" name="process" value="Process">

        </div>
    <?php
    }

    ?>


</form>


<?php

echo '</div>
	</div>
   </div>';

print "</body>";
