    function get_auth($data_array) {

    if(empty($data_array['amount']) || !is_numeric($data_array['amount'])) {
    $error->insert(�Please provide a valid amount for credit card processing.�);
    }

    if(empty($data_array['credit_card_number'])) {
    die(�Please provide a valid Credit Card Number.�);
    }

    if(empty($data_array['credit_card_exp_date'])) {
    die(�Please provide a valid Credit Card Expiration Date.�);
    }

    if(empty($data_array['credit_card_cvv'])) {
    die(�Please provide a valid Credit Card CVV Code.�);
    }

    if(empty($data_array['billing_first'])) {
    die(�Please provide the Billing First Name.�);
    }

    if(empty($data_array['billing_last'])) {
    die(�Please provide the Billing Last Name.�);
    }

    if(empty($data_array['billing_city'])) {
    die(�Please provide the Billing City.�);
    }

    if(empty($data_array['billing_state'])) {
    die(�Please provide the Billing State.�);
    }

    // set default api credentials
    $auth_net_tran_key = �abcdefg�;
    $auth_net_login_id = �abcdefg�;

    // Display additional information to track down problems
    $DEBUGGING = 1;

    // Set the testing flag so that transactions are not live
    $TESTING = 1;

    // Number of transactions to post if soft errors occur
    $ERROR_RETRIES = 2;

    if($gateway_status == �testing�) {
    $auth_net_url = �https://test.authorize.net/gateway/transact.dll�;
    } else if($gateway_status == �live�) {
    $auth_net_url = �https://secure.authorize.net/gateway/transact.dll�;
    } else {
    die(�Undefined Gateway Status. Unable to process trasactions. Contact the system Administrator.�);
    }

    $invoice_number = $order_details->cart_id + $invoice_start;

    $authnet_values = array
    (

    �x_login� => $auth_net_login_id,
    �x_version� => �3.1?,
    �x_delim_char� => �|�,
    �x_delim_data� => �TRUE�,
    �x_duplicate_window� => �30?,
    �x_url� => �FALSE�,
    �x_type� => �AUTH_CAPTURE�,
    �x_method� => �CC�,
    �x_tran_key� => $auth_net_tran_key,
    �x_relay_response� => �FALSE�,
    �x_card_num� => $data_array['credit_card_number'],
    �x_exp_date� => $data_array['credit_card_exp_date'],
    �x_card_code� => $data_array['credit_card_cvv'],
    �x_description� => $auth_net_description,
    �x_first_name� => $data_array['billing_first'],
    �x_last_name� => $data_array['billing_last'],
    �x_address� => $data_array['billing_street'],
    �x_city� => $data_array['billing_city'],
    �x_state� => $data_array['billing_state'],
    �x_zip� => $data_array['billing_zip'],
    �x_amount� => $data_array['amount'],
    �x_invoice_num� => $invoice_number
    );

    $line_item_num = 0;

    // you will need to create a mysql result here
    // to loop through if you want to pass in the invoice
    // and line item details. I have not included this code.

    $line_items = mysql_result; // replace �mysql_result� with your query
    line_item_array = array();

    while($line_item = mysql_fetch_assoc($line_items, MYSQL_ASSOC)) {
    $line_item_num++;
    // the following line is in this format: item_id<|>Item Name<|>Item Description<|>Quantity<|>Price<|>Taxable

    $line_item_text = �item{$line_item_num}<|>Item Name<|>Item Description<|>2<|>25.00<|>N�;
    array_push($line_item_array, $line_item_text);

    } // end of while loop

    // set for live mode, you will have to adjust this if processing test transactions
    $ch = curl_init(�https://secure.authorize.net/gateway/transact.dll�);

    // convert authnet_values to fields in post list
    $fields = ��;
    foreach( $authnet_values as $key => $value ) $fields .= �$key=� . urlencode( $value ) . �&�;
    // parse line item array
    foreach($line_item_array as $key=>$value) {
    if($key != count($line_item_array)-1) {
    $fields.=�x_line_item=�.$value.�&�;
    } else {
    $fields.=�x_line_item=�.$value;
    }
    }
    // post payment to Authorize.NET
    curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, rtrim( $fields, �& � )); // use HTTP POST to send form data
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response. ###
    $resp = curl_exec($ch); //execute post and get results
    curl_close ($ch);

    $response_array = explode(�|�, $resp);

    switch($response_array['0']){
    case �1?:
    $response['response_code']=�Approved�;
    break;
    case �2?:
    $response['response_code']=�Declined�;
    break;
    case �3?:
    $response['response_code']=�Error�;
    break;
    }

    $response['response_subcode'] = $response_array['1'];
    $response['response_reason_code'] = $response_array['2'];
    $response['response_reason_text'] = $response_array['3'];
    $response['approval_code'] = $response_array['4'];
    $response['avs_result_code'] = $response_array['5'];
    $response['transaction_id'] = $response_array['6'];

    if($response['response_code'] != �Approved�) {
    die($response['response_code'].�: �.$response['response_reason_text']);
    }

    // will only return response if the transaction was approved
    return $response;
    } // end of function

Within your page you will need to call this function and pass in the appropriate payment data:

    <?php

    $result = get_auth(array(
    �amount�=>$amount,
    �credit_card_number�=>$credit_card_number,
    �credit_card_exp� =>$credit_card_exp,
    �credit_card_cvv�=>$credit_card_cvv,
    �billing_first�=>$billing_first,
    �billing_last�=>$billing_last,
    �billing_street�=>$billing_street,
    �billing_city�=>$billing_city,
    �billing_state�=>$billing_state,
    �billing_zip�=>$billing_zip
    ));

    var_dump($result);

    ?>