<?php
include "globals.php"; 
include "adminheader.php";

include "../pager.php";

adminTitle("Manage Backups");

if(isset($_GET['page']))
	$page=$_GET['page'];

if($page == 0)
{
	$page = 1;	
}

$msg = ($_REQUEST['msg'] != '')?trim($_REQUEST['msg']):'';

$query = "SELECT COUNT(`id`) AS `num` FROM  `db_backup`";
$result = mysql_fetch_array(mysql_query($query)); 
$total = $result['num'];
$limit = 20; 
$pager  = Pager::getPagerData($total, $limit, $page); 
$offset = $pager->offset; 
$limit  = $pager->limit; 
$page   = $pager->page;  

//--------------------------------------------------
// OK, Now get all of the account type
//--------------------------------------------------
function showPages($page,$pager)
{
	//$search_str = 'search_by='.$_GET['search_by'].'&search_text='.$_GET['search_text'].'&frm_action='.$_GET['frm_action'].'&filter='.$_GET['filter'];
	print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"BackupDB.php?".$search_str."&page=" . 1 . "\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"BackupDB.php?".$search_str."&page=" . ($page - 1) . "\" target=\"_self\">Prev Page&nbsp;</a>";  
    }

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
			echo " | ";
			if ($i == $pager->page) 
            	echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>";
			else
				echo "<a href=\"BackupDB.php?".$search_str."&page=$i\" target=\"_self\">$i</a>"; 
         }
    } 
    if ($page == $pager->numPages) // this is the last page - there is no next page 
	{
		echo "|&nbsp;&nbsp;"; 
	}
	else 
	{
		// not the last page, link to the next page
		echo "|&nbsp;&nbsp;<a href=\"BackupDB.php?".$search_str."&page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>"; 
		echo "&nbsp;&nbsp;<a href=\"BackupDB.php?".$search_str."&page=" . $pager->numPages . "\" target=\"_self\"> >> Last Page</a></font>";
	}
    print "<br><br>";
}


function bytesToSize($bytes, $precision = 2)
{  
    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
   
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return $bytes . ' B';
 
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . ' KB';
 
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . ' MB';
 
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . ' GB';
 
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . ' TB';
    } else {
        return $bytes . ' B';
    }
}

if(isset($_GET['action']))
{
	if($_GET['action'] == 'del' && $_GET['id'] != '')
	{
		$id = mysql_real_escape_string($_GET['id']);
		$result_row = mysql_fetch_array(mysql_query("SELECT * FROM `db_backup` WHERE `id` = '$id'"));
		
		$del_query = mysql_query("DELETE FROM `db_backup` WHERE `id` = '$id'");
		if($del_query)
		{
			unlink('BACKUP/'.$result_row['file_name']);
			$msg = "Record Successfully deleted.<br>";
		}
		
	}
}
 

print "<head>";
print "</head>";

print "<body style=\"font-family:Arial;\">";

$resultID = mysql_query("SELECT * FROM `db_backup` ORDER BY `db_backup`.`id` DESC LIMIT $offset, $limit ", $linkID);

$m=0;
print "<h1>Manage Backups</h1><p>";

echo '<font size="+1"><a href="backuptables.php">Click here to Perform Daily Backup</a></font><br><br>';
if($msg != '')
{
	print "<font face=\"Arial\" color=\"#009933\" size=\"+1\">".$msg."</font><br>";
}



if($total == 0)
{
	print "<div style=\"width:100%;\" align=\"center\" ><font color=\"red\" size=\"+2\"><strong>Not Found</strong></font></div>";
}


showPages($page,$pager);

print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"990px\" id=\"listPhoneOrder\">";

print "<tr>";
print "<td width=\"50\"  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">ID</font></td>";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Backup Name</font></td>";

print "<td  width=\"80\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Backup Size</font></td>";

print "<td  width=\"100\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">User</font></td>";

print "<td width=\"80\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Date</font></td>";

print "<td width=\"80\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Time</font></td>";

print "<td width=\"100\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Action</font></td>";
print "</tr>";

while ($row = mysql_fetch_array($resultID))
{
	print "<tr>";
	//echo changeRowColor($m);
	echo "<td style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">$row[id]</td>";
	
	//echo changeRowColor($m);
	echo "<td  style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">".$row['file_name']."</td>";
	
	echo "<td  style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">".bytesToSize($row['file_size'])."</td>";
	
	//echo changeRowColor($m);
	echo "<td   style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">$row[user]</td>";
	
	//echo changeRowColor($m);
	echo "<td  style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">".date('m/d/Y',strtotime($row['date']))."</td>";
	
	//echo changeRowColor($m);
	echo "<td  style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">".date('h:i A',strtotime($row['time']))."</td>";
	
	
	//echo changeRowColor($m);
	echo "<td  style=\"border-style: none; border-width: medium\">";
	echo "<font face=\"Arial\" size=\"2\">";
	print "<a href=\"BackupDB.php?id=$row[id]&action=del&page=$page\" onclick=\"return confirm('Are you sure to delete this record?');\" >Delete</td>";	
	echo "</tr>";
   
  
   if ($m==1)
      $m=0;
    else 
      $m=1;
}
print "</table>";  
 
showPages($page,$pager);  


   
include "adminfooter.php";

?>



