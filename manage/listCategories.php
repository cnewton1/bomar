<?php
include "../globals.php";

include '../pager.php';
include "adminheader.php";
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------

adminTitle("List Categories");

$where_condition = "1";
if (isset($_REQUEST['q']) && $_REQUEST['q'] != '') {
    $q = mysql_real_escape_string(trim($_REQUEST['q']));
}


if ($q != '') {
    $where_condition .= " AND `Name` LIKE '%$q%'";
}
$page = ($_REQUEST['page'] > 1) ? $_REQUEST['page'] : 1;
$page_name = 'listCategories.php';
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');
$req_parameters = rtrim($req_parameters,'&');
$req_parameters = ltrim($req_parameters,'?');
$page = ($_GET['page'] > 1) ? $_GET['page'] : 1;
$pagestring = '?' . $req_parameters . '&page=';
$adjacents = 3;
$totalitems = mysql_fetch_array(mysql_query("SELECT COUNT(DISTINCT `id`) AS `num` FROM `category` WHERE $where_condition"));

$total = $totalitems['num'];
$limit = 25;
$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;
$limit = $pager->limit;
$page = $pager->page;

print "<div style=\" width: 700px;\">";
$resultID = mysql_query("Select id, Name from category WHERE $where_condition order by Name LIMIT $offset, $limit", $linkID);
$m = 0;

print "<b><font face=\"Arial\" size=\"2\"><a href=\"addCategories.php\">";
print "<img border=\"0\" src=\"../images/BNewCategory.gif\" width=\"159\" height=\"22\"></a></font></b>";
?>
<div style="text-align: right">
    <form method="GET" action="">
        <input type="text" name="q" value="<?php echo htmlspecialchars($q); ?>">
        <input type="submit" name="submit" value="Search">
    </form>
</div>
<div style="clear: both" >
    <?php
    echo getPaginationString($page, $total, $limit, $adjacents, $page_name, $pagestring);
    ?>    
</div>
<?php
print "<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";

echo '<tr>';
print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">ID</font></td>";

print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Category</font></td>";


print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">Action</font></td>";

echo '</tr>';
while ($row = mysql_fetch_row($resultID)) {
    print "<tr>";
    $fieldCount = 1;
    foreach ($row as $field) {

        // Save the Category Name Only
        if ($fieldCount == 1)
            $saveField = $field;

        if ($fieldCount == 2)
            $CatField = $field;

        $fieldCount++;
        changeRowColor($m);
        print "<font face=\"Arial\" size=\"2\">$field</td>";
    }
    changeRowColor($m);
    print "<font face=\"Arial\" size=\"2\">";
    print "<a href=\"editCategories.php?Catname=$CatField&id=$saveField\">Edit<a>&nbsp;&nbsp;&nbsp;&nbsp;";
    print "<a href=\"DBCategory.php?Catname=$CatField&id=$saveField\">Delete</td>";
    print "</tr>";

    if ($m == 1)
        $m = 0;
    else
        $m = 1;
}
print "</table>";
?>
<div style="clear: both" >
    <?php
    echo getPaginationString($page, $total, $limit, $adjacents, $page_name, $pagestring);
    ?>    
</div>
<?php
mysql_close($linkID);

include "adminfooter.php";
print "</div>";
?>

