<?php
include "../globals.php"; 
include "adminheader.php";
include "../MyImageFunction.php";

if(isset($_GET['page']))
	$page=$_GET['page'];

$key_val=$_GET['key'];

$about=$_POST['hidval'];

$main_key = 'help';
$PageTitle = 'Edit Help';


$allowedExtensions = array("jpg","jpeg","gif","png"); 
function isAllowedExtension($fileName) {
  global $allowedExtensions;

  return in_array(strtolower(end(explode(".", $fileName))), $allowedExtensions);
}

if(isset($about)&&($about=="hidabout")){
	$file_name = $_POST['file_name'];
	
	if($_FILES["file"]["size"] >0)
	{
		$orig_file_name = $_FILES["file"]["name"];
		if(isAllowedExtension($orig_file_name))
		{
			$file_size = $_FILES["file"]["size"];			
			$ul_file_name = time()."_".$_FILES["file"]["name"];
			$upload_file = move_uploaded_file($_FILES["file"]["tmp_name"],"../ProductImage/" . $ul_file_name);		
			if($upload_file)
			{
				$file_name =  $ul_file_name;
			}
			else
			{
				$error[] = 'File upload Failed!';
			}
		}
		else
		{
			$error[] = $orig_file_name.' is an invalid file type! Only <strong>'.implode(",",$allowedExtensions) .'</strong> filetype is allowed.';
		}
	}
	
	if(empty($error))
	{
		$pg_description = $_POST['pg_description'];
		$pg_description = htmlentities($pg_description);
		$updt=mysql_query("update main_pages set imagename='$file_name', main_text='$pg_description' where main_key='$main_key'");
		if($updt){
			$msg="Updation Successful";
		}
	}
}

$query = "select imagename,main_text from main_pages where main_key='$main_key'"; 
$result = mysql_query($query); 
$text=mysql_fetch_array($result);
$file_name=$text["imagename"];
$pg_description=$text['main_text'];

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/sample.js" type="text/javascript"></script>
	<link href="ckeditor/sample.css" rel="stylesheet" type="text/css" />
</head>
<p><font size="+1"><b><?php echo $PageTitle;?></b></font></p>
<body>
<p style="color:green; font-size:14px; font-weight:bold;"><?php echo $msg;?></p>
<?php
if(!empty($error))
{
	echo '<p style="color:red; font-weight:bold;">'.implode('<br>',$error).'</p>';
	
}
?>
<form name="frm_name" method="POST" action="" enctype="multipart/form-data">
<input type="hidden" name="hidval" value="hidabout">
<?php
print "<table>";
/*print "<tr><td valign=\"top\">Image</td><td valign=\"top\"><input type=\"file\" name=\"file\">";
if($file_name != '')
{
	echo '<br><img src="../ProductImage/'.$file_name.'" width="150px">';
	
}
echo "</td></tr>";*/
print "<tr><td>Text for this page</td><td>";
print "<textarea id=\"editor1\" name=\"pg_description\" rows=\"10\" cols=\"80\">$pg_description</textarea>";
print "<script type=\"text/javascript\">
				CKEDITOR.replace( 'editor1',
					{
						fullPage : false
					});
			</script>";
print "</td></tr>";
print "<tr><td></td><td>
<input type=\"hidden\" name=\"file_name\" value=\"$file_name\">
<input type=\"submit\" name=\"Update\" value=\"update\"></td></tr>";
print "</table>";
?>
</form>
</body>
</html>