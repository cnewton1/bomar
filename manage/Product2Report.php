<?php
include "../globals.php";
include "../pager.php";
include "../MyImageFunction.php";
include "../CategoryDropDownFunction.php";
include "../SubCategoryDropDownFunction.php";
include "adminheader.php";
include "functions.php";


$page_title = 'List Products';
$filename = 'Product2Report.php';
$table_name = "product2";

$category = $_REQUEST['category'];
$subcategory = $_REQUEST['subcategory'];
$item_number = $_REQUEST['item_number'];

$page = ($_REQUEST['page'] > 1) ? $_REQUEST['page'] : 1;
$searchText = trim($_GET['searchText']);
$page_name = $filename;
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');
$req_parameters = ltrim($req_parameters, '?');
$req_parameters = rtrim($req_parameters, '&');
$req_parameters = '?' . $req_parameters . '&page=';
if ($_REQUEST['action'] == 'del' && $_REQUEST['id'] != '') {
    $id = mysql_real_escape_string($_REQUEST['id']);
    $del_query = mysql_query("DELETE FROM `$table_name` WHERE `id` = '$id'");
    if ($del_query) {
        $msg = 'Selected record successfully deleted.';
    } else {
        $error[] = mysql_error();
    }
}

$where_condition = '';
if ($category != '') {
    $where_condition .= $where_condition . " AND `category` = '$category'";
}
if ($subcategory != '') {
    $where_condition .= $where_condition . " AND `subcategory` = '$subcategory'";
}

if ($item_number != '') {
    $where_condition .= " AND WESTCARB_PART_NUMBER LIKE '%$item_number%'";
}

$result = mysql_fetch_array(mysql_query("SELECT COUNT(`id`) AS `num` FROM `$table_name` WHERE 1 $where_condition", $linkID));

$total = $result['num'];
$limit = 25;
$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;
$limit = $pager->limit;
$page = $pager->page;


$resultID = mysql_query("SELECT `$table_name`.*
	FROM `$table_name` 
	WHERE 1 $where_condition
	ORDER BY `id` DESC 
	LIMIT $offset, $limit", $linkID);
?>

<form method="get" action="" enctype="multipart/form-data">
    <script>
        function loadSubcategory(category) {
            if (category.length == 0) {
                document.getElementById("subcategory").innerHTML = "";
                return;
            } else {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("subcategory").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "getSubcategory.php?category=" + category, true);
                xmlhttp.send();
            }
        }
    </script>
    <table width="1024" cellpadding="5" cellspacing="5" border="0">

        <tr>
            <td class="page_heading" align="center"><?php echo $page_title; ?> <span style="float:right; font-size:14px; text-transform:capitalize" >Item Count: <font color="#000000"><?php echo $total; ?></font></span></td>
        </tr>
        <?php
        if ($msg != '') {
            echo '<tr>
	<td style="color:green; font-weight:bold; font-size:14px;">' . $msg . '</td>
	</tr>';
        }
        ?>

        <tr>
            <td align="left">
                <a href="edit_product.php" style="padding: 5px; background: #008000">Add New Items</a>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="5" cellspacing="5" border="0">
                    <tr>
                        <td>Limit Search By Category </td>
                        <td>
                            <select name="category" onchange="loadSubcategory(this.value);">
                                <option value="">All Area</option>
                                <?php category_dropdown($category); ?>
                            </select>
                        </td>
                        <td>Limit Search By SubCategory </td>
                        <td>
                            <select name="subcategory" id="subcategory">
                                <option value="">All Area</option>
                                <?php subcategory_dropdown($category, $subcategory); ?>
                            </select>
                        </td>
                        <td>Item Num: </td>
                        <td>
                            <input type="text" value="<?php echo $item_number; ?>" name="item_number" style="max-width:100px;">
                        </td>
                        <td><input type="submit" name="submit" value="Search" /></td>
                    </tr>
                </table>
            </td>
        </tr>


<!--<tr>
<td align="right"><strong>Item Number Search:</strong> <input type="text" name="searchText" value="<?php echo $_REQUEST['searchText']; ?>" /><input type="submit" name="submit" value="Search" /> </td>
</tr>-->
        <tr>
            <td >
                <?php echo getPaginationString($page, $total, $limit, $adjacents = 3, $page_name, $req_parameters); ?>
                <?php //PageNavigation($page, $pager, $page_name, $req_parameters); ?></td>
        </tr>
        <tr>
            <td >
                <?php
                print "<table border=\"0\" cellpadding=\"5\" cellspacing=\"5\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"layout\">
<tr>";

                print "<td  width=\"50\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>ID</strong></font></td>";

                print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Category</strong></font></td>";

                print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Subcategory</strong></font></td>";

                print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Short Description</strong></font></td>";


                print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Item Num</strong></font></td>";

                print "<td height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1;\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Price</strong></font></td>";

                print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1;\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Position</strong></font></td>";


                print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1;\">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Image Name</strong></font></td>";

                print "<td width=\"100\" height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1; \">";
                print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Action</strong></font></td>";
                print "</tr>";

                if (mysql_num_rows($resultID)) 
                {
                    while ($row = mysql_fetch_array($resultID)) {

                        echo '<tr>';

                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['id'] . '</font></td>';

                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['category'] . '</font></td>';

                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['subcategory'] . '</font></td>';

                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['shortddesc'] . '</font></td>';

                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['WESTCARB_PART_NUMBER'] . '</font></td>';


                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['commercial_list_price'] . '</font></td>';

                        change_table_row_color($m);
                        echo '<font face="Arial" size="2">' . $row['position'] . '</font></td>';

                        change_table_row_color($m);

                        $NameofPicture=$row['image_hyperlink'];

                        if (strpos($NameofPicture,".jpg") < -1) {
                            $row['image_hyperlink']='../ProductImage/' . $NameofPicture . ".jpg";
                        }
                        if (strpos($NameofPicture,"ProductImage") < -1) {
                            $row['image_hyperlink']='../ProductImage/' . $NameofPicture;
                        }

                        echo '<img src="' . $row['image_hyperlink'] . '" width="100"></td>';


                        change_table_row_color($m);
                        $url = urlencode($_SERVER['REQUEST_URI']);
                        echo '<a href="edit_product.php?id=' . $row['id'] . '&action=edit&table=' . $table_name . '&url=' . $url . '">Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="' . $filename . '?page=' . $page . '&action=del&id=' . $row['id'] . '" onclick="return confirm(\'Are you sure to Delete?\');">Delete</a>';
                        echo '</td>';

                        echo '</tr>';
                        if ($m == 1)
                            $m = 0;
                        else
                            $m = 1;
                    }
                }
                else {
                    echo '<tr><td colspan="3" align="center" style="color:red">No record found!</td></tr>';
                }


                print "</table>";
                echo '<br>';
                //PageNavigation($page, $pager, $page_name, $req_parameters);
                ?>
                <?php echo getPaginationString($page, $total, $limit, $adjacents = 3, $page_name, $req_parameters); ?>


            </td>
        </tr>

    </table>
</form>

