<?php

include "../globals.php";
include '../pager.php';
include "adminheader.php";
include 'MyImageFunction.php';
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------
adminTitle("Manage Video Links");

function get_youtube_videoid($url) {
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
        $id = $match[1];
    }
    return $id;
}

print "<div style=\"width: 1000px;\">";


$page_name = 'ManageTube.php';
$req_parameters = $_SERVER['QUERY_STRING'];
$req_parameters = remove_querystring_var($req_parameters, 'page');
$page = ($_GET['page'] > 1) ? $_GET['page'] : 1;

if ($_GET['action'] == 'del' && $_GET['id'] != '') {
    $id = mysql_real_escape_string($_GET['id']);
    $delete_query = mysql_query("DELETE FROM `videos` WHERE `id` = '$id'");
    if ($delete_query) {
        $msg = 'Record Successfully Deleted.';
    }
}


$result = mysql_fetch_array(mysql_query("SELECT COUNT(`id`) AS `num` FROM `videos`"));
$total = $result['num'];
$limit = 20;
$pager = Pager::getPagerData($total, $limit, $page);
$offset = $pager->offset;
$limit = $pager->limit;
$page = $pager->page;



$m = 0;
print "<div style=\"width:100%; padding-top:10px; margin-bottom:10px;\">";
echo '<div style="text-align:center " ><font color="blue" size="+2">Manage Youtube Video Links</font></div>
<div style="clear:both"></div>';

echo '<a href="add_video.php">Add new video link</a>';
print "</div>";



print "<div style=\"width:100%\">";


print "<font face=\"Arial\">" . $msg . "<br></font>";

PageNavigation($page, $pager, $page_name, $req_parameters);

print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";

print "<td  height=\"35\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Title</strong></font></td>";

print "<td  height=\"35\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>URL</strong></font></td>";

print "<td  bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Image</strong></font></td>";

print "<td  bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><strong>Action</strong></font></td>
</tr>";


$resultID = mysql_query("SELECT * FROM `videos` ORDER BY `id` DESC LIMIT $offset, $limit");

while ($row = mysql_fetch_array($resultID)) {
    print "<tr>";

    change_table_row_color($m);
    echo '<font face="Arial" size="2">' . $row['video_title'] . '</font></td>';

    change_table_row_color($m);
    echo '<font face="Arial" size="2">' . $row['video_url'] . '</font></td>';

    change_table_row_color($m);
    $thumbnail_url = '';
    $vid_id = get_youtube_videoid($row['video_url']);
    if ($vid_id != '') {

        $thumbnail_url = 'http://img.youtube.com/vi/' . $vid_id . '/mqdefault.jpg';
        $newSize = myResize($thumbnail_url, 100, 100);
    }
    if ($thumbnail_url != '') {
        echo '<img src="' . $thumbnail_url . '" ' . $newSize . '>';
    }


    echo '</td>';


    change_table_row_color($m);

    echo "<a href=\"add_video.php?action=edit&id=" . $row['id'] . "\">Edit<a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
    echo "<a href=\"ManageTube.php?action=del&id=" . $row['id'] . "\" onclick=\"return confirm('Are you sure to delete this video?');\">Delete</td>";
    echo "</tr>";

    if ($m == 1)
        $m = 0;
    else
        $m = 1;
}
print "</table>";
echo '<br>';
PageNavigation($page, $pager, $page_name, $req_parameters);



include "adminfooter.php";
print "</div>";
?>

