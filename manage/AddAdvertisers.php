<?php
include "../globals.php"; 
include "../pager.php";
include "../MyImageFunction.php";

include "adminheader.php";
if(isset($_GET['page']))
	$page=$_GET['page'];

$msg ='';
if(isset($_POST['save']))
{
	$thumbnail 		= $_FILES['thumbnail'];
	$title 			= $_POST['title'];
	$sequence_no 	= $_POST['sequence_no'];
	$url 			= htmlentities($_POST['url']);
	$line1 			= $_POST['line1'];
	$line2 			= $_POST['line2'];
	$line3 			= $_POST['line3'];
	$line4 			= $_POST['line4'];
	$line5 			= $_POST['line5'];
	
	if($thumbnail == '')
	{
		 $msg .= "Select Thumbnail Image.<br>";
	}
		
	if($title == '')
	{
		 $msg .= "Enter Title.<br>";
	}
	
	if($sequence_no == '')
	{
		 $msg .= "Enter Sequence No.<br>";
	}
		
	if($url == '')
	{
		 $msg .= "Enter URL.<br>";
	}
	
	if($msg == '')
	 {
		 $thumb_name = 'Add' . time() . ".jpg";
		 move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "Advertisements/". $thumb_name);
		 
		//echo "INSERT INTO `advertisements` (`thumbnail`, `title`, `url`, `sequence_no`, `line1`, `line2`, `line3`, `line4`, `line5`) VALUES ('$thumb_name', '$title', '$url', '$sequence_no', '$line1', '$line2', '$line3', '$line4', '$line5')"; 
		
		$insert_query=mysql_query("INSERT INTO `advertisements` (`thumbnail`, `title`, `url`, `sequence_no`, `line1`, `line2`, `line3`, `line4`, `line5`) VALUES ('$thumb_name', '$title', '$url', '$sequence_no', '$line1', '$line2', '$line3', '$line4', '$line5')");
		
		if($insert_query)
		{
			$msg="Inserted Successful";
			header('Location:ManageAdvertisers.php?page='.$_POST['page']);
		}
		else
		{
			$thumbnail= '';
			$msg= "Error: ".mysql_error();
		}
		 
	 }
 
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Add Advertisers</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/sample.js" type="text/javascript"></script>
	<link href="ckeditor/sample.css" rel="stylesheet" type="text/css" />
</head>
<p><font size="+1"><b>Add New Advertisers</b></font></p>
<body>
<p><font size="+1" color="red"><?php echo $msg;?></font></p>
<form name="frm_name" method="POST" action="AddAdvertisers.php" enctype="multipart/form-data">

<input type="hidden" name="page" value="<?php echo $page; ?>">

<table width="100%">
<tr>
  <td width="5%" align="left">&nbsp;</td>
  <td width="13%" align="left">Thumbnail  <font color="red">*</font></td>
  <td width="82%"><input type="file" name="thumbnail" size="80" id="thumbnail"><br><br></td>
</tr>
<tr>

<tr>
  <td width="5%" align="left">&nbsp;</td>
  <td width="13%" align="left">Title  <font color="red">*</font></td>
  <td width="82%"><input type="text" name="title" id="title" size="80" value="<?php echo @$title;?>" ><br><br></td>
</tr>
<tr>
  <td align="left">&nbsp;</td>
  <td align="left">URL <font color="red">*</font></td>
  <td><input type="text" name="url" id="url" size="80" value="<?php echo @$url;?>" ><br><br></td>
</tr>

<tr>
  <td width="5%" align="left">&nbsp;</td>
  <td width="13%" align="left">Sequence  <font color="red">*</font></td>
  <td width="82%"><input type="text" name="sequence_no" id="sequence_no" size="10"  value="<?php echo @$sequence_no;?>" ><br><br></td>
</tr>

<tr>
  <td align="left">&nbsp;</td>
  <td align="left">Line 1</td>
  <td><input type="text" name="line1" id="line1" size="80" value="<?php echo @$line1;?>" ><br><br></td>
</tr>
<tr>
  <td align="left">&nbsp;</td>
  <td align="left">Line 2</td>
  <td><input type="text" name="line2" id="line2" size="80"  value="<?php echo @$line2;?>" ><br><br></td>
</tr>
<tr>
  <td align="left">&nbsp;</td>
  <td align="left">Line 3</td>
  <td><input type="text" name="line3" id="line3" size="80"  value="<?php echo @$line3;?>" ><br><br></td>
</tr>
<tr>
  <td align="left">&nbsp;</td>
  <td align="left">Line 4</td>
  <td><input type="text" name="line4" id="line4" size="80"  value="<?php echo @$line4;?>" ><br><br></td>
</tr>
<tr>
  <td align="left">&nbsp;</td>
  <td align="left">Line 5</td>
  <td><input type="text" name="line5" id="line5" size="80"  value="<?php echo @$line5;?>" ><br><br></td>
</tr>

<tr>
  <td></td>
  <td></td>
  <td><input type="submit" name="save" value="Save"></td></tr>
</table>

</form>
</body>
</html>