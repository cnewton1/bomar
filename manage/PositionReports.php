<?php
include "../globals.php"; 
include "../pager.php";
include "../MyImageFunction.php";
include "../CategoryDropDownFunction.php";
include "../SubCategoryDropDownFunction.php";
include "adminheader.php";


//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------
adminTitle("List of Most Popular Products");


$limit = 20; 
// $result = mysql_query("select count(*) from product"); 
// $total = mysql_result($result, 0, 0);  
global $MAINURL;



?>

<style>
.nav_links ul
{
  float:left;
  list-style:none;
  width:33%;
  padding:0px;
}
.nav_links ul:last-child
{
  width:34%;
}
.nav_links ul li
{
  padding:5px;
  list-style:none;
  
}
.nav_links ul li a
{
  background-color: #0000d1;
    border-color: #cccccc #333333 #333333 #cccccc;
    border-style: solid;
    border-width: 1px;
    color: #fff;
    display: block;
    font: bold 14px Arial;
    margin-top: 5px;
    padding: 6px 6px;
    text-decoration: none;
  
}
</style>
<div align="center">
<center>
<p>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-width: 0" bordercolor="#111111" width="800" id="AutoNumber1">
<tr>
<td colspan="3"  height="29" style="border-style: none; border-width: medium">
<p align="center"><b>
<a href="http://www.westcarb.com/new/manage/editProduct.php?Catname=$PID&calltype=U&catiname=$cat_name" style="text-decoration: none">
<font face="Arial" size="6">Front End Modifications</font></a></b></td>
</tr>
<tr>
<td colspan="3" height="19" style="border-style: none; border-width: none">
<p align="center"><b><font face="Arial" size="2">Click on the buttons below to 
modify key sections of the site.</font></b></td>
</tr>

<tr>
<td width="30%">
<a href="EditOrderingText.php"><img border="0" src="../images/BEditOrderingProcess.png"></a><br>
<a href="EditWarrantyText.php"><img border="0" src="../images/BEPrecisionWarranty.png"></a><br>
<a href="EditPrivacyText.php"><img border="0" src="../images/BEPrivacySecurity.png"></a><br>
<a href="EditLegalText.php"><img border="0" src="../images/BELegalStuff.png"></a><br>
</td>

<td width="30%">
<div class="nav_links">
  <ul style="width:100%">
   <li><a href="reset_popular_items.php" onclick="return confirm('Are you sure to Reset Popular Items?');">Reset Popular Items</a></li>   
   <li><a href="admins.php" >Manage Admins</a></li>   
  </ul>
</div>
</td>

<td width="30%">
<a href="ManageMidText.php"><img border="0" src="../images/BCenter1.png"></a><br>
<a href="ManageBottomText.php"><img border="0" src="../images/BCenter2.png"></a><br><br>
<a href="ManageContactText.php"><img border="0" src="../images/BContactPageInfo.png"></a><br>
</td>

</tr>

<tr>
<td colspan="3">

<div class="nav_links">
       <ul>
         <li><a href="page_help.php">Help</a></li>
         <li><a href="page_about_us.php">About Us</a></li>
         <li><a href="page_terms_condition.php">Terms &amp; Conditions</a></li>
         <li><a href="page_privacy_policy.php">Privacy Policy</a></li>
         <li><a href="page_shipping_returns.php">Shipping &amp; Returns</a></li>
         <li><a href="page_w9_form.php" target="_blank">W-9 Form</a></li>
         <li><a href="page_product_recall.php">Product Recall/Safety Notices</a></li>
          <li><a href="page_careers.php" style="font-weight:bold;">Careers</a></li>
       </ul>
       <ul>
         <li><a href="page_your_account.php">Your Account</a></li>
         <li><a href="page_order_status.php">Order Status</a></li>
         <li><a href="page_retrieve_quote.php">Retrieve Quote</a></li>
         <li><a href="page_shopping_cart.php">Shopping Cart</a></li>
         <li><a href="page_shpping_list.php">Shopping Lists</a></li>
         <li><a href="page_apply_for_credit.php">Apply For Credit</a></li>
         <li><a href="page_request_a_catalog.php">Request A Catalog</a></li>
         <li><a href="page_catalog_unsubscribe.php">Catalog Unsubscribe</a></li>
         <li><a href="page_market_place.php">Marketplace Seller</a></li>
       </ul> 
       <ul>
           <li><a href="page_hurricane.php">Hurricane Protection</a></li>
           <!--<li ><a href="page_site_map.php">Site Map</a></li>-->
          <li><a href="page_inventory_clearance.php">Inventory Clearance</a></li>
          <li><a href="page_affiliation_program.php">Affiliate Program</a></li>
          <li><a href="page_press_release.php">Press Releases</a></li>
          <li><a href="page_cant_find_it.php">Can't Find It</a></li>
          <li><a href="page_rabate_center.php">Rebate Center</a></li>
          <li><a href="page_limited_warranty_info.php">Limited Warranty Information</a></li>
          <li><a href="page_extended_service_plan.php">Extended Service Plan</a></li>
          <li><a href="page_feedback.php" >Feedback</a></li>
       </ul>   
    </div>


</td>

</tr>

</table>
</center>
</div>



<?php


   include "adminfooter.php";
   print "</div>";
    
?>

