<?php
include "../globals.php"; 
include "../pager.php";
include "../MyImageFunction.php";
include "adminheader.php";

$error='';

if(isset($_GET['page']))
	$page=$_GET['page'];
	
function isURL($url) 
{
	if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url))
	{
		return true;
	}
	else
	{
		return false;
	}
}

if(isset($_POST['save']))
{
	if($_POST['link'] == '')
	{
		$error .= 'Enter Link.<br>';
	}
	else
	{
		if(!isURL($_POST['link']))
		{
			$error .= 'Invalid Link.<br>';	  
		}
		else
		{
		   $link = $_POST['link'];
		}
	}
	if($_FILES["image"]["name"])
	{
	  $orig_name=preg_replace('/\s+/','_',$_FILES["image"]["name"]);
	  $orig_name=time().$orig_name;
	  move_uploaded_file($_FILES["image"]["tmp_name"], "../Logo/" . $orig_name);
	}
	else
	{
		$error .= 'Upload Image.<br>';	
	}
	
   if($error == '')
   {
	 //echo "INSERT INTO `lefticons` (`link`, `image`) VALUES ('$link','$orig_name')";
	 $insert_query=mysql_query("INSERT INTO `lefticons` (`link`, `image`) VALUES ('$link','$orig_name')");
	 if($insert_query)
	  {
		 $msg="Inserted Successful";
		 header('Location:MostLeftReport.php?page='.$_POST['page']);
	  }
   }
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Add New Social Network Logo</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/sample.js" type="text/javascript"></script>
	<link href="ckeditor/sample.css" rel="stylesheet" type="text/css" />
</head>
<p><font size="+1"><b>Add New Social Network Logo</b></font></p>
<body>
<p><font color="#FF0000" size="+1"><?php echo $error;?></font></p>
<form name="frm_name" method="POST" action=""  enctype="multipart/form-data">

<input type="hidden" name="page" value="<?php echo $page; ?>">
<?php
print "<table>";
print "<tr><td>Link</td><td><input type=\"text\" name=\"link\" size=\"70\" ></td></tr>";
print "<tr><td>Image</td><td>
<input type=\"file\" name=\"image\" size=\"70\" >
</td></tr>";
print "<tr><td></td><td><input type=\"submit\" name=\"save\" value=\"Save\"></td></tr>";
print "</table>";
?>
</form>
</body>
</html>