<?php
include "../globals.php"; 
include "adminheader.php";

$login_email = $_GET['login_email'];
$password = $_GET['password'];
$status = $_GET['status'];
$calltype = $_GET['calltype'];

$company_name = $_GET['company_name'];
$company_abbr = $_GET['company_abbr'];
$primary_contact_name = $_GET['primary_contact_name'];
$primary_contact_email = $_GET['primary_contact_email'];
$primary_contact_phone = $_GET['primary_contact_phone'];
$primary_contact_fax = $_GET['primary_contact_fax'];
$alt_contact_name = $_GET['alt_contact_name'];
$alt_contact_email = $_GET['alt_contact_email'];
$alt_contact_phone = $_GET['alt_contact_phone'];
$alt_contact_fax = $_GET['alt_contact_fax'];


$error="";

if ($calltype=='A')
{
  adminTitle("Corporate Account Added");
  add($linkID,$login_email,$password,$status,$company_name,$company_abbr,$primary_contact_name,$primary_contact_email,$primary_contact_phone,$primary_contact_fax,$alt_contact_name,$alt_contact_email,$alt_contact_phone,$alt_contact_fax);
  
}
else
 if ($calltype=='U')
 {  
   adminTitle("Corporate Account Updated ");
   $id=$_GET['id'];
   update($linkID,$login_email,$password,$status,$id);
 }
 else
 {
   adminTitle("Corporate Account Deleted ");
   $id=$_GET['id'];
   delete($linkID,$id);
 }

 include "gfooter.php";
 exit;

//======================================================================  
function add($linkID,$login_email,$password,$status,$company_name,$company_abbr,$primary_contact_name,$primary_contact_email,$primary_contact_phone,$primary_contact_fax,$alt_contact_name,$alt_contact_email,$alt_contact_phone,$alt_contact_fax)
{
	if (empty($login_email))
	 {
		$error.="You did not enter username. <br>";
	 }
	 else
	 {
		 if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $login_email))
		  {
			$error.= "Enter A Valid User Name<br>";
		  }
	 }
	 
	 if (empty($password))
	 {
		$error.= "You did not enter password. <br>";
	 }
	 else
	 {
		if(strlen($password)<8)
		 {
			 $error.= "Enter atleast 8 characters password<br>";
		 } 
	 }
	 
	 
	 if ($primary_contact_email)
	{
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $primary_contact_email))
		{
		   $error.= "Enter A Valid Primary Contact Email<br>";
		}
	}
	
	if ($alt_contact_email)
	{
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $alt_contact_email))
		{
		   $error.= "Enter A Valid Alternate Contact Email<br>";
		}
	}
	
	
	if($error=="")
	{
		//echo "INSERT into corp_admin (login_email , password, type, status, company_name, company_abbr, primary_contact_name, primary_contact_email, primary_contact_phone, primary_contact_fax, alt_contact_name, alt_contact_email, alt_contact_phone, alt_contact_fax) VALUES ('$login_email','$password', 'corp_member', '$status', '$company_name', '$company_abbr', '$primary_contact_name', '$primary_contact_email', '$primary_contact_phone', '$primary_contact_fax', '$alt_contact_name', '$alt_contact_email', '$alt_contact_phone', '$alt_contact_fax')";
		
		
		$resultID = mysql_query("INSERT into corp_admin (login_email , password, type, status, company_name, company_abbr, primary_contact_name, primary_contact_email, primary_contact_phone, primary_contact_fax, alt_contact_name, alt_contact_email, alt_contact_phone, alt_contact_fax) VALUES ('$login_email','$password', 'corp_member', '$status', '$company_name', '$company_abbr', '$primary_contact_name', '$primary_contact_email', '$primary_contact_phone', '$primary_contact_fax', '$alt_contact_name', '$alt_contact_email', '$alt_contact_phone', '$alt_contact_fax')",$linkID);
		mysql_close($linkID);
	   
		if ($resultID)
		{
		
		  
		  //----------------------EMAIL SENDING-----------------
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From: '.$FROM;
			//$headers .= "\r\n cc: s.praachi@gmail.com \r\n\r\n";	
			//$headers .= "From: ".$FROM."\r\nReply-To: ".$FROM ;
			//$headers .= "CC:".$FROM;	
			
			$to= $login_email;	
			$subject = " You are registerd in Corporate Account Managenment, Sampalrx";	
			
			$email_message="<html><body>";
			$email_message.="Dear, ".$primary_contact_name." <BR>";
			$email_message.="We are pleased to inform you that your account has been created in:
			http://sampalrx.chrondev.com/corporate.<br>
			Your details are as follows:<BR><BR>";
			$email_message.="<strong>Company Name:  </strong>".$company_name."<BR>";
			$email_message.="<strong>Comapny Abbreviations:  </strong>".$company_abbr."<BR>";
			$email_message.="<strong>Primary Contact Name:  </strong>".$primary_contact_name."<BR>";
			$email_message.="<strong>Primary Contact Phone:  </strong>".$primary_contact_phone."<BR>";
			$email_message.="<strong>Primary Contact Email:  </strong>".$primary_contact_email."<BR>";
			$email_message.="<strong>Primary Contact Fax:  </strong>".$primary_contact_fax."<BR><BR>";
			
			$email_message.="<strong>Alternate Contact Name:  </strong>".$alt_contact_name."<BR>";
			$email_message.="<strong>Alternate Contact Phone:  </strong>".$alt_contact_phone."<BR>";
			$email_message.="<strong>Alternate Contact Email:  </strong>".$alt_contact_email."<BR>";
			$email_message.="<strong>Alternate Contact Fax:  </strong>".$alt_contact_fax."<BR><BR>";
			
			$email_message.="<strong>Login:  </strong>".$login_email."<BR><BR>";
			$email_message.="<strong>Password:  </strong>".$password."<BR><BR>";
	
			$email_message.="<BR>Thanking you and assuring you of our best services.<BR><BR>";
	
			$email_message.=$COMPANYNAME."<BR>";
			$email_message.=$FROM;
			//echo $to."<br><br>".$subject."<br><br>".$email_message."<br><br>".$headers;
			//exit();
			
			  
			
				if(mail($to, $subject, $email_message, $headers))
				{
					print "<B><font face=\"Arial\">'$login_email'</B> Added to Web Site Database <br>";
					print "<B><font face=\"Arial\">Mail send to: $login_email</B><br>";
				} 
				else 
				{
					print "<B><font face=\"Arial\">'$login_email'</B> Added to Web Site Database <br>";
					print "<B><font face=\"Arial\">Mail cannot be send to: $login_email</B> <br>";	
				}
				
			 //----------------------------------------------------
		  
		  
		  
		}
		else
		{
		   print "<B><font face=\"Arial\">Error '$login_email' not added to Web Site Database</B><br>";
		   print "Please enter another username and try again";
		}
	}
	else
	{
		print $error;
	}
  
  nextProduct();
 
}
 

//----------------------
// Delete Function
//----------------------
function delete($linkID,$id)
{
	print "Deleted";
	
	$resultID = mysql_query("delete from corp_admin where id = '$id' ",$linkID);
	
	mysql_close($linkID);
	print "<META http-equiv=\"refresh\" content=\"0; 
                                URL=listCorporateAccounts.php\">"; 
}


//----------------------
// Update Function
//----------------------
function update($linkID,$login_email,$password,$status,$id)
{
	if (empty($login_email))
	 {
		$error.= "You did not enter username. <br>";
	 }
	 else
	 {
		 if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $login_email))
		  {
			 $error.= "Enter A Valid User Name<br>";
		  }
	 }
	 
	 if (empty($password))
	 {
		$error.= "You did not enter password. <br>";
	 }
	 else
	 {
		if(strlen($password)<8)
		 {
			 $error.= "Enter atleast 8 characters password<br>";
		 } 
	 }
	 
	
	
	if($error=="")
	{
		$resultID = mysql_query("UPDATE corp_admin SET login_email = '$login_email', password = '$password', status = '$status' WHERE id = '$id'",$linkID);
		
		mysql_close($linkID);
		print "Record Updated";
		print "<META http-equiv=\"refresh\" content=\"0; 
								URL=listCorporateAccounts.php\">"; 
	}
	else
	{
		print $error;
	}
}

function nextProduct()
{
	 print "</font>";
	 print "<div style=\"background-color: #DDFFEE\">";
	 
	 print "<p><a href=\"addCorporateAccounts.php\">";
	
	 print "Add Corporate Accounts</a></font></b>";
	
	 print "<a href=\"corporatelist.php\">";
	
	 print "<img border=\"0\" src=\"../images/but_backcategorylist.gif\"></a></font></b>";
	 
	 print "</div>";

}   
   
?>
