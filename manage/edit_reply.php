<?php
include "../globals.php"; 
include "adminheader.php";
include "../pager.php";
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------

adminTitle("Manage Forum Topic Reply");

$page_name = 'Add Forum Topic';
$action = 'add';

$forum_id = $_REQUEST['forum_id'];
if($_GET['action']=='edit' && $_GET['id'] != '')
{
	$page_name = 'Edit Forum Topic Reply';
	$action = 'edit';

	$id = mysql_real_escape_string($_GET['id']);
	$query = mysql_query("SELECT * FROM `forum_reply`  WHERE `id` = '$id'");
	if(mysql_num_rows($query))
	{
		$row 		= mysql_fetch_array($query);
		$forum_id 	= $row['forum_id'];
		$comment	= $row['comment'];
	}
	else
	{
		header('Location: showtopic.php?id='.$forum_id);
		exit();
	}
}


if(isset($_POST['action']))
{
	$action 	= $_POST['action'];
	$id			= $_POST['id'];
	$forum_id = $_REQUEST['forum_id'];
	$error = array();
	
	$comment 	= trim($_POST['comment']);
	if($comment == '')
	{
		$error[] = 'Please Enter Comment.';
	}
	if(empty($error))
	{
		$comment 	= mysql_real_escape_string($comment);
		$update_query = mysql_query("UPDATE `forum_reply` SET `comment` = '$comment' WHERE `id` = '$id'");
		if($update_query)
		{
			$msg = 'Reply Successfully Updated..';
			header('Location: showtopic.php?id='.$forum_id);
		}	
		else
		{
			$error[] = mysql_error();
			
		}
	}
	
}
echo '<style>
#comment td {
	color:black;
}
div.error{
	padding:2px 4px;
    margin:0px;
    border:solid 1px #FBD3C6;
    background:#FDE4E1;
    color:#CB4721;
    font-family:Arial, Helvetica, sans-serif;
    font-size:14px;
    font-weight:bold;
    text-align:center; 
}
div.success{
	padding:2px 4px;
	margin:0px;
	border:solid 1px #C0F0B9;
	background:#D5FFC6;
	color:#48A41C;
	font-family:Arial, Helvetica, sans-serif; font-size:14px;
	font-weight:bold;
	text-align:center; 
}
</style>';

echo '<div style="width: 900px;">';
echo '<form method="post" action="" target="_self">
<table width="" border="0" cellpadding="5" cellspacing="5" id="comment">
<tr>
<td colspan="2" align="center"><font size="+2">'.$page_name.'</font></td>
</tr>';
if(isset($msg))
{
	echo '<tr>
	<td colspan="2">
	<div class="success">'.$msg.'
	
	</div></td>
  </tr>';
}
if(!empty($error))
{
	echo '<tr>
	<td colspan="2">
	<div class="error">'.implode('<br>',$error).'
	
	</div></td>
  </tr>';
}
echo '
<tr>
<td width="100" align="right" valign="top"><strong>Comment:</strong></td>
<td><textarea name="comment" style="width:600px; height:300px">'.stripslashes(htmlspecialchars($comment)).'</textarea></td>
</tr>
<tr>
<td >&nbsp;</td>
<td align="right" style="padding-right:20px;">
<input type="hidden" name="action" value="'.$action.'">
<input type="hidden" name="forum_id" value="'.$forum_id.'">
<input type="hidden" name="id" value="'.$id.'">
<input type="submit" name="submit" value="Submit" style="font-size:14px; font-weight:bold">

</td>
</tr>

</table>
</form>';

echo '</div>';

?>

