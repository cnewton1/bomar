<?php
include "../users.php";
include "../connect.php";
           
include "../globals.php"; 
include "CategoryBoxFunction.php";
include "SubCategoryBoxFunction.php";
include "makeIconFunction.php";
include "adminheader.php";
include "../MyImageFunction.php";    

function filter_value($val)
{
	$val = str_replace(",",";",$val);
	$val = preg_replace("/\s+/", " ", $val);
	return trim($val);
}


function GetTableData($PID)
{
   $sql="SELECT table_data FROM tabledata where Product_ID=$PID";
   $result=mysql_query($sql); 
   while($row=mysql_fetch_assoc($result))
   {
      return $row['table_data'];
   }
    return '';
}

$filename = "TableProducts.csv";

$PATH = $_SERVER['DOCUMENT_ROOT']; #get server path
$save_path=$PATH; // . $filename; #define target path


$save_path = $filename;
$fp = fopen($save_path, "w"); #open for writing
//=====================================
// CREATE FIRST CSV ROW
$columns_description = array(
	'ProductID'=>'ID',
	'filename'=>'FileName',
	'tablename'=>'TableName'
	);

	fputcsv($fp, $columns_description);
	

//===============================
// Get DATABASE
//===============================

$sql="SELECT id, filename, tablename, price FROM product";
$result=mysql_query($sql); 
while($row=mysql_fetch_assoc($result))
{
      if ($row['tablename'] != '')
      {
	    foreach($row as $field=>$val)
	    { 
	     $row[$field] = filter_value($val);	
         $TableData=GetTableData($row[id]);
         $row['price']=$TableData;     
	    }                 

	    fputcsv($fp, $row);
      }
}

fclose($fp); #close the file
//===============================  
//EXIT DB  
//===============================



//=============================================
// Create the download link for the file
//============================================
print "<p><font face=\"Arial\">";
print "<b><a href=\"TableProducts.csv\">";
print "Click Here to DOWNLOAD TABLE CSV</a></b></font></p>";

include "adminfooter.php";

?>
