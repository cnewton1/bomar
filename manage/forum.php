<?php
include "../globals.php"; 
include "adminheader.php";
echo '<script src="forum.js" language="javascript"></script>';
include "pager.php";
adminTitle("Manage Forum");

?>
<?php
$submit_btn_name = 'Submit';
$act_as_forum = '1';
if($_GET['op'] == 'new' && $_GET['id'] != '')
{
	$parent_id = $_GET['id'];
	
	
}

if($_GET['op'] == 'edit' && $_GET['id'] != '')
{
	$forum_id = $_GET['id'];
	$forum_result = mysql_fetch_array(mysql_query("SELECT * FROM forum_forum WHERE id = '$forum_id'"));
	$forum_title = $forum_result['title'];
	$forum_description = $forum_result['description'];
	$parent_id = $forum_result['parentid'];
	$act_as_forum = $forum_result['actasforum'];
	
	$submit_btn_name = 'Save';
	
}

if(isset($_POST['Save']))
{
	$forum_title			= str_replace(":","&#58",trim($_POST['forum_title']));
	$forum_title			= str_replace("'","&#39",$forum_title);
	$forum_title			= htmlentities($forum_title); // Very Important Security Fix
	$act_as_forum			= $_POST['act_as_forum'];
	$parent_id 				= $_POST['parent_forum'];
	
	$forum_description 	= trim($_POST['forum_description']);
	$forum_description	= str_replace(":","&#58",$forum_description);
	$forum_description	= str_replace("'","&#39",$forum_description);
	$forum_description	= htmlentities($forum_description); // Very Important Security Fix
	
	$error_string="";

	if($forum_title=="" )
	{
		$error_string .="Forum Title is a required field.";
	}
	elseif(strlen($forum_title) < 2)
	{
		$error_string .="Forum Title should be greater than or equal to two characters.";
	}
	
	if($error_string!="")
	{
		$message_print = '';
		$message_print .= "<br> <b> - </b> $error_string <br><br>";
		?>
        <div class="info"><img src="images/error.gif" alt="Not Done" style="vertical-align: middle;"> <strong>Not Done:</strong> <font color="#ff3300"> 
			<strong>Saving Forum Failed</strong><br>
			<?php echo $message_print;?>
		   </font></div>
        <?php
	}
	else
	{
		$update_query =  "UPDATE `forum_forum` SET `title` = '$forum_title',`description` = '$forum_description',`parentid` = '$parent_id',`actasforum` = '$act_as_forum' WHERE `id` ='$forum_id'";
		if(@mysql_query($update_query))
		{
			$_SESSION['forum_message_display'] = "<div class=\"info\"><img style=\"vertical-align: middle;\" alt=\"Forum updated Successfully\" src=\"images/info-blue.png\"> <strong>Forum Updated:</strong> Forum ' <strong>$forum_title</strong> ' is successfully updated. </div><br><br>";
			header('Location: manageforum.php?id='.$forum_id);
			exit();
		}// End of Inner if block (Query Check)
		else
		{
			echo mysql_error();
			
		}
	}// End of Outer IF Block (Check Control is coming from which page EDIT / ADD ) 
}



if(isset($_POST['Submit']))
{
	$forum_title		= str_replace(":","&#58",trim($_POST['forum_title']));
	$forum_title		= str_replace("'","&#39",$forum_title);
	$forum_title		= htmlentities($forum_title); // Very Important Security Fix
	$act_as_forum		= $_POST['act_as_forum'];
	$parent_id 			= $_POST['parent_forum'];
	
	$forum_description 	= trim($_POST['forum_description']);
	$forum_description	= str_replace("'","&#39",$forum_description);
	$forum_description	= htmlentities($forum_description); // Very Important Security Fix
	
	$error_string="";

	if($forum_title=="" )
	{
		$error_string .="Forum Title is a required field.";
	}
	elseif(strlen($forum_title) < 2)
	{
		$error_string .="Forum Title should be greater than or equal to two characters.";
	}
	
	if($error_string!="")
	{
		$message_print = '';
		$message_print .= "<br> <b> - </b> $error_string <br><br>";
		?>
        <div class="info"><img src="images/error.gif" alt="Not Done" style="vertical-align: middle;"> <strong>Not Done:</strong> <font color="#ff3300"> 
			<strong>Saving Forum Failed</strong><br>
			<?php echo $message_print;?>
		   </font></div>
        <?php
	}
	else
	{
		$dateline = time();
		$insert_query =  "INSERT INTO `forum_forum` (`title`, `description`, `parentid`, `actasforum`, `createddate`) VALUES ('$forum_title', '$forum_description', '$parent_id', '$act_as_forum', '$dateline')";
		if(@mysql_query($insert_query))
		{
			$_SESSION['forum_message_display'] = "<div class=\"info\"><img style=\"vertical-align: middle;\" alt=\"Forum Created Successfully\" src=\"images/info-blue.png\"> <strong>Forum Created:</strong> Forum ' <strong>$forum_title</strong> ' is successfully added into Forum list. </div><br><br>";
			header('Location: manageforum.php?id='.mysql_insert_id());
			exit();
		}// End of Inner if block (Query Check)
	}// End of Outer IF Block (Check Control is coming from which page EDIT / ADD ) 
}
?>



<?php
print "<div style=\"position: relative; top: 30px; left: 10px; width: 700px;>\"";



$Catname = $HTTP_POST_VARS['Catname'];
$Catstatus = $HTTP_POST_VARS['Catstatus'];
$forum_id = '0';
?>
<?php
function list_forums($cat_id)
{
	
	$sql = "SELECT * FROM forum_forum ORDER BY title";
	$result = mysql_query($sql);
	$result or die('<option>' . mysql_error() .'</option>'); // debug
	while ($row = mysql_fetch_assoc($result))
	{
		$names[$row['id']] = $row['title'];
		$cats[$row['id']] = $row['parentid'];
	}
	display_options($cat_id, hierarchize($cats, 0), $names);
}

function hierarchize(&$cats, $parent)
{
	$subs = array_keys($cats, $parent);
	$tree = array();
	foreach ($subs as $sub)
	{
		$tree[$sub] = hierarchize($cats, $sub);
	}
	return count($tree) ? $tree : $parent;
}

function display_options($cat_id, &$tree, &$names, $nest = 0)
{
	
	foreach ($tree as $key => $branch)
	{
		$indent = $nest ? str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $nest) . '- ' : '';
		$x=0;
		if(is_array($cat_id))
		{
			foreach($cat_id as $category_id)
			{
				if($category_id==$key)
				{
					echo "<option value=\"$key\" selected>$indent{$names[$key]}</option>\n";
					$x=1;
				}				
			}						
		}
		if ($x==0)
		{
			$select_string	=	($cat_id==$key)?"selected":"";		
			echo "<option value=\"$key\" $select_string>$indent{$names[$key]}</option>\n";
		}
		if (is_array($branch))
		{
			display_options($cat_id, $branch, $names, $nest + 1);
		}
	}
}



?>

<div id="tabs">
<ul>
    <li><a href="manageforum.php"><span><img src="images/category.png" alt="" style="vertical-align: middle;"> All Forum</span></a></li>
    <li <?php if($submit_btn_name == 'Submit') echo 'id="current"';?>><a href="forum.php?op=new"><span><img src="images/category-add.png" alt="" style="vertical-align: middle;"> Add New Forum</span></a></li>
    <?php if($submit_btn_name == 'Save'){?>
    <li id="current"><a href="#focus"><span><img style="vertical-align: middle;" alt="Editing Forum" src="images/category_edit.png"> Editing Forum</span></a></li>
    <?php 	}?>
</ul>
</div>

 <form name="frmcategory" method="post" action="" onsubmit="return CheckForumForm();">
 <script type="text/javascript">
				//<![CDATA[
				function CheckForumForm()
				{
					var f = document.frmcategory;
					if(f.forum_title.value == '')
					{
						alert('Please specify the Forum Title');
						f.forum_title.focus(); return false;
					}
					else if(f.forum_title.value.length > 100)
					{
						alert('Forum Title must be within 100-characters');
						f.forum_title.focus(); return false;
					}					
				}
				//]]>
			</script>
<table class="formtable" summary="Create Category" border="0" cellpadding="0" cellspacing="0" width="100%">
			  <!-- Table body -->
				<tbody>
                <tr class="odd"> 
                    <td colspan="2" align="right"><strong>Important Note</strong>: Fields marked with <img src="images/required.gif" alt="Required" style="vertical-align: middle;" height="16" width="16"> are required to be filled up.</td>
                </tr>
                <tr> 
                    <td class="column1"><span id="lockimage1"></span> <strong>Forum Title</strong></td>
                    <td align="left"> 
                       	<input id="cname" name="forum_title" size="40" maxlength="100" type="text" value="<?php echo $forum_title;?>">
                    	<img src="images/required.gif" alt="Required" style="vertical-align: middle;" height="16" width="16">                    </td>
                </tr>
                <tr> 
                    <td class="column1">Act as Forum</td>
                    <td align="left">
                        <input name="act_as_forum" type="radio" value="1" <?php if($act_as_forum == '1') echo 'checked';?>> Yes
                        <input name="act_as_forum" value="0" type="radio" <?php if($act_as_forum == '0') echo 'checked';?>> No
                  </td>
                </tr>			  
                <tr> 
                    <td class="column1"><span id="lockimage2"></span> Parent Forum</td>
                    <td align="left">
                       <span id="publiccats">
                            <select name="parent_forum" id="category">
                            <option value="0" selected="selected">-- No Parent --</option>
                             <?php list_forums($parent_id);?>   
                            </select>
                       </span>

						
                    </td>
                </tr>
                <tr> 
                    <td class="column1" valign="top">Forum Description</td>
                    <td align="left" valign="top">
                        <textarea name="forum_description" cols="40" rows="5" onkeyup="return ismaxlength(this, 250);" onblur="return ismaxlength(this, 250);" onmouseout="return ismaxlength(this, 250);" onmouseup="return ismaxlength(this, 250);"><?php echo $forum_description;?></textarea>
                        (max. 250 Chars)
                    </td>
                </tr>
                <tr> 
                    <td>&nbsp;</td>
                    <td>
                    	<input class="groovybutton" name="<?php echo $submit_btn_name;?>" value="Save Forum" type="submit">
			  			<input class="groovybutton" onclick="javascript:confirm_cancel('manageforum.php');" value="Cancel" type="button">
                    </td>
                </tr>
				</tbody>
			</table>

</form>

</div>