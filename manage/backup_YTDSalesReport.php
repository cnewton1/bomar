<?php
ob_start();
session_start();

if(!isset($_SESSION['manager_id']))
	{
	header("Location:loginManager.php");
	exit;
	}
         include "../globals.php"; 
         include "../globalcss.php"; 
         include "../CSILib/CSIYTDSalesReportClass.php";

         print "<head><body>";
        //=======================================
        // MINIMUM DEFINITION for basic report  =
        //=======================================
        $Report=new CSIOrdersReport;
        $Report->tablename="orderhistory";  // define which table to work with
        $Report->columns="receive_date,unit_price,quantity";

	$Report->setCodition("order by receive_date");

        $Report->setReportTitle("YTD Sales Report");
        
        // Finally Display the report with page and limited rows  
        $Report->displayTable();

        print "</body></head>";
        
        exit;
?> 