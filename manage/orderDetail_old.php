<?php
// Include MySQL class
// Copyright (c) 2002-2007 Chronicles Systems, Inc.
include "../globals.php"; 
include "adminheader.php";
include '../utility/Shopping_Cart.php'; //Inluding Sooping Cart Function file
include "../ShopCartTempFunctions.php";
//--------------------------------------------------
// General list of a table
// in this cast the category
//--------------------------------------------------

adminTitle("Customer Order Detail");

include "../users.php";

include "../MyImageFunction.php";
require_once('../mysql.class.php');

// Process actions

$action = $_GET['action'];
$ORDERNUMBER = $_GET['OrderNumber'];
$page = $_GET['page'];
$searchText = $_GET['searchText'];
if($_GET['source']== 'completedorder')
{
	$sourcepage = 'mlistCompletedOrders.php';
}
else
{
	$sourcepage = 'mlistOrders.php';
}

?>
<script type="text/javascript">

function roundNumber(number,decimals) {
  var newString;// The new rounded number
  decimals = Number(decimals);
  if (decimals < 1) {
    newString = (Math.round(number)).toString();
  } else {
    var numString = number.toString();
    if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
      numString += ".";// give it one at the end
    }
    var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
    var d1 = Number(numString.substring(cutoff,cutoff+1));// The value of the last decimal place that we'll end up with
    var d2 = Number(numString.substring(cutoff+1,cutoff+2));// The next decimal, after the last one we want
    if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
      if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
        while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
          if (d1 != ".") {
            cutoff -= 1;
            d1 = Number(numString.substring(cutoff,cutoff+1));
          } else {
            cutoff -= 1;
          }
        }
      }
      d1 += 1;
    } 
    if (d1 == 10) {
      numString = numString.substring(0, numString.lastIndexOf("."));
      var roundedNum = Number(numString) + 1;
      newString = roundedNum.toString() + '.';
    } else {
      newString = numString.substring(0,cutoff) + d1.toString();
    }
  }
  if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
    newString += ".";
  }
  var decs = (newString.substring(newString.lastIndexOf(".")+1)).length;
  for(var i=0;i<decimals-decs;i++) newString += "0";
  //var newNumber = Number(newString);// make it a number if you like
  return newString; // Output the result to the form field (change for your purposes)
}


function showTax()
{
	if(document.getElementById('applySalesTax').checked == true)
	{
		var sales_tax = document.getElementById('sales_tax').value;
		document.getElementById('showSalesTax').innerHTML = '$'+sales_tax;
	}
	else
	{
		var sales_tax = '$0.00';
		document.getElementById('showSalesTax').innerHTML = sales_tax;
	}
	display_total();
}


function display_total()
{
	var grandtotal = 0 ;
	var shipping = document.getElementById('shipping').value;
	shipping = (shipping).replace(",","");
	if(document.getElementById('applySalesTax').checked == true)
	{
		var sales_tax = document.getElementById('sales_tax').value;
		sales_tax = (sales_tax).replace(",","");
	}
	else
	{
		sales_tax = 0.00;
	}
	
	
	var total_price = document.getElementById('total_price').value;
	total_price = (total_price).replace(",","");
	
	shipping = shipping*1;
	total_price = total_price*1;
	sales_tax = sales_tax*1;
	
	grandtotal = (shipping) + (total_price) + (sales_tax);
	grandtotal = roundNumber(grandtotal,2);
	//alert(grandtotal);
	document.getElementById('grand_total').innerHTML = grandtotal;
	
}
</script>

<?php
function extract_emails_from($string){
  preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);
  return $matches[0];
}



function BuilddisplayCustomerInfo( $first_name,$last_name,$email,$phone,$company, $address1, $address2 , $city , $state, $zip, $saddress1, $saddress2 ,$scity ,$sstate ,$szip ,$scountry, $shipping_address_info)
{
	
/* $FFName = $first_name;
 $FLastName = $last_name;
 $FPhone = $phone;
 $FEmail = $email;*/


$vout[]= ' <br><table width="900px" border="0" cellpadding="0" cellspacing="0">
        <tr>
       	 	<td width="300px;" valign="top">';
                $vout[]= "<h2>ON-LINE PAYMENT INVOICE </h2>";
				$vout[]= '<font face="Arial" size="2"><b>First Name:</b>&nbsp;&nbsp;' . $first_name;
                $vout[].= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>Last Name:&nbsp;&nbsp;</b>' . $last_name;
                $vout[]= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>Email:</b>&nbsp;&nbsp;' . $email;
                $vout[]= '<input type="hidden" name="UserEmail" value="'. $email .'"></font><br>';
                $vout[]= '</font><font face="Arial" size="2"><b>Phone:&nbsp;&nbsp;</b>' . $phone;
                $vout[]= '</font>';
        $vout[]= ' </td>
            <td width="300px;" valign="top">';
            	$vout[]= '<h2>Customer Address</h2>';
                // $content .= '<font face="Arial" size="2"><b>Company:&nbsp;&nbsp;</b>' . $company;
                //$content .= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>Address1:&nbsp;&nbsp;</b>' . $address1;
                $vout[]= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>Address2:&nbsp;&nbsp;</b>' . $address2;
                $vout[]= '</font><br>';
				$vout[]= '<font face="Arial" size="2"><b>City:&nbsp;&nbsp;</b>' . $city;
                $vout[].= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>State:&nbsp;&nbsp;</b>' . $state;
                $vout[]= '</font><br>';
				$vout[]= '<font face="Arial" size="2"><b>Country:&nbsp;&nbsp;</b>' . $scountry;
                $vout[]= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>ZIP:&nbsp;&nbsp;</b>' . $zip;
                $vout[]= '</font><br>';
               
      $vout[]= '  </td>
            <td width="300px;" valign="top">';
            	 $vout[]= '<h2>Shipping Address</h2>';
                // $content .= '<font face="Arial" size="2"><b>Company:&nbsp;&nbsp;</b>' . $company;
                //$content .= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>Address1:&nbsp;&nbsp;</b>' .$shipping_address_info['s_address1'];
                $vout[]= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>Address2:&nbsp;&nbsp;</b>' . $shipping_address_info['s_address2'];
                $vout[]= '</font><br>';
				$vout[]= '<font face="Arial" size="2"><b>City:&nbsp;&nbsp;</b>' . $shipping_address_info['s_city'];
                $vout[].= '</font><br>';
                $vout[]= '<font face="Arial" size="2"><b>State:&nbsp;&nbsp;</b>' . $shipping_address_info['s_state'];
                $vout[]= '</font><br>';
				$vout[]= '<font face="Arial" size="2"><b>ZIP:&nbsp;&nbsp;</b>' . $shipping_address_info['s_zip'];
                $vout[]= '</font><br>';
				$vout[]= '<font face="Arial" size="2"><b>Phone:&nbsp;&nbsp;</b>' . $shipping_address_info['s_phone'];
                $vout[]= '</font><br>';
               
        $vout[]= '    </td>
        </tr>
    </table>';

/*$vout[]= '
  <div style="width:50%; float:left;">';
 
  $vout[]="<h2>ON-LINE PAYMENT INVOICE</h2>";
  // $vout[]= '<font face=\"Arial\" size=\"3\">' . $COMPANYNAME;
  // $vout[]= '<br>';
  $vout[]= '<font face="Arial" size="2"><b>First Name:</b>&nbsp;&nbsp;' . $FFName;
  $vout[]= '</font><br>';
  $vout[]= '<font face="Arial" size="2"><b>Last Name:&nbsp;&nbsp;</b>' . $FLastName;
  $vout[]= '</font><br>';
  $vout[]= '<font face="Arial" size="2"><b>Email:</b>&nbsp;&nbsp;' . $FEmail;
  $vout[]= '</font><input type="hidden" name="UserEmail" value="'. $FEmail .'">';
  $vout[]= '<br>';
  $vout[]= '</font><font face="Arial" size="2"><b>Phone:&nbsp;&nbsp;</b>' . $FPhone;
  $vout[]= '</font></div>';
  
  $vout[]= '<div style="width:50%  float:left; ">';
  $vout[]= '<h2>Customer Address</h2>';
  $vout[]= '<font face="Arial" size="2"><b>Company:&nbsp;&nbsp;</b>' . $company;
  $vout[]= '</font><br>';
  $vout[]= '<font face="Arial" size="2"><b>Address1:&nbsp;&nbsp;</b>' . $address1;
  $vout[]= '</font><br>';
  $vout[]= '<font face="Arial" size="2"><b>Address2:&nbsp;&nbsp;</b>' . $address2;
  $vout[]= '</font><br>';
  $vout[]= '<font face="Arial" size="2"><b>State:&nbsp;&nbsp;</b>' . $state;
  $vout[]= '</font><br>';
  $vout[]= '<font face="Arial" size="2"><b>ZIP:&nbsp;&nbsp;</b>' . $zip;
   $vout[]= '</font></div>';*/

	return join('',$vout);
	

}



function showOrderDetail($OrderId, $notes='', $status='')
{
	$return 				= array();
	$select_order_details 	= mysql_fetch_array(mysql_query("SELECT * FROM `orderhistory` WHERE `order_number`= '$OrderId'"));
	$email 					= $select_order_details['email'];
	$shipping_options 		= $select_order_details['shipping_options'];
	
	$select_taxrate = mysql_fetch_array(mysql_query('SELECT * FROM `tax_rate` WHERE `name`="salestax"'));
	$sales_tax_rate = $select_taxrate['rate'];
	
	if($select_order_details['shipping'] !== '')
	{
		$shipping = $select_order_details['shipping'];
	}
	else
	{
		$shipping = '';
	}
	$sales_tax = $select_order_details['sales_tax'];
	
	
	$select_shipping_option = mysql_query("SELECT `detail` FROM `shipping_options` WHERE `id`='$shipping_options' ");
	if(mysql_num_rows($select_shipping_option))
	{
		$shipping_options_result = mysql_fetch_array($select_shipping_option);
		$shipping_details = $shipping_options_result['detail'];
	}
	else
	{
		$shipping_details = '<em>Not Selected</em>';
	}
	
	$cart_query = mysql_query("SELECT * FROM `item_order` WHERE `order_id` = '$OrderId'");
	if(mysql_num_rows($cart_query))
	{
		$return[] = '<table width="900" cellspacing="0" cellpadding="2" bordercolor="#FF0000" border="1" id="AutoNumber1" style="border-collapse: collapse"  fgcolor="#FFFFFF">
									<tbody>
										<tr>';
										$return[] = '<td width="26%" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Description</font>
												</b><input type="hidden" name="OrderId" value="'.$OrderId.'">
											</td>
											
											<td width="26%" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Item No.</font>
												</b>
											</td>
											<td width="26%" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Options</font>
												</b>
											</td>
											<td width="26%" bgcolor="#000000">
												<b>
													<font size="2" color="#FFFFFF">Price</font>
												</b>
											</td>
											
											<td width="10%" bgcolor="#000000">
												<b>
													<font size="1" color="#FFFFFF">Qty</font>
												</b>
											</td>
											
											<td width="10%" bgcolor="#000000">
												<b>
													<font size="1" color="#FFFFFF">EXT PRICE</font>
												</b>
											</td>
											
										</tr>';
		
		while($row = mysql_fetch_array($cart_query))
		{
			$cartid = $row['id']; 
			$item_number = $row['product_id'];
			$description = $row['description'];
			$price = $row['unit_price'];
			$options = $row['options'];
			$qty = $row['qty'];
			//$allId = $item_number . ',';
			
			$return[] = '<tr>';
			$return[] = ' <td>
                            <font size="2" color="#000000">
                                <b>'.$description.'
                                </b>
								 <input type="hidden" value="'.$cartid.'" name="cartid[]">
                            </font>
                        </td>
                        <td>
                            <font size="2" color="#000000">
                                '.$item_number.'
                            </font>
                        </td>
                       
                        <td>
                            <font size="2" color="#000000">'.$options.'</font>
                        </td>
						
						<td align="right">
                            <font size="2" color="#000000">$'.$price.'</font>
                        </td>
						
                        <td>
                            <font size="2" color="#000000">';
							
								if($status == 'clean')
								{
									$return[] = $qty;
								}
								else
								{
									$return[] = '<input type="text" name="qty['.$cartid.']" value="'.$qty.'" size="4">';
								}
								
							
							$total_item_row[$cartid] = $qty * trim(str_replace(array('$', ','), array('', ''), $price));
							$qty_item_row[$cartid]= $qty;
							
                           $return[] = ' </font>
                        </td>
						
						 <td align="right">
                            <font size="2" color="#000000">$'.number_format($total_item_row[$cartid],2).'</font>
                        </td>
						
                    </tr>';
			
		}
		//$allId = substr($allId,0,-1);
		$colspan = '4';
		$subtotal= number_format(array_sum($total_item_row), 2);
		
		$return[] ='<tr>
						<td  colspan="'.$colspan.'" align="right"><strong>Subtotal</strong></td>
						<td  align="center">'.array_sum($qty_item_row).'</td>
						<td  align="right">$'.number_format(array_sum($total_item_row), 2).'</td>
					</tr>';
		
		if($sales_tax == '0.00')
		{
			$sales_tax= number_format((($sales_tax_rate/100) * $subtotal), 2);
			$applySalesTaxCheckedStr = '';
			$salesTaxForGrandTotal = '0.00';
		}
		else
		{
			$applySalesTaxCheckedStr='checked';
			$salesTaxForGrandTotal = number_format($sales_tax, 2);
		}
		
		
		
		$return[] ='<tr>
						<td  colspan="'.($colspan-1).'"  align="right">';
						
						if($status == 'clean')
						{
							$return[] = '';
						}
						else
						{
							$return[] = '<strong>Apply Sales Tax:</strong> <input type="checkbox" name="applySalesTax" id="applySalesTax" onClick="javascript:showTax();" '. $applySalesTaxCheckedStr .'>';
						}
					$return[] = '	</td>
									<td  align="right"><strong>Sales Tax</strong></td>
									<td  align="center"><input type="hidden"  id="sales_tax" name="sales_tax" value="'.$sales_tax.'" ></td>
									<td  align="right"><span id="showSalesTax">$'.$salesTaxForGrandTotal.'</span></td>
								</tr>';	
								
					
		$return[] ='<tr>
						<td  colspan="'.($colspan-1).'"  align="left"><strong>Buyer Selected Shipping Type is: '.$shipping_details.'</strong></td>
						<td  align="right"><strong>Shipping</strong></td>
						<td  align="center"></td>
						<td  align="right">
							<input type="hidden"  id="total_price" name="total_price" value="'.number_format(array_sum($total_item_row), 2).'" >';
							if($status == 'clean')
							{
								$return[] = $shipping;
							}
							else
							{
								$return[] = '<input type="text"  id="shipping" name="shipping" value="'.$shipping.'" size="4" onBlur="javascript:display_total();">';
							}
							
					$return[] ='	</td>
								</tr>';
					
					if($shipping != '')
					{
						$grand_total = number_format(($shipping +  array_sum($total_item_row) + $salesTaxForGrandTotal), 2);
					}
					else
					{
						$grand_total = number_format((array_sum($total_item_row) + $salesTaxForGrandTotal) , 2);
					}
					
									
		$return[] ='<tr>
						<td  colspan="'.$colspan.'" align="right"><strong>Grand Total</strong></td>
						<td  align="center"><input type="hidden" name="grand_total_temp" value="'.$grand_total.'"></td>
						<td  align="right">$<span id="grand_total" >'.$grand_total.'</span></td>
					</tr>';			
		
		
		$return[] = '</tbody>
				  </table>';
		if($status != 'clean')
		{
			$return[] = '<br><div align="center"  style="width:900;">
						<input type="submit" name="emailCopy"  value="Email Copy">
					</div>';
					
			$return[] = '<br><div align="right"  style="width:900;">
						<input type="submit" name="update"  value="Update Cart Quantity">
					</div>';
			
		}
		
				  
		$return[] = '<br><br>
					  <div align="left"  style="width:900;">
						  <strong>NOTES:</strong><br>';
						  
						  if($status == 'clean')
							{
								$return[] = nl2br($notes);
							}
							else
							{
								$return[] ='<textarea name="notes" rows="5" cols="100">'.$notes.'</textarea>
					 						 </div>';
							}
							
		if($status != 'clean')
		{
			$return[] = '<br><br>
					  <div align="right"  style="width:900;">
						  <input type="submit" name="submit" value="Save Changes">
					  </div>';
		}
	 
	}
	else
	{
		$return[] =  'Shopping Cart is empty.';
	}	
	return implode('',$return);
}


//....................UPDATE NOTES AND ORDER STATUS................
if(isset($_POST['submit']))
{
	$updateStr= '';
	$ID = $_POST['ID'];
	$page = $_POST['page'];
	$searchText = $_POST['searchText'];
	$shipping = $_POST['shipping'];
	
	$UserEmail 	= $_POST['UserEmail'];
	
	$order_shipping_address_id	= $_POST['order_shipping_address_id'];
	$shipping_address_info 		= shipping_address_info($order_shipping_address_id, $UserEmail);
	
	
	$updateStr .= "`shipping` = '$shipping', ";
	
	if($_POST['applySalesTax'])
	{
		$sales_tax = $_POST['sales_tax'];
	}
	else
	{
		$sales_tax = '0.00';
	}
	$updateStr .= "`sales_tax` = '$sales_tax', ";
	
	
	$notes = $_POST['notes'];
	$updateStr .= "`notes` = '$notes', ";
	
	$orderStatus = $_POST['orderStatus'];
	$updateStr .= "`orderstatus` = '$orderStatus'";
	
	$update_orderhistory_query = mysql_query("UPDATE `orderhistory` SET $updateStr  WHERE `order_number`= '$ID'");
	if($update_orderhistory_query)
	{
		header('LOCATION:mlistOrders.php?msg=updated&page='.$page);
	}
	else
	{
		echo '<font color=\"red\" size=\"+2\"> Error:'. mysql_error().'</font><br><br><br><br>';
	}
}
/////////////////////////////////////////////////////////////////////////



//.............UPDATE CART.................
if(isset($_POST['update']))
{
	//-----------------update taxes-----------------
	$updateStr= '';
	$ID = $_POST['ID'];
	$page = $_POST['page'];
	$searchText = $_POST['searchText'];
	$shipping = $_POST['shipping'];
	
	$UserEmail 	= $_POST['UserEmail'];
	
	$order_shipping_address_id	= $_POST['order_shipping_address_id'];
	$shipping_address_info 		= shipping_address_info($order_shipping_address_id, $UserEmail);
	
	
	$updateStr .= "`shipping` = '$shipping', ";
	
	if($_POST['applySalesTax'])
	{
		$sales_tax = $_POST['sales_tax'];
	}
	else
	{
		$sales_tax = '0.00';
	}
	$updateStr .= "`sales_tax` = '$sales_tax', ";
	
	
	$notes = $_POST['notes'];
	$updateStr .= "`notes` = '$notes', ";
	
	$orderStatus = $_POST['orderStatus'];
	$updateStr .= "`orderstatus` = '$orderStatus'";
	
	$update_orderhistory_query = mysql_query("UPDATE `orderhistory` SET $updateStr  WHERE `order_number`= '$ID'");
	
	//update cart
	$order_id 	= $_POST['OrderId'];
	$cartid 	= $_POST['cartid'];
	$qty 		= $_POST['qty'];
	$shipping 	= $_POST['shipping'];
	
	foreach ($cartid as $new_cartid)
	{
		$new_qty = $qty[$new_cartid];
		$update_item_query = mysql_query("UPDATE `item_order` SET `qty` = '$new_qty' WHERE `id` = '$new_cartid' AND `order_id` = '$order_id'");
		
	}
	mysql_query("UPDATE `orderhistory` SET `shipping` = '$shipping' WHERE  `order_number` = '$order_id'");

}
///////////////////////////////////////////////////////////////////


function writeShoppingCart() {
	$cart = $_SESSION['cart'];
	if (!$cart) {
		return '<p>No Items exist in this cart</p>';
	} else {
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return '<p><font face="Arial" size=\"3\"><b>There are '.count($items).' item'.$s.' in this customers shopping cart</p></font>';
	}
	
	// ==============================================================
	// SAVE Shopping Cart OrderNumber and DateSet to ordernum Table
	//===============================================================
}





//...............  EMAIL  ................//
if(isset($_POST['emailCopy']))
{
	$UserEmail 	= $_POST['UserEmail'];
	$cust_notes = $_POST['cust_notes'];
	$date 		= $_POST['date'];
	
	$order_shipping_address_id	= $_POST['order_shipping_address_id'];
	$shipping_address_info 		= shipping_address_info($order_shipping_address_id);


	$selectMemberDetails = mysql_fetch_array(mysql_query("SELECT * FROM `member` WHERE `email`= '$UserEmail'"));
	$first_name			= $selectMemberDetails['name'];
	$last_name 			= $selectMemberDetails['last'];
	$phone				= $selectMemberDetails['phone'];
	$company 			= $selectMemberDetails['company'];
	$address1 			= $selectMemberDetails['address1'];
	$address2 			= $selectMemberDetails['address2'];
	$city 				= $selectMemberDetails['city'];
	$state 				= $selectMemberDetails['state'];
	$zip 				= $selectMemberDetails['zip'];
	$saddress1			= $selectMemberDetails['saddress1'];
	$saddress2 			= $selectMemberDetails['saddress2'];
	$scity  			= $selectMemberDetails['scity'];
	$sstate				= $selectMemberDetails['sstate'];
	$szip				= $selectMemberDetails['szip'];
	$scountry 			= $selectMemberDetails['scountry'];
	
	$message = "<html><body><img src='http://flooramerica.chrondev.com/images/mainheader_blue.jpg'><br><br>" ;
	$message .= BuilddisplayCustomerInfo($first_name,$last_name,$UserEmail,$phone,$company, $address1, $address2 , $city , $state, $zip, $saddress1, $saddress2 ,$scity ,$sstate ,$szip ,$scountry,$shipping_address_info);
	
	$message .= '<br><strong>Transaction date: </strong>'. date("M d, Y",strtotime($date)). '<br><br>';
	$message .= '<br><strong>Special Notes About this Order: </strong>'. $cust_notes. '<br><br>';
	$message .= '<strong>ORDER#  '. $ORDERNUMBER. '</strong><br><br>';
	$message . "<br><br><strong>ORDER STATUS :</strong>&nbsp;&nbsp;".$orderstatus. "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>";
    
	$message .= showOrderDetail($ORDERNUMBER, $notes, 'clean');
	$message .="</body></html>";
	
	global $FROM;
	global $BCC;
	
	//$to = 's.praachi@gmail.com';
	$to = $UserEmail;
	$subject = "FLOORAMERICA INVOICE  " . $ORDERNUMBER;
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= "From: ".implode(",", extract_emails_from($FROM))."\r\n";
	//$headers .= "BCC: invoice@flooramerica.com"."\r\n";
	$headers .= "CC: invoice@flooramerica.com "."\r\n";
	
	
	//echo 'to:'.$to.'<br><br>subject:'. $subject.'<br><br>message:'.$message.'<br><br>headers:'.$headers;
	//exit();
	
	$a = mail($to, $subject, $message, $headers);
	if($a)
	{
		print "<h2><font color='green'> Mail sent successfully!</font></h2>";
	}
	else
	{
		print "<h2><font color='red'> Mail coudn't not be sent, please try again </font></h2>";
	}

	
}
//////////////////////////////////////////


//...............  EMAIL  COMPLETE INVOICE  ................//
if(isset($_POST['emailCompleteInvoice']))
{
	//-----------------update taxes-----------------
	$updateStr 				= '';
	$ID 					= $_POST['ID'];
	$page 					= $_POST['page'];
	$searchText 			= $_POST['searchText'];
	$shipping 				= $_POST['shipping'];
	$date 					= $_POST['date'];
	$grand_total_temp 		= $_POST['grand_total_temp'];
	
	$UserEmail 	= $_POST['UserEmail'];
	
	$order_shipping_address_id	= $_POST['order_shipping_address_id'];
	$shipping_address_info 		= shipping_address_info($order_shipping_address_id, $UserEmail);

	$updateStr .= "`shipping` = '$shipping', ";
	
	if($_POST['applySalesTax'])
	{
		$sales_tax = $_POST['sales_tax'];
	}
	else
	{
		$sales_tax = '0.00';
	}
	$updateStr .= "`sales_tax` = '$sales_tax', ";
	
	
	$notes = $_POST['notes'];
	$updateStr .= "`notes` = '$notes', ";
	
	$orderStatus = "INVOICE MAILED";
	$updateStr .= "`orderstatus` = '$orderStatus'";
	
	$update_orderhistory_query = mysql_query("UPDATE `orderhistory` SET $updateStr  WHERE `order_number`= '$ID'");
	//-----------------update cart--------------
	$order_id 	= $_POST['OrderId'];
	$cartid 	= $_POST['cartid'];
	$qty 		= $_POST['qty'];
	$shipping 	= $_POST['shipping'];
	
	foreach ($cartid as $new_cartid)
	{
		$new_qty = $qty[$new_cartid];
		$update_item_query = mysql_query("UPDATE `item_order` SET `qty` = '$new_qty' WHERE `id` = '$new_cartid' AND `order_id` = '$order_id'");
		
	}
	mysql_query("UPDATE `orderhistory` SET `shipping` = '$shipping' WHERE  `order_number` = '$order_id'");
	
	//-------------------email-----------------
	
	$UserEmail 	= $_POST['UserEmail'];
	$cust_notes = $_POST['cust_notes'];
	
	$selectMemberDetails = mysql_fetch_array(mysql_query("SELECT * FROM `member` WHERE `email`= '$UserEmail'"));
	$first_name			= $selectMemberDetails['name'];
	$last_name 			= $selectMemberDetails['last'];
	$phone				= $selectMemberDetails['phone'];
	$company 			= $selectMemberDetails['company'];
	$address1 			= $selectMemberDetails['address1'];
	$address2 			= $selectMemberDetails['address2'];
	$city 				= $selectMemberDetails['city'];
	$state 				= $selectMemberDetails['state'];
	$zip 				= $selectMemberDetails['zip'];
	$saddress1			= $selectMemberDetails['saddress1'];
	$saddress2 			= $selectMemberDetails['saddress2'];
	$scity  			= $selectMemberDetails['scity'];
	$sstate				= $selectMemberDetails['sstate'];
	$szip				= $selectMemberDetails['szip'];
	$scountry 			= $selectMemberDetails['scountry'];
	
	
	$message = "<html><body><img src='http://flooramerica.chrondev.com/images/mainheader_blue.jpg'><br><br>" ;
	
	$message .= "<div align='right' style=\"width:500;padding-left:300px;\">";
	/*$message .= '<a href="http://flooramerica.chrondev.com/MyPayment.php?orderid='.$order_id.'"><input type="button" value="Link Here to Pay by Credit Card" name"payByCreditCard"></a>';*/
	//$message .= BasicPayPalCartMyPayment($SHOPEMAIL,'Flooramerica Products', $ID, $grand_total_temp, $orderStatus);
	
	$message .= EmailPayPalCartLinkMyPayment($PAYPAL_EMAIL,'Flooramerica Products', $ID, $grand_total_temp, $orderStatus);
	
	$message .= "</div>";
			
	$message .= BuilddisplayCustomerInfo($first_name,$last_name,$UserEmail,$phone,$company, $address1, $address2 , $city , $state, $zip, $saddress1, $saddress2 ,$scity ,$sstate ,$szip ,$scountry, $shipping_address_info);
	
	
	$message .= '<br><strong>Transaction date: </strong>'. date("M d, Y",strtotime($date)). '<br><br>';
	$message .= '<br><strong>Special Notes About this Order: </strong>'. $cust_notes. '<br><br>';
	$message .= '<strong>ORDER#  '. $ORDERNUMBER. '</strong><br><br>';
	$message . "<br><br><strong>ORDER STATUS :</strong>&nbsp;&nbsp;".$orderstatus. "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>";
    
	$message .= showOrderDetail($ORDERNUMBER, $notes, 'clean');
	$message .="</body></html>";
	
	global $FROM;
	global $BCC;
	//$to = 's.praachi@gmail.com';
	$to = $UserEmail;
	//$BCC = 'mdadilsiddiqui@gmail.com';
	
	$subject = "FLOORAMERICA INVOICE " . $ORDERNUMBER;
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= "From: ".implode(",", extract_emails_from($FROM))."\r\n";
	$headers .= "CC: invoice@flooramerica.com"."\r\n";
	//$headers .= "BCC: invoice@flooramerica.com"."\r\n";
	
	/*echo 'to:'.$to.'<br><br>subject:'. $subject.'<br><br>message:'.$message.'<br><br>headers:'.$headers;
	exit();*/
	
	$a = mail($to, $subject, $message, $headers);
	if($a)
	{
		print "<h2><font color='green'> Mail sent successfully!</font></h2>";
	}
	else
	{
		print "<h2><font color='red'> Mail coudn't not be sent, please try again </font></h2>";
	}

}
//////////////////////////////////////////





//$_SESSION['cart'] = $cart;
// print "<p>" . $ORDERNUMBER . "<p>";
//----------------------------------------------------------------
// Now get the order shopping cart from the orderhistroy file
//----------------------------------------------------------------

$resultID = mysql_query("SELECT * FROM orderhistory WHERE `order_number` = '$ORDERNUMBER' ", $linkID);

$row = mysql_fetch_array($resultID);
$first_name 	= $row['first_name'];
$last_name 		= $row['last_name'];
$email 			= $row['email'];
$phone 			= $row['phone'];
$notes 			= $row['notes'];
$orderstatus 	= $row['orderstatus'];
$order_number	= $row['order_number'];
$cust_notes		= $row['cust_notes'];
$date			= $row['date'];

$order_shipping_address_id	= $row['order_shipping_address_id'];
$shipping_address_info 		= shipping_address_info($order_shipping_address_id, $email);

$select_customer_address = mysql_fetch_array(mysql_query("SELECT * FROM `member`WHERE `email` = '$email'"));
$company		= $select_customer_address['company'];
$address1		= $select_customer_address['address1'];
$address2 		= $select_customer_address['address2'];
$city  			= $select_customer_address['city'];

$state			= $select_customer_address['state'];
$zip			= $select_customer_address['zip'];
$country 		= $select_customer_address['country'];
$saddress1		= $select_customer_address['saddress1'];
$saddress2 		= $select_customer_address['saddress2'];
$scity  		= $select_customer_address['scity'];
$sstate			= $select_customer_address['sstate'];
$szip			= $select_customer_address['szip'];
$scountry 		= $select_customer_address['scountry'];


/*print "<div>";
	print "<div align='left'>";
		 echo BuilddisplayCustomerInfo($first_name,$last_name,$email,$phone);
	print "</div>";
	print "<div align='right'>";
	 	print "<strong>ORDER STATUS :</strong>&nbsp;&nbsp;<input  type='text' name='orderStatus' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>";
	print "</div>";
print "</div>";*/

 //writeShoppingCart($cart);
 //showCart($cart,$Size,$Color);
 
//




?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>$COMPANYNAME</title>
	
</head>

<body>
<form name="form" method="post" action="">
    <div id="shoppingcart">
        <?php
        print "<div>";
			print "<br><div align='center'  style=\"width:900;\">";
				  print "<input  type='hidden' name='ID' value='$ORDERNUMBER' >";
				  print '<input  type="hidden" name="order_shipping_address_id" value="'.$order_shipping_address_id.'">';
				  print "<input  type='hidden' name='page' value='$page' >";
				  print "<input  type='hidden' name='searchText' value='$searchText' >";
				  echo '<a href="'.$sourcepage.'?searchText='. $searchText.'&search=Search&page='. $page.'">BACK TO REPORT LEVEL</a>';
            print "</div>";
			
            print "<div style=\"width:900; float:left; clear:both;\">";
                 echo BuilddisplayCustomerInfo($first_name,$last_name,$email,$phone, $company, $address1, $address2 , $city , $state, $zip, $saddress1, $saddress2 ,$scity ,$sstate ,$szip ,$scountry, $shipping_address_info);
            print "</div>";
			print "<div style=\"loat:left; clear:both;\"></div><br>";
			
			print "<input  type='hidden' name='cust_notes' value='$cust_notes' >
			<input  type='hidden' name='date' value='$date' >";
			print '<br><strong>Transaction date:  </strong>'. date("M d, Y",strtotime($date)). '<br>';
			
			print '<br><strong>Special Notes About this Order:  </strong>'. $cust_notes . '<br><br>';
			
			print '<strong>ORDER#  '. $order_number. '</strong>';
            print "<div align='center' style=\"width:900;\">";
					print "<strong>ORDER STATUS :</strong>&nbsp;&nbsp;
						<input  type='text' name='orderStatus' value='$orderstatus' >
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
						<input type=\"submit\" name=\"emailCompleteInvoice\"  value=\"EMAIL COMPLETE INVOICE\">
					<br><br>";
           	print "</div>";
        print "</div>";
        ?>
    </div>
    
    <div id="contents" style="width:900px;">
        <?php
        //echo writeShoppingCart();
        echo showOrderDetail($ORDERNUMBER, $notes);
        ?>
    </div>

</form>
</body>
</html>