<?php

include "../globals.php";
include "adminheader.php";
include 'global_functions.php';

include "functions.php";
adminTitle(" New Thumbnail Image ");

$pageTitle = "Add New Image";
$page = $_REQUEST['page'];
$calltype = 'A';

$extensions = array('.png', '.gif', '.jpg', '.jpeg', '.PNG', '.GIF', '.JPG', '.JPEG');

if (isset($_POST['submit'])) {
    $calltype = $_POST['calltype'];
    $page = $_POST['page'];
    $id = $_POST['id'];

    $title = $_POST['title'];
    $category = $_POST['category'];
    $subcategory = $_POST['subcategory'];
    $description = $_POST['description'];
    $position = $_POST['position'];



    if ($category_id != '' && $category_id == 'other') {
        $other_category = $_POST['other_category'];
        $category_id = insert_other_category($other_category);
    }
    if ($_FILES["imagename1"]["size"] > 0) {
        $extension = strrchr($_FILES["imagename1"]["name"], '.');
        if (!in_array($extension, $extensions)) {
            $error[] = 'wrong image file format, alowed only .png , .gif, .jpg, .jpeg';
        } else {
            $ul_imgae_name = time() . "_" . $_FILES["imagename1"]["name"];
            $upload_image = move_uploaded_file($_FILES["imagename1"]["tmp_name"], "../ProductImage/" . $ul_imgae_name);
            if ($upload_image) {
                $imagename1 = $ul_imgae_name;
                $image_list[] = $imagename1;
            }
        }
    }

    if ($_FILES["imagename2"]["size"] > 0) {
        $extension = strrchr($_FILES["imagename2"]["name"], '.');
        if (!in_array($extension, $extensions)) {
            $error[] = 'wrong image file format, alowed only .png , .gif, .jpg, .jpeg';
        } else {
            $ul_imgae_name = time() . "_" . $_FILES["imagename2"]["name"];
            $upload_image = move_uploaded_file($_FILES["imagename2"]["tmp_name"], "../ProductImage/" . $ul_imgae_name);
            if ($upload_image) {
                $imagename2 = $ul_imgae_name;
                $image_list[] = $imagename2;
            }
        }
    }

    if ($_FILES["imagename3"]["size"] > 0) {
        $extension = strrchr($_FILES["imagename3"]["name"], '.');
        if (!in_array($extension, $extensions)) {
            $error[] = 'wrong image file format, alowed only .png , .gif, .jpg, .jpeg';
        } else {
            $ul_imgae_name = time() . "_" . $_FILES["imagename3"]["name"];
            $upload_image = move_uploaded_file($_FILES["imagename3"]["tmp_name"], "../ProductImage/" . $ul_imgae_name);
            if ($upload_image) {
                $imagename3 = $ul_imgae_name;
                $image_list[] = $imagename3;
            }
        }
    }

    if ($_FILES["imagename4"]["size"] > 0) {
        $extension = strrchr($_FILES["imagename4"]["name"], '.');
        if (!in_array($extension, $extensions)) {
            $error[] = 'wrong image file format, alowed only .png , .gif, .jpg, .jpeg';
        } else {
            $ul_imgae_name = time() . "_" . $_FILES["imagename4"]["name"];
            $upload_image = move_uploaded_file($_FILES["imagename4"]["tmp_name"], "../ProductImage/" . $ul_imgae_name);
            if ($upload_image) {
                $imagename4 = $ul_imgae_name;
                $image_list[] = $imagename4;
            }
        }
    }

    if ($_FILES["imagename5"]["size"] > 0) {
        $extension = strrchr($_FILES["imagename5"]["name"], '.');
        if (!in_array($extension, $extensions)) {
            $error[] = 'wrong image file format, alowed only .png , .gif, .jpg, .jpeg';
        } else {
            $ul_imgae_name = time() . "_" . $_FILES["imagename5"]["name"];
            $upload_image = move_uploaded_file($_FILES["imagename5"]["tmp_name"], "../ProductImage/" . $ul_imgae_name);
            if ($upload_image) {
                $imagename5 = $ul_imgae_name;
                $image_list[] = $imagename5;
            }
        }
    }

    if (empty($image_list)) {
        $error[] = 'Please upload an Image';
    }


    if (empty($error)) {
        $title = mysql_real_escape_string($title);
        $description = mysql_real_escape_string($description);
        foreach ($image_list as $key => $imagename) {
            $insert_banners = mysql_query("INSERT INTO `product2` (`category`, `subcategory`, `description`, `longdesc`, `position`, `image_hyperlink`) VALUES ('$category', '$subcategory', '$title', '$description', '$display_order', '$imagename')");
        }

        if ($insert_banners) {
            header('Location: Product2Report.php?page=' . $page);
            exit();
        } else {
            $error[] = 'Databsae Error: ' . mysql_error();
        }
    }//END Error Free
}
?>
<script>
    function loadSubcategory(category) {
        if (category.length == 0) {
            document.getElementById("subcategory").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("subcategory").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "getSubcategory.php?category=" + category, true);
            xmlhttp.send();
        }
    }
</script>
<?php

print "<body>";
print "<p><b><font face=\"Arial\" size=\"4\">$pageTitle</font></b></p>";
print "<form method=\"POST\" action=\"\" enctype=\"multipart/form-data\">";
print "<p style=\"color:red\">" . implode('<br>', $error) . "</p>";
print "<table>";

echo '
<tr>
<td colspan="2" align="right">';

echo '<input type="submit" name="submit" value="Submit">

</td>
</tr>';

print "<tr><td width=\"200px\"><strong>Category:</strong></td><td>";
echo '<select name="category" onchange="loadSubcategory(this.value);" required >';
echo ' <option value="">Select Category</option>';
category_dropdown($category);
echo '</select>';
//echo '<select name="category_id" id="category_id" onchange="check_category_name(this.value);" style="min-width:250px;">
//<option value=""></option>';
//image_gallery_category_dropdown($category_id);
//echo '<option value="other">Other</option></select>';

echo "</td></tr>";
print "<tr><td width=\"200px\"><strong>SubCategory:</strong></td><td>";
echo '<select name="subcategory" id="subcategory"  >';
echo ' <option value="">Select SubCategory</option>';
subcategory_dropdown($category, $subcategory);
echo '</select>';
echo "</td></tr>";

print "<tr><td width=\"200px\"><strong>Title:</strong></td><td><input type=\"text\" name=\"title\" value=\"$title\" style=\"width:400px;\" ></td></tr>";



print "<tr><td width=\"200px\"><strong>Image File:</strong></td><td><input type=\"file\" name=\"imagename1\" ></td></tr>";

if ($imagename1 != '') {
    print "<tr><td>&nbsp;</td><td ><img src=\"../ProductImage/$imagename1\"></td></tr>";
}

print "<tr><td width=\"200px\"><strong>Image File:</strong></td><td><input type=\"file\" name=\"imagename2\" ></td></tr>";

if ($imagename2 != '') {
    print "<tr><td>&nbsp;</td><td ><img src=\"../ProductImage/$imagename2\"></td></tr>";
}
print "<tr><td width=\"200px\"><strong>Image File:</strong></td><td><input type=\"file\" name=\"imagename3\" ></td></tr>";

if ($imagename3 != '') {
    print "<tr><td>&nbsp;</td><td ><img src=\"../ProductImage/$imagename3\"></td></tr>";
}
print "<tr><td width=\"200px\"><strong>Image File:</strong></td><td><input type=\"file\" name=\"imagename4\" ></td></tr>";

if ($imagename4 != '') {
    print "<tr><td>&nbsp;</td><td ><img src=\"../ProductImage/$imagename4\"></td></tr>";
}
print "<tr><td width=\"200px\"><strong>Image File:</strong></td><td><input type=\"file\" name=\"imagename5\" ></td></tr>";
if ($imagename5 != '') {
    print "<tr><td>&nbsp;</td><td ><img src=\"../ProductImage/$imagename5\"></td></tr>";
}
print "<tr><td width=\"200px\" valign=\"top\"><strong>Description Text:</strong></td>
<td><textarea name=\"description\" style=\"width:400px; height:250px;\">" . $description . "</textarea></td>
</tr>";

print "<tr><td width=\"200px\"><strong>Display Order:</strong></td><td><input type=\"number\" name=\"position\" value=\"$position\" style=\"width:100px;\" ></td></tr>";


print "<tr><td></td><td>";

print '<input type="hidden" name="calltype" value="A" />';
echo '<input type="submit" name="submit" value="Submit">';
echo '<input type="reset" name="B2" value="Reset">';
print "<input type=\"hidden\" name=\"page\" size=\"20\" value=\"$page\">";
print "</td></tr>";
print "</table>";
?>
<script type="text/javascript">
    function check_category_name(category_id)
    {
        if (category_id == 'other')
        {
            document.getElementById('other_category').style.display = 'block';
        } else
        {
            document.getElementById('other_category').style.display = 'none';
        }

    }
</script>
<?php

print "</form></body>";

include "adminfooter.php";
?>

