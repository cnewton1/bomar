<?php

function isURL($url) {
    if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) {
        return true;
    } else {
        return false;
    }
}

function category_dropdown($category) {
    $query = mysql_query("SELECT * FROM `category` ORDER BY `Name`");
    if (mysql_num_rows($query)) {
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) {
            $selected_str = ($category == $row['Name']) ? 'selected' : '';
            echo '<option value="' . $row['Name'] . '" ' . $selected_str . '>' . $row['Name'] . '</option>';
        }
    }
}

function subcategory_dropdown($category, $subcategory) {
    $query = mysql_query("SELECT * FROM `subcategory` WHERE `Parent` = '$category' ORDER BY `Name`");
    if (mysql_num_rows($query)) {
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) {
            $selected_str = ($subcategory == $row['Name']) ? 'selected' : '';
            echo '<option value="' . $row['Name'] . '" ' . $selected_str . '>' . $row['Name'] . '</option>';
        }
    }
}

function submit_update_button($table_name, $id) {
    if ($id > 0) {
        $prev_query = mysql_query("SELECT `id` FROM `$table_name` WHERE `id` > '$id' ORDER BY `id` ASC LIMIT 0,1");
        if (mysql_num_rows($prev_query)) {
            $prev_row = mysql_fetch_array($prev_query);

            echo '<input type="submit" name="PREV_RECORD" value="Prev Record" style="font-size:18px; height:35px; margin:10px">
		<input type="hidden" name="prev_id" value="' . $prev_row['id'] . '">
		';
        }

        $next_query = mysql_query("SELECT `id` FROM `$table_name` WHERE `id` < '$id' ORDER BY `id` DESC LIMIT 0,1");
        if (mysql_num_rows($next_query)) {
            $next_row = mysql_fetch_array($next_query);

            echo '<input type="submit" name="NEXT_RECORD" value="Next Record" style="font-size:18px; height:35px; margin:10px">
		<input type="hidden" name="next_id" value="' . $next_row['id'] . '">
		';
        }
    }

    echo '<input type="submit" name="submit" value="Save" style="font-size:18px; height:35px; margin:10px">';
}

function input_box($type = 'text', $name, $value, $style) {
    $id = str_replace(array('[',']'), array('_',''),$name);
    if ($type == 'text') {
        $value = htmlspecialchars($value);
        return '<input type="text" name="' . $name . '" value="' . $value . '" style="' . $style . '">';
    } else if ($type == 'textarea') {
        $value = htmlentities($value);
        return '<textarea id="'.$id.'" name="' . $name . '" style="' . $style . '">' . $value . '</textarea>';
    }
}

if (!function_exists('change_table_row_color')) {

    function change_table_row_color($check, $width = '') {
        if ($check == 1)
            print "<td width=\"$width\" bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium\">";
        else
            print "<td width=\"$width\" bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium\">";
    }

}

function table_fields_list($table_name, $Type = '', $exclude_fields = array()) {
    $condition = ($Type != '') ? "WHERE `Type` LIKE '%$Type%'" : "";
    $query = mysql_query("SHOW FIELDS FROM `$table_name` $condition");
    $fields = array();
    if (mysql_num_rows($query)) {
        while ($row = mysql_fetch_assoc($query)) {
            if (!in_array($row['Field'], $exclude_fields)) {
                $fields[] = $row['Field'];
            }
        }
    }
    return $fields;
}

function table_fields($table_name, $exclude_fields = array()) {
    $table_field_query = mysql_query("SHOW FIELDS FROM `$table_name`");
    while ($row = mysql_fetch_array($table_field_query)) {
        if (!in_array($row['Field'], $exclude_fields)) {
            $table_fields_array[] = $row['Field'];
        }
    }
    $columns = "`" . implode("`, `", $table_fields_array) . "`";

    return $columns;
}

function query_to_csv($db_conn, $query, $filename, $attachment = false, $headers = true) {
    if ($attachment) {
        // send response headers to the browser
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=' . $filename);
        $fp = fopen('php://output', 'w');
    } else {
        $fp = fopen($filename, 'w');
    }

    $result = mysql_query($query, $db_conn) or die(mysql_error($db_conn));

    if ($headers) {
        $row = mysql_fetch_assoc($result);
        if ($row) {
            fputcsv($fp, array_keys($row));
            mysql_data_seek($result, 0);
        }
    }

    while ($row = mysql_fetch_assoc($result)) {
        fputcsv($fp, $row);
    }
    fclose($fp);
}

function query_to_csv_tab_delimited($db_conn, $query, $start, $limit, $filename, $attachment = false, $headers = true) {
    if ($attachment) {
        // send response headers to the browser
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=' . $filename);
        $fp = fopen('php://output', 'w');
    } else {
        if ($start > 0) {
            $fp = fopen($filename, 'a');
        } else {
            $fp = fopen($filename, 'w');
        }
    }

    $result = mysql_query($query);

    if ($headers && $start < 1) {
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        if ($row) {
            fputcsv($fp, array_keys($row), "\t");
            mysql_data_seek($result, 0);
        }
    }

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
        fputcsv($fp, $row, "\t");
    }

    fclose($fp);
}

function csv_to_array($csv) {
    $len = strlen($csv);


    $table = array();
    $cur_row = array();
    $cur_val = "";
    $state = "first item";


    for ($i = 0; $i < $len; $i++) {
        //sleep(1000);
        $ch = substr($csv, $i, 1);
        if ($state == "first item") {
            if ($ch == '"') {
                $state = "we're quoted hea";
            } elseif ($ch == ",") { //empty
                $cur_row[] = ""; //done with first one
                $cur_val = "";
                $state = "first item";
            } elseif ($ch == "\n") {
                $cur_row[] = $cur_val;
                $table[] = $cur_row;
                $cur_row = array();
                $cur_val = "";
                $state = "first item";
            } elseif ($ch == "\r") {
                $state = "wait for a line feed, if so close out row!";
            } else {
                $cur_val .= $ch;
                $state = "gather not quote";
            }
        } elseif ($state == "we're quoted hea") {
            if ($ch == '"') {
                $state = "potential end quote found";
            } else {
                $cur_val .= $ch;
            }
        } elseif ($state == "potential end quote found") {
            if ($ch == '"') {
                $cur_val .= '"';
                $state = "we're quoted hea";
            } elseif ($ch == ',') {
                $cur_row[] = $cur_val;
                $cur_val = "";
                $state = "first item";
            } elseif ($ch == "\n") {
                $cur_row[] = $cur_val;
                $table[] = $cur_row;
                $cur_row = array();
                $cur_val = "";
                $state = "first item";
            } elseif ($ch == "\r") {
                $state = "wait for a line feed, if so close out row!";
            } else {
                $cur_val .= $ch;
                $state = "we're quoted hea";
            }
        } elseif ($state == "wait for a line feed, if so close out row!") {
            if ($ch == "\n") {
                $cur_row[] = $cur_val;
                $cur_val = "";
                $table[] = $cur_row;
                $cur_row = array();
                $state = "first item";
            } else {
                $cur_row[] = $cur_val;
                $table[] = $cur_row;
                $cur_row = array();
                $cur_val = $ch;
                $state = "gather not quote";
            }
        } elseif ($state == "gather not quote") {
            if ($ch == ",") {
                $cur_row[] = $cur_val;
                $cur_val = "";
                $state = "first item";
            } elseif ($ch == "\n") {
                $cur_row[] = $cur_val;
                $table[] = $cur_row;
                $cur_row = array();
                $cur_val = "";
                $state = "first item";
            } elseif ($ch == "\r") {
                $state = "wait for a line feed, if so close out row!";
            } else {
                $cur_val .= $ch;
            }
        }
    }

    return $table;
}

?>