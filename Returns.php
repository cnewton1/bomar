<?php
include "users.php";
include "connect.php";
include "./rightglobals.php"; 
include "MyImageFunction.php";
include "getCartFunction.php";
include "ShopCartTempFunctions.php";
include "pager.php";
include "mainheader.php";

print " <table width=\"950px\" align=\"left\" border=\"0\">";
print "<TR><TD valign=\"top\" width=\"100px\">";
include "LeftNavBar.php"; 
print "</TD><TD valign=\"top\" width=\"850px\" style='padding-left:40px;'>";
print "<table cellspacing=\"0\" cellpadding=\"0\" hspace=\"0\" vspace=\"0\" width=\"100%\" height=\"48\" border=\"0\">";
print "<tr><td valign=\"top\" align=\"left\" height=\"48\" style=\"padding: 0in\" width=\"852\">";


$FromID=$HTTP_GET_VARS['ID'];
$ListName = $HTTP_GET_VARS['listName'];
$PrevPage=$HTTP_GET_VARS['PrevPAGE'];
$CateName=$HTTP_GET_VARS['CateName'];   
$page = $_GET['page']; 

if ($page == NULL)
{
 $page=1;   
 $PrevPage=1;
}
$limit=30;
$count_sql = "select count(*) from product";
if($ListName != "")
$count_sql.=" where subcategory LIKE '$ListName%'";

$count_sql.=" group by subcategory";

$result = mysql_query($count_sql);     

$total = mysql_num_rows($result);

if ($total==0)
{
  print "<br><br><font face=\"Arial\" size=\"2\"><b>No images were found in this category</b>";
  print "<br><font face=\"Arial\" size=\"2\"><b>Please search again for another images</b>";
  exit;
}
 
$pager  = Pager::getPagerData($total, $limit, $page); 
$offset = $pager->offset; 
$limit  = $pager->limit; 
$page   = $pager->page;  

//--------------------------------------------------
// General list of a table in this cast the category
//--------------------------------------------------
 
 
  
$sql="Select distinct manufacturername from product";
if($ListName != "")
$sql.=" where manufacturername LIKE '$ListName%'";
$sql.=" order by manufacturername limit $offset, $limit";
$resultID=mysql_query($sql);  
$inc=0;
?>
   
<table border="0" cellpadding="2" cellspacing="0" width="772">
<tr>
<td class="main" width="768">
<p align="center"><b><font size="6" face="Arial">Shipping &amp; Returns</font></b></p>
<p><font face="Arial"><b>General info about shipping</b><br>
<br>
The shipping cost is tabulated and declared after you select your delivery 
location / shipping address. The shipping charges are tabulated according to the 
shipping location. Prices for the same product can vary within American and 
International locations. Please note that there are some countries with 
restrictions imposed on shipment, the reasons are subjective to the country and 
we will not be able to ship to such countries.<br>
<br>
<b>Some dos and don'ts of delivery and shipping!</b><br>
<br>
For deliveries outside US, please note that we do not accept PO Box as delivery 
information. Please provide us with complete shipping information along with the 
contact details of the recipient. In the event of non-delivery that can occur 
due to a mistake in information provided by you, (i.e. wrong name or address) 
any extra cost incurred by us for redelivery shall be claimed from the customer 
placing the order. Please note that items ordered together are not always 
shipped together. For security purpose, your order will not be delivered without 
a recipient signature. If you are not available to sign for your package, please 
be sure that an adult who is authorized to sign for your package on your behalf 
will be available at the designated shipping address. For products with the 
price range of $90 and above, we do not accept orders to any destination in the 
whole of US, due to Postal Permit limitations.<br>
<br>
<b>How is the shipping charge calculated?</b><br>
<br>
Calculation of the shipping charges is dependent on the Quantity of products: 
The shipping cost varies with the quantity of products purchased as this has a 
direct bearing on the shipment weight! <br>
<br>
<b>Will I be charged an extra fee as a consignee?</b><br>
<br>
Central Governments have introduced special charges on any shipment landing in 
their state. This is a statutory levy that has to be paid by the consignee 
during the time of delivery. The Special charges depend upon the product price 
and the state where the shipment is landing. It is charged only for few 
destinations within US. It is payable by the consignee and our Customer Support 
team will intimate you wherever applicable.<br>
<br>
<b>General info about delivery</b><br>
<br>
We ship merchandise to American and International destinations. The delivery 
locations are clearly mentioned against each product description. Please note 
that the delivery locations can vary with each product. Please note that we do 
not deliver merchandise on Sundays and public / government holidays in US as the 
courier facilities are not available.<br>
<br>
<b>Are the items delivered exactly as it is displayed on the website?</b><br>
<br>
There may be some variation in the gifts especially in perishable items like 
furnitures, presentations like its color, model, etc . They will definitely 
match all the specifications in terms of weight, size, count and functionality. 
However, some physical attributes like color may vary from that displayed on 
screen.<br>
<br>
<br>
<b>What if you don't deliver or deliver late?</b><br>
<br>
We have outsourced our logistics to ensure timely delivery as specified at the 
time of purchase. <br>
Due to reasons of force majeure such as riots, transportation strikes beyond our 
control may be responsible for delay in delivery. Another common reason is when 
the recipient of the gift is not at home, or his/her address has changed. We 
will cooperate with you to rectify this situation if it does occur.<br>
<br>
<b>How do you provide proof of delivery?</b><br>
<br>
A Declaration of Goods is provided to the recipient on receipt of the 
merchandise. Declaration Proof Is Positive Evidence and Support Material A 
Declaration of Goods is given to a recipient (who shops for self or for purpose 
of a gift) in order to protect the person from these scenarios: At the receipt 
of any consignment, a recipient has to check if any item is missing &amp; if the 
gift is appropriate, the very same one that has been sent over. This information 
is clearly provided in the Declaration. If the item is damaged or lost, due to a 
mishap, the same can be claimed only in the possession of such a Declaration. It 
is the prerogative of the recipient to demand such a declaration from the 
logistics company. It is also legally entailed in the Government rules.<br>
<br>
<b>Returns, Refund &amp; Cancellation</b><br>
<b>What if I have a problem or need to return something?</b><br>
<br>
If you receive a damaged package, refuse the package and contact the shipper. 
Then contact us to resolve the issue. Ensure you file a complaint within a week 
of delivery to enable us to take appropriate action. The complaint received 
after a week will not be entertained. If you receive a damaged or defective 
item, please send a secure, non-tampered, unused package back to us within 15 
days of the receipt of the product for any claim. Attach a short note stating 
the reason for return, quote your order number, and send it to us via a reliable 
courier or mail. We will promptly take up the issue with our vendor/logistic 
partner for replacement. <br>
<br>
<b>How and when will I get a refund?</b><br>
<br>
If you have paid initially through Credit Card, and the order has resulted in a 
case for refund, the total amount is refunded to you through our payment 
gateway, CC Avenue. It takes 12 business days for orders to be validated as a 
refund and a 7-day period is extended after that for the same to reflect in your 
account. We will keep you personally informed if due to unavoidable 
circumstances, the above-declared period is extended. Please be assured that in 
a case of refund from company, you will be refunded in full to the extent of the 
order value.</font></td>
</tr>
</table>
 
   
<?php   

//***************************************
// FUNCTION SECTION
//***************************************
   
  include "gfooter.php"; 




