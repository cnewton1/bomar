<!DOCTYPE html>
<html>
	<head>
		<?php include 'global_head.php';?>
        
		<!-- <hs:title> -->
        
		<title>Westcarb Enterprises | Springfield, MA 01108</title>
	
		<style>
		div.lpxcenterpageouter { text-align: center; position: absolute; top: 0px; left: 0px; width: 100% }
			div#element36outer { position:absolute; 
			width: 220px;
			height: 115px; }
			.corner_112c45_10_topleft { background-image: url(
			images/About-Us~~112c45~~10~~topleft.png); top: 0px; left: 0px; }
			.corner_112c45_10_topright { background-image: url(
			images/About-Us~~112c45~~10~~topright.png); top: 0px; right: 0px; }
			.corner_112c45_10_bottomleft { background-image: url(
			images/About-Us~~112c45~~10~~bottomleft.png); bottom: 0px; left: 0px; }
			.corner_112c45_10_bottomright { background-image: url(
			images/About-Us~~112c45~~10~~bottomright.png); bottom: 0px; right: 0px; }
			.corner_112c45_10_topleft,
			.corner_112c45_10_topright,
			.corner_112c45_10_bottomleft,
			.corner_112c45_10_bottomright {
			position: absolute; width: 10px; height: 10px; }
			div#element36horiz { background-color:#112c45; position: absolute;
			top: 10px;
			width: 220px;
			height: 95px; }
			div#element36vert { background-color: #112c45; color: #112c45; position: absolute;
			left: 10px;
			width: 200px;
			height: 115px; }
			
			
		
			</style>
    <link rel="stylesheet" type="text/css" href="css/contact.css" />
            
	<script type='text/javascript' src='javascript/jquery.simplemodal.js'></script>
   
    <script type='text/javascript' src='javascript/contact.js'></script>
    
    
    
    </head>
	<body id="element1" onunload="" scroll="auto">
		<noscript>
			<img height="40" width="373" border="0" alt="" src="images/javascript_disabled.gif">
		</noscript>
        
        
<div class="lpxcenterpageouter">
	<div class="lpxcenterpageinner">
		<!-- <hs:bodyinclude> -->
		<?php include 'head_top.php';?>
		
		
		<!-- </hs:bodyinclude> -->
		<!-- <hs:element36> -->
		<div id="element36" style="position: absolute; top: 180px; left: 660px; width: 220px; height: 115px; z-index: 1000;">
			<div id="element36outer">
				<div class="corner_112c45_10_topleft">
				</div>
				<div class="corner_112c45_10_topright">
				</div>
				<div class="corner_112c45_10_bottomleft">
				</div>
				<div class="corner_112c45_10_bottomright">
				</div>
				<div id="element36horiz">
				</div>
				<div id="element36vert">
				</div>
			</div>
		</div>
		<!-- </hs:element36> -->
		<!-- <hs:element37> -->
		<div id="element37" style="position: absolute; top: 185px; left: 672px; width: 202px; height: 63px; z-index: 1001;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#f9f4de" class="size12 Tahoma12">Interested in learning more about us?<br>
				</font>
			</div>
		</div>
		<!-- </hs:element37> -->
		<!-- <hs:element38> -->
		<div id='contact-form' style="position: absolute; top: 249px; left: 720px; width: 104px; height: 32px; z-index: 1102;">
			<a id="ContactUs-1" class="contact" href="#ContactUs-1ContactUs" rel="nofollow"><img height="32" width="104" style="display:block" border="0" alt="Contact Us" src="images/About-Us~~element38.png">
            <input value="CMDContactButton" name="CMD" type="hidden"><input value="ContactUs" name="SUB_CMD" type="hidden"></a>
            <div style='display:none'>
			<img src='images/loading.gif' alt='' />
		</div>
		</div>
		<!-- </hs:element38> -->
		<!-- <hs:element40> -->
		<div id="element40" style="position: absolute; top: 130px; left: 100px; width: 539px; height: 29px; z-index: 103;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#112c45" class="size18 Tahoma18">About Westcarb Enterprises<br>
				</font>
			</div>
		</div>
		<!-- </hs:element40> -->
		<!-- <hs:element41> -->
		<div id="element41" style="position: absolute; top: 168px; left: 100px; width: 540px; height: 672px; z-index: 1000;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Westcarb Enterprises, Incorporated was formed in 2002. Westcarb is a Small Business Administration (SBA) Certified Woman Owned Minority Business Enterprise (WMBE). Westcarb is also a SBA certified 8(a) Small Disadvantaged Business (SDB).<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">From a facilities management and maintenance aspect, our customers generally request services such as equipment maintenance, repair and operations, janitorial, landscaping, lighting, office and construction services. Westcarb has delivered, and continues to deliver, these services in a very satisfactory, efficient, and effective professional manner, which includes the use of environmentally green products.<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Westcarb provides services to government agencies and commercial entities along the Atlantic Coast from Maine to Florida. Some of the government agencies we service include the Navy, Army and Air National Guards, Department of Justice, Air Force, Department of Defense, and the General Services Administration (GSA). Westcarb also worked as a sub-contractor on various Verizon projects throughout Florida, Maryland and Pennsylvania, as an underground contractor laying the housing and hand holds to bring FIOS to the property of many communities. Westcarb is registered with the Defense Logistics Agency's Central Contractor Registry, and is eligible for all federal facilitiy contracts under the SBA 8(a) and SDB programs.<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Westcarb continually demonstrates having the ability, as a small business, to provide strategic management processes and solutions, as well as preventive and predictive maintenance. Westcarb has recently added Infrared inspections to its predictive maintenance program. Westcarb partners with large and small successful companies to provide the best in class, dependable facilities maintenance services.<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><b>Corporate Management</b><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Westcarb has developed a core management structure that takes responsibility for the direction, growth, and success of the company, as well as providing guidance to the daily operations, ensuring the provision of quality service to all customers. This management group also receives and solicits feedback from both the on-site personnel and the customer, in order to aid in further enhancing the company's service offering.<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><b>Project Management</b><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Westcarb's overall approach to Project Management implements an Integrated Project Team for the management of all services. The Integrated Project Team consists of: General Managers, Project Managers, Contract Managers, Engineers, also a Safety &amp; Quality Control Manager, as required to meet the needs of each specific contract. The Integrated Project Team is committed to successful delivery of the services provided.<br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10"><br>
				</font>
			</div>
		</div>
		<!-- </hs:element41> -->
		<!-- <hs:element46> -->
		<div id="element46" style="position: absolute; top: 355px; left: 659px; width: 220px; height: 173px; z-index: 105;">
			<div style="overflow: hidden; height: 173px; width: 220px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="173" width="220" style="display: block; border-radius: 0px;" title="" alt="" src="images/About-Us~~element46.jpg"/>
			</div>
		</div>
		<!-- </hs:element46> -->
		
		<!-- <hs:element55> -->
		<div id="element55" style="position: absolute; top: 594px; left: 715px; width: 126px; height: 155px; z-index: 107;">
			<div style="overflow: hidden; height: 155px; width: 126px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="155" width="126" style="display: block; border-radius: 0px;" title="" alt="" src="images/imagesCAZMYD8G.jpg"/>
			</div>
		</div>
		<!-- </hs:element55> -->
        
        
      <?php include 'footer.php';?>
        
        
        
	</div>
</div>



		
		
	</body>
</html>
