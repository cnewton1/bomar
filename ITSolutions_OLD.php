<!DOCTYPE html>
<html>
	<head>
		<?php include 'global_head.php';?>
        
		<!-- <hs:title> -->
        
		<title>Westcarb Enterprises | Springfield, MA 01108</title>
	
		
	</head>
	<body id="element1" onunload="" scroll="auto">
		<noscript>
			<img height="40" width="373" border="0" alt="" src="images/javascript_disabled.gif">
		</noscript>
        
        
<div class="lpxcenterpageouter">
	<div class="lpxcenterpageinner">
		<!-- <hs:bodyinclude> -->
		<?php include 'head_top.php';?>
		
		
		<!-- </hs:bodyinclude> -->
		<!-- <hs:element40> -->
		<div id="element40" style="position: absolute; top: 130px; left: 101px; width: 539px; height: 29px; z-index: 1000;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#112c45" class="size18 Tahoma18">Westcarb Enterprises IT Solutions<br>
				</font>
			</div>
		</div>
		<!-- </hs:element40> -->
		<!-- <hs:element41> -->
		<div id="element41" style="position: absolute; top: 162px; left: 101px; width: 540px; height: 64px; z-index: 1001;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Tahoma, Arial, Helvetica, sans-serif" color="#555555" class="size10 Tahoma10">Westcarb's IT Solutions &amp; Services Division has two vital sub-groups that include a Specialized Services Group and a Consumable/Repairable Products Group. WCE utilizes strategic partnerships, such as SMA Microsystems, to provide value to our customers by offering "world class" solutions at competitive pricing and doing the job right the first time.<br>
				</font>
			</div>
		</div>
		<!-- </hs:element41> -->
		<!-- <hs:element69> -->
		<div id="element69" style="position: absolute; top: 271px; left: 102px; width: 333px; height: 20px; z-index: 1002;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000066" class="size10 Helvetica10"><b>Mobile Technology Staging &amp; Integration</b><br>
				</font>
			</div>
		</div>
		<!-- </hs:element69> -->
		<!-- <hs:element77> -->
		<div id="element77" style="position: absolute; top: 290px; left: 83px; width: 482px; height: 171px; z-index: 1003;">
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Hardware and Software Procurement <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Computer Systems Design Services<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Staging, Integration and Configuration <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Imaging and Asset Tagging <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Online Shipping, Receiving <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Rollouts, Deployment and Installation <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Rapid Deployment <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Custom Packaging and Delivery <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Real Time - Web-Enabled Project Tracking Tools<br>
					</font></li>
				</ul>
			</div>
		</div>
		<!-- </hs:element77> -->
		<!-- <hs:element78> -->
		<div id="element78" style="position: absolute; top: 471px; left: 96px; width: 300px; height: 16px; z-index: 1004;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000066" class="size10 Helvetica10"></font><font face="Helvetica, Arial, sans-serif" color="#000066" class="size10 Helvetica10"><b>Connectivity, Cabling &amp; Telecom Solutions</b><br>
				</font>
			</div>
		</div>
		<!-- </hs:element78> -->
		<!-- <hs:element79> -->
		<div id="element79" style="position: absolute; top: 490px; left: 81px; width: 627px; height: 132px; z-index: 1005;">
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size9 Helvetica9" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size9 Helvetica9">Voice and Data Cabling Infrastructure (Data Centers, Structured Wiring, Custom Design Build) <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Networking and voice electronic devices <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Enterprise Cabling Solutions <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Videoconferencing: end stations and bridges<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Wireless Solutions (Single or Building to Building and LAN to LAN )<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Building Structured Wiring <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Paging, and Sound Masking Solutions<br>
					</font></li>
				</ul>
			</div>
		</div>
		<!-- </hs:element79> -->
		<!-- <hs:element81> -->
		<div id="element81" style="position: absolute; top: 634px; left: 100px; width: 300px; height: 20px; z-index: 1006;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000066" class="size10 Helvetica10"><b>Maintenance, Support &amp; Asset Disposition </b><br>
				</font>
			</div>
		</div>
		<!-- </hs:element81> -->
		<!-- <hs:element82> -->
		<div id="element82" style="position: absolute; top: 653px; left: 81px; width: 357px; height: 190px; z-index: 1007;">
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Logistics / Supply Chain Management <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">On-Site Desk-Side Support <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Data backup and Recovery <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Warranty Extensions and Upgrades<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Training <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">IMAC Coordination <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Schedule Preventive Maintenance<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Management Reporting (SLA's, Parts, etc)<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">SME Staff Augmentation <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Asset Disposition and Refurbishment <br>
					</font></li>
				</ul>
			</div>
		</div>
		<!-- </hs:element82> -->
		<!-- <hs:element83> -->
		<div id="element83" style="position: absolute; top: 635px; left: 511px; width: 300px; height: 20px; z-index: 1008;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000066" class="size10 Helvetica10"><b>Security and Web Hosting Solutions </b></font><font face="Helvetica, Arial, sans-serif" color="#000066" class="size10 Helvetica10"><br>
				</font>
			</div>
		</div>
		<!-- </hs:element83> -->
		<!-- <hs:element84> -->
		<div id="element84" style="position: absolute; top: 654px; left: 490px; width: 356px; height: 171px; z-index: 1009;">
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Gap Assessment<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Penetration Testing <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Risk Management Compliance (SOX, HIPAA, etc)<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Security Information Event Management <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Managed Forensic Security<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Cyber Security<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Hosting for Test and Development Environments<br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Managed Collocation <br>
					</font></li>
				</ul>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<ul class="lpx">
					<li class="size10 Helvetica10" style="font-family:Helvetica, Arial, sans-serif; color:#000000"><font face="Helvetica, Arial, sans-serif" color="#000000" class="size10 Helvetica10">Website Hosting <br>
					</font></li>
				</ul>
			</div>
		</div>
		<!-- </hs:element84> -->
		<!-- <hs:element87> -->
		<div id="element87" style="position: absolute; top: 241px; left: 371px; width: 235px; height: 23px; z-index: 1010;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000066" class="size16 Helvetica16">Services &amp; Capabilities<br>
				</font>
			</div>
		</div>
		<!-- </hs:element87> -->
		<!-- <hs:element91> -->
		<div id="element91" style="position: absolute; top: 164px; left: 693px; width: 179px; height: 192px; z-index: 1011;">
			<div style="overflow: hidden; height: 192px; width: 179px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="192" width="179" style="display: block; border-radius: 0px;" title="" alt="" src="images/IT-Solutions~~element91.jpg"/>
			</div>
		</div>
		<!-- </hs:element91> -->
		
		<!-- <hs:element94> -->
		<div id="element94" style="position: absolute; top: 386px; left: 740px; width: 91px; height: 48px; z-index: 1013;">
			<div style="overflow: hidden; height: 48px; width: 91px; border: 0px solid #52A8EC; border-radius: 0px; box-shadow: none;">
				<img height="48" width="91" style="display: block; border-radius: 0px;" title="" alt="" src="images/IT-Solutions~~element94.gif"/>
			</div>
		</div>
		<!-- </hs:element94> -->
		<!-- <hs:element95> -->
		<div id="element95" style="position: absolute; top: 451px; left: 669px; width: 233px; height: 37px; z-index: 1014;">
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size12 Helvetica12"><b><a target="_blank" href="http://www.gsa.gov/portal/content/105243">GSA STARS II Contract Holder</a></b><br>
				</font>
			</div>
			<div style="font-size: 1px; line-height: 1px;">
				<font face="Helvetica, Arial, sans-serif" color="#000000" class="size12 Helvetica12"><a target="_blank" href="http://www.gsa.gov/portal/content/105243">Contract Number GS-06F-0882Z</a><br>
				</font>
			</div>
		</div>
		<!-- </hs:element95> -->
        
        
      <?php include 'footer.php';?>
        
        
        
	</div>
</div>



		
		
	</body>
</html>
