<?php
include "../pager.php";
include "../MyImageFunction.php";

class CSIReport
{     
   var $tablename;
   var $columns; 
   var $conditions="";
   var $altColor=1;
   var $CallDelete;
   var $CallEdit;
   var $CallActivate;
   var $addFunctionTitle;
   var $addFunctionCaller;
   // Pager Vars
   var $page;
   var $limit=20; // default
   var $result;
   var $total;
   var $offset;
   var $PagerX;
   var $includePager="YES"; // default
   var $caller="";
   var $Title="";
   var $EditTitle="Edit";
   var $DeleteTitle="Delete";

//========================================
// BUILT IN IMPROVED PAGER
//========================================
function setPager($page,$limit)
{   
  $tableName = $this->tablename;  
  $this->result = mysql_query("SELECT count(*) FROM $tableName $this->conditions");     
  $total = mysql_result($this->result, 0, 0);  

  $pager  = Pager::getPagerData($total, $limit, $page); 
  $this->offset = $pager->offset; 
  $this->limit  = $pager->limit; 
  $this->page   = $pager->page;  
  $this->PagerX = $pager;
}

// IMPROVED PAGER        
function showPages($pages,$pager)
{
 $page=$pages;
 print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
 

     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"$this->caller?page=" . 1 . "\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"$this->caller?&page=" . ($page - 1) . "\" target=\"_self\">Prev Page&nbsp;</a>";  
    }
        

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
         echo " | "; 
         if ($i == $pager->page) 
            echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
         else 
            echo "<a href=\"$this->caller?page=$i\" target=\"_self\">$i</a>"; 
         }
    } 

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;"; 
    else 
     if ($SearchCategory != "") 
        {
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>";      
        } 
        else           // not the last page, link to the next page 
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>"; 
          
         echo "&nbsp;&nbsp;<a href=\"$this->caller?page=" . $pager->numPages . "\" target=\"_self\"> >> Last Page</a>";   
         
 print "<b><br><br>";         
} 
  
//-----------------------------------------------------        
// Methood section
//-----------------------------------------------------
        function setDeleteRow($CallString)
        {
          $this->CallDelete=$CallString;
        }


        function setEditTitle($CallString)
        {
          $this->EditTitle=$CallString;
        }
        
        
        function setDeleteTitle($CallString)
        {
          $this->DeleteTitle=$CallString;
        }
                
        
        function setEditRow($CallString)
        {
          $this->CallEdit=$CallString;
        }
        
        
        function setPreCodition($CallString)
        {
          $this->preCondition=$CallString;
        }   
        
        function setCodition($CallString)
        {
          $this->conditions=$CallString;
        }    
                
        function setActivateRow($CallString)
        {
          $this->CallActivate=$CallString;
        }

        function setAddFunction($Title,$CallString)
        {
          $this->addFunctionTitle=$Title;
          $this->addFunctionCaller=$CallString;
        }
        
        function setReportTitle($Title)
        {
          $this->Title=$Title;
        }

//----------------------------------------------
// Define the columns
//----------------------------------------------
        function showColumns($thesecolumn)
        {
          return explode (",",$thesecolumn);
        }

//----------------------------------------------
// Define the columns into table headers
//----------------------------------------------
        function showColumnsHeaders($thesecolumn)
        {
          $headerNameArray=explode (",",$thesecolumn);
          
          $headerNameCount=count($headerNameArray);
          for ($i=0;$i<$headerNameCount;$i++)
          {         
            print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
            print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">$headerNameArray[$i]</font></td>";            
          }
            print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
            print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">ACTION</font></td>";   
        }


     function PrintCell($data)
     {
       if ($this->altColor==1)
        print "<td bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium\">" . $data . "</td>";
       else     
        print "<td bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium\">" . $data . "</td>";
     }

     function PrintCell1($data)
     {
	$THEIMAGE = "../ProductImage/$data";
	$NewSize=myResize($THEIMAGE,120,120);
       if ($this->altColor==1)
        print "<td bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium\"><img border=\"0\" src=\"$THEIMAGE\"" . $NewSize . "></td>";
       else     
        print "<td bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium\"><img border=\"0\" src=\"$THEIMAGE\"" . $NewSize . "></td>";
     }

     function changeRowColor()
     {       
       if ($this->altColor == 1)
       {   
       $this->altColor=0;
       }
       else
       {
       $this->altColor=1;
       }
     }


//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable($page,$limit,$caller)
        {

        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                                 


        $this->setPager($page,$limit);
        $this->caller = $caller;

            
        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"3\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br><br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
        
        $columnsHolder=$this->columns;
        $this->showColumnsHeaders($this->columns);
        
        print "<tr>";
        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
         $sql_vertical="SELECT $this->preCondition $columnsHolder FROM $tableName $this->conditions limit $offset, $limit";

         $result_vertical=mysql_query($sql_vertical);  
         while($row_vertical=mysql_fetch_array($result_vertical))
         {
             
          $arrayCount=count($this->columns);

          for ($i=0;$i<$arrayCount;$i++)
          {
           $THEDATA=$row_vertical[$i];
           $THEDATA = str_replace("\n", "<br>", $THEDATA);
   	   if($i == 1)
	   $this->PrintCell1($THEDATA); // display      
	   else
           $this->PrintCell($THEDATA); // print data
          }
          
          $IID=$row_vertical[id];


        // Print the actions   
        if ($this->CallEdit != "")
        {
          print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
          print "<a href=\"$this->CallEdit?caller=$this->caller&calltype=U&id=$IID\">$this->EditTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        
        if ($this->CallDelete != "")
        {
          print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
          print "<a href=\"$this->CallDelete?$this->caller&id=$IID\">$this->DeleteTitle</td><td bgcolor=\"#FFFFFF\">"; 
        }
           
         print "</td>";
   
         $this->changeRowColor();          
          print "</tr>";      
         }

	print "</table>";

	print "<br>";

	$this->setPager($page,$limit);
        $this->caller = $caller;


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager);
        }
         
        }


}         
?> 