<?php
include_once "CSIReportClass.php";

class CSIForm extends CSIReport
{     
   var $tablename;
   var $columns; 
   var $conditions="";
   var $altColor=1;
   var $CallDelete;
   var $CallEdit;
   var $CallActivate;
   var $addFunctionTitle;
   var $addFunctionCaller;
   // Pager Vars
   var $page;
   var $limit=20; // default
   var $result;
   var $total;
   var $offset;
   var $PagerX;
   var $includePager="YES"; // default
   var $caller="";



  
//-----------------------------------------------------        
// Methood section
//-----------------------------------------------------
        function setDeleteRow($CallString)
        {
          $this->CallDelete=$CallString;
        }

        function setEditRow($CallString)
        {
          $this->CallEdit=$CallString;
        }
        
        function setActivateRow($CallString)
        {
          $this->CallActivate=$CallString;
        }

        function PrintLine($Title,$Data)
        {       
         print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>$Title:&nbsp;</b></td>";
         print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	if($Title == "username")
	{
	print "<input type=\"text\" value=\"$Data\" disabled=\"true\"  size=\"90\">";
	print "<input type=\"hidden\" name=\"$Title\" value=\"$Data\">";
	}
	else if($Title=="status")
	{
	print "<select name=\"$Title\" value=\"$Data\">";
		if($Data=="active")
		$action = "";
		else
		$action = "selected='selected'";
	print "<option value=\"active\">active</option>";
	print "<option value=\"not active\" $action>not active</option>";
	print "</select>";
	}
	else
	print "<input type=\"text\" name=\"$Title\" value=\"$Data\" size=\"90\">";
	print "</td></tr>";
        }

//----------------------------------------------
// Define the columns
//----------------------------------------------
        function showColumns($thesecolumn)
        {
          return explode (",",$thesecolumn);
        }



//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayForm($id,$Title,$caller,$page,$Mes)
        {

        $this->caller = $caller;
	
	print "<br>";
        print "<font face=\"Arial\" size=\"4\" color=\"red\"><b>" . $Mes . "</b></font></p>";
	print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";
	print "<br>";
	print "<font face=\"Arial\" size=\"4\"><a href=\"usersconfig.php\">Back To Admin Listing</a></font>";
	print "<br>";

	$DBUPDATE = "DBAdmin.php";
        
        print "<form method=\"POST\" action=\"$DBUPDATE\">";    
        
        print "<br>";
        print "<font face=\"Arial\"><b>$Title</b></font></p>";                                 
        print "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\" bordercolor=\"#111111\" id=\"AutoNumber1\">";

        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        $sql_vertical="SELECT * FROM $tableName where username='$id'";

        $result_vertical=mysql_query($sql_vertical);  
        while($row_vertical=mysql_fetch_array($result_vertical))
        {
             
          $arrayCount=count($this->columns);
          for ($i=0;$i<$arrayCount;$i++)
          {         
           $THEDATA=$row_vertical[$this->columns[$i]];
           print $this->PrintLine($this->columns[$i],$THEDATA); // print data
          }
         
        }
         
        print "</table>";
        
        print "<br><input type=\"submit\" value=\"Click here to update this record\" name=\"B1\">";
         
        print "</form>";
         
        }


}         
?> 