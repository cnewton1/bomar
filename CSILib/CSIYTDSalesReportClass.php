<?php
include "../pager.php";
include "CSIReportClass.php";


//========================================================================
// Provide better control over the sort by extending the report class
//========================================================================

class CSIOrdersReport extends CSIReport
{

//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable()
        {

		$total=0;
		print "<br>";
		print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                              
		print "<br>";

		if ($this->addFunctionTitle != NULL)
		{
			print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
		} 
		else 
		{ 
			print "<br>";
		}
        
		print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
		
		$columnsHolder=$this->columns;
		$columnsHolder1=str_replace('orderhistory.','',$columnsHolder);
		$columnsHolder1=str_replace('product.','',$columnsHolder1);

		print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\" width=\"33%\"><font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">MONTH</font></td>";

		print "<td  height=\"1\" bgcolor=\"#000000\" align=\"right\" style=\"border-style: solid; border-width: 1\" width=\"33%\"><font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">PRICE</font></td>";

		print "<td  height=\"1\" bgcolor=\"#000000\" align=\"right\" style=\"border-style: solid; border-width: 1\" width=\"34%\"><font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">QUANTITY</font></td>";
		
		$this->columns = $this->showColumns($columnsHolder1);
		$tableName = $this->tablename;

		for($year_num=2007;$year_num<=2015;$year_num++)
		{
		$year_select = mysql_query("select receive_date from $tableName where receive_date LIKE '$year_num%'");

		$num_year = mysql_num_rows($year_select);
		if($num_year==0)
		continue;

		$rec_date = "";
		$old_rec_date = "";

		$grant_total = 0;
		$grant_quantity = 0;

		$total=0;
		$quantity = 0;

		print "<tr>";
		
		$sql_vertical="SELECT $this->preCondition $columnsHolder FROM $tableName where receive_date LIKE '$year_num%' $this->conditions";


		$result_vertical=mysql_query($sql_vertical);

		while($row_vertical=mysql_fetch_array($result_vertical))
		{
		$unit_price=  (double) $row_vertical['unit_price'];
		$ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
		$grant_total+=$ext_price;
		$grant_quantity+=$row_vertical['quantity'];
		$d_date = $row_vertical['receive_date'];
		$d_date_arr = explode('-',$d_date);
		$d = $d_date_arr[0]."-".$d_date_arr[1];
		$rec_date = date('F Y',strtotime($row_vertical['receive_date']));

			if (($rec_date != $old_rec_date)  && ( $old_rec_date != ""))     
			{	
				$array[] = "<a href=\"mlistDODSales.php?date=$old_d\">".$old_rec_date."</a>";
				$array[] = $total;
				$array[] = $quantity;
					
				$old_rec_date = $rec_date;
				$old_d = $d;
				$total=0;
				$quantity = 0;
				$unit_price=  (double) $row_vertical['unit_price'];    
				$ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
				$total=$total +  $ext_price;
				$quantity+=$row_vertical['quantity'];
			}
			else
			{
				$unit_price=  (double) $row_vertical['unit_price'];    
				$ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
				$total=$total +  $ext_price;

				$quantity+=$row_vertical['quantity'];

				$rec_date = date('F Y',strtotime($row_vertical['receive_date']));

				$old_rec_date = $rec_date;
				$old_d = $d;
			}
	         } // end of while($row_vertical=mysql_fetch_array($result_vertical))

		$array[] = "<a href=\"mlistDODSales.php?date=$d\">".$rec_date."</a>";
		$array[] = $total;
		$array[] = $quantity;

		$arrayCount=count($array);
		$j=0;
		for ($i=0;$i<$arrayCount;$i++,$j++)
		{
			$THEDATA=$array[$i];
		
			if (($j == 1))
			{
			$THEDATA = sprintf ("\$%4.2f",$THEDATA);
			}
	
			$THEDATA = str_replace("\n", "<br>", $THEDATA);
		
			if($i%3 == 1 || $i%3 == 2)
			$this->PrintCell_price($THEDATA);
			else
			$this->PrintCell($THEDATA); // print data

			if($j==2){
			$this->changeRowColor();
			print "</tr><tr>";
			$j=-1;
			}
		
			if (($array[$i] == "order_number"))
			{
			$ORDERNUMBER = $array[$i];
			}
		}

		print "</tr><tr><td colspan=\"3\">";

		print "<table width=\"100%\" bgcolor=\"green\"><tr><td width=\"33.33%\"></td>";

		print "<td width=\"33.33%\" align=\"right\"><b><font color=\"white\">Grand YTD Total : ".sprintf ("\$%4.2f",$grant_total)."</font></b></td>";

		print "<td width=\"33.34%\" align=\"right\"><b><font color=\"white\">Grand YTD Quantity : $grant_quantity</font></b></td>";

		print "</tr>";

		print "</table></td>";

		$array = array();

		print "</tr><tr>";
		} //End of Year forloop

		print "</tr></td><table>";
	} // end of function displayTable()
}
?> 