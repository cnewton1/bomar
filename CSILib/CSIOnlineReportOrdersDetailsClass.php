<?php
include "../pager.php";
include "CSIReportClass.php";


//========================================================================
// Provide better control over the sort by extending the report class
//========================================================================

class CSIOrdersReportDetail extends CSIReport
{     

  var $ORDERNUM="";
  var $PO_NUMBER="";
  var $TRANS_LINK_ID="";
//========================================
// BUILT IN IMPROVED PAGER
//========================================
function setPager($page,$limit)
{   
  $tableName = $this->tablename;  
  $this->result = mysql_query("SELECT count(*) FROM $tableName $this->conditions");     
  $total = mysql_result($this->result, 0, 0);  

  $pager  = Pager::getPagerData($total, $limit, $page); 
  $this->offset = $pager->offset; 
  $this->limit  = $pager->limit; 
  $this->page   = $pager->page;  
  $this->PagerX = $pager;
 
}

// IMPROVED PAGER        
function showPages($pages,$pager,$keyword,$mom)
{
 $page=$pages;
 print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
 

     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"$this->caller?page=" . 1 . "&keyword=$keyword&mom=$mom\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"$this->caller?&page=" . ($page - 1) . "&keyword=$keyword&mom=$mom\" target=\"_self\">Prev Page&nbsp;</a>";  
    }
        

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
         echo " | "; 
         if ($i == $pager->page) 
            echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
         else 
            echo "<a href=\"$this->caller?page=$i&keyword=$keyword&mom=$mom\" target=\"_self\">$i</a>"; 
         }
    } 

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;"; 
    else 
     if ($SearchCategory != "") 
        {
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "&keyword=$keyword&mom=$mom\" target=\"_self\">Next Page</a>";      
        } 
        else           // not the last page, link to the next page 
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "&keyword=$keyword&mom=$mom\" target=\"_self\">Next Page</a>"; 
          
         echo "&nbsp;&nbsp;<a href=\"$this->caller?page=" . $pager->numPages . "&keyword=$keyword&mom=$mom\" target=\"_self\"> >> Last Page</a>";   
         
 print "<b><br><br>";         
} 

//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable($page,$limit,$caller,$keyword,$mom)
        {
        $total=0;
        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                              
	print "<br>";
	print "<font face=\"Arial\" size=\"4\"><a href=\"listOnlineOrders.php?page=$page&keyword=$keyword&mom=$mom\">".$this->Link1."</a></font>";
	print "<br>";
	print "<br>"; 

       print "<form>"; // This form does not really post anything instead we use buttons.
             
        $this->setPager($page,$limit);
        $this->caller = $caller;

        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         //$this->showPages($page,$pager,$keyword,$mom);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
        
        $columnsHolder=$this->columns;
        $this->showColumnsHeaders($this->columns);
        
        print "<tr>";
        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        
        $OLDORDERNUMBER="";
        $ORDERNUMBER ="";
        
         $sql_vertical="SELECT $this->preCondition * FROM $tableName $this->conditions limit $offset, $limit";
         $result_vertical=mysql_query($sql_vertical);  
         while($row_vertical=mysql_fetch_array($result_vertical))
         {
             
                       // PLACE LOGIC READ
        $ORDERNUMBER = $row_vertical['order_number'];    
        $this->ORDERNUM = $row_vertical['order_number'];  
        $this->PO_NUMBER = $row_vertical['purchase_order'];
        
        $this->TRANS_LINK_ID = $row_vertical['translink'];
        
                       
         if (($ORDERNUMBER != $OLDORDERNUMBER)  && ( $OLDORDERNUMBER != ""))     
         {
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;\$$total </font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td><br> </td></tr>";   
            print "<tr><td><br> </td></tr>";     
                                    
            $OLDORDERNUMBER = $ORDERNUMBER;   
            $total=0;  
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;            
         }
          else
         {
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;    
             
            $ORDERNUMBER = $row_vertical['order_number'];   
           $OLDORDERNUMBER = $ORDERNUMBER;
         }
             
             
             
             
          $arrayCount=count($this->columns);
          for ($i=0;$i<$arrayCount;$i++)
          {         
                  
           $THEDATA=$row_vertical[$this->columns[$i]];

           if (($this->columns[$i] == "unit_price"))
           {
            $THEDATA = sprintf ("\$%4.2f",$THEDATA);
           }

           $THEDATA = str_replace("\n", "<br>", $THEDATA);
          
           $this->PrintCell($THEDATA); // print data
           
          
           if (($this->columns[$i] == "order_number"))
           {
            $ORDERNUMBER = $row_vertical[$this->columns[$i]];
           }
           
           if (($this->columns[$i] == "purchase_order"))
           {
            $PO_NUMBER = $row_vertical[$this->columns[$i]];
           }           
              
          }
          
          $IID=$row_vertical[id];
         

        // Print the actions   
        if ($this->CallEdit != "")
        {
          print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
          print "<a href=\"$this->CallEdit?caller=$this->caller&calltype=U&id=$IID&page=$page&keyword=$keyword&mom=$mom\">$this->EditTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        
        if ($this->CallDelete != "")
        {
          print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
          print "<a href=\"$this->CallDelete?$this->caller&calltype=D&id=$IID&keyword=$keyword&mom=$mom\">$this->DeleteTitle</td><td bgcolor=\"#FFFFFF\">"; 
        }
           
         print "</td>";
   
         $this->changeRowColor();          
         print "</tr>";      

         }
         // PRINT THE LAST TOTAL ROW
         
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL:".sprintf ("\$%4.2f",$total)."</b></font></td></tr>";   
            print "<tr><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td></td><td></td>";
            print "<td></td></tr>";    
       
          
          // FORM DATA
          // THESE BUTTONS ARE REALLY ADDED TO THE TOP
            print "<a href=\"$MAINURL/manage/SendPO.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\"><img border=\"0\" src=\"../images/BPurchaseOrder.png\"></a>";
            
            print "<a href=\"https://www.westcarb.com/new/manage/ProcessPOST.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BProcessCredit.png\"></a>";
            
            print "<a href=\"https://www.westcarb.com/new/manage/ProcessPOST.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&PROCESSTYPE=CR\">";            
            print "<img border=\"0\" src=\"../images/BIssueRefund.png\"></a>";            
           
//
            print "<a href=\"SendInvoice.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\"><img border=\"0\" src=\"../images/BCUSINVOICE.png\"></a>";
            print "<a href=\"SendOMXML.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BOMPO.png\"></a>";        
       
       
            print "<a href=\"SendxCBL.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BEnporion.gif\"></a>";  
                   
       
            print "<p></table>";
            print "</form>";


	        $this->setPager($page,$limit);
        $this->caller = $caller;

            
        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         //$this->showPages($page,$pager);
        }
        }


}         



?> 