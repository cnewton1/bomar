<?php
include "../pager.php";
include "CSIReportClass.php";


//========================================================================
// Provide better control over the sort by extending the report class
//========================================================================

class CSIOrdersReportDetail extends CSIReport
{     

  var $ORDERNUM="";
  var $PO_NUMBER="";
  var $TRANS_LINK_ID="";
//========================================
// BUILT IN IMPROVED PAGER
//========================================
function setPager($page,$limit)
{   
  $tableName = $this->tablename;  
  $this->result = mysql_query("SELECT count(*) FROM $tableName $this->conditions");     
  $total = mysql_result($this->result, 0, 0);  

  $pager  = Pager::getPagerData($total, $limit, $page); 
  $this->offset = $pager->offset; 
  $this->limit  = $pager->limit; 
  $this->page   = $pager->page;  
  $this->PagerX = $pager;
 
}

// IMPROVED PAGER        
function showPages($pages,$pager,$keyword,$mom,$status)
{
 $page=$pages;
 print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
 

     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"$this->caller?page=" . 1 . "&keyword=$keyword&mom=$mom&status=$status\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"$this->caller?&page=" . ($page - 1) . "&keyword=$keyword&mom=$mom&status=$status\" target=\"_self\">Prev Page&nbsp;</a>";  
    }
        

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
         echo " | "; 
         if ($i == $pager->page) 
            echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
         else 
            echo "<a href=\"$this->caller?page=$i&keyword=$keyword&mom=$mom&status=$status\" target=\"_self\">$i</a>"; 
         }
    } 

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;"; 
    else 
     if ($SearchCategory != "") 
        {
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "&keyword=$keyword&mom=$mom&status=$status\" target=\"_self\">Next Page</a>";      
        } 
        else           // not the last page, link to the next page 
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "&keyword=$keyword&mom=$mom&status=$status\" target=\"_self\">Next Page</a>"; 
          
         echo "&nbsp;&nbsp;<a href=\"$this->caller?page=" . $pager->numPages . "&keyword=$keyword&mom=$mom&status=$status\" target=\"_self\"> >> Last Page</a>";   
         
 print "<b><br><br>";         
} 

//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable($page,$limit,$caller,$keyword,$mom,$status,$IID)
        {
        $total=0;
	$sup_total = 0;
        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                              
	print "<br>";
	print "<font face=\"Arial\" size=\"4\"><a href=\"mlistDODorders.php?page=$page&keyword=$keyword&mom=$mom&status=$status\">".$this->Link1."</a></font>";
	print "<br>";
	print "<br>"; 

       print "<form>"; // This form does not really post anything instead we use buttons.
             
        $this->setPager($page,$limit);
        $this->caller = $caller;

        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager,$keyword,$mom,$status);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
        
        $columnsHolder=$this->columns;
        $this->showColumnsHeaders1($this->columns);
        
        print "<tr>";
        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        
        $OLDORDERNUMBER="";
        $ORDERNUMBER ="";
	$oldsupplier = "";
	$supplier = "";
        
         $sql_vertical="SELECT $this->preCondition * FROM $tableName $this->conditions limit $offset, $limit";

         $result_vertical=mysql_query($sql_vertical); 

$row_vertical=mysql_fetch_array($result_vertical);

// print "<pre>";
// print_r($row_vertical);
// print "</pre>";
// exit;
 
         while($row_vertical=mysql_fetch_array($result_vertical))
         {
                       // PLACE LOGIC READ
        $ORDERNUMBER = $row_vertical['order_number'];
	$oldsupplier = $supplier;
	$THEDATA1 = $row_vertical['supplier'];
	$supplier = $THEDATA1;

	if($supplier != $oldsupplier && $total != 0){
	$sup_total = $total-$sup_total;
	print "<td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td bgcolor=\"#000000\"><font color=\"#FFFFFF\"><b>SUB TOTAL: &nbsp;&nbsp;\$$sup_total </font></td></tr>";   
            print "<tr><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td></td><td></td>";
            print "<td></td></tr><tr>";
	$sup_total = $total;
	}
	$this->ORDERNUM = $row_vertical['order_number'];  
        $this->PO_NUMBER = $row_vertical['purchase_order'];

        $this->TRANS_LINK_ID = $row_vertical['translink'];
	$id = $row_vertical['id'];

         if (($ORDERNUMBER != $OLDORDERNUMBER)  && ( $OLDORDERNUMBER != ""))     
         {
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;\$$total </font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td><br> </td></tr>";   
            print "<tr><td><br> </td></tr>";     
                                    
            $OLDORDERNUMBER = $ORDERNUMBER;
            $total=0;
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;
         }
          else
         {
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;    
             
            $ORDERNUMBER = $row_vertical['order_number'];   
           $OLDORDERNUMBER = $ORDERNUMBER;
         }

          $arrayCount=count($this->columns);

          for ($i=0;$i<$arrayCount;$i++)
          { 

	$tmp = trim($this->columns[$i]);
	$tmp = str_replace('online_','',$tmp);

	if($tmp == "description")
	$tmp = 18;

	$THEDATA = $row_vertical[$tmp];

           $THEDATA = str_replace("\n", "<br>", $THEDATA);
          
           $this->PrintCell($THEDATA); // print data
           
          
           if (($this->columns[$i] == "order_number"))
           {
            $ORDERNUMBER = $row_vertical[$this->columns[$i]];
           }
           
           if (($this->columns[$i] == "purchase_order"))
           {
            $PO_NUMBER = $row_vertical[$this->columns[$i]];
           }           
              
          }
         
	print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";

        // Print the actions   
        if ($this->CallEdit != "")
        {
          print "<a href=\"$this->CallEdit?caller=$this->caller&calltype=U&id=$IID&page=$page&keyword=$keyword&mom=$mom&status=$status\">$this->EditTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        
        if ($this->CallDelete != "")
        {
          print "<a href=\"$this->CallDelete?$this->caller&calltype=U&ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&page=$page&keyword=$keyword&mom=$mom&status=$status&id=$id\">$this->DeleteTitle<a>"; 
        }


         print "</td>";
   
         $this->changeRowColor();          
         print "</tr>";

         }
	// PRINT LAST SUB TOTAL

	if($total != 0){
	$sup_total = $total-$sup_total;
	    print "<td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td bgcolor=\"#000000\"><font color=\"#FFFFFF\"><b>SUB TOTAL: &nbsp;&nbsp;\$$sup_total </font></td></tr>";   
            print "<tr><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td></td><td></td>";
            print "<td></td></tr><tr>";
	}

         // PRINT THE LAST TOTAL ROW
         
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;\$$total </font></td></tr>";   
            print "<tr><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td></td><td></td>";
            print "<td></td></tr>";    
       
          
          // FORM DATA
          // THESE BUTTONS ARE REALLY ADDED TO THE TOP
            print "<a href=\"ProcessPOSummery.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&caller=$caller&id=$IID&page=$page&keyword=$keyword&mom=$mom&status=$status\"><img border=\"0\" src=\"../images/POSummery.png\"></a>";
            
            print "<a href=\"https://www.westcarb.com/new/manage/ProcessPOST.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BProcessCredit.png\"></a>";
            
            print "<a href=\"https://www.westcarb.com/new/manage/ProcessPOST.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&PROCESSTYPE=CR\">";            
            print "<img border=\"0\" src=\"../images/BIssueRefund.png\"></a>";            
           
//
            print "<a href=\"SendInvoice.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\"><img border=\"0\" src=\"../images/BCUSINVOICE.png\"></a>";
            print "<a href=\"SendOMXML.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BOMPO.png\"></a>";        
       
            print "<a href=\"AddOrderItem.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&page=$page&keyword=$keyword&mom=$mom&status=$status\">";            
            print "<img border=\"0\" src=\"../images/ADDORDERITEM.png\"></a>";  
     
            print "<a href=\"http://www.westcarb.com/new/manage/SendPO.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\"><img border=\"0\" src=\"../images/BPurchaseOrder.png\"></a>";     
     
//            print "<a href=\"SendxCBL.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
//            print "<img border=\"0\" src=\"../images/BEnporion.gif\"></a>";       
                   
       
            print "<p></table>";
            print "</form>";


	        $this->setPager($page,$limit);
        $this->caller = $caller;

            
        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager);
        }

        }

        function displayTable1($page,$limit,$caller,$keyword,$mom,$status,$IID)
        {
        $total=0;
	$sup_total = 0;
        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                              
	print "<br>";
	print "<font face=\"Arial\" size=\"4\"><a href=\"mlistDODorders.php?page=$page&keyword=$keyword&mom=$mom&status=$status\">".$this->Link1."</a></font>";
	print "<br>";
	print "<br>"; 

       print "<form>"; // This form does not really post anything instead we use buttons.
             
        $this->setPager($page,$limit);
        $this->caller = $caller;

        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager,$keyword,$mom,$status);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";

	print "<tr>";

        print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><b>Translink</b></font></td>";
	print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><b>Supplier</b></font></td>";
	print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><b>Status</b></font></td>";
	print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><b>Sub Total</b></font></td>";
	print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><b>No. Of Items</b></font></td>";
	print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\"><b>ACTION</b></font></td>";
        
        print "</tr><tr>";
        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        
        $OLDORDERNUMBER="";
        $ORDERNUMBER ="";
	$oldsupplier = "";
	$supplier = "";
        
         $sql_vertical="SELECT $this->preCondition * FROM $tableName $this->conditions limit $offset, $limit";
         $result_vertical=mysql_query($sql_vertical);

	$quantity1 = 0;
	$quantity2 = 0;
	$bgcolor = "#FFECC6";

         while($row_vertical=mysql_fetch_array($result_vertical))
         {
        $ORDERNUMBER = $row_vertical['order_number'];
	$oldsupplier = $supplier;
	$THEDATA1 = $row_vertical['supplier'];
	$supplier = $THEDATA1;

	if($supplier != $oldsupplier && $total != 0){
	$sup_total = $total-$sup_total;
	$sup_total = $total;
	}
	$this->ORDERNUM = $row_vertical['order_number'];  
        $this->PO_NUMBER = $row_vertical['purchase_order'];

        $this->TRANS_LINK_ID = $row_vertical['translink'];
	$id = $row_vertical['id'];

         if (($ORDERNUMBER != $OLDORDERNUMBER)  && ( $OLDORDERNUMBER != ""))     
         {
            $OLDORDERNUMBER = $ORDERNUMBER;
            $total=0;
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;
         }
          else
         {
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;    
             
            $ORDERNUMBER = $row_vertical['order_number'];   
           $OLDORDERNUMBER = $ORDERNUMBER;
         }

          $arrayCount=count($this->columns);

          for ($i=0;$i<$arrayCount;$i++)
          {
	$tmp = trim($this->columns[$i]);

	$THEDATA = $row_vertical[$tmp];

           $THEDATA = str_replace("\n", "<br>", $THEDATA);

           if (($this->columns[$i] == "order_number"))
           {
            $ORDERNUMBER = $row_vertical[$this->columns[$i]];
           }

           if (($this->columns[$i] == "purchase_order"))
           {
            $PO_NUMBER = $row_vertical[$this->columns[$i]];
           }

           if (($this->columns[$i] == "translink"))
           {
            $translink = $row_vertical[$this->columns[$i]];
           }
          }
	$quantity3 = $row_vertical[quantity];

	if($supplier != $oldsupplier && $sup_total != 0){
	print "<td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$translink</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$oldsupplier</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$status</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$sup_total</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$quantity1</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\"><a href=\"POTranslator.php?supplier=$oldsupplier&ordernum=$ORDERNUMBER&translink=$translink\">Process</a></td>";

	if($bgcolor == "#FFECC6")
	$bgcolor = "#FFFFFF";
	else
	$bgcolor = "#FFECC6";

	$quantity2+=$quantity1;
	}

        $quantity1+=$quantity3;
	if($supplier != $oldsupplier && $sup_total != 0){
	$quantity1=$quantity3;
	}
   
         $this->changeRowColor();          
         print "</tr>";

         }

	// PRINT LAST SUB TOTAL

	if($total != 0){
	$sup_total = $total-$sup_total;
	$quantity2+=$quantity1;
	print "<tr>";
	print "<td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$translink</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$supplier</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$status</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$sup_total</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\">$quantity1</td><td bgcolor=\"$bgcolor\" style=\"border-style: none; border-width: medium\"><a href=\"POTranslator.php?supplier=$supplier&ordernum=$ORDERNUMBER&translink=$translink\">Process</a></td>";
	print "</tr><tr>";
	}

         // PRINT THE LAST TOTAL ROW
         
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;\$$total </font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td>";
            print "<td></td></tr>";

	 	print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td bgcolor=\"#000000\"><font color=\"#FFFFFF\"><b>TOTAL QUANTITY: &nbsp;&nbsp;$quantity2 </font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td>";
            print "<td></td></tr>";
       
          
          // FORM DATA
          // THESE BUTTONS ARE REALLY ADDED TO THE TOP
            print "<a href=\"ProcessPOSummery.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&caller=$caller&id=$IID&page=$page&keyword=$keyword&mom=$mom&status=$status\"><img border=\"0\" src=\"../images/POSummery.png\"></a>";
            
            print "<a href=\"https://www.westcarb.com/new/manage/ProcessPOST.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BProcessCredit.png\"></a>";
            
            print "<a href=\"https://www.westcarb.com/new/manage/ProcessPOST.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&PROCESSTYPE=CR\">";            
            print "<img border=\"0\" src=\"../images/BIssueRefund.png\"></a>";            
           
//
            print "<a href=\"SendInvoice.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\"><img border=\"0\" src=\"../images/BCUSINVOICE.png\"></a>";
            print "<a href=\"SendOMXML.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
            print "<img border=\"0\" src=\"../images/BOMPO.png\"></a>";        
       
            print "<a href=\"AddOrderItem.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID&page=$page&keyword=$keyword&mom=$mom&status=$status\">";            
            print "<img border=\"0\" src=\"../images/ADDORDERITEM.png\"></a>";  
     
            print "<a href=\"http://www.westcarb.com/new/manage/SendPO.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\"><img border=\"0\" src=\"../images/BPurchaseOrder.png\"></a>";     
     
//            print "<a href=\"SendxCBL.php?ordernum=$this->ORDERNUM&po_number=$this->PO_NUMBER&TRANSLINK=$this->TRANS_LINK_ID\">";            
//            print "<img border=\"0\" src=\"../images/BEnporion.gif\"></a>";       
                   
       
            print "<p></table>";
            print "</form>";


	        $this->setPager($page,$limit);
        $this->caller = $caller;

            
        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager);
        }

        }

function showColumnsHeaders1($thesecolumn)
        {
          $headerNameArray=explode (",",$thesecolumn);
          
          $headerNameCount=count($headerNameArray);
          for ($i=0;$i<$headerNameCount;$i++)
          {         
            print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
	$header = $headerNameArray[$i];
	$header = str_replace("online_orderhistory.","",$header);
	$header = str_replace("product.","",$header);
		
            	print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">".$header."</font></td>"; 
          }
            print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
            print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">ACTION</font></td>";   
        }
}         



?> 