<?php
include_once "CSIReportClass.php";

class CSIForm extends CSIReport
{     
   var $tablename;
   var $columns; 
   var $conditions="";
   var $altColor=1;
   var $CallDelete;
   var $CallEdit;
   var $CallActivate;
   var $addFunctionTitle;
   var $addFunctionCaller;
   // Pager Vars
   var $page;
   var $limit=20; // default
   var $result;
   var $total;
   var $offset;
   var $PagerX;
   var $includePager="YES"; // default
   var $caller="";



  
//-----------------------------------------------------        
// Methood section
//-----------------------------------------------------
        function setDeleteRow($CallString)
        {
          $this->CallDelete=$CallString;
        }

        function setEditRow($CallString)
        {
          $this->CallEdit=$CallString;
        }
        
        function setActivateRow($CallString)
        {
          $this->CallActivate=$CallString;
        }

//----------------------------------------------
// Define the columns
//----------------------------------------------
        function showColumns($thesecolumn)
        {
          return explode (",",$thesecolumn);
        }



//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayForm($Title,$caller,$page,$keyword,$mom,$Mes,$ORDERNUMBER1,$PO_NUMBER1,$TRANSLINK,$status,$part_number,$unit_price,$quantity,$unit_of_measure,$description,$aux_part_number,$line_number,$receive_date,$calltype,$id)
        {
        $this->caller = $caller;

	$this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        $sql_vertical="SELECT * FROM $tableName where order_number='$ORDERNUMBER1'";

        $result_vertical=mysql_query($sql_vertical);

 	$row_vertical=mysql_fetch_array($result_vertical);

	if($id=="")
	$id = $row_vertical['id'];

	print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";
	print "<br>";
	print "<font face=\"Arial\" size=\"4\"><a href=\"mlistDODordersDetailList.php?caller=$caller&calltype=U&id=$id&page=$page&keyword=$keyword&mom=$mom&status=$status\">".$this->Link1."</a></font>";
	print "<br>";

	$DBUPDATE = "DBAddOrderHistory.php";
        
        print "<form method=\"POST\" action=\"$DBUPDATE\">";
        
        print "<br>";
        print "<font face=\"Arial\"><b>$Title</b></font>&nbsp;&nbsp;&nbsp;&nbsp;";
        print "<font face=\"Arial\" size=\"4\" color=\"red\"><b>" . $Mes . "</b></font></p>";
        print "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-width: 0\" bordercolor=\"#111111\" id=\"AutoNumber1\">";

	$email = $row_vertical['email'];
	$phone = $row_vertical['phone'];
	$ship_via = $row_vertical['ship_via'];
	$internal_reference = $row_vertical['internal_reference'];
	$transmitted = $row_vertical['transmitted'];
	$contract = $row_vertical['contract'];
	$vendor_rebate_code = $row_vertical['vendor_rebate_code'];
	$nsn = $row_vertical['nsn'];
	
 
	print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>order_number:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"order_number\" value=\"$ORDERNUMBER1\" readonly=\"true\"  size=\"90\">";
	print "<input type=\"hidden\" name=\"id\" value=\"$id\">";
	print "</td></tr>";

	print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>email:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"email\" value=\"$email\" readonly=\"true\" size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>phone:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"phone\" value=\"$phone\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

	print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>purchase_order:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"purchase_order\" value=\"$PO_NUMBER1\" readonly=\"true\"  size=\"90\">";
	print "<input type=\"hidden\" name=\"translink\" value=\"$TRANSLINK\">";
	print "<input type=\"hidden\" name=\"keyword\" value=\"$keyword\">";
	print "<input type=\"hidden\" name=\"status\" value=\"$status\">";
	print "<input type=\"hidden\" name=\"mom\" value=\"$mom\">";
	print "<input type=\"hidden\" name=\"page\" value=\"$page\">";
	print "</td></tr>";

	print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>line_number:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"line_number\" value=\"$line_number\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>part_number:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"part_number\" value=\"$part_number\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>ship_via:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"ship_via\" value=\"$ship_via\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>unit_price:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"unit_price\" value=\"$unit_price\" size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>quantity:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"quantity\" value=\"$quantity\" size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>unit_of_measure:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"unit_of_measure\" value=\"$unit_of_measure\" size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>description:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"description\" value=\"$description\" size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>internal_reference:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"internal_reference\" value=\"$internal_reference\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b> transmitted:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\" transmitted\" value=\"$transmitted\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>contract:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"contract\" value=\"$contract\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>vendor_rebate_code:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"vendor_rebate_code\" value=\"$vendor_rebate_code\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>nsn:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"nsn\" value=\"$nsn\" readonly=\"true\"  size=\"90\">";
	print "</td></tr>";

        print "<tr><td width=\"25%\" style=\"border-style: none; border-width: none\" align=\"right\" bgcolor=\"#FFEBC1\"> <font face=\"Arial\" size=\"2\"><b>aux_part_number:&nbsp;</b></td>";
        print "<td width=\"75%\" style=\"border-style: none; border-width: medium\">";
	print "<input type=\"text\" name=\"aux_part_number\" value=\"$aux_part_number\" size=\"90\">";
	print "<input type=\"hidden\" name=\"receive_date\" value=\"$receive_date\">";
	print "<input type=\"hidden\" name=\"calltype\" value=\"$calltype\">";
	print "</td></tr>";
         
        print "</table>";
        
	if($calltype == "U")
	print "<br><input type=\"submit\" value=\"Click here to Change this record\" name=\"B1\">";
	else
        print "<br><input type=\"submit\" value=\"Click here to Add this record\" name=\"B1\">";
         
        print "</form>";
         
        }
}         
?> 