
<?php
include "../pager.php";
//include "CSIReportClass.php";

//**** Order list function
function customer_info($email)
{
	$select_query = mysql_query("SELECT `FName`,`LName` FROM `product_order` WHERE `user_email` = '$email' LIMIT 0,1");
	$result=mysql_fetch_array($select_query);
	$fname=$result['FName'];
	$lname=$result['LName'];
	return $fname. " " .$lname;
	
}

function showReport($email)
{
	global $offset;
	global $limit;
		
  if($email != "")
  {
	$where_clause=" WHERE `user_email` = '$email'";
	$page_heading="Order History for <u> ". customer_info($email)."</u><br>"; 
	echo "<div style=\"float:right; padding-right:300px; clear:both;\"><a href=\"listOnlineOrders.php\" >Back to Main List </a></div>"; 
  }
  
  $select_query = mysql_query("SELECT * FROM `product_order` $where_clause ORDER BY `id` DESC limit $offset,$limit");
  if(mysql_num_rows($select_query)!=0)
  {
	  ?>
      
      <strong><?php echo $page_heading."<br><br>";?></strong>
      <table width="900px" cellpadding="5" border="0" style="border-collapse:collapse; border-width:1px">
      <thead><tr>
      <td scope="col" style="background-color:#000; color:#FFF;" height="25px">Order Date</td>
      <td scope="col" style="background-color:#000; color:#FFF;" height="25px">Customer Name</td>
      <td scope="col" style="background-color:#000; color:#FFF;" height="25px">Order No.</td>
      <td scope="col" style="background-color:#000; color:#FFF;" height="25px">No. of Items</td>
      <td scope="col" style="background-color:#000; color:#FFF;" height="25px">Total Quantity</td>
      <!--<td scope="col" style="background-color:#000; color:#FFF;" height="25px">Total Cost</td>-->
      <td scope="col" style="background-color:#000; color:#FFF;" height="25px">View Order</td>
      </tr></thead>
	  <?php
	  
		$count =0;
		while($result = mysql_fetch_array($select_query))
		{ 
			$item_arr=explode(',',$result['items']);
			$count_item=count($item_arr);
			
			$unique_item_arr = array_unique($item_arr);
			
			if($count&1)
			{
				$trClass = "class=\"odd\"";
				$td_style= "bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium\"";
			}
			else
			{
				$trClass = '';
				$td_style= "bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium\"";
			}
			?>
			<tr>
           
			  <td <?php echo $td_style; ?>><?php echo date('M d, Y',strtotime($result['timestamp'])); ?></td>
              <td <?php echo $td_style; ?>><a href="listOnlineOrders.php?email=<?php echo $result['user_email']; ?>" title="View All Orders of <?php echo $result['FName']." ".$result['LName']; ?>"> <strong><?php echo $result['FName']." ".$result['LName']; ?></strong></a></td>
			  <td <?php echo $td_style; ?>><a href="orderHistoryDetails.php?orderid=<?php echo $result['id']; ?>">Order #<?php echo $result['id']; ?></a></td>
			  <td <?php echo $td_style; ?>><?php echo count($unique_item_arr);?></td>
              <td <?php echo $td_style; ?>><?php echo $count_item;  ?></td>
			  <!--<td <?php echo $td_style; ?>><?php echo $result['total']; ?></td>-->
              <td <?php echo $td_style; ?>><a href="orderHistoryDetails.php?orderid=<?php echo $result['id']; ?>" title="View Order No. <?php echo $result['id']; ?>">View Current Detail</a></td>
			</tr>
			<?php 
			$count++;
		 }
		 
		 echo  "</table>";
   }
   else
   {
	   echo "<br><strong>You have not ordered any product.</strong>";
	   
   }
}

function orderReport($OID)
{
	
	$order_query = mysql_query("SELECT * FROM `product_order` WHERE `id` = $OID");
	$order_result = mysql_fetch_array($order_query);
	
	$emailContent = '';
	
    
     $emailContent .= '<form method="post" action="orderHistoryDetails.php" ><table border="0" cellpadding="3" cellspacing="0" width="100%">		
    <tbody>
	<tr>
	<td height="24" ><font size=\"2\"><b>Customer Details</b></font></td>
	<td height="24"><a href="listOnlineOrders.php"><font size=\"2\"><strong>Back to Main List</strong></font></a></td>
	<td height="24"><font size=\"2\"><b>Order Date:</b> '.date('m/d/Y',strtotime($order_result['timestamp'])).' </font></td>
	</tr>
    </tbody></table> </form>	
    
    <table>		<tbody><tr style="height: 8px;"><td></td></tr>	</tbody></table>
    <table id="receipt"><tbody>
    <tr>
    <td style="text-align: left; width: 170px; font-weight: bold;">Customer Name:</td>
    <td>'.$order_result['FName'].' '.$order_result['LName'].'</td>    
    </tr>
     <tr><td style="text-align: left; width: 170px; font-weight: bold;">Billing Email Address:</td>			
   <td>'.$order_result['bill_email'].'</td></tr>   
   
    <tr><td style="text-align: left; width: 170px; font-weight: bold;">Phone:</td>			
   <td>'.$order_result['phone'].'</td></tr>  
    </tbody></table>	
	
   <table>		<tbody><tr style="height: 8px;"><td></td></tr>	</tbody></table>	
   
   <table border="0" cellpadding="3" cellspacing="0" >		<tbody><tr><td height="24"><font size=\"2\"><b>Address Details</b></font></td></tr>	</tbody></table>	
   
   <table>		<tbody><tr style="height: 8px;"><td></td></tr>	</tbody></table>	
   
   <table id="receipt" cellpadding="0" cellspacing="0">		 <tbody><tr>
   <td width="50%">		
   <table width="100%" cellspacing="1"> 			<tbody>
   <tr><td width="20">&nbsp;</td>				<td><b><u>Billing</u></b></td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['FName'].' '.$order_result['LName'].'</td></tr>	
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['address1'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['address2'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['city'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['state'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['zip'].'</td></tr>	
   
   </tbody></table>
   
   </td>	<td width="4">&nbsp;</td>	
   
   <td width="50%">	
   <table width="100%" cellspacing="1"> 		<tbody>
   <tr><td width="20">&nbsp;</td>			<td><b><u>Shipping</u></b></td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['FName'].' '.$order_result['LName'].'</td></tr>	
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['saddress1'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['saddress2'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['scity'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['sstate'].'</td></tr>
   <tr><td width="20">&nbsp;</td>				<td>'.$order_result['szip'].'</td></tr>
   
   </tbody>
   </table>
   </td>
   </tr>
   </tbody>
   </table>
   
   <table>		<tbody><tr style="height: 8px;"><td></td></tr>	</tbody></table>';
   
   
	
	$items = $order_result['items'];
	$items = explode(",",$items);
	$contents = array();
	foreach ($items as $item)
	{
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}
	
	$emailContent .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" style=\"border-collapse:collapse; border-width:1px\">
	  <tr  >
		<td align=\"left\"  style=\"background-color:black; color:white;\"><strong>Item</strong></td>
		<td align=\"left\"  style=\"background-color:black; color:white;\"><strong>Image</strong></td>
		
		<td align=\"left\"  style=\"background-color:black; color:white;\"><strong>Company Name</strong></td>
		<td align=\"left\"  style=\"background-color:black; color:white;\"><strong>Company Abbr</strong></td>
		
		<td width=\"80\" style=\"background-color:black; color:white;\"><strong>Unit Price</strong></td>
		<td width=\"80\" style=\"background-color:black; color:white;\"><strong>Qty</strong></td>
		<td width=\"100\" style=\"background-color:black; color:white;\"><strong>Cost</strong></td>
	  </tr>";
	
	$subtotal = 0.00;
	foreach($contents as $productID => $qty)
	{
		$field_to_price = "retailcustomerprice";
		
		//echo "SELECT * FROM `product` WHERE `id` = $productID";
		$product_query = mysql_query("SELECT * FROM `product` WHERE `id` = $productID");
		$product_result = mysql_fetch_array($product_query);
		
		//--------For showing all prices as '0'------------
		$price=$product_result[$field_to_price];
		$price=0;
		//-------------------------------------------------
		
		$cost = $price*$qty;
		$subtotal += $cost;
		
		$imageName=$product_result['filename'];
		$item_num = $product_result['item_num'];
		$description = $product_result['description'];
		
		$imageName = check_file1($imageName);
		$THEIMAGE="../ProductImage/".$imageName;
		$NewSize=myResize($THEIMAGE,100,100);
		
		
		if($count&1)
		{
			$trClass = "class=\"odd\"";
			$td_style= "bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium\"";
		}
		else
		{
			$trClass = '';
			$td_style= "bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium\"";
		}
		
		
		$select_comp_name_query = mysql_query("SELECT `company_name` FROM `corp_admin` WHERE `company_abbr`= '$product_result[corporate_id]'");
		$comp_name_result = mysql_fetch_array($select_comp_name_query);
		
		
	  $emailContent .= "<tr>
		<td style=\"padding-bottom:10px;\" $td_style>$description</td>
		<td style=\"padding-bottom:10px;\" $td_style><img src=\"$THEIMAGE\" $NewSize>
		<br> <br> Item # $item_num
		</td>
		
		<td style=\"padding-bottom:10px;\" $td_style>$comp_name_result[company_name]</td>
		<td style=\"padding-bottom:10px;\" $td_style>$product_result[corporate_id]</td>
		
		<td style=\"padding-bottom:10px;\" $td_style>".number_format($price,2)."</td>
		<td style=\"padding-bottom:10px;\" $td_style>$qty</td>
		<td style=\"padding-bottom:10px;\" $td_style>$".number_format($cost,2)."</td>
	  </tr>";
	
		$count++;
		
	}
	

	$emailContent .= "
	<tr>
		<td colspan=\"7\" style=\"padding-bottom:10px; text-align:right\"><hr style=\"height:4px; background-color:#000\"></td>
	</tr>
	
	<tr>
		<td colspan=\"6\" style=\"padding-bottom:10px; text-align:right\"  bgcolor=\"#FAEFC5\"><strong>Subtotal:</strong></td>
		<td style=\"padding-bottom:10px;\" bgcolor=\"#FAEFC5\">$".number_format($subtotal,2)."</td>
	  </tr>
	<tr>
		<td colspan=\"6\" style=\"padding-bottom:10px; text-align:right\"><strong>Shipping: </strong></td>
		<td style=\"padding-bottom:10px;\">$"."0.00"."</td>
	</tr>
	<tr>
		<td colspan=\"6\" style=\"padding-bottom:10px; text-align:right\" bgcolor=\"#FAEFC5\"><strong>Tax:</strong></td>
		<td style=\"padding-bottom:10px;\" bgcolor=\"#FAEFC5\">$"."0.00"."</td>
	</tr>
	<tr>
		<td colspan=\"6\" style=\"padding-bottom:10px; text-align:right\"><strong>Total:</strong></td>
		<td style=\"padding-bottom:10px;\">$".number_format($subtotal,2)."</td>
	</tr>
	</table>";
	
	return $emailContent;
}



function showPages($pages,$pager,$email)
	{
	$page=$pages;
	print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>" . $ListName . "&nbsp;" . $CateName . "</b>&nbsp;&nbsp;</font><font face=\"Arial\" size=\"2\">";
	
	
	// output paging system (could also do it before we output the page content) 
	if ($page == 1) // this is the first page - there is no previous page 
		echo "&nbsp; "; // FIRST PAGE NO PREV
	else            // not the first page, link to the previous page 
		echo "<a href=\"listOnlineOrders.php?email=$email&PrevPAGE=$PrevPAGE&page=" . ($page - 1) . "\" target=\"_self\">Prev Page&nbsp;</a>"; 
	
	for ($i = 1; $i <= $pager->numPages; $i++) { 
		if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
		{
		$nothing=0;
		}
		else
		{
		echo " | "; 
		if ($i == $pager->page) 
		echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
		else 
		if ($SearchCategory != "") 
		{
		echo "<a href=\"listOnlineOrders.php?email=$email&page=$i&PrevPAGE=$PrevPAGE\" target=\"_self\">$i</a>";      
		}
		else
		echo "<a href=\"listOnlineOrders.php?email=$email&page=$i&PrevPAGE=$PrevPAGE&page=$i\" target=\"_self\">$i</a>";
		} 
	} 
	
	if ($page == $pager->numPages) // this is the last page - there is no next page 
		echo "|&nbsp;&nbsp;"; 
	else 
	if ($SearchCategory != "") 
		{
		echo "<a href=\"listOnlineOrders.php?email=$email&PrevPAGE=$PrevPAGE&page=" . ($page + 1) . "\" target=\"_self\">|&nbsp;&nbsp;Next Page</a>";      
		} 
		else           // not the last page, link to the next page 
		echo "<a href=\"listOnlineOrders.php?email=$email&PrevPAGE=$PrevPAGE&page=" . ($page + 1) . "\" target=\"_self\">|&nbsp;&nbsp;Next Page</a>"; 
		
	print "<br><br>";         
	}



?> 