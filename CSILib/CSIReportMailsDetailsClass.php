<?php
include "../pager.php";
include "CSIReportClass.php";


//========================================================================
// Provide better control over the sort by extending the report class
//========================================================================

class CSIMailsReportDetail extends CSIReport
{     

  var $ORDERNUM="";
//========================================
// BUILT IN IMPROVED PAGER
//========================================
function setPager($page,$limit)
{   
  $tableName = $this->tablename;  
  $this->result = mysql_query("SELECT count(*) FROM $tableName $this->conditions");     
  $total = mysql_result($this->result, 0, 0);  

  $pager  = Pager::getPagerData($total, $limit, $page); 
  $this->offset = $pager->offset; 
  $this->limit  = $pager->limit; 
  $this->page   = $pager->page;  
  $this->PagerX = $pager;
 
}

// IMPROVED PAGER        
function showPages($pages,$pager)
{
 $page=$pages;
 print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
 

     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"$this->caller?page=" . 1 . "\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"$this->caller?&page=" . ($page - 1) . "\" target=\"_self\">Prev Page&nbsp;</a>";  
    }
        

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
         echo " | "; 
         if ($i == $pager->page) 
            echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
         else 
            echo "<a href=\"$this->caller?page=$i\" target=\"_self\">$i</a>"; 
         }
    } 

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;"; 
    else 
     if ($SearchCategory != "") 
        {
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>";      
        } 
        else           // not the last page, link to the next page 
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>"; 
          
         echo "&nbsp;&nbsp;<a href=\"$this->caller?page=" . $pager->numPages . "\" target=\"_self\"> >> Last Page</a>";   
         
 print "<b><br><br>";         
} 

//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable($page,$limit,$caller)
        {
        $total=0;
        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                                 

       print "<form>"; // This form does not really post anything instead we use buttons.
             
        $this->setPager($page,$limit);
        $this->caller = $caller;

        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
        
        $columnsHolder=$this->columns;
        $this->showColumnsHeaders($this->columns);
        
        print "<tr>";
        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        
        $OLDORDERNUMBER="";
        $ORDERNUMBER ="";
        
         $sql_vertical="SELECT $this->preCondition * FROM $tableName $this->conditions limit $offset, $limit";
         $result_vertical=mysql_query($sql_vertical);  
         while($row_vertical=mysql_fetch_array($result_vertical))
         {
             
                       // PLACE LOGIC READ
        $ORDERNUMBER = $row_vertical['order_number'];    
        $this->ORDERNUM = $row_vertical['order_number'];  
        
                       
         if (($ORDERNUMBER != $OLDORDERNUMBER)  && ( $OLDORDERNUMBER != ""))     
         {
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;\$$total </font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td><br> </td></tr>";   
            print "<tr><td><br> </td></tr>";     
                                    
            $OLDORDERNUMBER = $ORDERNUMBER;   
            $total=0;  
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;            
         }
          else
         {
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;    
             
            $ORDERNUMBER = $row_vertical['order_number'];   
           $OLDORDERNUMBER = $ORDERNUMBER;
         }
             
             
             
             
          $arrayCount=count($this->columns);
          for ($i=0;$i<$arrayCount;$i++)
          {         
                  
           $THEDATA=$row_vertical[$this->columns[$i]];
           $THEDATA = str_replace("\n", "<br>", $THEDATA);
          
           $this->PrintCell($THEDATA); // print data
           
          
           if (($this->columns[$i] == "order_number"))
           {
            $ORDERNUMBER = $row_vertical[$this->columns[$i]];
           }
           
          }
          
          $IID=$row_vertical[id];


        // Print the actions   
        if ($this->CallEdit != "")
        {
          print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
          print "<a href=\"$this->CallEdit?caller=$this->caller&calltype=U&id=$IID\">$this->EditTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        
        if ($this->CallDelete != "")
        {
          print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
          print "<a href=\"$this->CallDelete?$this->caller&calltype=D&id=$IID\">$this->DeleteTitle</td><td bgcolor=\"#FFFFFF\">"; 
        }
           
         print "</td>";
   
         $this->changeRowColor();          
         print "</tr>";      

         }
         // PRINT THE LAST TOTAL ROW
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><<td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;\$$total </font></td></tr>";   
            print "<tr><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td><td></td><td>";
                                           
            print "<a href=\"SendPO.php?ordernum=$this->ORDERNUM\"><img border=\"0\" src=\"../images/BPurchaseOrder.png\"></a>";
            print "<a href=\"ProcessCredit.php?ordernum=$this->ORDERNUM\">";
            print "<img border=\"0\" src=\"../images/BProcessCredit.png\"></a></p>";
           
            print "</td></tr>";    
          
          // FORM DATA
    
       
            print "</form>";
        }


}         



?> 