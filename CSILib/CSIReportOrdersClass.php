<?php
include "../pager.php";
include "CSIReportClass.php";


//========================================================================
// Provide better control over the sort by extending the report class
//========================================================================

class CSIOrdersReport extends CSIReport
{     

//========================================
// BUILT IN IMPROVED PAGER
//========================================
function setPager($page,$limit)
{   
  $tableName = $this->tablename;  
  $this->result = mysql_query("SELECT count(*) FROM $tableName $this->conditions");     
  $total = mysql_result($this->result, 0, 0);  

  $pager  = Pager::getPagerData($total, $limit, $page); 
  $this->offset = $pager->offset; 
  $this->limit  = $pager->limit; 
  $this->page   = $pager->page;  
  $this->PagerX = $pager;
}

// IMPROVED PAGER        
function showPages($pages,$pager,$keyword,$mom,$status,$date)
{
 $page=$pages;
 print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
 

     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"$this->caller?page=" . 1 . "&keyword=$keyword&mom=$mom&status=$status&date=$date\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"$this->caller?&page=" . ($page - 1) . "&keyword=$keyword&mom=$mom&status=$status&date=$date\" target=\"_self\">Prev Page&nbsp;</a>";  
    }
        

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
         echo " | "; 
         if ($i == $pager->page) 
            echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
         else 
            echo "<a href=\"$this->caller?page=$i&keyword=$keyword&mom=$mom&status=$status&date=$date\" target=\"_self\">$i</a>"; 
         }
    } 

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;"; 
    else 
     if ($SearchCategory != "") 
        {
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "&keyword=$keyword&mom=$mom&status=$status&date=$date\" target=\"_self\">Next Page</a>";      
        } 
        else           // not the last page, link to the next page 
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "&keyword=$keyword&mom=$mom&status=$status&date=$date\" target=\"_self\">Next Page</a>"; 
          
         echo "&nbsp;&nbsp;<a href=\"$this->caller?page=" . $pager->numPages . "&keyword=$keyword&mom=$mom&status=$status&date=$date\" target=\"_self\"> >> Last Page</a>";   
         
 print "<b><br><br>";         
} 

//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable($page,$limit,$caller,$keyword,$mom,$status,$date)
        {
        $total=0;
        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";                              
	print "<br>";
	?>
	<form action="" method="GET">
	<font color="brown" size="2">Search by MOM#:</font>
	<input type="text" name="mom" value="<?=$mom?>" />&nbsp;&nbsp;&nbsp;
	<font color="brown" size="2">Search by Email :</font>
	<input type="text" name="keyword" value="<?=$keyword?>" />&nbsp;&nbsp;&nbsp;
	<input type="hidden" name="page" value="<?=$page?>" />
	<input type="hidden" name="date" value="<?=$date?>" />
	<font color="brown" size="2">Search by Status :</font>
	<select name="status">
	<option value="">SELECT STATUS</option>
	<?
	$sel_stat_query = "SELECT DISTINCT status FROM `orderhistory` WHERE status != ''";
	$sel_stat_sql = mysql_query($sel_stat_query);
	while($res_stat = mysql_fetch_assoc($sel_stat_sql)) {
	$status1 = $res_stat['status'];
	if($status1 == $status)
	$selected = "selected";
	else
	$selected = "";
	?>
	<option value="<?=$status1?>" <?=$selected?>><?=$status1?></option>
	<? } ?>
	</select>
	<input type="submit" name="search" value="Search" />
	</form>

	<?
	print "<br>";
        $this->setPager($page,$limit);
        $this->caller = $caller;

            
        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager,$keyword,$mom,$status,$date);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
        
	$columnsHolder=$this->columns;
        $columnsHolder1=str_replace('online_orderhistory.','',$columnsHolder);
	$columnsHolder1=str_replace('allproducts.','',$columnsHolder1);
        $this->showColumnsHeaders($columnsHolder1);
        
        print "<tr>";
        
        $this->columns = $this->showColumns($columnsHolder1);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
        
        $OLDORDERNUMBER="";
        $ORDERNUMBER ="";
        
         $sql_vertical="SELECT $this->preCondition $columnsHolder FROM $tableName $this->conditions limit $offset, $limit";

         $result_vertical=mysql_query($sql_vertical);  
         while($row_vertical=mysql_fetch_array($result_vertical))
         {
             
                       // PLACE LOGIC READ
        $ORDERNUMBER = $row_vertical['order_number'];    
                       
         if (($ORDERNUMBER != $OLDORDERNUMBER)  && ( $OLDORDERNUMBER != ""))     
         {
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;".sprintf("\$%4.2f",$total)."</font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td><br> </td></tr>";   
            print "<tr><td><br> </td></tr>";     
                                    
            $OLDORDERNUMBER = $ORDERNUMBER;   
            $total=0;  
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;            
         }
          else
         {
            $unit_price=  (double) $row_vertical['unit_price'];    
            $ext_price = $unit_price *  (int) $row_vertical['quantity'];                 
            $total=$total +  $ext_price;    
             
            $ORDERNUMBER = $row_vertical['order_number'];   
           $OLDORDERNUMBER = $ORDERNUMBER;
         }

// print "<pre>";
// print_r($row_vertical);
// print "</pre>";
// 
// exit;

          $arrayCount=count($this->columns);
          for ($i=0;$i<$arrayCount;$i++)
          {         
                  
           $THEDATA=$row_vertical[$this->columns[$i]];

           if (($this->columns[$i] == "unit_price"))
           {
            $THEDATA = sprintf ("\$%4.2f",$THEDATA);
           }

           $THEDATA = str_replace("\n", "<br>", $THEDATA);
          
           $this->PrintCell($THEDATA); // print data
           
          
           if (($this->columns[$i] == "order_number"))
           {
            $ORDERNUMBER = $row_vertical[$this->columns[$i]];
           }
           
          }
          
          $IID=$row_vertical[id];
	
	print "<td bgcolor=\"#FFFFFF\"><font face=\"Arial\" size=\"2\">";
	print "<table><tr>";

        // Print the actions   
        if ($this->CallEdit != "")
        {
	print "<td>";
          print "<a href=\"$this->CallEdit?caller=$this->caller&calltype=U&id=$IID&page=$page&keyword=$keyword&mom=$mom&status=$status&date=$date\">$this->EditTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;";
	print "</td>";
        }

	if ($this->CallEdit1 != "")
        {
	print "<td>";
          print "<a href=\"$this->CallEdit1?caller=$this->caller&calltype=U&id=$IID&page=$page&keyword=$keyword&mom=$mom&status=$status&date=$date\">$this->EditTitle1<a>&nbsp;&nbsp;&nbsp;&nbsp;";
	print "</td>";
        }
        
        if ($this->CallDelete != "")
        {
	print "<td>";
          print "<a href=\"$this->CallDelete?$this->caller&calltype=D&id=$IID\">$this->DeleteTitle</td><td bgcolor=\"#FFFFFF\">";
	print "</td>"; 
        }
          
	print "</tr></table>"; 
         print "</td>";
   
         $this->changeRowColor();          
         print "</tr>";      
          

             
         }
         // PRINT THE LAST TOTAL ROW
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td bgcolor=\"#009933\"><font color=\"#FFFFFF\"><b>TOTAL: &nbsp;&nbsp;".sprintf("\$%4.2f",$total)."</font></td></tr>";   
            print "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>__________________<br><br></td></tr>";   
            print "<tr><td><br> </td></tr>";   
            print "<tr><td><br> </td></tr>";

print "</table>";

	print "<br>";
        $this->setPager($page,$limit);
        $this->caller = $caller;

            
        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"2\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br>";
        }


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
         $this->showPages($page,$pager,$keyword,$mom,$status,$date);
        }
         
        }

}         
?> 