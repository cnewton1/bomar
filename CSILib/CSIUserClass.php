<?php
include "pager.php";

class CSIReport
{     
   var $tablename;
   var $columns; 
   var $conditions="";
   var $altColor=1;
   var $CallDelete;
   var $CallEdit;
   var $CallActivate;
   var $addFunctionTitle;
   var $addFunctionCaller;
   // Pager Vars
   var $page;
   var $limit=20; // default
   var $result;
   var $total;
   var $offset;
   var $PagerX;
   var $includePager="YES"; // default
   var $caller="";
   var $Title="";
   var $EditTitle="Edit";
   var $DeleteTitle="Delete";

//========================================
// BUILT IN IMPROVED PAGER
//========================================
function setPager($page,$limit)
{   
  $tableName = $this->tablename;  

  $this->result = mysql_query("SELECT count(*) FROM $tableName $this->conditions");     
  $total = mysql_result($this->result, 0, 0);  

  $pager  = Pager::getPagerData($total, $limit, $page); 
  $this->offset = $pager->offset; 
  $this->limit  = $pager->limit; 
  $this->page   = $pager->page;  
  $this->PagerX = $pager;

}

// IMPROVED PAGER        
function showPages($pages,$pager)
{
 $page=$pages;
 print " <font face=\"Arial\" size=\"2\" color=\"#FF6600\"><b>";
 

     // output paging system (could also do it before we output the page content) 
    if ($page == 1) // this is the first page - there is no previous page 
        echo "&nbsp; "; // FIRST PAGE NO PREV
    else            // not the first page, link to the previous page 
    {
       echo "<a href=\"$this->caller?page=" . 1 . "\" target=\"_self\">First Page << </a>&nbsp;&nbsp; ";   
       echo "<a href=\"$this->caller?&page=" . ($page - 1) . "\" target=\"_self\">Prev Page&nbsp;</a>";  
    }
        

    for ($i = 1; $i <= $pager->numPages; $i++)
    { 
        if (($i > ($pager->page + 5)) or ($i < ($pager->page - 5)))
        {
          $nothing=0; // do nothing
        }
        else
        {
         echo " | "; 
         if ($i == $pager->page) 
            echo "<b><font face=\"Arial\" size=\"2\" color=\"#FF0000\">$i</font></b>"; 
         else 
            echo "<a href=\"$this->caller?page=$i\" target=\"_self\">$i</a>"; 
         }
    } 

    if ($page == $pager->numPages) // this is the last page - there is no next page 
        echo "|&nbsp;&nbsp;"; 
    else 
     if ($SearchCategory != "") 
        {
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>";      
        } 
        else           // not the last page, link to the next page 
          echo "|&nbsp;&nbsp;<a href=\"$this->caller?page=" . ($page + 1) . "\" target=\"_self\">Next Page</a>"; 
          
         echo "&nbsp;&nbsp;<a href=\"$this->caller?page=" . $pager->numPages . "\" target=\"_self\"> >> Last Page</a>";   
         
 print "<b><br><br>";         
} 
  
//-----------------------------------------------------        
// Methood section
//-----------------------------------------------------
        function setDeleteRow($CallString)
        {
          $this->CallDelete=$CallString;
        }


        function setEditTitle($CallString)
        {
          $this->EditTitle=$CallString;
        }


	function setPageTitle($CallString)
        {
          $this->PageTitle=$CallString;
        }

	function setBacklink($backlink)
        {
          $this->Backlink=$backlink;
        }
        
        
        function setDeleteTitle($CallString)
        {
          $this->DeleteTitle=$CallString;
        }
                
        
        function setEditRow($CallString)
        {
          $this->CallEdit=$CallString;
        }

	function setPageRow($CallString)
        {
          $this->CallPage=$CallString;
        }
        
        
        function setPreCodition($CallString)
        {
          $this->preCondition=$CallString;
        }   
        
        function setCodition($CallString)
        {
          $this->conditions=$CallString;
        }    
                
        function setActivateRow($CallString)
        {
          $this->CallActivate=$CallString;
        }

        function setAddFunction($Title,$CallString)
        {
          $this->addFunctionTitle=$Title;
          $this->addFunctionCaller=$CallString;
        }

	function setUploadFunction($Title,$CallString)
        {
          $this->uploadFunctionTitle=$Title;
          $this->uploadFunctionCaller=$CallString;
        }
        
        function setReportTitle($Title)
        {
          $this->Title=$Title;
        }

	function setAccountAddress($AccInfo)
        {
          $this->Address=$AccInfo;
        }

	function setMessage($Message)
        {
          $this->Message=$Message;
        }

//----------------------------------------------
// Define the columns
//----------------------------------------------
        function showColumns($thesecolumn)
        {
          return explode (",",$thesecolumn);
        }

//----------------------------------------------
// Define the columns into table headers
//----------------------------------------------
        function showColumnsHeaders($thesecolumn)
        {
          $headerNameArray=explode (",",$thesecolumn);
          
          $headerNameCount=count($headerNameArray);
          for ($i=0;$i<$headerNameCount;$i++)
          {         
            print "<td  height=\"1\" bgcolor=\"#000000\" align=\"left\" style=\"border-style: solid; border-width: 1\">";
            print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">$headerNameArray[$i]</font></td>";            
          }
            print "<td  height=\"1\" bgcolor=\"#000000\" align=\"center\" style=\"border-style: solid; border-width: 1\">";
		if ($this->CallEdit != "")
            print "<font color=\"#FFFFFF\" face=\"Arial\" size=\"2\">ACTION</font>";
		print "</td>";   
        }


     function PrintCell($data)
     {
       if ($this->altColor==1)
        print "<td bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium; color:black;\">" . $data . "</td>";
       else     
        print "<td bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium; color:black;\">" . $data . "</td>";
     }

	function PrintCell1($data)
     {
	$THEIMAGE="../pictures/".$data;
         $NewSize=myResize($THEIMAGE,100,100);

      if ($this->altColor==1)
        print "<td bgcolor=\"#FFECC6\" style=\"border-style: none; border-width: medium\"><img src=\"../pictures/$data\"" . $NewSize . "></td>";
       else     
        print "<td bgcolor=\"#FFFFFF\" style=\"border-style: none; border-width: medium\"><img src=\"../pictures/$data\"" . $NewSize . "></td>";
     }

     function changeRowColor()
     {       
       if ($this->altColor == 1)
       {   
       $this->altColor=0;
       }
       else
       {
       $this->altColor=1;
       }
     }


//----------------------------------------------
// MAIN REPORT Display FUNCTION
//----------------------------------------------
        function displayTable($page,$limit,$caller)
        {
	$total = 0;
	$total_flag = 0;

        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b>" . $this->Title . "</b></font></p>";

        print "<br>";
        print "<font face=\"Arial\" size=\"4\"><b><a href=\"userManager.php\">" . $this->Backlink . "</a></b></font></p>";

	print "<br>";
        print "<font face=\"Arial\" color=\"red\" size=\"2\"><b>" . $this->Message . "</b></font></p>";


        if ($this->Address != NULL)
        {
	$select_query = "select * from user_manager where user_id = '".$this->Address."'";
	$select_sql = mysql_query($select_query);
	$result_user = mysql_fetch_assoc($select_sql);
	print "<b>";
	print $result_user['firstname']." ".$result_user['lastname']."<br>";
	print $result_user['companyname']."<br>";
	print $result_user['address1']."<br>";
	print $result_user['city']." ".$result_user['state']." ".$result_user['zip']."<br>";
	print $result_user['country']."<br>";
	print "</b>";
        }


        $this->setPager($page,$limit);

        $this->caller = $caller;

	if ($this->uploadFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->uploadFunctionCaller . "\"><font face=\"Arial\" size=\"3\">" . $this->uploadFunctionTitle . "</a><br><br>";
        }

        if ($this->addFunctionTitle != NULL)
        {
         print "<br><b><a href=\"" . $this->addFunctionCaller . "\"><font face=\"Arial\" size=\"3\">" . $this->addFunctionTitle . "</a><br><br>";
        } 
        else 
        { 
         print "<br><br>";
        }

        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
	if ($this->Address == NULL)
         $this->showPages($page,$pager);
        }
        
        print "<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"border-collapse: collapse; border-width: 1\" bordercolor=\"#111111\" width=\"100%\" id=\"AutoNumber1\">";    
        
        $columnsHolder=$this->columns;
        $this->showColumnsHeaders($this->columns);
        
        print "<tr>";
        
        $this->columns = $this->showColumns($this->columns);
        $tableName = $this->tablename;
        
        $offset=$this->offset;
        $limit =$this->limit;
        
         $sql_vertical="SELECT $this->preCondition $columnsHolder FROM $tableName $this->conditions limit $offset, $limit";

         $result_vertical=mysql_query($sql_vertical);  
         while($row_vertical=mysql_fetch_array($result_vertical))
         {
             
          $arrayCount=count($this->columns);

          for ($i=0;$i<$arrayCount;$i++)
          {
           $THEDATA=$row_vertical[$i];
           $THEDATA = str_replace("\n", "<br>", $THEDATA);
	if((($i==2)&&($row_vertical['date']===$THEDATA)) || (($i==3)&&($row_vertical['date']===$THEDATA))) {
	$date_arr = explode(' ',$THEDATA);
	$date = $date_arr[0];
	$d_arr = explode('-',$date);
	$new_arr = Array("$d_arr[1]","$d_arr[2]","$d_arr[0]");
	$THEDATA = implode('-',$new_arr);
	$total_flag = 1;
	$this->PrintCell($THEDATA); }
	else if(($i==1)&&($row_vertical['image']===$THEDATA)&&($THEDATA!="")){
	$this->PrintCell1($THEDATA);
	}
	else
           $this->PrintCell($THEDATA); // print data
          }
          
          $IID = $row_vertical[id];
	  $user_id = $row_vertical[user_id];
	$lpage = $row_vertical['login_pages'];

	if ($this->altColor==1)
	 print "<td bgcolor=\"#FFECC6\"  align=\"center\"><font face=\"Arial\" size=\"2\">";
	else
	 print "<td bgcolor=\"#FFFFFF\"  align=\"center\"><font face=\"Arial\" size=\"2\">";
	
        // Print the actions   
        if ($this->CallEdit != "")
        {
          print "<a href=\"$this->CallEdit?calltype=U&id=$IID\">$this->EditTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }

	if ($this->CallPage != "")
        {
          print "<a href=\"$this->CallPage?user_id=$user_id\">$this->PageTitle<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        
        if ($this->CallDelete != "")
        {
          print "<a href=\"$this->CallDelete?calltype=D&lpage=$lpage&id=$IID\">$this->DeleteTitle</td><td bgcolor=\"#FFFFFF\">"; 
        }
           
         print "</td>";
   
         $this->changeRowColor();          
          print "</tr>";      
         }

	print "</table>";

	if($total_flag) {
	print "<table width=\"100%\">";
	print "<tr><td colspan=\"2\" height=\"15px\"></td></tr>";
	print "</table>";
	}

	print "<br>";

	$this->setPager($page,$limit);
        $this->caller = $caller;


        if ($this->includePager == "YES")
        {
         $pager=$this->PagerX;
	if ($this->Address == NULL)
         $this->showPages($page,$pager);
        }
         
        }


}         
?> 