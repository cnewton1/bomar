<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
?>
<?php
require_once('recaptcha-config.php');

?>

<?php
function isEmail($email)
{
	return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
}

function extract_email_from_string($string)
{
	$pattern = '/([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
	'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)/i';
	
	preg_match ($pattern, $string, $matches);
	return $matches[0];
}
function Quote($string)
{
	return addslashes($string);	
}

function EmailText($info)
{
	$email_body = '<table rules="all" style="border-color: #666;" cellpadding="10">
	  <tr style="background: #eee;">
		<td valign="top"><strong>Name:</strong></td>
		<td>'.$info['name'].'</td>
	  </tr>	  
	  	  <tr>
		<td valign="top"><strong>Email:</strong></td>
		<td>'.$info['email'].'</td>
	  </tr>
	  <tr>
		<td valign="top"><strong>Comments:</strong></td>
		<td>'.nl2br($info['comments']).'</td>
	  </tr>
	</table>';
	
	return $email_body;
}

if(isset($_POST['submit']))
{
	$name 		= trim($_POST['name']);
	$email 		= trim($_POST['email']);
	$comments 	= trim($_POST['comments']);
	
	if($name == '')
	{
		$error[] = 'Please enter your name';		
	}
	if($email == '')
	{
		$error[] = 'Please enter your email address';		
	}
	else
	{
		if(!isEmail($email))
		{
			$error[] = 'Enter Valid Email Address.';
		}
	}
	
	if($comments == '')
	{
		$error[] = 'Please enter Comments';		
	}
	
	$recaptcha=$_POST['g-recaptcha-response'];
	
	if(!empty($recaptcha))
	{
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret=GOOGLE_RECAPTCHA_SECRET_KEY;
		$ip=$_SERVER['REMOTE_ADDR'];
		$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
		$res=getCurlData($url);
		$res= json_decode($res, true);
		if($res['success'])
		{
		}
		else 
		{
			$error[] = "Please re-enter your reCAPTCHA.";
		}
	}
	else
	{
		$error[] = "Please re-enter your reCAPTCHA.";
	}
	
	
	if(empty($error))
	{
		$name 		= Quote($name);
		$email 		= Quote($email);
		$comments 	= Quote($comments);
		
		$date = date('Y-m-d');
		$query = mysql_query("INSERT INTO `contactus` (`name`, `email`, `customercomment`, `date`) VALUES 
		('$name', '$email', '$comments', '$date')");
		if($query)
		{
			$emailinfo = array(
			'name'=>$name,
			'email'=>$email,
			'comments'=>$comments
			);
			
			$to      = extract_email_from_string($COMPEMAIL);
			//$to = 'mdadilsiddiqui@gmail.com'; // Adil
			
			$returnaddress = $to;
			$subject = 'Contact Us - '.$COMPANYNAME;
			$message = '<html><body>';
			$message .= '<img src="http://bomar.chrondev.com/images/logo.jpg" alt="'.$subject.'" />';
			$message .= EmailText($emailinfo);
			$message .= "</body></html>";
			
			$headers = 'From: '.$email. "\r\n" .
				'Reply-To: '.$returnaddress . "\r\n" .
				'Return-Path: <'.$returnaddress . ">\r\n" .
				'X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				
			$mail = mail($to, $subject, $message, $headers);
			if(!$mail){
				$error[] = 'Email Sending Failed!.';
			} 
			$name 		= '';
			$email 		= '';
			$comments	= '';
		
			$msg = "Your request has been sent.";
		}
		
		
	}
	
	
}
?>

<style>
div.error{
	padding:2px 4px;
    margin:0px;
    border:solid 1px #FBD3C6;
    background:#FDE4E1;
    color:#CB4721;
    font-family:Arial, Helvetica, sans-serif;
    font-size:14px;
    font-weight:bold;
    text-align:center; 
}
div.success{
	padding:2px 4px;
	margin:0px;
	border:solid 1px #C0F0B9;
	background:#D5FFC6;
	color:#48A41C;
	font-family:Arial, Helvetica, sans-serif; font-size:14px;
	font-weight:bold;
	text-align:center; 
}

#contact_us td
{
	font-family:Arial, Helvetica, sans-serif;
	
}

input[type="text"], input[type="password"], select 
{
	width:250px; 
	height:25px; 
	border:1px solid #000;
}

td .label
{
	text-align:right;
	
}
.required
{
	color:#F00;
	font-weight:bold;
	
}
</style>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <div id="home_body">
        

        <div id="midsec">





        <table id="contact_us" width="100%" align="center" border="0" bgcolor="#FFFFFF"  >
<TR>
<td style="padding-left:10px;">
<font size="+3"><strong>Contact Us</strong></font><br />
<font size="+1">
To contact us directly, please email<br />
<br />
</font>
<br />
<font size="+1">
<strong>Westcarb Enterprises Inc</strong><br />
3632 Land O Lakes Blvd, STE 104,<br />
Land O Lakes, FL 34639<br /><br />
<br />
<font size="+3">
SEND US AN EMAIL</font><br />
<strong><i>Please send us an email using the form below</i></strong><br /><br />
<form method="post" action="">
<div class="g-recaptcha" data-sitekey="6LeOsyYTAAAAAJpBwGZ8M8qLazC-OZynKq5oCthW"></div>
<table width="100%" cellpadding="5" cellspacing="5" border="0"  style=" font-size:12px">
<?php
if(isset($msg))
{
	echo '<tr>
	<td colspan="2">
	<div class="success">'.$msg.'
	
	</div></td>
  </tr>';
}
if(!empty($error))
{
	echo '<tr>
	<td colspan="2">
	<div class="error">'.implode('<br>',$error).'
	
	</div></td>
  </tr>';
}
?>
                  
<tr>
<td align="right"><strong>First Name & Last Name</strong><span style="color:red">*</span></td>
<td><input type="text" name="name" value="<?php echo $name;?>" style="width:250px;" /></td>
<td rowspan="5" valign="top">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.897265820479!2d-82.46326468452295!3d28.210433709808875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88c2bb931bb3bd45%3A0x47ef41d302d5e956!2sCopperstone+Executive+Suites+and+Business+Club!5e0!3m2!1sen!2sus!4v1470271209985" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>


</td>
</tr>
<tr>
<td align="right"><strong>Email Address</strong><span style="color:red">*</span></td>
<td><input type="text" name="email" value="<?php echo $email;?>"  style="width:250px;" /></td>
</tr>
<tr>
<td align="right" valign="top"><strong>Comments</strong><span style="color:red">*</span></td>
<td><textarea name="comments"  style="width:250px; height:150px;"><?php echo $comments;?></textarea></td>
</tr>
<tr>
<td class="label">&nbsp; </td>
<td>
<div class="g-recaptcha" data-sitekey="<?php echo GOOGLE_RECAPTCHA_SITE_KEY;?>"></div>
</td>
</tr>
  
<tr>
<td >&nbsp;</td>
<td><div style="margin:10px 0px 10px 0px;"><input type="submit" name="submit" value="Send Your Email" class="btn btn-success" style="background:#F68CBA; padding:5px; font-family:Arial, Helvetica, sans-serif; font-weight:bold" /></div></td>
</tr>
</table>

</form>
</td>
            
</TR>
</table>


     
        
 
   


        </div>
    </div>

    




<?php include 'footer.php';?>


    
  </body>
</html>