<?php
include 'mainheader.php';
include 'MyImageFunction.php';
include 'functions.php';
?>
<?php
$category = $_REQUEST['category'];
$mfgname = $_REQUEST['mfgname'];
$q = (isset($_REQUEST['q'])) ? mysql_real_escape_string(trim($_REQUEST['q'])) : '';

$where_condition = "";
$query_parm = array();

if ($q != '') {
    $where_condition = $where_condition . " AND (
		`WESTCARB_PART_NUMBER` LIKE '%$q%' OR 	
		`mfgname` LIKE '%$q%' OR 
		`upc` LIKE '%$q%')";

    $query_parm['q'] = $q;
}
if (trim($_REQUEST['category']) != '') {
    $category = mysql_real_escape_string(trim($_REQUEST['category']));
    $where_condition = $where_condition . " AND `category` LIKE '%$category%'";
    $query_parm['category'] = $category;
}
if (trim($_REQUEST['mfgname']) != '') {
    $mfgname = mysql_real_escape_string(trim($_REQUEST['mfgname']));
    $where_condition = $where_condition . " AND `mfgname` = '$mfgname'";
    $query_parm['mfgname'] = $mfgname;
}

if (isset($_POST['action']) && $_POST['action'] == 'send_email') {

    $data = $_POST['data'];

    ob_start();
    product_details_email_content($data);

    $email_content = ob_get_clean();

    $to = $data['to_email'];

    $returnaddress = $FROM;
    $subject = 'Product Suggestion';
    $message = $email_content;

    $headers = 'From: ' . $FROM . "\r\n" .
            'Reply-To: ' . $returnaddress . "\r\n" .
            'Return-Path: <' . $returnaddress . ">\r\n" .
            'X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    $mail = mail($to, $subject, $message, $headers);
    if (!$mail) {
        $error[] = 'Email Sending Failed!.';
    } else {
        $msg = 'Email successfully sent';
        header('Location: product_details.php?id=' . $data['id'] . '&msg=' . $msg);
        exit();
    }
}
?>
<script type="text/javascript">
    function submit_form(form_name) {
        document.getElementById("addtocart").submit();
        // body...
    }
</script>

<style>
    .product_description{
        max-width: 800px;
    }
    .prodInfo_txt ul li {
        background: none;
        line-height: 13px;
        margin-bottom: 3px;
        padding-left: 0px  !important;
    }

    .btns a.email {
        background-position: left;
        background-repeat: no-repeat;
        padding: 5px 10px 5px 25px;
        display: inline;
    }
    .btns a.email {
        background-image: url(images/ico_email.png);
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 99999999999; /* Sit on top */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto; /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
        width: 50%; /* Could be more or less, depending on screen size */
    }

    /* The Close Button */
    .close {
        color: #aaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }

    div.error{
        padding:2px 4px;
        margin:0px;
        border:solid 1px #FBD3C6;
        background:#FDE4E1;
        color:#CB4721;
        font-family:Arial, Helvetica, sans-serif;
        font-size:14px;
        font-weight:bold;
        text-align:center; 
    }
    div.success{
        padding:2px 4px;
        margin:0px;
        border:solid 1px #C0F0B9;
        background:#D5FFC6;
        color:#48A41C;
        font-family:Arial, Helvetica, sans-serif; font-size:14px;
        font-weight:bold;
        text-align:center; 
    }

</style>


<div id="home_body">


    <div id="midsec">
        <?php
        if (isset($_GET['msg']) && $_GET['msg'] != '') {
            $msg = $_GET['msg'];
        }
        if (isset($msg)) {
            echo '
	<div class="success">' . htmlspecialchars($msg) . '
	
	</div>';
        }
        if (!empty($error)) {
            echo '
	<div class="error">' . implode('<br>', $error) . '
	
	</div>';
        }
        ?>


        <?php
        if ($_REQUEST['id'] != '') {
            $FREE_SHIPPING = "NO";
            $ID = mysql_real_escape_string($_REQUEST['id']);
            $product_query = mysql_query("SELECT * FROM `product2` WHERE `id` = '$ID'");
            if (mysql_num_rows($product_query)) {
                $row = mysql_fetch_array($product_query);

                $category = $row['category'];
                $subcategory = $row['subcategory'];
                $header = $row['header'];
                $subheader = $row['subheader'];
                $description = $row['description'];
                $comment = $row['comment'];
                $colorstext = $row['colorstext'];
                // MORE
                $UNIT_OF_ISSUE = $row['UNIT_OF_ISSUE'];
                $PACKAGE_QTY = $row['PACKAGE_QTY'];
                $PROD_WIDTH = $row['PROD_WIDTH'];
                $PROD_HEIGHT = $row['PROD_HEIGHT'];
                $PROD_LENGTH = $row['PROD_LENGTH'];
                // EVEN MORE
                $UL_LISTED = $row['UL_LISTED'];
                $ADA = $row['ADA'];
                $JWOD = $row['JWOD'];
                $Clearance = $row['clearance'];
                $FOB = $row['FOB'];

                //KEYWORDS BREAK UP
                $KEYWORDS = $row['KEYWORD'];
                $GSA = "N\A";
                $GSAAPROVE = "N\A";
                $USMADE = " ";
                if (strpos($KEYWORDS, "GSA APPROVED") > 1) {
                    $GSAAPROVE = "YES";
                } else {
                    $GSAAPROVE = "N\A";
                }

                if (strpos($KEYWORDS, "MADE IN USA") > 1) {
                    $USMADE = "YES";
                }

                if (strtoupper($FOB) == "YES") {
                    $FREE_SHIPPING = "YES";
                }

                //

                $key_features_array = array();
                if (!empty($row['b1'])) {
                    $key_features_array[] = $row['b1'];
                }
                if (!empty($row['b2'])) {
                    $key_features_array[] = $row['b2'];
                }
                if (!empty($row['b3'])) {
                    $key_features_array[] = $row['b3'];
                }
                if (!empty($row['b4'])) {
                    $key_features_array[] = $row['b4'];
                }
                if (!empty($row['b5'])) {
                    $key_features_array[] = $row['b5'];
                }
                if (!empty($row['b6'])) {
                    $key_features_array[] = $row['b6'];
                }
                if (!empty($row['b7'])) {
                    $key_features_array[] = $row['b7'];
                }
                if (!isURL($row['image_hyperlink'])) {
                    $thumbnail_url = 'ProductImage/' . basename($row['image_hyperlink']);
                } else {
                    $thumbnail_url = $row['image_hyperlink'];
                }

                $image_get_url = getimagesize($thumbnail_url);

                if (!is_array($image_get_url)) {
                    $thumbnail_url = 'images/ImageNotAvailable.jpg';
                }

                //$newSize = myResize($thumbnail_url,400,400);
                $price = $row['price'];

                $thumbnail_url = str_replace("http:", "https:", $thumbnail_url);


                RecordAccess($EMAIL_CC, "Product Details");  //2018
                $product_info = array('product_id' => $row['id'], 'item_number' => $row['WESTCARB_PART_NUMBER'], 'item_title' => mysql_real_escape_string($row['prodname']));
                insertProductAccessLog($_SESSION['CURRENT_SESSION_UID'], $product_info);
                $prev_result = mysql_fetch_assoc(mysql_query("SELECT `id` FROM `product2` WHERE `id` > '$ID' $where_condition ORDER BY `id` ASC LIMIT 0,1"));
                $next_result = mysql_fetch_assoc(mysql_query("SELECT `id` FROM `product2` WHERE `id` < '$ID' $where_condition ORDER BY `id` DESC LIMIT 0,1"));
            } else {
                header('Location: index.php');
                exit();
            }
        } else {
            header('Location: index.php');
            exit();
        }
        ?>


        <div class="picgroup_topsec">
            <div>
                <?php
                if (!empty($prev_result)) {
                    echo '<a href="product_details.php?id=' . $prev_result['id'] . '&' . http_build_query($query_parm) . '" class="btn-bs btn-primary">Previous</a></td>';
                }
                if (!empty($next_result)) {
                    echo '<a style="margin-left:20px;" href="product_details.php?id=' . $next_result['id'] . '&' . http_build_query($query_parm) . '" class="btn-bs btn-primary">Next</a></td>';
                }
                ?>
            </div>
            <div class="picgroup">
                <div class="prod_img">
                    <?php echo '<img id="zoom_02" src="' . $thumbnail_url . '" width="400" border="0" data-zoom-image="' . $thumbnail_url . '">'; ?>
                </div> 
                <div class="info">
                    <h1 class="title"><?php echo $row['prodname']; ?></h1>      
                    <p>

                    <div class="prodInfo_txt">

                        <div class="btns">
                            <a class="email " id="myBtn" href="javascript:void(0);" rel="nofollow">Email</a>
                        </div>
                        <!-- The Modal -->
                        <div id="myModal" class="modal">

                            <!-- Modal content -->
                            <div class="modal-content">
                                <span class="close">&times;</span>

                                <div class="emailcart_popup" id="emailCartPopup">
                                    <div class="closeBtn">Email Product</div>
                                    <div class="body" id="popup"> 
                                        <!-- start error message -->



                                        <div class="error_sec hide" origin="logIn">
                                            <div class="error_icon"></div>
                                            <div class="error">           
                                                <span id="reqFldHlt">Required fields are highlighted.</span>

                                                <span id="customErrMsg"></span>

                                            </div>
                                        </div>

                                        <!-- end error message -->
                                        <div class="form_2col">
                                            <form action="" name="emailProduct"  method="POST">
                                                <input type="hidden" name="data[id]" value="<?php echo $row['id']; ?>"/>
                                                <legend>Enter Requested Information Below:</legend>
                                                <label id="friendNameLabel">Recipient's Name:</label><input type="text" id="friendName" name="data[to_name]" onblur="clearHilighted(this.id)" size="35" maxlength="100" required /><br />
                                                <label id="friendEmailLabel">Recipient's Email Address:</label><input type="email" id="friendEmail" name="data[to_email]" onblur="clearHilighted(this.id)" size="35" maxlength="100" required /><br />
                                                <label id="yourNameLabel">Your Name:</label><input type="text" id="yourName" name="data[from_name]" size="35" onblur="clearHilighted(this.id)" maxlength="100" value="" required /><br />
                                                <label id="yourEmailLabel">Your Email Address:</label><input type="email" id="yourEmail" name="data[from_email]" size="35" maxlength="100" onblur="clearHilighted(this.id)" value="" required /><br />
                                                <input type="hidden" name="action" value="send_email">
                                                <input type="submit" class="btn submit" value='Submit'/>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <script type="text/javascript">
                            // Get the modal
                            var modal = document.getElementById('myModal');

                            // Get the button that opens the modal
                            var btn = document.getElementById("myBtn");

                            // Get the <span> element that closes the modal
                            var span = document.getElementsByClassName("close")[0];

                            // When the user clicks on the button, open the modal 
                            btn.onclick = function () {
                                modal.style.display = "block";
                            }

                            // When the user clicks on <span> (x), close the modal
                            span.onclick = function () {
                                modal.style.display = "none";
                            }

                            // When the user clicks anywhere outside of the modal, close it
                            window.onclick = function (event) {
                                if (event.target == modal) {
                                    modal.style.display = "none";
                                }
                            }
                        </script>


                        <?php
                        if ($row['UNIT_OF_ISSUE'] == "EA") {
                            $UNITTYPE = "Each";
                        }
                        print "<B>PART NUMBER: </B>" . $row['mfgpart'] . "</B>";

                        $ProductDescription = $row['EXPANDED_PRODUCT_DESCRIPTION'];

                        $ProductDescription = str_replace("877-694-4932", "866-507-1576", $ProductDescription);

                        if ($FREE_SHIPPING == "YES") {
                            $ProductDescription = $ProductDescription . "<P><BR><span style=\"color: rgb(255, 0, 0);\"><B> FREE SHIPPING ON THIS ITEM <B></span>";
                        }

                        echo '<div  style="margin-top:20px;">' . $ProductDescription . '</div>';
                        echo '<form method="GET" action="addToCart.php" name="addtocart" id="addtocart">
    	<div class="prodSpec" style="margin-top:20px;">  
	    		<ul>
		        <li><span style="font-size:18px; font-weight:bold; height:30px; vertical-align:middle;"></span><span style="font-size:18px; font-weight:bold; height:30px; vertical-align:middle; color:#FF0000">Price:&nbsp; ' . $row['CUSTOMER'] . '&nbsp;' . $UNITTYPE . '</span>
		        
		        <div style="color:#000; font-weight:bold; font-size:1.2em; float:right;clear:both; text-align:right; padding-right:50px;">
		        Quantity: <input type="number" name="qty" value="1" style="width:100px; padding:5px;">
		        <br><br>
		        <input type="hidden" name="action" value="add">
		        <input type="hidden" name="ID" value="' . $row['id'] . '">

		        <input type="button" name="add" value="&nbsp" class="btn addtocart" style="background-position: 0 -568px;height: 27px;width: 124px;"  onclick="submit_form(\'addtocart\');">
		        </div>

		        </li>
							
					</ul> 
				</div>
				</form>';
                        $ProductDescription = "";
                        ?>
                    </div>
                    </p>

                    <br>

                </div>
            </div>

            <div style="clear:both" class="prodInfo_txt product_description">
                <?php
                echo '<div class="prodSpec">
					<p>Product Details</p>    
	    			<ul>		
	    			
							<li><span>WESTCARB PART NUMBER</span><span>' . $row['WESTCARB_PART_NUMBER'] . '</span></li>
							<li><span>MANUFACTURER</span><span>' . $row['mfgname'] . '</span></li>
			
							<li><span>Unit of Issue</span><span>' . $row['UNIT_OF_ISSUE'] . '</span></li>
							<li><span>Package Quantity</span><span>' . $row['PACKAGE_QTY'] . '</span></li>
							<li><span>Product Width</span><span>' . $row['PROD_WIDTH'] . '</span></li>
							<li><span>Product Heigth</span><span>' . $row['PROD_HEIGHT'] . '</span></li>
							<li><span>Product Length</span><span>' . $row['PROD_LENGTH'] . '</span></li>	
							<li><span>COLOR FINISH</span><span>' . $row['colorstext'] . '</span></li>																						
					</ul> 
					<p>MAKE & MODEL</p>    	
					<ul>					
							<li><span>UNSPSC</span><span>' . $row['UNSPSC'] . '</span></li>
							<li><span>Brand</span><span>' . $row['mfgname'] . '</span></li>							
							<li><span>MANUFACTURER PART NUMBER</span><span>' . $row['mfgpart'] . '</span></li>		
					</ul> 	
					<p>Specifications</p>
					<ul>						
							<li><span>Country of Origin</span><span>' . $row['current_country_of_origin'] . '</span></li>		
							<li><span>BTU Rating</span><span>' . $row['current_country_of_origin'] . '</span></li>		
							<li><span>Made in the USA</span>' . $USMADE . '<span></span></li>									
							
					</ul> 	
					<p>Packaging Dimensions</p>
					<ul>					
							<li><span>Depth</span><span>' . $row['PROD_LENGTH'] . '</span></li>		
							<li><span>Height</span><span>' . $row['PROD_HEIGHT'] . '</span></li>		
							<li><span>Weigth</span><span>' . $row['PROD_WEIGHT'] . '</span></li>		
							<li><span>Width</span><span>' . $row['PROD_WIDTH'] . '</span></li>																
					</ul> 	
					<p>Product Features</p>
					<ul>					
							<li><span>FOB</span><span>' . $row['FOB'] . '</span></li>							
							<li><span>JWOD</span><span>' . $JWOD . '</span></li>	
							<li><span>Clearance</span><span>' . 'NO' . '</span></li>							
							<li><span>TAA/BAA</span><span>' . ' ' . '</span></li>							
							<li><span>UPC Code</span><span>' . $row['upc'] . '</span></li>								

					</ul> 	
                    <p>Certification & Standards</p>	
					<ul>
							<tr><td style="vertical-align: top;"><img
							style="width: 52px; height: 45px;" src="ULLOGO.jpg"><br>
							</td>
                            <li><span><src="ULLOGO.jpg"></span><span>' . ' ' . '</span></li>								
							<li><span>ADA Approved</span><span>' . $ADA . '</span></li>							
							<li><span>GSA Approved</span><span>' . $GSAAPROVE . '</span></li>									
							<li><span>UL Listed</span><span>' . $UL_LISTED . '</span></li>		
					</ul> 						
					
					
					
				</div>';
                ?>
            </div>		     
        </div>

        <div style="clear:both"></div>
        <?php display_related_category_product($row['id'], $row['category'], $row['subcategory'], $limit = 6); ?>

    </div>
</div>


<?php display_footer_shop_by_category(); ?>

<?php include 'footer.php'; ?>
<script src='js/jquery.elevatezoom.js'></script>
<script type="text/javascript">
                            $(document).ready(function () {
                                $('#zoom_02').elevateZoom({
                                    cursor: "crosshair",
                                    zoomWindowFadeIn: 500,
                                    zoomWindowFadeOut: 750
                                });
                            });

</script>
</body>
</html>