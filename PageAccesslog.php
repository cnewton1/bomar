<?php
function RecordAccess($EMAIL_CC, $pagename="", $check_session = true){
	if($pagename == ''){
		$pagename = $_SERVER['REQUEST_URI'];
	}
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$current_datetime = date('m/d/Y h:i A');
	$session_hash = md5($_SESSION['SES_PAGE_ACCESS'].$pagename);
	if($_SESSION['SES_PAGE_ACCESS'] == ''){
		$_SESSION['SES_PAGE_ACCESS'] = md5(time().$ip_address);
		$session_hash = md5($_SESSION['SES_PAGE_ACCESS'].$pagename);		
	}
	if(!in_array($session_hash, $_SESSION['SES_PAGE_HASH'])){
		$_SESSION['SES_PAGE_HASH'][] = $session_hash;
		insertRecordAccess($session_hash, $EMAIL_CC, $pagename);
	}
	else
	{
		update_hits_access_log($session_hash);

	}
	if($session_hash != ''){
		$_SESSION['CURRENT_SESSION_UID'] = $session_hash;	
	}	
}
function update_hits_access_log($session_uid){
	if($session_uid != ''){
		$query = mysql_query("UPDATE `access_log` SET `hits` = `hits`+1 WHERE `session_uid` = '$session_uid'");
	}
}
function insertRecordAccess($session_uid, $EMAIL_CC, $pagename=""){
	global $FROM, $COMPANYNAME;
	
	$SERVER_OS=$_SERVER['HTTP_USER_AGENT'];        
	$SERVER_NAME=$_SERVER['SERVER_NAME'];
	$REQUEST_URL=$_SERVER['REQUEST_URI'];
	
			
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$current_datetime = date('m/d/Y h:i A');

	$query = mysql_query("INSERT INTO `access_log` (`session_uid`, `hits`, `page`, `ip_address`, `user_agent`, `servername`, `request_uri`) VALUES ('$session_uid', '1', '$pagename', '$ip_address', '$SERVER_OS', '$SERVER_NAME', '$REQUEST_URL')");

	$email_body = '<table rules="all" style="border-color: #666;" cellpadding="10">
	    <tr style="background: #eee;">
	    <td valign="top"><strong>Datetime:</strong></td>
	    <td>'.$current_datetime.'</td>
	    </tr>
	    <tr>
	    <td valign="top"><strong>Page:</strong></td>
	    <td>'.$pagename.'</td>
	    </tr>
	    <tr>
	    <td valign="top"><strong>IP Address:</strong></td>
	    <td>'.$ip_address.'</td>
	    </tr>
	    
	    <tr>
	    <td valign="top"><strong>Visitor OS</strong></td>
	    <td>'.$SERVER_OS.'</td>
	    </tr>
	    	    
	    <tr>
	    <td valign="top"><strong>Server Name</strong></td>
	    <td>'.$SERVER_NAME.'</td>
	    </tr>
	    
	    <tr>
	    <td valign="top"><strong>Requested URL</strong></td>
	    <td>'.$REQUEST_URL.'</td>
	    </tr>
	    
	</table>';


	$to      = $EMAIL_CC;
	//$to = 'mdadilsiddiqui@gmail.com'; // Adil
	$returnaddress = $to;
	$subject = 'Access Log - '.$pagename;
	$message = '<html><body>';
	$message .= '<img src="http://westcarbcom.chrondev.com/images/logo.jpg" alt="'.$subject.'" />';
	$message .= $email_body;
	$message .= "</body></html>";
        
	$headers = 'From: '.$COMPANYNAME.' <'.$FROM. ">\r\n" .
	'Reply-To: '.$returnaddress . "\r\n" .
	'Return-Path: <'.$returnaddress . ">\r\n" .
	'X-Mailer: PHP/' . phpversion();
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//	$mail = mail($to, $subject, $message, $headers);


}


function getUserBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
   
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
   
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
   
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
   
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
   
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}
function insertProductAccessLog($session_uid, $product_info = array()){
	$check_exists_record = mysql_fetch_array(mysql_query("SELECT COUNT(`id`) AS `num` FROM `product_access_log` WHERE `session_uid` = '$session_uid' AND `product_id` = '$product_info[product_id]'"));
	if($check_exists_record['num'] > 0){
		$query = mysql_query("UPDATE `product_access_log` SET `hits` = `hits`+1 WHERE `session_uid` = '$session_uid' AND `product_id` = '$product_info[product_id]'");
		return true;
	}

	$ua=getUserBrowser();
	$browser = $ua['name'];
	
	$SERVER_OS=$_SERVER['HTTP_USER_AGENT'];        
	$SERVER_NAME=$_SERVER['SERVER_NAME'];
	$REQUEST_URL=$_SERVER['REQUEST_URI'];			
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$current_datetime = date('m/d/Y h:i A');
	$query = mysql_query("INSERT INTO `product_access_log` (`session_uid`, `product_id`, `item_number`, `item_title`, `hits`, `ip_address`, `user_agent`, `browser`, `servername`, `request_uri`) VALUES ('$session_uid', '$product_info[product_id]', '$product_info[item_number]', '$product_info[item_title]', '1', '$ip_address', '$SERVER_OS', '$browser', '$SERVER_NAME', '$REQUEST_URL')");
}
?>